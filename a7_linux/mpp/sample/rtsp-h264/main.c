#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <pthread.h>
#include <time.h>

#include "rtspservice.h"
#include "rtputils.h"
#include "ringfifo.h"
#include "sample_comm.h"

extern int g_s32Quit ;

/* H.264视频编码线程 */
extern void *SAMPLE_VENC_H265_H264(void *p);

int main(void)
{
	int s32MainFd,temp;
	struct timespec ts = { 2, 0 };
	pthread_t id;

	//rtsp
	ringmalloc(256*1024);					/* 环形缓冲区malloc */
	printf("RTSP server START\n");
	PrefsInit();							/* RTSP Server Init */
	printf("listen for client connecting...\n");
	signal(SIGINT, IntHandl);				/* Ctrl + C（产生 SIGINT 信号）中断 */
	s32MainFd = tcp_listen(SERVER_RTSP_PORT_DEFAULT); /* 创建TCP服务器 */
	if (ScheduleInit() == ERR_FATAL)		/* 创建处理主线程 */
	{
		fprintf(stderr,"Fatal: Can't start scheduler %s, %i \nServer is aborting.\n", __FILE__, __LINE__);
		return 0;
	}
	RTP_port_pool_init(RTP_DEFAULT_PORT);	/* RTP端口号初始化 */

	pthread_create(&id, NULL, SAMPLE_VENC_H265_H264, NULL); /* H.264视频编码线程 */
	pthread_detach(id);

	while (!g_s32Quit)
	{
		nanosleep(&ts, NULL);
		EventLoop(s32MainFd);
	}
	sleep(2);
	ringfree();
	printf("The Server quit!\n");

	return 0;
}
