#include "lcd-mipi-config.h"
#include <sys/ioctl.h>

/*============================= mipi 3 inch 480X854 lcd config ====================================*/
combo_dev_cfg_t MIPI_TX_3INCH_480X854_60_CONFIG = {
	.devno = 0,
	.lane_id = {0, 1 ,-1, -1},
	.output_mode = OUTPUT_MODE_DSI_VIDEO,

	.output_format = OUT_FORMAT_RGB_24_BIT,
	.video_mode =  BURST_MODE,

	.sync_info = {
		.vid_pkt_size=480,
		.vid_hsa_pixels=10,
		.vid_hbp_pixels=50,
		.vid_hline_pixels=590,
		.vid_vsa_lines=5,
		.vid_vbp_lines=21,
		.vid_vfp_lines=21,
		.vid_active_lines=854,
		.edpi_cmd_size=0,
	},
	.phy_data_rate=383,
	.pixel_clk=31896,
};

VO_SYNC_INFO_S MIPI_TX_3INCH_480X854_60_SYNC_INFO = {
	.u16Hact=480,
	.u16Hbb=60,
	.u16Hfb=50,
	.u16Hpw=10,
	.u16Vact=854,
	.u16Vbb=26,
	.u16Vfb=21,
	.u16Vpw=5,
};

VO_USER_INTFSYNC_INFO_S MIPI_TX_3INCH_480X854_60_USER_INTFSYNC_INFO = {
	.stUserIntfSyncAttr =
	{
		.stUserSyncPll	=
		{
			.u32Fbdiv=260,
			.u32Frac=0x7AA64C,
			.u32Refdiv=4,
			.u32Postdiv1=7,
			.u32Postdiv2=7,
		},
	},
	.u32DevDiv			= 1,
	.u32PreDiv			= 1,
};

lcd_resoluton_t MIPI_TX_3INCH_480X854_60_LCD_RESOLUTION = {
	.pu32W	= 480,
	.pu32H	= 854,
	.pu32Frm= 60,
};

HI_VOID InitScreen_mipi_3INCH_480X854(HI_S32 s32fd)
{
	SAMPLE_PRT("INIT successfuly!!!\n");
	SAMPLE_PRT("%s,%d.\n",__FUNCTION__,__LINE__);
	HI_S32     fd     = s32fd;
	HI_S32     s32Ret;
	HI_U8      cmd[30];
	cmd_info_t cmd_info = {0};

	#if defined (KODOBoard)
	//LCD-BL-RST
	system("himm 0x111f0024 0x0400");//复用:GPIO6-6
	system("himm 0x112f0094 0x0400");//复用:GPIO0-3
	system("himm 0x120D6400 0x40");  //BL设置输出模式
	system("himm 0x120D0400 0x08");  //RST设置输出模式
	system("himm 0x120D6100 0x40");  //BL复位1
	system("himm 0x120D0020 0x08");  //RST复位1
	usleep(150000);
	system("himm 0x120D0020 0x00");  //RST复位0
	usleep(10000);
	system("himm 0x120D0020 0x08");  //RST置位1
	usleep(150000);
	#endif

	cmd[0] = 0xFF;
	cmd[1] = 0x77;
	cmd[2] = 0x01;
	cmd[3] = 0x00;
	cmd[4] = 0x00;
	cmd[5] = 0x13;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x6;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);
	
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x08EF;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xFF;
	cmd[1] = 0x77;
	cmd[2] = 0x01;
	cmd[3] = 0x00;
	cmd[4] = 0x00;
	cmd[5] = 0x10;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x6; 
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xC0;
	cmd[1] = 0xE9;
	cmd[2] = 0x03;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x3;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xC1;
	cmd[1] = 0x10;
	cmd[2] = 0x0C;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x3;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xC2;
	cmd[1] = 0x20;
	cmd[2] = 0x0A;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x3;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x10CC;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xB0;
	cmd[1] = 0x00;
	cmd[2] = 0x23;
	cmd[3] = 0x2A;
	cmd[4] = 0x0A;
	cmd[5] = 0x0E;
	cmd[6] = 0x03;
	cmd[7] = 0x12;
	cmd[8] = 0x06;
	cmd[9] = 0x06;
	cmd[10] = 0x2A;
	cmd[11] = 0x00;
	cmd[12] = 0x10;
	cmd[13] = 0x0F;
	cmd[14] = 0x2D;
	cmd[15] = 0x34;
	cmd[16] = 0x1F;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x11;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xB1;
	cmd[1] = 0x00;
	cmd[2] = 0x24;
	cmd[3] = 0x2B;
	cmd[4] = 0x0F;
	cmd[5] = 0x12;
	cmd[6] = 0x07;
	cmd[7] = 0x15;
	cmd[8] = 0x0A;
	cmd[9] = 0x0A;
	cmd[10] = 0x2B;
	cmd[11] = 0x08;
	cmd[12] = 0x13;
	cmd[13] = 0x10;
	cmd[14] = 0x2D;
	cmd[15] = 0x33;
	cmd[16] = 0x1F;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x11;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);
	
	cmd[0] = 0xFF;
	cmd[1] = 0x77;
	cmd[2] = 0x01;
	cmd[3] = 0x00;
	cmd[4] = 0x00;
	cmd[5] = 0x11;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x6;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x4DB0;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x48B1;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x84B2;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x80B3;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x45B5;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x85B7;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x33B8;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x78C1;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x78C2;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x88D0;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);
	
	cmd[0] = 0xE0;
	cmd[1] = 0x00;
	cmd[2] = 0x00;
	cmd[3] = 0x02;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x4;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE1;
	cmd[1] = 0x06;
	cmd[2] = 0xA0;
	cmd[3] = 0x08;
	cmd[4] = 0xA0;
	cmd[5] = 0x05;
	cmd[6] = 0xA0;
	cmd[7] = 0x07;
	cmd[8] = 0xA0;
	cmd[9] = 0x00;
	cmd[10] = 0x44;
	cmd[11] = 0x44;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x0c;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE2;
	cmd[1] = 0x30;
	cmd[2] = 0x30;
	cmd[3] = 0x44;
	cmd[4] = 0x44;
	cmd[5] = 0x6E;
	cmd[6] = 0xA0;
	cmd[7] = 0x00;
	cmd[8] = 0x00;
	cmd[9] = 0x6E;
	cmd[10] = 0xA0;
	cmd[11] = 0x00;
	cmd[12] = 0x00;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x0d;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE3;
	cmd[1] = 0x00;
	cmd[2] = 0x00;
	cmd[3] = 0x33;
	cmd[4] = 0x33;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x5;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE4;
	cmd[1] = 0x44;
	cmd[2] = 0x44;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x3;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE5;
	cmd[1] = 0x0D;
	cmd[2] = 0x69;
	cmd[3] = 0x0A;
	cmd[4] = 0xA0;
	cmd[5] = 0x0F;
	cmd[6] = 0x6B;
	cmd[7] = 0x0A;
	cmd[8] = 0xA0;
	cmd[9] = 0x09;
	cmd[10] = 0x65;
	cmd[11] = 0x0A;
	cmd[12] = 0xA0;
	cmd[13] = 0x0B;
	cmd[14] = 0x67;
	cmd[15] = 0x0A;
	cmd[16] = 0xA0;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x11;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE6;
	cmd[1] = 0x00;
	cmd[2] = 0x00;
	cmd[3] = 0x33;
	cmd[4] = 0x33;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x05;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE7;
	cmd[1] = 0x44;
	cmd[2] = 0x44;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x3;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE8;
	cmd[1] = 0x0C;
	cmd[2] = 0x68;
	cmd[3] = 0x0A;
	cmd[4] = 0xA0;
	cmd[5] = 0x0E;
	cmd[6] = 0x6A;
	cmd[7] = 0x0A;
	cmd[8] = 0xA0;
	cmd[9] = 0x08;
	cmd[10] = 0x64;
	cmd[11] = 0x0A;
	cmd[12] = 0xA0;
	cmd[13] = 0x0A;
	cmd[14] = 0x66;
	cmd[15] = 0x0A;
	cmd[16] = 0xA0;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x11;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xE9;
	cmd[1] = 0x36;
	cmd[2] = 0x00;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x3;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xEB;
	cmd[1] = 0x00;
	cmd[2] = 0x01;
	cmd[3] = 0xE4;
	cmd[4] = 0xE4;
	cmd[5] = 0x44;
	cmd[6] = 0x88;
	cmd[7] = 0x40;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x08;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xED;
	cmd[1] = 0xFF;
	cmd[2] = 0x45;
	cmd[3] = 0x67;
	cmd[4] = 0xFA;
	cmd[5] = 0x01;
	cmd[6] = 0x2B;
	cmd[7] = 0xCF;
	cmd[8] = 0xFF;
	cmd[9] = 0xFF;
	cmd[10] = 0xFC;
	cmd[11] = 0xB2;
	cmd[12] = 0x10;
	cmd[13] = 0xAF;
	cmd[14] = 0x76;
	cmd[15] = 0x54;
	cmd[16] = 0xFF;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x11;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd[0] = 0xEF;
	cmd[1] = 0x10;
	cmd[2] = 0x0D;
	cmd[3] = 0x04;
	cmd[4] = 0x08;
	cmd[5] = 0x3F;
	cmd[6] = 0x1F;
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x07;
	cmd_info.data_type = 0x29;
	cmd_info.cmd       = cmd;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x0036;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x0035;
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	/*****************************/
	SAMPLE_PRT("==========================\n");

	cmd_info.devno = 0;
	cmd_info.cmd_size = 0x11;
	cmd_info.data_type    = 0x05;
	cmd_info.cmd          = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX_SET CMD failed\n");
		close(fd);
		return;
	}           
	usleep(120000);

	/*
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x553A; 
	cmd_info.data_type = 0x23;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);
	*/

	cmd_info.devno = 0;
	cmd_info.cmd_size = 0x29;
	cmd_info.data_type    = 0x05;
	cmd_info.cmd          = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX_SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(200000);
	/*****************************/
}
