/*
* Copyright (C) Hisilicon Technologies Co., Ltd. 2012-2019. All rights reserved.
* Description:
* Author: Hisilicon multimedia software group
* Create: 2011/06/28
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include "hi_comm_video.h"
#include "hi_sns_ctrl.h"

#ifdef HI_GPIO_I2C
#include "gpioi2c_ex.h"
#else
#include "hi_i2c.h"
#endif

const unsigned char gc2093_i2c_addr     =    0x6e;        /* I2C Address of GC2093 */
const unsigned int  gc2093_addr_byte    =    2;
const unsigned int  gc2093_data_byte    =    1;
static int g_fd[ISP_MAX_PIPE_NUM] = {[0 ... (ISP_MAX_PIPE_NUM - 1)] = -1};

extern ISP_SNS_STATE_S       *g_pastGc2093[ISP_MAX_PIPE_NUM];
extern ISP_SNS_COMMBUS_U      g_aunGc2093BusInfo[];

int gc2093_i2c_init(VI_PIPE ViPipe)
{
    char acDevFile[16] = {0};
    HI_U8 u8DevNum;

    if (g_fd[ViPipe] >= 0)
    {
        return HI_SUCCESS;
    }
#ifdef HI_GPIO_I2C
    int ret;

    g_fd[ViPipe] = open("/dev/gpioi2c_ex", O_RDONLY, S_IRUSR);
    if (g_fd[ViPipe] < 0)
    {
        printf( "Open gpioi2c_ex error!\n");
        return HI_FAILURE;
    }
#else
    int ret;

    u8DevNum = g_aunGc2093BusInfo[ViPipe].s8I2cDev;
    snprintf(acDevFile, sizeof(acDevFile),  "/dev/i2c-%u", u8DevNum);

    g_fd[ViPipe] = open(acDevFile, O_RDWR, S_IRUSR | S_IWUSR);

    if (g_fd[ViPipe] < 0)
    {
        printf( "Open /dev/hi_i2c_drv-%u error!\n", u8DevNum);
        return HI_FAILURE;
    }

    ret = ioctl(g_fd[ViPipe], I2C_SLAVE_FORCE, (gc2093_i2c_addr >> 1));
    if (ret < 0)
    {
        printf( "I2C_SLAVE_FORCE error!\n");
        close(g_fd[ViPipe]);
        g_fd[ViPipe] = -1;
        return ret;
    }
#endif

    return HI_SUCCESS;
}

int gc2093_i2c_exit(VI_PIPE ViPipe)
{
    if (g_fd[ViPipe] >= 0)
    {
        close(g_fd[ViPipe]);
        g_fd[ViPipe] = -1;
        return HI_SUCCESS;
    }
    return HI_FAILURE;
}

int gc2093_read_register(VI_PIPE ViPipe, int addr)
{
    // TODO:

    return HI_SUCCESS;
}


int gc2093_write_register(VI_PIPE ViPipe, int addr, int data)
{
    if (0 > g_fd[ViPipe])
    {
        return HI_SUCCESS;
    }

#ifdef HI_GPIO_I2C
    i2c_data.dev_addr = gc2093_i2c_addr;
    i2c_data.reg_addr = addr;
    i2c_data.addr_byte_num = gc2093_addr_byte;
    i2c_data.data = data;
    i2c_data.data_byte_num = gc2093_data_byte;

    ret = ioctl(g_fd[ViPipe], GPIO_I2C_WRITE, &i2c_data);

    if (ret)
    {
        printf( "GPIO-I2C write faild!\n");
        return ret;
    }
#else
    int idx = 0;
    int ret;
    char buf[8];

    if (gc2093_addr_byte == 2)
    {
        buf[idx] = (addr >> 8) & 0xff;
        idx++;
        buf[idx] = addr & 0xff;
        idx++;
    }
    else
    {
        
    }

    if (gc2093_data_byte == 2)
    {
        
    }
    else
    {
        buf[idx] = data & 0xff;
        idx++;
    }

    ret = write(g_fd[ViPipe], buf, gc2093_addr_byte + gc2093_data_byte);
    if (ret < 0)
    {
        printf( "I2C_WRITE error!\n");
        return HI_FAILURE;
    }

#endif
    return HI_SUCCESS;
}

static void delay_ms(int ms)
{
    usleep(ms * 1000);
}

void gc2093_prog(VI_PIPE ViPipe, int *rom)
{
    int i = 0;
    while (1)
    {
        int lookup = rom[i++];
        int addr = (lookup >> 16) & 0xFFFF;
        int data = lookup & 0xFFFF;
        if (addr == 0xFFFE)
        {
            delay_ms(data);
        }
        else if (addr == 0xFFFF)
        {
            return;
        }
        else
        {
            gc2093_write_register(ViPipe, addr, data);
        }
    }
}

void gc2093_standby(VI_PIPE ViPipe)
{
    return;
}

void gc2093_restart(VI_PIPE ViPipe)
{
    return;
}

#define GC2093_SENSOR_1080P_30FPS_LINEAR_MODE         (1)
#define GC2093_SENSOR_1080P_30FPS_WDR_2to1_MODE       (2)

void gc2093_wdr_1080p30_2to1_init(VI_PIPE ViPipe);
void gc2093_linear_1080p30_init(VI_PIPE ViPipe);

void gc2093_default_reg_init(VI_PIPE ViPipe)
{
    HI_U32 i;

    for (i = 0; i < g_pastGc2093[ViPipe]->astRegsInfo[0].u32RegNum; i++)
    {
        gc2093_write_register(ViPipe, g_pastGc2093[ViPipe]->astRegsInfo[0].astI2cData[i].u32RegAddr, g_pastGc2093[ViPipe]->astRegsInfo[0].astI2cData[i].u32Data);
    }
}

void gc2093_init(VI_PIPE ViPipe)
{
    WDR_MODE_E       enWDRMode;
    HI_BOOL          bInit;
    HI_U8            u8ImgMode;

    bInit       = g_pastGc2093[ViPipe]->bInit;
    enWDRMode   = g_pastGc2093[ViPipe]->enWDRMode;
    u8ImgMode   = g_pastGc2093[ViPipe]->u8ImgMode;

    gc2093_i2c_init(ViPipe);

    /* When sensor first init, config all registers */
    if (HI_FALSE == bInit)
    {
        if (WDR_MODE_2To1_LINE == enWDRMode)
        {
            if (GC2093_SENSOR_1080P_30FPS_WDR_2to1_MODE == u8ImgMode)    /* GC2093_SENSOR_1080P_30FPS_WDR_2to1_MODE */
            {
                gc2093_wdr_1080p30_2to1_init(ViPipe);
            }
            else
            {
            }
        }
        else
        {
            gc2093_linear_1080p30_init(ViPipe);
        }
    }
    /* When sensor switch mode(linear<->WDR or resolution), config different registers(if possible) */
    else
    {
        if (WDR_MODE_2To1_LINE == enWDRMode)
        {
            if (GC2093_SENSOR_1080P_30FPS_WDR_2to1_MODE == u8ImgMode)    /* GC2093_SENSOR_1080P_30FPS_WDR_2to1_MODE */
            {
                gc2093_wdr_1080p30_2to1_init(ViPipe);
            }
            else
            {
            }
        }
        else
        {
            gc2093_linear_1080p30_init(ViPipe);
        }
    }


    g_pastGc2093[ViPipe]->bInit = HI_TRUE;
    return ;
}

void gc2093_exit(VI_PIPE ViPipe)
{
    gc2093_i2c_exit(ViPipe);

    return;
}

/* 1080P30 and 1080P25 */
void gc2093_linear_1080p30_init(VI_PIPE ViPipe)
{
	/****system****/
	gc2093_write_register(ViPipe, 0x03fe, 0x80);
	gc2093_write_register(ViPipe, 0x03fe, 0x80);
	gc2093_write_register(ViPipe, 0x03fe, 0x80);
	gc2093_write_register(ViPipe, 0x03fe, 0x00);
	gc2093_write_register(ViPipe, 0x03f2, 0x00); 
	gc2093_write_register(ViPipe, 0x03f3, 0x00);
	gc2093_write_register(ViPipe, 0x03f4, 0x36); 
	gc2093_write_register(ViPipe, 0x03f5, 0xc0); 
	gc2093_write_register(ViPipe, 0x03f6, 0x0A); 
	gc2093_write_register(ViPipe, 0x03f7, 0x01); 
	gc2093_write_register(ViPipe, 0x03f8, 0x2C); 
	gc2093_write_register(ViPipe, 0x03f9, 0x10); 
	gc2093_write_register(ViPipe, 0x03fc, 0x8e);

	/****CISCTL & ANALOG****/
	gc2093_write_register(ViPipe, 0x0087, 0x18);
	gc2093_write_register(ViPipe, 0x00ee, 0x30);
	gc2093_write_register(ViPipe, 0x00d0, 0xb7);
	
	gc2093_write_register(ViPipe, 0x01a0, 0x00);
	gc2093_write_register(ViPipe, 0x01a4, 0x40);
	gc2093_write_register(ViPipe, 0x01a5, 0x40);
	gc2093_write_register(ViPipe, 0x01a6, 0x40);
	gc2093_write_register(ViPipe, 0x01af, 0x09);
	
	gc2093_write_register(ViPipe, 0x0001, 0x00);
	gc2093_write_register(ViPipe, 0x0002, 0x02);
	gc2093_write_register(ViPipe, 0x0003, 0x00);
	gc2093_write_register(ViPipe, 0x0004, 0x02);
	gc2093_write_register(ViPipe, 0x0005, 0x04);
	gc2093_write_register(ViPipe, 0x0006, 0x4c);
	gc2093_write_register(ViPipe, 0x0007, 0x00);
	gc2093_write_register(ViPipe, 0x0008, 0x11);
	gc2093_write_register(ViPipe, 0x0009, 0x00);
	gc2093_write_register(ViPipe, 0x000a, 0x02);
	gc2093_write_register(ViPipe, 0x000b, 0x00);
	gc2093_write_register(ViPipe, 0x000c, 0x04);
	gc2093_write_register(ViPipe, 0x000d, 0x04);
	gc2093_write_register(ViPipe, 0x000e, 0x40);
	gc2093_write_register(ViPipe, 0x000f, 0x07);
	gc2093_write_register(ViPipe, 0x0010, 0x8c);
	gc2093_write_register(ViPipe, 0x0013, 0x15);
	gc2093_write_register(ViPipe, 0x0019, 0x0c);
	gc2093_write_register(ViPipe, 0x0041, 0x04);
	gc2093_write_register(ViPipe, 0x0042, 0x65);

	gc2093_write_register(ViPipe, 0x0053, 0x60);
	gc2093_write_register(ViPipe, 0x008d, 0x92);
	gc2093_write_register(ViPipe, 0x0090, 0x00);
	gc2093_write_register(ViPipe, 0x00c7, 0xe1);
	
	gc2093_write_register(ViPipe, 0x001b, 0x73);
	gc2093_write_register(ViPipe, 0x0028, 0x0d);
	gc2093_write_register(ViPipe, 0x0029, 0x40);
	gc2093_write_register(ViPipe, 0x002b, 0x04);
	gc2093_write_register(ViPipe, 0x002e, 0x23);
	gc2093_write_register(ViPipe, 0x0037, 0x03);
	gc2093_write_register(ViPipe, 0x0038, 0x88);
	gc2093_write_register(ViPipe, 0x0044, 0x20);
	gc2093_write_register(ViPipe, 0x004b, 0x14);
	gc2093_write_register(ViPipe, 0x0055, 0x20);
	
	gc2093_write_register(ViPipe, 0x0068, 0x20);
	gc2093_write_register(ViPipe, 0x0069, 0x20);
	gc2093_write_register(ViPipe, 0x0077, 0x00);
	gc2093_write_register(ViPipe, 0x0078, 0x04);
	gc2093_write_register(ViPipe, 0x007c, 0x91);
	gc2093_write_register(ViPipe, 0x00ce, 0x7c);
	gc2093_write_register(ViPipe, 0x00d3, 0xdc);
	gc2093_write_register(ViPipe, 0x00e6, 0x50);

	/*gain*/
	gc2093_write_register(ViPipe, 0x00b6, 0xc0);
	gc2093_write_register(ViPipe, 0x00b0, 0x60);

	/*isp*/
	gc2093_write_register(ViPipe, 0x0102, 0x89);
	gc2093_write_register(ViPipe, 0x0104, 0x01);
	gc2093_write_register(ViPipe, 0x0158, 0x00);

	/*blk*/
	gc2093_write_register(ViPipe, 0x0026, 0x20); //[4]д0��ȫn mode
	gc2093_write_register(ViPipe, 0x0142, 0x00);
	gc2093_write_register(ViPipe, 0x0149, 0x1e);
	gc2093_write_register(ViPipe, 0x014a, 0x07);
	gc2093_write_register(ViPipe, 0x014b, 0x80);
	gc2093_write_register(ViPipe, 0x0155, 0x07);
	gc2093_write_register(ViPipe, 0x0414, 0x7e);
	gc2093_write_register(ViPipe, 0x0415, 0x7e);
	gc2093_write_register(ViPipe, 0x0416, 0x7e);
	gc2093_write_register(ViPipe, 0x0417, 0x7e); 

	/*window*/
	gc2093_write_register(ViPipe, 0x0192, 0x02); //win y1
	gc2093_write_register(ViPipe, 0x0194, 0x03); //win x1
	gc2093_write_register(ViPipe, 0x0195, 0x04);
	gc2093_write_register(ViPipe, 0x0196, 0x38); //[10:0]out_height
	gc2093_write_register(ViPipe, 0x0197, 0x07);
	gc2093_write_register(ViPipe, 0x0198, 0x80); //[11:0]out_width

	/* mirror & flip */
	gc2093_write_register(ViPipe, 0x0017, 0x01);
	
	/****DVP & MIPI****/
	gc2093_write_register(ViPipe, 0x019a, 0x06);
	gc2093_write_register(ViPipe, 0x007b, 0x2a);
	gc2093_write_register(ViPipe, 0x0023, 0x2d);
	gc2093_write_register(ViPipe, 0x0201, 0x27);
	gc2093_write_register(ViPipe, 0x0202, 0x56);
	gc2093_write_register(ViPipe, 0x0203, 0xce);
	gc2093_write_register(ViPipe, 0x0212, 0x80);
	gc2093_write_register(ViPipe, 0x0213, 0x07);
	//0x0215, 0x12
	gc2093_write_register(ViPipe, 0x003e, 0x91);


    printf("==============================================================\n");
    printf("=====Sony gc2093 sensor 1080P30fps(MIPI) init success!=====\n");
    printf("==============================================================\n");

    return;
}

void gc2093_wdr_1080p30_2to1_init(VI_PIPE ViPipe)
{
	/*MIPI clk:792Mbps
	pclk:99Mbyte
	HB:660*2
	Linelength:2640
	Wpclk:396Mbps
	Rpclk:198Mbps
	row time:13.3333us
	window height:1250 
	size:1920*1080*/
	/****system****/
	gc2093_write_register(ViPipe, 0x03fe, 0x80);
	gc2093_write_register(ViPipe, 0x03fe, 0x80);
	gc2093_write_register(ViPipe, 0x03fe, 0x80);
	gc2093_write_register(ViPipe, 0x03fe, 0x00);
	gc2093_write_register(ViPipe, 0x03f2, 0x00);
	gc2093_write_register(ViPipe, 0x03f3, 0x00);
	gc2093_write_register(ViPipe, 0x03f4, 0x36);
	gc2093_write_register(ViPipe, 0x03f5, 0xc0);
	gc2093_write_register(ViPipe, 0x03f6, 0x0B);
	gc2093_write_register(ViPipe, 0x03f7, 0x01);
	gc2093_write_register(ViPipe, 0x03f8, 0x58);
	gc2093_write_register(ViPipe, 0x03f9, 0x40);
	gc2093_write_register(ViPipe, 0x03fc, 0x8e);
	/****CISCTL & ANALOG****/
	gc2093_write_register(ViPipe, 0x0087, 0x18);
	gc2093_write_register(ViPipe, 0x00ee, 0x30);
	gc2093_write_register(ViPipe, 0x00d0, 0xb7);
	gc2093_write_register(ViPipe, 0x01a0, 0x00);
	gc2093_write_register(ViPipe, 0x01a4, 0x40);
	gc2093_write_register(ViPipe, 0x01a5, 0x40);
	gc2093_write_register(ViPipe, 0x01a6, 0x40);
	gc2093_write_register(ViPipe, 0x01af, 0x09);
	gc2093_write_register(ViPipe, 0x0001, 0x00);
	gc2093_write_register(ViPipe, 0x0002, 0x02);
	gc2093_write_register(ViPipe, 0x0003, 0x00);
	gc2093_write_register(ViPipe, 0x0004, 0x02);
	gc2093_write_register(ViPipe, 0x0005, 0x02);
	gc2093_write_register(ViPipe, 0x0006, 0x94);
	gc2093_write_register(ViPipe, 0x0007, 0x00);
	gc2093_write_register(ViPipe, 0x0008, 0x11);
	gc2093_write_register(ViPipe, 0x0009, 0x00);
	gc2093_write_register(ViPipe, 0x000a, 0x02);
	gc2093_write_register(ViPipe, 0x000b, 0x00);
	gc2093_write_register(ViPipe, 0x000c, 0x04);
	gc2093_write_register(ViPipe, 0x000d, 0x04);
	gc2093_write_register(ViPipe, 0x000e, 0x40);
	gc2093_write_register(ViPipe, 0x000f, 0x07);
	gc2093_write_register(ViPipe, 0x0010, 0x8c);
	gc2093_write_register(ViPipe, 0x0013, 0x15);
	gc2093_write_register(ViPipe, 0x0019, 0x0c);
	gc2093_write_register(ViPipe, 0x0041, 0x04);
	gc2093_write_register(ViPipe, 0x0042, 0xE2);
	gc2093_write_register(ViPipe, 0x0053, 0x60);
	gc2093_write_register(ViPipe, 0x008d, 0x92);
	gc2093_write_register(ViPipe, 0x0090, 0x00);
	gc2093_write_register(ViPipe, 0x00c7, 0xe1);
	gc2093_write_register(ViPipe, 0x001b, 0x73);
	gc2093_write_register(ViPipe, 0x0028, 0x0d);
	gc2093_write_register(ViPipe, 0x0029, 0x24);
	gc2093_write_register(ViPipe, 0x002b, 0x04);
	gc2093_write_register(ViPipe, 0x002e, 0x23);
	gc2093_write_register(ViPipe, 0x0037, 0x03);
	gc2093_write_register(ViPipe, 0x0043, 0x04);
	gc2093_write_register(ViPipe, 0x0044, 0x20);
	gc2093_write_register(ViPipe, 0x0046, 0x0b);
	gc2093_write_register(ViPipe, 0x004a, 0x01);
	gc2093_write_register(ViPipe, 0x004b, 0x20);
	gc2093_write_register(ViPipe, 0x0055, 0x30);

	gc2093_write_register(ViPipe, 0x0077, 0x00); 
	gc2093_write_register(ViPipe, 0x0078, 0x04); 
	gc2093_write_register(ViPipe, 0x007c, 0x91); 
	gc2093_write_register(ViPipe, 0x00ce, 0x7c); 
	gc2093_write_register(ViPipe, 0x00d3, 0xdc); 
	gc2093_write_register(ViPipe, 0x00e6, 0x50);  

	/*gain*/
	gc2093_write_register(ViPipe, 0x00b6, 0xc0);
	gc2093_write_register(ViPipe, 0x00b0, 0x60);
	/*dark sun*/
	/*ISP*/

	gc2093_write_register(ViPipe, 0x0123, 0x08);
	gc2093_write_register(ViPipe, 0x0123, 0x00);
	gc2093_write_register(ViPipe, 0x0120, 0x01);
	gc2093_write_register(ViPipe, 0x0121, 0x04);
	gc2093_write_register(ViPipe, 0x0122, 0x90);
	gc2093_write_register(ViPipe, 0x0124, 0x03);
	gc2093_write_register(ViPipe, 0x0125, 0xff);
	gc2093_write_register(ViPipe, 0x0126, 0x3c);
	gc2093_write_register(ViPipe, 0x001a, 0x8c);
	gc2093_write_register(ViPipe, 0x00c6, 0xd0);
	/*blk*/
	gc2093_write_register(ViPipe, 0x0026, 0x30);
	gc2093_write_register(ViPipe, 0x0142, 0x00);
	gc2093_write_register(ViPipe, 0x0149, 0x1e);
	gc2093_write_register(ViPipe, 0x014a, 0x0f);
	gc2093_write_register(ViPipe, 0x014b, 0x00);
	gc2093_write_register(ViPipe, 0x0155, 0x07);
	gc2093_write_register(ViPipe, 0x0160, 0x40);
	gc2093_write_register(ViPipe, 0x0414, 0x7e);
	gc2093_write_register(ViPipe, 0x0415, 0x7e);
	gc2093_write_register(ViPipe, 0x0416, 0x7e);
	gc2093_write_register(ViPipe, 0x0417, 0x7e);
	gc2093_write_register(ViPipe, 0x0454, 0x7e);
	gc2093_write_register(ViPipe, 0x0455, 0x7e);
	gc2093_write_register(ViPipe, 0x0456, 0x7e);
	gc2093_write_register(ViPipe, 0x0457, 0x7e);
	gc2093_write_register(ViPipe, 0x04e0, 0x18);

/*ISP*/
	gc2093_write_register(ViPipe, 0x0102,  0x89);
	gc2093_write_register(ViPipe, 0x0104,  0x01);
	gc2093_write_register(ViPipe, 0x010e,  0x01);
	gc2093_write_register(ViPipe, 0x0158,  0x00);
	/*window*/
	gc2093_write_register(ViPipe, 0x0192, 0x02); //win y1
	gc2093_write_register(ViPipe, 0x0194, 0x03); //win x1
	gc2093_write_register(ViPipe, 0x0195, 0x04);
	gc2093_write_register(ViPipe, 0x0196, 0x38); //[10:0]out_height
	gc2093_write_register(ViPipe, 0x0197, 0x07);
	gc2093_write_register(ViPipe, 0x0198, 0x80); //[11:0]out_width
	
	/* mirror & flip */

	/****DVP & MIPI****/
	gc2093_write_register(ViPipe, 0x019a, 0x06);
	gc2093_write_register(ViPipe, 0x007b, 0x2a);
	gc2093_write_register(ViPipe, 0x0023, 0x2d);
	gc2093_write_register(ViPipe, 0x0201, 0x27);
	gc2093_write_register(ViPipe, 0x0202, 0x56);
	gc2093_write_register(ViPipe, 0x0203, 0xce);
	gc2093_write_register(ViPipe, 0x0212, 0x80);
	gc2093_write_register(ViPipe, 0x0213, 0x07);
	gc2093_write_register(ViPipe, 0x0215, 0x12);
	gc2093_write_register(ViPipe, 0x003e, 0x91);

	/****HDR EN****/
	gc2093_write_register(ViPipe, 0x0027, 0x71); //[0]hdr_en
	gc2093_write_register(ViPipe, 0x0215, 0x92); //[7]virtual channel en
	gc2093_write_register(ViPipe, 0x024d, 0x01);
    printf("============================================================================\n");
    printf("===Gc2093 sensor 1080P30fps 10bit 2to1 WDR(60fps->30fps) init success!===\n");
    printf("============================================================================\n");

    return;
}
