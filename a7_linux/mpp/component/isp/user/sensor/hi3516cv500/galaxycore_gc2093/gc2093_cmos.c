/*
* Copyright (C) Hisilicon Technologies Co., Ltd. 2012-2019. All rights reserved.
* Description:
* Author: Hisilicon multimedia software group
* Create: 2011/06/28
*/

#if !defined(__GC2093_CMOS_H_)
#define __GC2093_CMOS_H_

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "hi_comm_sns.h"
#include "hi_comm_video.h"
#include "hi_sns_ctrl.h"
#include "mpi_isp.h"
#include "mpi_ae.h"
#include "mpi_awb.h"

#include "gc2093_cmos_ex.h"
#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */


#define GC2093_ID   2093

/****************************************************************************
 * global variables                                                            *
 ****************************************************************************/

#define HIGHER_4BITS(x) (((x) & 0x30000) >> 16)
#define HIGH_8BITS(x) (((x) & 0xff00) >> 8)
#define LOW_8BITS(x)  ((x) & 0x00ff)

#ifndef MAX
#define MAX(a, b) (((a) < (b)) ?  (b) : (a))
#endif

#ifndef MIN
#define MIN(a, b) (((a) > (b)) ?  (b) : (a))
#endif

ISP_SNS_STATE_S *g_pastGc2093[ISP_MAX_PIPE_NUM] = {HI_NULL};

#define GC2093_SENSOR_GET_CTX(dev, pstCtx)   (pstCtx = g_pastGc2093[dev])
#define GC2093_SENSOR_SET_CTX(dev, pstCtx)   (g_pastGc2093[dev] = pstCtx)
#define GC2093_SENSOR_RESET_CTX(dev)         (g_pastGc2093[dev] = HI_NULL)

ISP_SNS_COMMBUS_U g_aunGc2093_BusInfo[ISP_MAX_PIPE_NUM] = {
    [0] = { .s8I2cDev = 0},
    [1 ... ISP_MAX_PIPE_NUM - 1] = { .s8I2cDev = -1}
};

static ISP_FSWDR_MODE_E genFSWDRMode[ISP_MAX_PIPE_NUM] = {
    [0 ... ISP_MAX_PIPE_NUM - 1] = ISP_FSWDR_NORMAL_MODE
};

static HI_U32 gu32MaxTimeGetCnt[ISP_MAX_PIPE_NUM] = {0};
static HI_U32 g_au32InitExposure[ISP_MAX_PIPE_NUM]  = {0};
static HI_U32 g_au32LinesPer500ms[ISP_MAX_PIPE_NUM] = {0};

static HI_U16 g_au16InitWBGain[ISP_MAX_PIPE_NUM][3] = {{0}};
static HI_U16 g_au16SampleRgain[ISP_MAX_PIPE_NUM] = {0};
static HI_U16 g_au16SampleBgain[ISP_MAX_PIPE_NUM] = {0};

static HI_BOOL bFirstFps[ISP_MAX_PIPE_NUM] = {[0 ... (ISP_MAX_PIPE_NUM - 1)] = 1};
typedef struct hiGC2093_STATE_S {
    HI_U8       u8Hcg;
    HI_U32      u32BRL;
    HI_U32      u32RHS1_MAX;
    HI_U32      u32RHS2_MAX;
} GC2093_STATE_S;

GC2093_STATE_S g_astgc2093_State[ISP_MAX_PIPE_NUM] = {{0}};

/****************************************************************************
 * extern                                                                   *
 ****************************************************************************/
extern const unsigned int gc2093_i2c_addr;
extern unsigned int gc2093_addr_byte;
extern unsigned int gc2093_data_byte;
extern void gc2093_init(VI_PIPE ViPipe);
extern void gc2093_exit(VI_PIPE ViPipe);
extern void gc2093_standby(VI_PIPE ViPipe);
extern void gc2093_restart(VI_PIPE ViPipe);
extern int  gc2093_write_register(VI_PIPE ViPipe, int addr, int data);
extern int  gc2093_read_register(VI_PIPE ViPipe, int addr);

/****************************************************************************
 * local variables                                                            *
 ****************************************************************************/
#define GC2093_FULL_LINES_MAX 0x3FFF
#define GC2093_FULL_SHORT_LINES_MAX_2TO1_WDR 0x89
#define GC2093_FULL_LINES_MAX_2TO1_WDR 0x8ff  // considering the YOUT_SIZE and bad frame

/* Gc2093 Register Address */

#define GC2093_INCREASE_LINES                  (1) /* make real fps less than stand fps because NVR require */

#define GC2093_VMAX_1080P30_LINEAR             (1125+GC2093_INCREASE_LINES)
// #define GC2093_VMAX_1080P60TO30_WDR            (1156+GC2093_INCREASE_LINES)  // 12bit
#define GC2093_VMAX_1080P60TO30_WDR            (1250 + GC2093_INCREASE_LINES)   // 10bit

// sensor fps mode
#define GC2093_SENSOR_1080P_30FPS_LINEAR_MODE  (1)
#define GC2093_SENSOR_1080P_30FPS_2t1_WDR_MODE (2)

#define GC2093_RES_IS_1080P(w, h)      ((w) <= 1920 && (h) <= 1080)

#define GC2093_ERR_MODE_PRINT(pstSensorImageMode, pstSnsState)\
    do{\
        ISP_ERR_TRACE("Not support! Width:%d, Height:%d, Fps:%f, WDRMode:%d\n",\
                  pstSensorImageMode->u16Width,  \
                  pstSensorImageMode->u16Height, \
                  pstSensorImageMode->f32Fps,    \
                  pstSnsState->enWDRMode);\
    }while(0)

static HI_S32 cmos_get_ae_default(VI_PIPE ViPipe, AE_SENSOR_DEFAULT_S *pstAeSnsDft)
{
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    CMOS_CHECK_POINTER(pstAeSnsDft);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER(pstSnsState);

    memset(&pstAeSnsDft->stAERouteAttr, 0, sizeof(ISP_AE_ROUTE_S));

    pstAeSnsDft->u32FullLinesStd = pstSnsState->u32FLStd;
    pstAeSnsDft->u32FlickerFreq = 50 * 256;
    pstAeSnsDft->u32FullLinesMax = GC2093_FULL_LINES_MAX;

    pstAeSnsDft->stIntTimeAccu.enAccuType = AE_ACCURACY_LINEAR;
    pstAeSnsDft->stIntTimeAccu.f32Accuracy = 1;
    pstAeSnsDft->stIntTimeAccu.f32Offset = 0;

    pstAeSnsDft->stAgainAccu.enAccuType = AE_ACCURACY_TABLE;
    pstAeSnsDft->stAgainAccu.f32Accuracy = 1;

    pstAeSnsDft->stDgainAccu.enAccuType = AE_ACCURACY_LINEAR;
    pstAeSnsDft->stDgainAccu.f32Accuracy = 0.015625;

    pstAeSnsDft->u32ISPDgainShift = 8;
    pstAeSnsDft->u32MinISPDgainTarget = 1 << pstAeSnsDft->u32ISPDgainShift;
    pstAeSnsDft->u32MaxISPDgainTarget = 2 << pstAeSnsDft->u32ISPDgainShift;

    pstAeSnsDft->enMaxIrisFNO = ISP_IRIS_F_NO_1_0;
    pstAeSnsDft->enMinIrisFNO = ISP_IRIS_F_NO_32_0;

    pstAeSnsDft->bAERouteExValid = HI_FALSE;
    pstAeSnsDft->stAERouteAttr.u32TotalNum = 0;
    pstAeSnsDft->stAERouteAttrEx.u32TotalNum = 6;

    if (g_au32LinesPer500ms[ViPipe] == 0) {
        pstAeSnsDft->u32LinesPer500ms = pstSnsState->u32FLStd * 30 / 2;
    } else {
        pstAeSnsDft->u32LinesPer500ms = g_au32LinesPer500ms[ViPipe];
    }

    switch (pstSnsState->enWDRMode) {
        default:
        case WDR_MODE_NONE:   /* linear mode */
            pstAeSnsDft->au8HistThresh[0] = 0xd;
            pstAeSnsDft->au8HistThresh[1] = 0x28;
            pstAeSnsDft->au8HistThresh[2] = 0x60;
            pstAeSnsDft->au8HistThresh[3] = 0x80;

            pstAeSnsDft->u32MaxAgain = 113168;
            pstAeSnsDft->u32MinAgain = 1024;
            pstAeSnsDft->u32MaxAgainTarget = pstAeSnsDft->u32MaxAgain;
            pstAeSnsDft->u32MinAgainTarget = pstAeSnsDft->u32MinAgain;

            pstAeSnsDft->u32MaxDgain = 512;
            pstAeSnsDft->u32MinDgain = 64;
            pstAeSnsDft->u32MaxDgainTarget = pstAeSnsDft->u32MaxDgain;
            pstAeSnsDft->u32MinDgainTarget = pstAeSnsDft->u32MinDgain;

            pstAeSnsDft->u8AeCompensation = 0x38;
            pstAeSnsDft->enAeExpMode = AE_EXP_HIGHLIGHT_PRIOR;

            pstAeSnsDft->u32InitExposure = g_au32InitExposure[ViPipe] ? g_au32InitExposure[ViPipe] : 148859;

            pstAeSnsDft->u32MaxIntTime = pstSnsState->u32FLStd - 2;
			pstAeSnsDft->u32MinIntTime = 1;
			pstAeSnsDft->u32MaxIntTimeTarget = 65535; /* max 65535 */
			pstAeSnsDft->u32MinIntTimeTarget = 1;
            break;

        case WDR_MODE_2To1_LINE:
			pstAeSnsDft->au8HistThresh[0] = 0xC;
			pstAeSnsDft->au8HistThresh[1] = 0x18;
			pstAeSnsDft->au8HistThresh[2] = 0x60;
			pstAeSnsDft->au8HistThresh[3] = 0x80;


			pstAeSnsDft->u32MaxIntTime = pstSnsState->u32FLStd - 2;
			pstAeSnsDft->u32MinIntTime = 2;

			pstAeSnsDft->u32MaxIntTimeTarget = 65535;
			pstAeSnsDft->u32MinIntTimeTarget = pstAeSnsDft->u32MinIntTime;

			pstAeSnsDft->u32MaxAgain = 113168;
			pstAeSnsDft->u32MinAgain = 1024;
			pstAeSnsDft->u32MaxAgainTarget = pstAeSnsDft->u32MaxAgain;
			pstAeSnsDft->u32MinAgainTarget = pstAeSnsDft->u32MinAgain;

			pstAeSnsDft->u32MaxDgain = 512;
			pstAeSnsDft->u32MinDgain = 64;
			pstAeSnsDft->u32MaxDgainTarget = pstAeSnsDft->u32MaxDgain;
			pstAeSnsDft->u32MinDgainTarget = pstAeSnsDft->u32MinDgain;
			pstAeSnsDft->u8AeCompensation = 24;

            pstAeSnsDft->u32InitExposure = g_au32InitExposure[ViPipe] ? g_au32InitExposure[ViPipe] : 16462;

            if (genFSWDRMode[ViPipe] == ISP_FSWDR_LONG_FRAME_MODE) {
                pstAeSnsDft->u8AeCompensation = 56;
                pstAeSnsDft->enAeExpMode = AE_EXP_HIGHLIGHT_PRIOR;
            } else {
                pstAeSnsDft->u32MaxISPDgainTarget = 1024;
                pstAeSnsDft->enAeExpMode = AE_EXP_LOWLIGHT_PRIOR;
		        pstAeSnsDft->u16ManRatioEnable = HI_FALSE;
	        	pstAeSnsDft->au32Ratio[0] = 0x400;
                pstAeSnsDft->au32Ratio[1] = 0x40;
                pstAeSnsDft->au32Ratio[2] = 0x40;
            }
            break;
    }

    return HI_SUCCESS;
}


/* the function of sensor set fps */
static HI_VOID cmos_fps_set(VI_PIPE ViPipe, HI_FLOAT f32Fps, AE_SENSOR_DEFAULT_S *pstAeSnsDft)
{
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;
    HI_U32 u32VMAX = GC2093_VMAX_1080P30_LINEAR;

    CMOS_CHECK_POINTER_VOID(pstAeSnsDft);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER_VOID(pstSnsState);
f32Fps=25;
    switch (pstSnsState->u8ImgMode) {
        case GC2093_SENSOR_1080P_30FPS_2t1_WDR_MODE:
            if ((f32Fps <= 30) && (f32Fps >= 15.22)) {
                u32VMAX = GC2093_VMAX_1080P60TO30_WDR * 30 / DIV_0_TO_1_FLOAT(f32Fps);
            } else {
                ISP_ERR_TRACE("Not support Fps: %f\n", f32Fps);
                return;
            }
            u32VMAX = (u32VMAX > GC2093_FULL_LINES_MAX) ? GC2093_FULL_LINES_MAX : u32VMAX;
		
            break;

        case GC2093_SENSOR_1080P_30FPS_LINEAR_MODE:
            if ((f32Fps <= 30) && (f32Fps >= 0.119)) {
                u32VMAX = GC2093_VMAX_1080P30_LINEAR * 30 / DIV_0_TO_1_FLOAT(f32Fps);
            } else {
                ISP_ERR_TRACE("Not support Fps: %f\n", f32Fps);
                return;
            }
            u32VMAX = (u32VMAX > GC2093_FULL_LINES_MAX) ? GC2093_FULL_LINES_MAX : u32VMAX;
            pstAeSnsDft->u32LinesPer500ms = GC2093_VMAX_1080P30_LINEAR * 15;
            break;

        default:
            return;
    }



    if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {
        pstSnsState->u32FLStd = u32VMAX; /* x2 */
		pstAeSnsDft->u32LinesPer500ms = pstSnsState->u32FLStd * f32Fps;

    } else {
        pstSnsState->u32FLStd = u32VMAX;
		pstAeSnsDft->u32LinesPer500ms = pstSnsState->u32FLStd * f32Fps / 2;
    }

	pstSnsState->astRegsInfo[0].astI2cData[0].u32Data = 0x0;
	pstSnsState->astRegsInfo[0].astI2cData[1].u32Data = ((u32VMAX & 0xFF00) >> 8);
	pstSnsState->astRegsInfo[0].astI2cData[2].u32Data = (u32VMAX & 0xFF);
    pstAeSnsDft->f32Fps = f32Fps;
    //pstAeSnsDft->u32LinesPer500ms = pstSnsState->u32FLStd * f32Fps / 2;
    pstAeSnsDft->u32FullLinesStd = pstSnsState->u32FLStd;
    pstAeSnsDft->u32MaxIntTime = pstSnsState->u32FLStd - 2;
    pstSnsState->au32FL[0] = pstSnsState->u32FLStd;
    pstAeSnsDft->u32FullLines = pstSnsState->au32FL[0];

    if (bFirstFps[ViPipe]) {
        bFirstFps[ViPipe] = HI_FALSE;
        pstSnsState->au32FL[1] = pstSnsState->au32FL[0];
    }
    return;
}

static HI_VOID cmos_slow_framerate_set(VI_PIPE ViPipe, HI_U32 u32FullLines,
                                       AE_SENSOR_DEFAULT_S *pstAeSnsDft)
{
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    CMOS_CHECK_POINTER_VOID(pstAeSnsDft);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER_VOID(pstSnsState);

    if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {
        u32FullLines = (u32FullLines > 2 * GC2093_FULL_LINES_MAX_2TO1_WDR) ? /* 2 * Fl */
            2 * GC2093_FULL_LINES_MAX_2TO1_WDR : u32FullLines; /* 2 * Fl */
        pstSnsState->au32FL[0] = (u32FullLines >> 1) << 1;
        g_astgc2093_State[ViPipe].u32RHS1_MAX = pstSnsState->au32FL[0] - g_astgc2093_State[ViPipe].u32BRL * 2 - 21;
    } else {
        u32FullLines = (u32FullLines > GC2093_FULL_LINES_MAX) ? GC2093_FULL_LINES_MAX : u32FullLines;
        pstSnsState->au32FL[0] = u32FullLines;
    }

    if (pstSnsState->enWDRMode == WDR_MODE_NONE) {

    } else if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {

    } else {
    }

    pstAeSnsDft->u32FullLines = pstSnsState->au32FL[0];
    pstAeSnsDft->u32MaxIntTime = pstSnsState->au32FL[0] - 2;

    return;
}

/* while isp notify ae to update sensor regs, ae call these funcs. */
static HI_VOID cmos_inttime_update(VI_PIPE ViPipe, HI_U32 u32IntTime)
{
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;
    HI_U32 u32Val;
    static HI_U32 short_exp=37,long_exp=128;
    static HI_BOOL bFirst[ISP_MAX_PIPE_NUM] = {[0 ...(ISP_MAX_PIPE_NUM - 1)] = 1};
    static HI_U32 u32ShortIntTime[ISP_MAX_PIPE_NUM] = {0};
    static HI_U32 u32LongIntTime[ISP_MAX_PIPE_NUM] = {0};

    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER_VOID(pstSnsState);

    if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {
        if (bFirst[ViPipe]) { /* short exposure */
            pstSnsState->au32WDRIntTime[0] = u32IntTime;
            u32ShortIntTime[ViPipe] = u32IntTime;
			u32Val = (u32IntTime > GC2093_FULL_LINES_MAX) ? GC2093_FULL_LINES_MAX : u32IntTime;
			short_exp =u32Val;
			
		    if(short_exp<=0x04){
            pstSnsState->astRegsInfo[0].astI2cData[16].u32Data = 0xfc;
            }	
            else{	  	
	        pstSnsState->astRegsInfo[0].astI2cData[16].u32Data = 0xf8;
            }
            bFirst[ViPipe] = HI_FALSE;
        } else { /* long exposure */

            pstSnsState->au32WDRIntTime[1] = u32IntTime;
            u32LongIntTime[ViPipe] = u32IntTime;

        u32Val = (u32IntTime > GC2093_FULL_LINES_MAX) ? GC2093_FULL_LINES_MAX : u32IntTime; 
        long_exp=u32Val;

				pstSnsState->astRegsInfo[0].astI2cData[3].u32Data = ((long_exp& 0xFF00) >> 8);
				pstSnsState->astRegsInfo[0].astI2cData[4].u32Data = (long_exp & 0xFF);
				pstSnsState->astRegsInfo[0].astI2cData[14].u32Data = ((short_exp & 0xFF00) >> 8);
				pstSnsState->astRegsInfo[0].astI2cData[15].u32Data = (short_exp & 0xFF);


            bFirst[ViPipe] = HI_TRUE;
        }
    } else {
        u32Val = (u32IntTime > GC2093_FULL_LINES_MAX) ? GC2093_FULL_LINES_MAX : u32IntTime;
        pstSnsState->astRegsInfo[0].astI2cData[3].u32Data = ((u32Val & 0xFF00) >> 8);
        pstSnsState->astRegsInfo[0].astI2cData[4].u32Data = (u32Val & 0xFF);
      
        bFirst[ViPipe] = HI_TRUE;
    }

    return;
}

static HI_U32 regValTable[25][6] = {
	                           //0xb3 0xb8 0xb9 0x7c 0x78 0xc2
	                            {0x00,0x01,0x00,0x93,0x20,0x17},
                                {0x10,0x01,0x0c,0x93,0x20,0x17},
                                {0x20,0x01,0x1b,0x93,0x20,0x17},
                                {0x30,0x01,0x2c,0x93,0x20,0x17},
                                {0x40,0x01,0x3f,0x93,0x20,0x17},
															  
                                {0x50,0x02,0x16,0x93,0x20,0x17},
                                {0x60,0x02,0x35,0x93,0x20,0x17},
                                {0x70,0x03,0x16,0x93,0x20,0x17},
                                {0x80,0x04,0x02,0x93,0x20,0x17},
                                {0x90,0x04,0x31,0x93,0x20,0x17},

							    {0xa0,0x05,0x32,0x91,0x20,0x17},
                                {0xb0,0x06,0x35,0x91,0x20,0x17},
                                {0xc0,0x08,0x04,0x91,0x20,0x27},
                                {0x5a,0x09,0x19,0x91,0x20,0x30},
                                {0x83,0x0b,0x0f,0x91,0x20,0x30},
                                
                                {0x93,0x0d,0x12,0x91,0x20,0x30}, 
                                {0x84,0x10,0x00,0x91,0x04,0x07},
                                {0x94,0x12,0x3a,0x91,0x04,0x07},
                                {0x5d,0x1a,0x02,0x91,0x04,0x07},
                                {0x9b,0x1b,0x20,0x91,0x04,0x07},
								
                                {0x8c,0x20,0x0f,0x91,0x04,0x07},       
                                {0x9c,0x26,0x07,0x91,0x04,0x07},
                                {0xb6,0x36,0x21,0x91,0x04,0x07},									  
                                {0xad,0x37,0x3a,0x91,0x04,0x07},
                                {0xbd,0x3d,0x02,0x91,0x04,0x07},
};

static HI_U32 analog_gain_table[25] = {
	1024,
	1184,
	1424,
	1664,
	2016,
	2336,
	2864,
	3344,
	4064,
	4720,
	5424,
	6304,
	7664,
	9056,
	10912,
	12688,
	15408,
	17904,
	23440,
	26016,
	30832,
	35824,
	45296,
	54544,
	63376,
};

static HI_VOID cmos_again_calc_table(VI_PIPE ViPipe, HI_U32 *pu32AgainLin, HI_U32 *pu32AgainDb)
{
    int again;
    int i;
    static HI_U8 againmax = 24;

    CMOS_CHECK_POINTER_VOID(pu32AgainLin);
    CMOS_CHECK_POINTER_VOID(pu32AgainDb);

     again = *pu32AgainLin;

    if (again >= analog_gain_table[againmax]) {
        *pu32AgainLin = analog_gain_table[24];
        *pu32AgainDb = againmax;
    } else {
        for (i = 1; i < 25; i++) {
            if (again < analog_gain_table[i]) {
                *pu32AgainLin = analog_gain_table[i - 1];
                *pu32AgainDb = i - 1;
                break;
            }
    }
   }

    return;
}

static HI_VOID cmos_dgain_calc_table(VI_PIPE ViPipe, HI_U32 *pu32DgainLin, HI_U32 *pu32DgainDb)
{
    return;
}

static HI_VOID cmos_gains_update(VI_PIPE ViPipe, HI_U32 u32Again, HI_U32 u32Dgain)
{
     HI_U8 u8DgainHigh, u8DgainLow;
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;
  //  HI_U32 u32HCG = g_astgc2093_State[vi_pipe].u8Hcg;
  //  HI_U32 u32Tmp;

    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER_VOID(pstSnsState);

    u8DgainHigh = (u32Dgain >> 6) & 0x0f;
    u8DgainLow = (u32Dgain & 0x3f) << 2;
  
	pstSnsState->astRegsInfo[0].astI2cData[5].u32Data = 0x58;//0x0087
	pstSnsState->astRegsInfo[0].astI2cData[6].u32Data = regValTable[u32Again][0];//b3
	pstSnsState->astRegsInfo[0].astI2cData[7].u32Data = regValTable[u32Again][1];//b8
	pstSnsState->astRegsInfo[0].astI2cData[8].u32Data = regValTable[u32Again][2];//b9
	pstSnsState->astRegsInfo[0].astI2cData[9].u32Data = regValTable[u32Again][4];//78
	pstSnsState->astRegsInfo[0].astI2cData[10].u32Data = regValTable[u32Again][5];//c2
	pstSnsState->astRegsInfo[0].astI2cData[11].u32Data = u8DgainHigh;//b1
	pstSnsState->astRegsInfo[0].astI2cData[12].u32Data = u8DgainLow;//b2
    pstSnsState->astRegsInfo[0].astI2cData[13].u32Data = 0x18;//0x0087

    return;
}


static HI_VOID cmos_get_inttime_max(VI_PIPE ViPipe, HI_U16 u16ManRatioEnable, HI_U32 *au32Ratio, HI_U32 *au32IntTimeMax, HI_U32 *au32IntTimeMin, HI_U32 *pu32LFMaxIntTime)
{
    HI_U32 u32IntTimeMaxTmp0 = 0;
    HI_U32 u32IntTimeMaxTmp  = 0;
    HI_U32 u32RatioTmp = 0x40;
    HI_U32 u32ShortTimeMinLimit = 0;

    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    CMOS_CHECK_POINTER_VOID(au32Ratio);
    CMOS_CHECK_POINTER_VOID(au32IntTimeMax);
    CMOS_CHECK_POINTER_VOID(au32IntTimeMin);
    CMOS_CHECK_POINTER_VOID(pu32LFMaxIntTime);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER_VOID(pstSnsState);

	u32ShortTimeMinLimit = 1;

    if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {
        /*  limitation for DOL 2t1

            SHS1 limitation:
            2 or more
            RHS1 - 2 or less

            SHS2 limitation:
            RHS1 + 2 or more
            FSC - 2 or less

            RHS1 Limitation
            2n + 5 (n = 0,1,2...)
            RHS1 <= FSC - BRL * 2 - 21

            short exposure time = RHS1 - (SHS1 + 1) <= RHS1 - 3
            long exposure time = FSC - (SHS2 + 1) <= FSC - (RHS1 + 3)
            ExposureShort + ExposureLong <= FSC - 6
            short exposure time <= (FSC - 6) / (ratio + 1)
        */
        if (genFSWDRMode[ViPipe] == ISP_FSWDR_LONG_FRAME_MODE) {
			 u32IntTimeMaxTmp0  = pstSnsState->au32FL[1] - 6 - pstSnsState->au32WDRIntTime[0];
			 u32IntTimeMaxTmp  = pstSnsState->au32FL[0] - 8;
			 u32IntTimeMaxTmp = (u32IntTimeMaxTmp0 < u32IntTimeMaxTmp) ? u32IntTimeMaxTmp0 : u32IntTimeMaxTmp;
			 au32IntTimeMax[0] = u32IntTimeMaxTmp;
			 au32IntTimeMin[0] = u32ShortTimeMinLimit;
			 return; 
		   }
		   else
		   {
			 u32IntTimeMaxTmp0 = ((pstSnsState->au32FL[1]-16)- pstSnsState->au32WDRIntTime[0])* 0x40  / DIV_0_TO_1(au32Ratio[0]);
			 u32IntTimeMaxTmp = ((pstSnsState->au32FL[0]-16) * 0x40)  / DIV_0_TO_1(au32Ratio[0] + 0x40);	
			 u32IntTimeMaxTmp  = (u32IntTimeMaxTmp0 < u32IntTimeMaxTmp) ? u32IntTimeMaxTmp0 : u32IntTimeMaxTmp;		 
			 u32IntTimeMaxTmp  = (u32IntTimeMaxTmp > (pstSnsState->au32FL[0]-1088-20-20)) ?( pstSnsState->au32FL[0]-1088-20-20) : u32IntTimeMaxTmp;	 
             u32IntTimeMaxTmp  = (0 == u32IntTimeMaxTmp) ? 1 : u32IntTimeMaxTmp;

		   }

   	  		au32IntTimeMax[0] = u32IntTimeMaxTmp;			
			au32IntTimeMax[1] = au32IntTimeMax[0] * au32Ratio[0] >> 6;		
			au32IntTimeMax[2] = au32IntTimeMax[1] * au32Ratio[1] >> 6;	
			au32IntTimeMax[3] = au32IntTimeMax[2] * au32Ratio[2] >> 6;		
			au32IntTimeMin[0] = u32ShortTimeMinLimit;		
			au32IntTimeMin[1] = au32IntTimeMin[0] * au32Ratio[0] >> 6;	
			au32IntTimeMin[2] = au32IntTimeMin[1] * au32Ratio[1] >> 6;		
			au32IntTimeMin[3] = au32IntTimeMin[2] * au32Ratio[2] >> 6;
	 }    
    return;
}

/* Only used in LINE_WDR mode */
static HI_VOID cmos_ae_fswdr_attr_set(VI_PIPE ViPipe, AE_FSWDR_ATTR_S *pstAeFSWDRAttr)
{
    CMOS_CHECK_POINTER_VOID(pstAeFSWDRAttr);

    genFSWDRMode[ViPipe] = pstAeFSWDRAttr->enFSWDRMode;
    gu32MaxTimeGetCnt[ViPipe] = 0;
}

static HI_S32 cmos_init_ae_exp_function(AE_SENSOR_EXP_FUNC_S *pstExpFuncs)
{
    CMOS_CHECK_POINTER(pstExpFuncs);

    memset(pstExpFuncs, 0, sizeof(AE_SENSOR_EXP_FUNC_S));

    pstExpFuncs->pfn_cmos_get_ae_default    = cmos_get_ae_default;
    pstExpFuncs->pfn_cmos_fps_set           = cmos_fps_set;
    pstExpFuncs->pfn_cmos_slow_framerate_set = cmos_slow_framerate_set;
    pstExpFuncs->pfn_cmos_inttime_update    = cmos_inttime_update;
    pstExpFuncs->pfn_cmos_gains_update      = cmos_gains_update;
    pstExpFuncs->pfn_cmos_again_calc_table  = cmos_again_calc_table;
    pstExpFuncs->pfn_cmos_dgain_calc_table  = HI_NULL;
    pstExpFuncs->pfn_cmos_get_inttime_max   = cmos_get_inttime_max;
    pstExpFuncs->pfn_cmos_ae_fswdr_attr_set = cmos_ae_fswdr_attr_set;

    return HI_SUCCESS;
}

/* Rgain and Bgain of the golden sample */
#define GOLDEN_RGAIN                           0
#define GOLDEN_BGAIN                           0
static HI_S32 cmos_get_awb_default(VI_PIPE ViPipe, AWB_SENSOR_DEFAULT_S *pstAwbSnsDft)
{
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    CMOS_CHECK_POINTER(pstAwbSnsDft);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER(pstSnsState);

    memset(pstAwbSnsDft, 0, sizeof(AWB_SENSOR_DEFAULT_S));
    pstAwbSnsDft->u16WbRefTemp = 4884;


    switch (pstSnsState->enWDRMode) {
        default:
        case WDR_MODE_NONE:
			pstAwbSnsDft->au16GainOffset[0] = 479;
			pstAwbSnsDft->au16GainOffset[1] = 256;
			pstAwbSnsDft->au16GainOffset[2] = 256;
			pstAwbSnsDft->au16GainOffset[3] = 474;

			pstAwbSnsDft->as32WbPara[0] = 124;
			pstAwbSnsDft->as32WbPara[1] = -57;
			pstAwbSnsDft->as32WbPara[2] = -190;
			pstAwbSnsDft->as32WbPara[3] = 176932;
			pstAwbSnsDft->as32WbPara[4] = 128;
			pstAwbSnsDft->as32WbPara[5] = -125545;
			pstAwbSnsDft->u16GoldenRgain = GOLDEN_RGAIN;
			pstAwbSnsDft->u16GoldenBgain = GOLDEN_BGAIN;
			
            memcpy(&pstAwbSnsDft->stCcm, &g_stAwbCcm, sizeof(AWB_CCM_S));
            memcpy(&pstAwbSnsDft->stAgcTbl, &g_stAwbAgcTable, sizeof(AWB_AGC_TABLE_S));
            break;

        case WDR_MODE_2To1_LINE:
		#if 0
		    pstAwbSnsDft->au16GainOffset[0] = 476;
			pstAwbSnsDft->au16GainOffset[1] = 256;
			pstAwbSnsDft->au16GainOffset[2] = 256;
			pstAwbSnsDft->au16GainOffset[3] = 483;

			pstAwbSnsDft->as32WbPara[0] = 113;
			pstAwbSnsDft->as32WbPara[1] = -37;
			pstAwbSnsDft->as32WbPara[2] = -180;
			pstAwbSnsDft->as32WbPara[3] = 202846;
			pstAwbSnsDft->as32WbPara[4] = 128;
			pstAwbSnsDft->as32WbPara[5] = -155264;
		  #else
		   	pstAwbSnsDft->au16GainOffset[0] = 479;
			pstAwbSnsDft->au16GainOffset[1] = 256;
			pstAwbSnsDft->au16GainOffset[2] = 256;
			pstAwbSnsDft->au16GainOffset[3] = 474;

			pstAwbSnsDft->as32WbPara[0] = 124;
			pstAwbSnsDft->as32WbPara[1] = -57;
			pstAwbSnsDft->as32WbPara[2] = -190;
			pstAwbSnsDft->as32WbPara[3] = 176932;
			pstAwbSnsDft->as32WbPara[4] = 128;
			pstAwbSnsDft->as32WbPara[5] = -125545;
	      #endif
			pstAwbSnsDft->u16GoldenRgain = GOLDEN_RGAIN;
			pstAwbSnsDft->u16GoldenBgain = GOLDEN_BGAIN;
			
            memcpy(&pstAwbSnsDft->stCcm,    &g_stAwbCcmFsWdr, sizeof(AWB_CCM_S));
            memcpy(&pstAwbSnsDft->stAgcTbl, &g_stAwbAgcTableFSWDR, sizeof(AWB_AGC_TABLE_S));

            break;
    }

    pstAwbSnsDft->u16InitRgain = g_au16InitWBGain[ViPipe][0];
    pstAwbSnsDft->u16InitGgain = g_au16InitWBGain[ViPipe][1];
    pstAwbSnsDft->u16InitBgain = g_au16InitWBGain[ViPipe][2];
    pstAwbSnsDft->u16SampleRgain = g_au16SampleRgain[ViPipe];
    pstAwbSnsDft->u16SampleBgain = g_au16SampleBgain[ViPipe];

    return HI_SUCCESS;
}

static HI_S32 cmos_init_awb_exp_function(AWB_SENSOR_EXP_FUNC_S *pstExpFuncs)
{
    CMOS_CHECK_POINTER(pstExpFuncs);

    memset(pstExpFuncs, 0, sizeof(AWB_SENSOR_EXP_FUNC_S));
    pstExpFuncs->pfn_cmos_get_awb_default = cmos_get_awb_default;
    return HI_SUCCESS;
}


static ISP_CMOS_DNG_COLORPARAM_S g_stDngColorParam = {
    {378, 256, 430},
    {439, 256, 439}
};


static HI_S32 cmos_get_isp_default(VI_PIPE ViPipe, ISP_CMOS_DEFAULT_S *pstDef)
{
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    CMOS_CHECK_POINTER(pstDef);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER(pstSnsState);

    memset(pstDef, 0, sizeof(ISP_CMOS_DEFAULT_S));
#ifdef CONFIG_HI_ISP_CA_SUPPORT
    pstDef->unKey.bit1Ca      = 1;
    pstDef->pstCa             = &g_stIspCA;
#endif
    pstDef->unKey.bit1Clut    = 1;
    pstDef->pstClut           = &g_stIspCLUT;

    pstDef->unKey.bit1Dpc     = 1;
    pstDef->pstDpc            = &g_stCmosDpc;

    pstDef->unKey.bit1Wdr     = 1;
    pstDef->pstWdr            = &g_stIspWDR;

#ifdef CONFIG_HI_ISP_HLC_SUPPORT
    pstDef->unKey.bit1Hlc      = 0;
    pstDef->pstHlc             = &g_stIspHlc;
#endif
    pstDef->unKey.bit1Lsc      = 1;
    pstDef->pstLsc             = &g_stCmosLsc;
#ifdef CONFIG_HI_ISP_EDGEMARK_SUPPORT
    pstDef->unKey.bit1EdgeMark = 0;
    pstDef->pstEdgeMark        = &g_stIspEdgeMark;
#endif
#ifdef CONFIG_HI_ISP_PREGAMMA_SUPPORT
    pstDef->unKey.bit1PreGamma = 0;
    pstDef->pstPreGamma        = &g_stPreGamma;
#endif
    switch (pstSnsState->enWDRMode) {
        default:
        case WDR_MODE_NONE:
            pstDef->unKey.bit1Demosaic       = 1;
            pstDef->pstDemosaic              = &g_stIspDemosaic;
            pstDef->unKey.bit1Sharpen        = 1;
            pstDef->pstSharpen               = &g_stIspYuvSharpen;
            pstDef->unKey.bit1Drc            = 1;
            pstDef->pstDrc                   = &g_stIspDRC;
            pstDef->unKey.bit1BayerNr        = 1;
            pstDef->pstBayerNr               = &g_stIspBayerNr;
            pstDef->unKey.bit1AntiFalseColor = 1;
            pstDef->pstAntiFalseColor        = &g_stIspAntiFalseColor;
            pstDef->unKey.bit1Ldci           = 1;
            pstDef->pstLdci                  = &g_stIspLdci;
            pstDef->unKey.bit1Gamma          = 1;
            pstDef->pstGamma                 = &g_stIspGamma;
            pstDef->unKey.bit1Detail         = 1;
            pstDef->pstDetail                = &g_stIspDetail;
#ifdef CONFIG_HI_ISP_CR_SUPPORT
            pstDef->unKey.bit1Ge             = 1;
            pstDef->pstGe                    = &g_stIspGe;
#endif
            pstDef->unKey.bit1Dehaze         = 1;
            pstDef->pstDehaze                = &g_stIspDehaze;
            memcpy(&pstDef->stNoiseCalibration,   &g_stIspNoiseCalibration, sizeof(ISP_CMOS_NOISE_CALIBRATION_S));
            break;

        case WDR_MODE_2To1_LINE:
            pstDef->unKey.bit1Demosaic       = 1;
            pstDef->pstDemosaic              = &g_stIspDemosaicWdr;
            pstDef->unKey.bit1Sharpen        = 1;
            pstDef->pstSharpen               = &g_stIspYuvSharpenWdr;
            pstDef->unKey.bit1Drc            = 1;
            pstDef->pstDrc                   = &g_stIspDRCWDR;
            pstDef->unKey.bit1Gamma          = 1;
            pstDef->pstGamma                 = &g_stIspGammaFSWDR;
#ifdef CONFIG_HI_ISP_PREGAMMA_SUPPORT
            pstDef->unKey.bit1PreGamma       = 0;
            pstDef->pstPreGamma              = &g_stPreGamma;
#endif
            pstDef->unKey.bit1BayerNr        = 1;
            pstDef->pstBayerNr               = &g_stIspBayerNrWdr2To1;
            pstDef->unKey.bit1Detail         = 1;
            pstDef->pstDetail                = &g_stIspDetailWdr2To1;
#ifdef CONFIG_HI_ISP_CR_SUPPORT
            pstDef->unKey.bit1Ge             = 1;
            pstDef->pstGe                    = &g_stIspWdrGe;
#endif
            pstDef->unKey.bit1AntiFalseColor = 1;
            pstDef->pstAntiFalseColor        = &g_stIspWdrAntiFalseColor;
            pstDef->unKey.bit1Ldci           = 1;
            pstDef->pstLdci                  = &g_stIspWdrLdci;
            pstDef->unKey.bit1Dehaze         = 1;
            pstDef->pstDehaze                = &g_stIspDehazeWDR;
            memcpy(&pstDef->stNoiseCalibration, &g_stIspNoiseCalibration, sizeof(ISP_CMOS_NOISE_CALIBRATION_S));
            break;
    }

    pstDef->stSensorMode.u32SensorID = GC2093_ID;
    pstDef->stSensorMode.u8SensorMode = pstSnsState->u8ImgMode;

    if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {
        pstDef->stWdrSwitchAttr.au32ExpRatio[0] = 0x100;
    }

    memcpy(&pstDef->stDngColorParam, &g_stDngColorParam, sizeof(ISP_CMOS_DNG_COLORPARAM_S));

    switch (pstSnsState->u8ImgMode) {
        default:
        case GC2093_SENSOR_1080P_30FPS_LINEAR_MODE:
            pstDef->stSensorMode.stDngRawFormat.u8BitsPerSample = 12;
            pstDef->stSensorMode.stDngRawFormat.u32WhiteLevel = 4095;
            break;

        case GC2093_SENSOR_1080P_30FPS_2t1_WDR_MODE:
            pstDef->stSensorMode.stDngRawFormat.u8BitsPerSample = 10;
            pstDef->stSensorMode.stDngRawFormat.u32WhiteLevel = 1023;
            break;
    }

    pstDef->stSensorMode.stDngRawFormat.stDefaultScale.stDefaultScaleH.u32Denominator = 1;
    pstDef->stSensorMode.stDngRawFormat.stDefaultScale.stDefaultScaleH.u32Numerator = 1;
    pstDef->stSensorMode.stDngRawFormat.stDefaultScale.stDefaultScaleV.u32Denominator = 1;
    pstDef->stSensorMode.stDngRawFormat.stDefaultScale.stDefaultScaleV.u32Numerator = 1;
    pstDef->stSensorMode.stDngRawFormat.stCfaRepeatPatternDim.u16RepeatPatternDimRows = 2;
    pstDef->stSensorMode.stDngRawFormat.stCfaRepeatPatternDim.u16RepeatPatternDimCols = 2;
    pstDef->stSensorMode.stDngRawFormat.stBlcRepeatDim.u16BlcRepeatRows = 2;
    pstDef->stSensorMode.stDngRawFormat.stBlcRepeatDim.u16BlcRepeatCols = 2;
    pstDef->stSensorMode.stDngRawFormat.enCfaLayout = CFALAYOUT_TYPE_RECTANGULAR;
    pstDef->stSensorMode.stDngRawFormat.au8CfaPlaneColor[0] = 0;
    pstDef->stSensorMode.stDngRawFormat.au8CfaPlaneColor[1] = 1;
    pstDef->stSensorMode.stDngRawFormat.au8CfaPlaneColor[2] = 2;
    pstDef->stSensorMode.stDngRawFormat.au8CfaPattern[0] = 0;
    pstDef->stSensorMode.stDngRawFormat.au8CfaPattern[1] = 1;
    pstDef->stSensorMode.stDngRawFormat.au8CfaPattern[2] = 1;
    pstDef->stSensorMode.stDngRawFormat.au8CfaPattern[3] = 2;
    pstDef->stSensorMode.bValidDngRawFormat = HI_TRUE;

    return HI_SUCCESS;
}


static HI_S32 cmos_get_isp_black_level(VI_PIPE ViPipe, ISP_CMOS_BLACK_LEVEL_S *pstBlackLevel)
{
    HI_S32  i;
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    CMOS_CHECK_POINTER(pstBlackLevel);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER(pstSnsState);

    /* Don't need to update black level when iso change */
    pstBlackLevel->bUpdate = HI_FALSE;

    /* black level of linear mode */
    if (pstSnsState->enWDRMode == WDR_MODE_NONE) {
        for (i = 0; i < 4; i++) {
            pstBlackLevel->au16BlackLevel[i] = 256;    // 240
        }
    } else {

    /* black level of DOL mode */
        pstBlackLevel->au16BlackLevel[0] = 256;
        pstBlackLevel->au16BlackLevel[1] = 256;
        pstBlackLevel->au16BlackLevel[2] = 256;
        pstBlackLevel->au16BlackLevel[3] = 256;
    }

    return HI_SUCCESS;
}

static HI_VOID cmos_set_pixel_detect(VI_PIPE ViPipe, HI_BOOL bEnable)
{

    HI_U32 u32FullLines_5Fps, u32MaxIntTime_5Fps;
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER_VOID(pstSnsState);

    if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {
        return;
    } else {

        if (pstSnsState->u8ImgMode == GC2093_SENSOR_1080P_30FPS_LINEAR_MODE) {
         //   u32FullLines_5Fps = (gc2093_VMAX_1080P30_LINEAR * 30) / 5;
        } else {
            return;
        }
    }

   // u32MaxIntTime_5Fps = 4;

    if (bEnable)/* setup for ISP pixel calibration mode */
    {


    } else { /* setup for ISP 'normal mode' */
        pstSnsState->bSyncInit = HI_FALSE;
    }

    return;
}

static HI_S32 cmos_set_wdr_mode(VI_PIPE ViPipe, HI_U8 u8Mode)
{
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER(pstSnsState);

    pstSnsState->bSyncInit = HI_FALSE;

    switch (u8Mode & 0x3F) {
        case WDR_MODE_NONE:
            pstSnsState->enWDRMode = WDR_MODE_NONE;
            printf("linear mode\n");
            break;

        case WDR_MODE_2To1_LINE:
            pstSnsState->enWDRMode = WDR_MODE_2To1_LINE;
            printf("2to1 line WDR 1080p mode(60fps->30fps)\n");
            break;

        default:
            ISP_ERR_TRACE("NOT support this mode!\n");
            return HI_FAILURE;
    }

    memset(pstSnsState->au32WDRIntTime, 0, sizeof(pstSnsState->au32WDRIntTime));

    return HI_SUCCESS;
}

static HI_S32 cmos_get_sns_regs_info(VI_PIPE ViPipe, ISP_SNS_REGS_INFO_S *pstSnsRegsInfo)
{
    HI_S32 i;
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    CMOS_CHECK_POINTER(pstSnsRegsInfo);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER(pstSnsState);

    if ((pstSnsState->bSyncInit == HI_FALSE) || (pstSnsRegsInfo->bConfig == HI_FALSE)) {
        pstSnsState->astRegsInfo[0].enSnsType = ISP_SNS_I2C_TYPE;
        pstSnsState->astRegsInfo[0].unComBus.s8I2cDev = g_aunGc2093_BusInfo[ViPipe].s8I2cDev;
      pstSnsState->astRegsInfo[0].u8Cfg2ValidDelayMax = 2;
        pstSnsState->astRegsInfo[0].u32RegNum = 14;
            if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {
            pstSnsState->astRegsInfo[0].u32RegNum += 3;
        }

        for (i = 0; i < pstSnsState->astRegsInfo[0].u32RegNum; i++) {
            pstSnsState->astRegsInfo[0].astI2cData[i].bUpdate        = HI_TRUE;
            pstSnsState->astRegsInfo[0].astI2cData[i].u8DevAddr      = gc2093_i2c_addr;
            pstSnsState->astRegsInfo[0].astI2cData[i].u32AddrByteNum = gc2093_addr_byte;
            pstSnsState->astRegsInfo[0].astI2cData[i].u32DataByteNum = gc2093_data_byte;
        }

        // Linear Mode Regs
        pstSnsState->astRegsInfo[0].astI2cData[0].u8DelayFrmNum = 0;
        pstSnsState->astRegsInfo[0].astI2cData[0].u32RegAddr = 0x0090;
        pstSnsState->astRegsInfo[0].astI2cData[1].u8DelayFrmNum = 0;
        pstSnsState->astRegsInfo[0].astI2cData[1].u32RegAddr = 0x0041;      
        pstSnsState->astRegsInfo[0].astI2cData[2].u8DelayFrmNum = 0;       
        pstSnsState->astRegsInfo[0].astI2cData[2].u32RegAddr = 0x0042;  // gain

        pstSnsState->astRegsInfo[0].astI2cData[3].u8DelayFrmNum = 0;
        pstSnsState->astRegsInfo[0].astI2cData[3].u32RegAddr = 0x0003;
        pstSnsState->astRegsInfo[0].astI2cData[4].u8DelayFrmNum = 0;
        pstSnsState->astRegsInfo[0].astI2cData[4].u32RegAddr = 0x0004;    
		
        pstSnsState->astRegsInfo[0].astI2cData[5].u8DelayFrmNum = 0;     
        pstSnsState->astRegsInfo[0].astI2cData[5].u32RegAddr = 0x0087;
		pstSnsState->astRegsInfo[0].astI2cData[6].u8DelayFrmNum = 0;
        pstSnsState->astRegsInfo[0].astI2cData[6].u32RegAddr = 0x00b3;
        pstSnsState->astRegsInfo[0].astI2cData[7].u8DelayFrmNum = 0;       
        pstSnsState->astRegsInfo[0].astI2cData[7].u32RegAddr = 0x00b8; 
		pstSnsState->astRegsInfo[0].astI2cData[8].u8DelayFrmNum = 0;       
		pstSnsState->astRegsInfo[0].astI2cData[8].u32RegAddr = 0x00b9;
		pstSnsState->astRegsInfo[0].astI2cData[9].u8DelayFrmNum = 0;
		pstSnsState->astRegsInfo[0].astI2cData[9].u32RegAddr = 0x0078;
		pstSnsState->astRegsInfo[0].astI2cData[10].u8DelayFrmNum = 0;       
		pstSnsState->astRegsInfo[0].astI2cData[10].u32RegAddr = 0x00c2; 
		pstSnsState->astRegsInfo[0].astI2cData[11].u8DelayFrmNum = 0;       
		pstSnsState->astRegsInfo[0].astI2cData[11].u32RegAddr = 0x00b1; 
		pstSnsState->astRegsInfo[0].astI2cData[12].u8DelayFrmNum = 0;
		pstSnsState->astRegsInfo[0].astI2cData[12].u32RegAddr = 0x00b2;
		pstSnsState->astRegsInfo[0].astI2cData[13].u8DelayFrmNum = 0;       
		pstSnsState->astRegsInfo[0].astI2cData[13].u32RegAddr = 0x0087;
		
        // DOL 2t1 Mode Regs
        if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {

		  pstSnsState->astRegsInfo[0].astI2cData[14].u8DelayFrmNum = 0;
          pstSnsState->astRegsInfo[0].astI2cData[14].u32RegAddr = 0x0001;
          pstSnsState->astRegsInfo[0].astI2cData[15].u8DelayFrmNum = 0;
          pstSnsState->astRegsInfo[0].astI2cData[15].u32RegAddr = 0x0002;
		  pstSnsState->astRegsInfo[0].astI2cData[16].u8DelayFrmNum = 0;
          pstSnsState->astRegsInfo[0].astI2cData[16].u32RegAddr = 0x0032;
        }

        pstSnsState->bSyncInit = HI_TRUE;
    } else {
    
        for (i = 0; i < pstSnsState->astRegsInfo[0].u32RegNum; i++)
		{
            if (pstSnsState->astRegsInfo[0].astI2cData[i].u32Data == pstSnsState->astRegsInfo[1].astI2cData[i].u32Data)
			{
                pstSnsState->astRegsInfo[0].astI2cData[i].bUpdate = HI_FALSE;//FALSE;
            }
			else 
			{
                pstSnsState->astRegsInfo[0].astI2cData[i].bUpdate = HI_TRUE;
            }
        }
		
        if ((pstSnsState->astRegsInfo[0].astI2cData[6].bUpdate == HI_TRUE) || (pstSnsState->astRegsInfo[0].astI2cData[7].bUpdate == HI_TRUE)|| (pstSnsState->astRegsInfo[0].astI2cData[8].bUpdate == HI_TRUE)
			|| (pstSnsState->astRegsInfo[0].astI2cData[9].bUpdate == HI_TRUE)|| (pstSnsState->astRegsInfo[0].astI2cData[10].bUpdate == HI_TRUE)|| (pstSnsState->astRegsInfo[0].astI2cData[11].bUpdate == HI_TRUE)
			|| (pstSnsState->astRegsInfo[0].astI2cData[12].bUpdate == HI_TRUE))
		{
			pstSnsState->astRegsInfo[0].astI2cData[5].bUpdate = HI_TRUE;
			pstSnsState->astRegsInfo[0].astI2cData[6].bUpdate = HI_TRUE;
			pstSnsState->astRegsInfo[0].astI2cData[7].bUpdate = HI_TRUE;
			pstSnsState->astRegsInfo[0].astI2cData[8].bUpdate = HI_TRUE;
			pstSnsState->astRegsInfo[0].astI2cData[9].bUpdate = HI_TRUE;
			pstSnsState->astRegsInfo[0].astI2cData[10].bUpdate = HI_TRUE;
			pstSnsState->astRegsInfo[0].astI2cData[11].bUpdate = HI_TRUE;
            pstSnsState->astRegsInfo[0].astI2cData[12].bUpdate = HI_TRUE;
			pstSnsState->astRegsInfo[0].astI2cData[13].bUpdate = HI_TRUE;
        }

        if (pstSnsState->enWDRMode == WDR_MODE_2To1_FRAME)
		{
		  if ((pstSnsState->astRegsInfo[0].astI2cData[3].bUpdate == HI_TRUE) || (pstSnsState->astRegsInfo[0].astI2cData[4].bUpdate == HI_TRUE) 
		    || (pstSnsState->astRegsInfo[0].astI2cData[14].bUpdate == HI_TRUE)|| (pstSnsState->astRegsInfo[0].astI2cData[15].bUpdate == HI_TRUE)|| (pstSnsState->astRegsInfo[0].astI2cData[16].bUpdate == HI_TRUE))
            pstSnsState->astRegsInfo[0].astI2cData[3].bUpdate = HI_TRUE;
            pstSnsState->astRegsInfo[0].astI2cData[4].bUpdate = HI_TRUE;
            pstSnsState->astRegsInfo[0].astI2cData[14].bUpdate = HI_TRUE;
            pstSnsState->astRegsInfo[0].astI2cData[15].bUpdate = HI_TRUE;
			pstSnsState->astRegsInfo[0].astI2cData[16].bUpdate = HI_TRUE;
        }
}

    pstSnsRegsInfo->bConfig = HI_FALSE;
    memcpy(pstSnsRegsInfo, &pstSnsState->astRegsInfo[0], sizeof(ISP_SNS_REGS_INFO_S));
    memcpy(&pstSnsState->astRegsInfo[1], &pstSnsState->astRegsInfo[0], sizeof(ISP_SNS_REGS_INFO_S));

    pstSnsState->au32FL[1] = pstSnsState->au32FL[0];

    return HI_SUCCESS;
}

static HI_S32 cmos_set_image_mode(VI_PIPE ViPipe, ISP_CMOS_SENSOR_IMAGE_MODE_S *pstSensorImageMode)
{
    HI_U8 u8SensorImageMode = 0;
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    CMOS_CHECK_POINTER(pstSensorImageMode);
    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER(pstSnsState);

    u8SensorImageMode = pstSnsState->u8ImgMode;
    pstSnsState->bSyncInit = HI_FALSE;

    if (pstSensorImageMode->f32Fps <= 30) {
        if (pstSnsState->enWDRMode == WDR_MODE_NONE) {
            if (GC2093_RES_IS_1080P(pstSensorImageMode->u16Width, pstSensorImageMode->u16Height)) {
                u8SensorImageMode                  = GC2093_SENSOR_1080P_30FPS_LINEAR_MODE;
                pstSnsState->u32FLStd              = GC2093_VMAX_1080P30_LINEAR;
                //g_astgc2093State[ViPipe].u8Hcg = 0x2;
            } else {
                GC2093_ERR_MODE_PRINT(pstSensorImageMode, pstSnsState);
                return HI_FAILURE;
            }
        } else if (pstSnsState->enWDRMode == WDR_MODE_2To1_LINE) {
            if (GC2093_RES_IS_1080P(pstSensorImageMode->u16Width, pstSensorImageMode->u16Height)) {
                u8SensorImageMode                   = GC2093_SENSOR_1080P_30FPS_2t1_WDR_MODE;
               // g_astgc2093State[ViPipe].u32BRL = 1109;
               // g_astgc2093State[ViPipe].u8Hcg  = 0x1;
                pstSnsState->u32FLStd               = GC2093_VMAX_1080P60TO30_WDR * 2;
            } else {
                GC2093_ERR_MODE_PRINT(pstSensorImageMode, pstSnsState);
                return HI_FAILURE;
            }
        } else {
            GC2093_ERR_MODE_PRINT(pstSensorImageMode, pstSnsState);
            return HI_FAILURE;
        }
    } else {
    }

    if ((pstSnsState->bInit == HI_TRUE) && (u8SensorImageMode == pstSnsState->u8ImgMode)) {
        /* Don't need to switch SensorImageMode */
        return ISP_DO_NOT_NEED_SWITCH_IMAGEMODE;
    }

    pstSnsState->u8ImgMode = u8SensorImageMode;
    pstSnsState->au32FL[0] = pstSnsState->u32FLStd;
    pstSnsState->au32FL[1] = pstSnsState->au32FL[0];

    return HI_SUCCESS;
}

static HI_VOID sensor_global_init(VI_PIPE ViPipe)
{
    ISP_SNS_STATE_S *pstSnsState = HI_NULL;

    GC2093_SENSOR_GET_CTX(ViPipe, pstSnsState);
    CMOS_CHECK_POINTER_VOID(pstSnsState);

    pstSnsState->bInit = HI_FALSE;
    pstSnsState->bSyncInit = HI_FALSE;
    pstSnsState->u8ImgMode = GC2093_SENSOR_1080P_30FPS_LINEAR_MODE;
    pstSnsState->enWDRMode = WDR_MODE_NONE;
    pstSnsState->u32FLStd = GC2093_VMAX_1080P30_LINEAR;
    pstSnsState->au32FL[0] = GC2093_VMAX_1080P60TO30_WDR;
    pstSnsState->au32FL[1] = GC2093_VMAX_1080P60TO30_WDR;

    memset(&pstSnsState->astRegsInfo[0], 0, sizeof(ISP_SNS_REGS_INFO_S));
    memset(&pstSnsState->astRegsInfo[1], 0, sizeof(ISP_SNS_REGS_INFO_S));
}

static HI_S32 cmos_init_sensor_exp_function(ISP_SENSOR_EXP_FUNC_S *pstSensorExpFunc)
{
    CMOS_CHECK_POINTER(pstSensorExpFunc);

    memset(pstSensorExpFunc, 0, sizeof(ISP_SENSOR_EXP_FUNC_S));

    pstSensorExpFunc->pfn_cmos_sensor_init = gc2093_init;
    pstSensorExpFunc->pfn_cmos_sensor_exit = gc2093_exit;
    pstSensorExpFunc->pfn_cmos_sensor_global_init = sensor_global_init;
    pstSensorExpFunc->pfn_cmos_set_image_mode = cmos_set_image_mode;
    pstSensorExpFunc->pfn_cmos_set_wdr_mode = cmos_set_wdr_mode;

    pstSensorExpFunc->pfn_cmos_get_isp_default = cmos_get_isp_default;
    pstSensorExpFunc->pfn_cmos_get_isp_black_level = cmos_get_isp_black_level;
    pstSensorExpFunc->pfn_cmos_set_pixel_detect = cmos_set_pixel_detect;
    pstSensorExpFunc->pfn_cmos_get_sns_reg_info = cmos_get_sns_regs_info;

    return HI_SUCCESS;
}

/****************************************************************************
 * callback structure                                                       *
 ****************************************************************************/

static HI_S32 gc2093_set_bus_info(VI_PIPE ViPipe, ISP_SNS_COMMBUS_U unSNSBusInfo)
{
    g_aunGc2093_BusInfo[ViPipe].s8I2cDev = unSNSBusInfo.s8I2cDev;

    return HI_SUCCESS;
}

static HI_S32 sensor_ctx_init(VI_PIPE ViPipe)
{
    ISP_SNS_STATE_S *pastSnsStateCtx = HI_NULL;

    GC2093_SENSOR_GET_CTX(ViPipe, pastSnsStateCtx);

    if (pastSnsStateCtx == HI_NULL) {
        pastSnsStateCtx = (ISP_SNS_STATE_S *)malloc(sizeof(ISP_SNS_STATE_S));
        if (pastSnsStateCtx == HI_NULL) {
            ISP_ERR_TRACE("Isp[%d] SnsCtx malloc memory failed!\n", ViPipe);
            return HI_ERR_ISP_NOMEM;
        }
    }

    memset(pastSnsStateCtx, 0, sizeof(ISP_SNS_STATE_S));

    GC2093_SENSOR_SET_CTX(ViPipe, pastSnsStateCtx);

    return HI_SUCCESS;
}

static HI_VOID sensor_ctx_exit(VI_PIPE ViPipe)
{
    ISP_SNS_STATE_S *pastSnsStateCtx = HI_NULL;
    bFirstFps[ViPipe] = HI_TRUE;
    GC2093_SENSOR_GET_CTX(ViPipe, pastSnsStateCtx);
    SENSOR_FREE(pastSnsStateCtx);
    GC2093_SENSOR_RESET_CTX(ViPipe);
}

static HI_S32 sensor_register_callback(VI_PIPE ViPipe, ALG_LIB_S *pstAeLib, ALG_LIB_S *pstAwbLib)
{
    HI_S32 s32Ret;
    ISP_SENSOR_REGISTER_S stIspRegister;
    AE_SENSOR_REGISTER_S  stAeRegister;
    AWB_SENSOR_REGISTER_S stAwbRegister;
    ISP_SNS_ATTR_INFO_S   stSnsAttrInfo;

    CMOS_CHECK_POINTER(pstAeLib);
    CMOS_CHECK_POINTER(pstAwbLib);

    s32Ret = sensor_ctx_init(ViPipe);

    if (s32Ret != HI_SUCCESS) {
        return HI_FAILURE;
    }

    stSnsAttrInfo.eSensorId = GC2093_ID;

    s32Ret  = cmos_init_sensor_exp_function(&stIspRegister.stSnsExp);
    s32Ret |= HI_MPI_ISP_SensorRegCallBack(ViPipe, &stSnsAttrInfo, &stIspRegister);

    if (s32Ret != HI_SUCCESS) {
        ISP_ERR_TRACE("sensor register callback function failed!\n");
        return s32Ret;
    }

    s32Ret  = cmos_init_ae_exp_function(&stAeRegister.stSnsExp);
    s32Ret |= HI_MPI_AE_SensorRegCallBack(ViPipe, pstAeLib, &stSnsAttrInfo, &stAeRegister);

    if (s32Ret != HI_SUCCESS) {
        ISP_ERR_TRACE("sensor register callback function to ae lib failed!\n");
        return s32Ret;
    }

    s32Ret  = cmos_init_awb_exp_function(&stAwbRegister.stSnsExp);
    s32Ret |= HI_MPI_AWB_SensorRegCallBack(ViPipe, pstAwbLib, &stSnsAttrInfo, &stAwbRegister);

    if (s32Ret != HI_SUCCESS) {
        ISP_ERR_TRACE("sensor register callback function to awb lib failed!\n");
        return s32Ret;
    }

    return HI_SUCCESS;
}

static HI_S32 sensor_unregister_callback(VI_PIPE ViPipe, ALG_LIB_S *pstAeLib, ALG_LIB_S *pstAwbLib)
{
    HI_S32 s32Ret;

    CMOS_CHECK_POINTER(pstAeLib);
    CMOS_CHECK_POINTER(pstAwbLib);

    s32Ret = HI_MPI_ISP_SensorUnRegCallBack(ViPipe, GC2093_ID);
    if (s32Ret != HI_SUCCESS) {
        ISP_ERR_TRACE("sensor unregister callback function failed!\n");
        return s32Ret;
    }

    s32Ret = HI_MPI_AE_SensorUnRegCallBack(ViPipe, pstAeLib, GC2093_ID);
    if (s32Ret != HI_SUCCESS) {
        ISP_ERR_TRACE("sensor unregister callback function to ae lib failed!\n");
        return s32Ret;
    }

    s32Ret = HI_MPI_AWB_SensorUnRegCallBack(ViPipe, pstAwbLib, GC2093_ID);
    if (s32Ret != HI_SUCCESS) {
        ISP_ERR_TRACE("sensor unregister callback function to awb lib failed!\n");
        return s32Ret;
    }

    sensor_ctx_exit(ViPipe);

    return HI_SUCCESS;
}

static HI_S32 sensor_set_init(VI_PIPE ViPipe, ISP_INIT_ATTR_S *pstInitAttr)
{
    CMOS_CHECK_POINTER(pstInitAttr);

    g_au32InitExposure[ViPipe] = pstInitAttr->u32Exposure;
    g_au32LinesPer500ms[ViPipe] = pstInitAttr->u32LinesPer500ms;
    g_au16InitWBGain[ViPipe][0] = pstInitAttr->u16WBRgain;
    g_au16InitWBGain[ViPipe][1] = pstInitAttr->u16WBGgain;
    g_au16InitWBGain[ViPipe][2] = pstInitAttr->u16WBBgain;
    g_au16SampleRgain[ViPipe] = pstInitAttr->u16SampleRgain;
    g_au16SampleBgain[ViPipe] = pstInitAttr->u16SampleBgain;

    return HI_SUCCESS;
}

ISP_SNS_OBJ_S stSnsGc2093Obj = {
    .pfnRegisterCallback    = sensor_register_callback,
    .pfnUnRegisterCallback  = sensor_unregister_callback,
    .pfnStandby             = gc2093_standby,
    .pfnRestart             = gc2093_restart,
    .pfnMirrorFlip          = HI_NULL,
    .pfnWriteReg            = gc2093_write_register,
    .pfnReadReg             = gc2093_read_register,
    .pfnSetBusInfo          = gc2093_set_bus_info,
    .pfnSetInit             = sensor_set_init
};

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

#endif /* __GC2093_CMOS_H_ */
