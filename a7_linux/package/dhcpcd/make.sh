#!/usr/bin/env bash
# bash debug option
#set -x

SOFTWARE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SOFTWARE_SITE=http://roy.marples.name/downloads/dhcpcd
SOFTWARE_VER="dhcpcd-6.11.5"
SOFTWARE_PACKAGE="$SOFTWARE_VER".tar.xz
SOFTWARE_SHA256=6f9674dc7e27e936cc787175404a6171618675ecfb6903ab9887b1b66a87d69e

INSTALL_DIR=$SOFTWARE_DIR/install

CC=arm-himix200-linux-gcc

cd $SOFTWARE_DIR
if [ -e $SOFTWARE_PACKAGE ] && [ $(sha256sum $SOFTWARE_PACKAGE | awk '{printf $1}') == $SOFTWARE_SHA256 ]; then
	:
else
	wget $SOFTWARE_SITE/$SOFTWARE_PACKAGE
	if [ $? != 0 ]; then
		echo "=================== ${FUNCNAME[0]} $LINENO download failed ========================="
		exit 1
	fi
	if [ $(sha256sum $SOFTWARE_PACKAGE) != $SOFTWARE_SHA256 ]; then
		echo "=================== ${FUNCNAME[0]} $LINENO wrong software package, checksum failed  ========================="
		exit 1
	fi
fi

mkdir -p $INSTALL_DIR

software_compile()
{	
	cd $SOFTWARE_DIR
	if [ ! -e .decompressed ]; then
		tar xvf $SOFTWARE_PACKAGE
		
		if [ $? != 0 ]; then
			echo "=================== ${FUNCNAME[0]} $LINENO failed ========================="
			exit 1
		fi
		touch .decompressed
	fi
	
	if [ ! -e .compiled ]; then
		cd $SOFTWARE_VER
		./configure --libexecdir=/lib/dhcpcd --sysconfdir=/etc --sbindir=/sbin --mandir=/usr/share/man --datadir=/usr/share --libdir=/lib --dbdir=/var/lib/dhcpcd --host=arm-himix200-linux \
			--prefix=$INSTALL_DIR
		make -j32
	
		if [ $? != 0 ]; then
			echo "=================== ${FUNCNAME[0]} $LINENO failed ========================="
			exit 1
		fi
	
		touch ../.compiled
	fi
	
	cd $SOFTWARE_DIR
	mkdir -p $INSTALL_DIR/etc/init.d
	mkdir -p $INSTALL_DIR/lib/dhcpcd/{dev,dhcpcd-hooks}
	mkdir -p $INSTALL_DIR/sbin
	mkdir -p $INSTALL_DIR/usr/share/dhcpcd/hooks
	cp $SOFTWARE_VER/dhcpcd.conf $INSTALL_DIR/etc/
	cp $SOFTWARE_VER/dhcpcd $INSTALL_DIR/sbin/
	cp $SOFTWARE_VER/dhcpcd-hooks/{01-test,02-dump,20-resolv.conf,30-hostname} $INSTALL_DIR/lib/dhcpcd/dhcpcd-hooks/
	cp $SOFTWARE_VER/dhcpcd-run-hooks $INSTALL_DIR/lib/dhcpcd/
	chmod 0666 $INSTALL_DIR/lib/dhcpcd/dhcpcd-run-hooks
	cp $SOFTWARE_VER/dhcpcd-hooks/{10-wpa_supplicant,15-timezone,29-lookup-hostname} $INSTALL_DIR/usr/share/dhcpcd/hooks/
	chmod 0444 $INSTALL_DIR/usr/share/dhcpcd/hooks/* $INSTALL_DIR/lib/dhcpcd/dhcpcd-hooks/*
	cp user-config-dhcpcd/etc/init.d/* $INSTALL_DIR/etc/init.d/ -rf
}

clean()
{
	cd $SOFTWARE_DIR
	rm .compiled
	cd $SOFTWARE_VER
	make clean
}

distclean()
{
	cd $SOFTWARE_DIR
	rm -rf $SOFTWARE_VER
	rm -rf $INSTALL_DIR
	rm -rf .decompressed .compiled 
}

if [ X$1 == Xclean ]; then
	clean
elif [ X$1 == Xdistclean ]; then
	distclean
else
	software_compile
fi


