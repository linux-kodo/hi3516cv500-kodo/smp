#!/usr/bin/env bash 
set -x

CC=arm-himix200-linux-gcc
AR=arm-himix200-linux-ar

ZLIB_VER="zlib-1.2.11"
OPENSSL_VER="openssl-1.0.2n"
OPENSSH_VER="openssh-7.6p1"

PACKAGE_DIR=`pwd`
ZLIB_INSTALL_DIR=$PACKAGE_DIR/zlib/zlib-install
OPENSSL_INSTALL_DIR=$PACKAGE_DIR/openssl/openssl-install

zlib_build()
{	
	cd $PACKAGE_DIR/zlib
	tar xvf zlib-1.2.11.tar.gz
	mkdir -p zlib-install
	ZLIB_INSTALL_DIR=$(realpath zlib-install)
	cd zlib-1.2.11
	prefix=$ZLIB_INSTALL_DIR CC=arm-himix200-linux-gcc ./configure
	make -j32
	make install
	cd $PACKAGE_DIR
}

openssl_build()
{	
	cd $PACKAGE_DIR/openssl
	tar xvf openssl-1.0.2n.tar.gz
	mkdir -p openssl-install
	OPENSSL_INSTALL_DIR=$(realpath openssl-install)
	cd openssl-1.0.2n
	./Configure --prefix=$OPENSSL_INSTALL_DIR os/compiler:$CC
	make -j32
	make install
	cd $PACKAGE_DIR
}

openssh_build()
{
	cd $PACKAGE_DIR/openssh
	tar xvf openssh-7.6p1.tar.gz
	cd openssh-7.6p1
	./configure --host=arm-himix200-linux --with-libs --with-zlib=$ZLIB_INSTALL_DIR --with-ssl-dir=$OPENSSL_INSTALL_DIR --disable-etc-default-login 
	make -j32

	# 使用“ssh-keygen”生成个四个 key 文件:
	# “ssh_host_rsa_key”   “ssh_host_dsa_key”
	# “ssh_host_ecdsa_key” “ssh_host_ed25519_key”
	ssh-keygen -t rsa -f ssh_host_rsa_key -N ""
	ssh-keygen -t dsa -f ssh_host_dsa_key -N ""
	ssh-keygen -t ecdsa -f ssh_host_ecdsa_key -N ""
	ssh-keygen -t dsa -f ssh_host_ed25519_key -N ""

	cd $PACKAGE_DIR
}

ssh_install()
{
	cd $PACKAGE_DIR/openssh
	mkdir -p openssh-install/usr/{libexec,local/{bin,etc},share,lib/ssl}
	mkdir -p openssh-install/etc/ssl
	mkdir -p openssh-install/var/{run,empty/sshd}
	chmod -R 0744 openssh-install/var/empty
	cd	openssh-7.6p1

	cp scp sftp ssh sshd ssh-add ssh-agent ssh-keygen ssh-keyscan ../openssh-install/usr/local/bin/
	cp moduli ssh_config sshd_config ../openssh-install/usr/local/etc/
	cp sftp-server ssh-keysign ../openssh-install/usr/libexec
	# 拷贝KEY
	cp ssh_host_rsa_key ssh_host_dsa_key ssh_host_ecdsa_key ssh_host_ed25519_key ../openssh-install/usr/local/etc/
	
	# copy zlib
	cp -rf $ZLIB_INSTALL_DIR/include ../openssh-install/usr/
	cp -rf $ZLIB_INSTALL_DIR/lib ../openssh-install/usr/

	# copy openssl
	cp -rf $OPENSSL_INSTALL_DIR/bin ../openssh-install/usr/local/
	cp -rf $OPENSSL_INSTALL_DIR/include ../openssh-install/usr/
	cp -rf $OPENSSL_INSTALL_DIR/lib ../openssh-install/usr/
	cp -rf $OPENSSL_INSTALL_DIR/ssl/man ../openssh-install/usr/share
	cp -rf $OPENSSL_INSTALL_DIR/ssl/{certs,private,openssl.cnf} ../openssh-install/etc/ssl/
	cp -rf $OPENSSL_INSTALL_DIR/ssl/misc ../openssh-install/usr/lib/ssl/

	# copy user config
	cp -rf $PACKAGE_DIR/openssh/openssh-user-config/* ../openssh-install/ 
}

clean()
{
	cd $PACKAGE_DIR/zlib/$ZLIB_VER
	make clean
	cd $PACKAGE_DIR/openssl/$OPENSSL_VER
	make clean
	cd $PACKAGE_DIR/openssh/$OPENSSH_VER
	make clean
}

distclean()
{
	cd $PACKAGE_DIR/zlib
	rm -rf $ZLIB_VER
	rm -rf $ZLIB_INSTALL_DIR
	cd $PACKAGE_DIR/openssl
	rm -rf $OPENSSL_VER
	rm -rf $OPENSSL_INSTALL_DIR
	cd $PACKAGE_DIR/openssh
	rm -rf $OPENSSH_VER
	rm -rf $PACKAGE_DIR/openssh/openssh-install

	#rm -rf .decompressed .compiled 
}

if [ X$1 == Xclean ]; then
	clean
elif [ X$1 == Xdistclean ]; then
	distclean
else
	zlib_build
	openssl_build
	openssh_build
	ssh_install
fi

