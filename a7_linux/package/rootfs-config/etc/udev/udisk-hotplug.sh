#!/bin/sh
################################################################################
#    Create By rpdzkj ivy
################################################################################
CONSOLE=/dev/ttyS000
BASENAME=`basename $DEVNAME`
MOUNT_DIR=/mount/udisk/$BASENAME
# test script for test hi3516dv300 hardware function
TEST_DIR=$MOUNT_DIR/hi3516dv300-test
TEST_SH=test.sh
################################################################################
show_env ()
{
	local RED="\033[31m"
	local NORMAL="\033[00m"
	{
		echo -e ${RED}"ACTION=$ACTION"           ${NORMAL}
		echo -e ${RED}"DEVNAME=$DEVNAME"         ${NORMAL}
		echo -e ${RED}"DEVTYPE=$DEVTYPE"         ${NORMAL}
		echo -e ${RED}"DEVPATH=$DEVPATH"         ${NORMAL}
		echo -e ${RED}"SUBSYSTEM=$SUBSYSTEM"     ${NORMAL}
		echo -e ${RED}"SEQNUM=$SEQNUM"           ${NORMAL}
		echo -e ${RED}"UDEVD_EVENT=$UDEVD_EVENT" ${NORMAL}
	} > ${CONSOLE}
}
# new_name orgin_name

add_udisk ()
{
	if [ ! -d $MOUNT_DIR ]; then
		mkdir -p $MOUNT_DIR
	fi
	mount $DEVNAME $MOUNT_DIR

	if [ -d $TEST_DIR ] && [ -e $TEST_DIR/$TEST_SH ]; then
		source $TEST_DIR/$TEST_SH
	fi
}
# new_name
remove_udisk ()
{
	umount $MOUNT_DIR
}
hotplug_udisk ()
{
	if [ ss"$SUBSYSTEM" != ss"block" ]; then
		return 1
	fi

	if [ ss"$(echo $DEVPATH | grep -r "sd")" = ss"" ]; then
		return 1
	fi

	case "$ACTION" in
	"add"    )
		add_udisk 
	;;
	"remove" )
		remove_udisk 
	;;
	* )
		echo "Not recognise ACTION:${ACTION}" > ${CONSOLE}
	;;
	esac

	return 0
}
#show_env
hotplug_udisk
