#!/usr/bin/env bash
# bash debug option
#set -x
SOFTWARE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SOFTWARE_VER="qt-everywhere-src-5.12.7"
SOFTWARE_PACKAGE="$SOFTWARE_VER".tar.xz

INSTALL_DIR=$SOFTWARE_DIR/install

CC=arm-himix200-linux-gcc

cd $SOFTWARE_DIR
mkdir -p $INSTALL_DIR

software_compile()
{
	cd $SOFTWARE_DIR
	if [ ! -e .decompressed ]; then
		tar xvf $SOFTWARE_PACKAGE

		if [ $? != 0 ]; then
			echo "=================== ${FUNCNAME[0]} $LINENO failed ========================="
			exit 1
		fi
		touch .decompressed
	fi

	if [ ! -e .compiled ]; then
		cp -a user-config-qt5.12/linux-hi3516cv500-g++/ $SOFTWARE_VER/qtbase/mkspecs/
		cd $SOFTWARE_VER
		# config qt 5.12, hi3516cv500 does not support opengl
		./configure -prefix $INSTALL_DIR \
			-opensource -confirm-license -release \
			-linuxfb -qt-zlib -no-gif -qt-libpng \
			-qt-libjpeg -qt-freetype -no-rpath -no-pch \
			-no-avx -no-openssl -no-cups -no-dbus -no-eglfs \
			-no-pkg-config -no-glib -no-iconv -no-opengl \
			-xplatform linux-hi3516cv500-g++ -make libs \
			-nomake tools -qt-sqlite -nomake tests \
			-skip qtgamepad \
			-skip qtandroidextras \
			-skip qtmacextras \
			-skip qtx11extras \
			-skip qtsensors \
			-skip qtserialbus \
			-skip qtwebchannel \
			-skip qtwebsockets \
			-skip qtlocation \
			-skip qtquickcontrols \
			-skip qtpurchasing \
			-skip qtconnectivity \
			-skip qtscxml \
			-skip qtxmlpatterns \
			-skip qtnetworkauth \
			-skip qtspeech \
			-skip qtscript \
			-skip qtremoteobjects \
			-skip qtcharts \
			-skip qtdatavis3d \
			-skip qtwebview

		make -j16	# 16 thread compile qt 5.12

		if [ $? != 0 ]; then
			echo "=================== ${FUNCNAME[0]} $LINENO failed ========================="
			exit 1
		fi

		touch ../.compiled
	fi
	make install	# install to install
}

rootfs_install()
{
	if [ ! -e .compiled ]; then
		software_compile
	fi

	cd $SOFTWARE_DIR
	mkdir -p rootfs-Qt/usr/qt5.12/{include,lib/fonts,plugins,demos}
	#cp -a $INSTALL_DIR/include/{QtCore,QtWidgets} rootfs-Qt/usr/qt5.12/include/
	#cp -a $INSTALL_DIR/lib/{}
	cp -af $INSTALL_DIR/* rootfs-Qt/usr/qt5.12/
	cd rootfs-Qt/usr/qt5.12/
	# delete we don't need
	rm -rf bin translations include
	rm -rf doc
	rm -rf mkspecs
	rm lib/*.a
	rm lib/*.la
	rm lib/*.prl
	rm lib/*Quick*
	rm lib/*PrintSupport*
	rm lib/*Test*
	rm -fr lib/cmake
	rm -fr lib/pkgconfig
	rm -fr plugins/gamepads
	rm -fr plugins/sensorgestures
	rm -fr plugins/sensors
	rm -fr plugins/position
	rm -fr plugins/canbus

	cd $SOFTWARE_DIR
	cp user-config-qt5.12/animatedtiles rootfs-Qt/usr/qt5.12/demos/
	cp user-config-qt5.12/StartQT.sh rootfs-Qt/usr/qt5.12/demos/
	cp user-config-qt5.12/qt-env.sh rootfs-Qt/usr/qt5.12/
	cp user-config-qt5.12/wqy-microhei.ttc rootfs-Qt/usr/qt5.12/lib/fonts/
}

clean()
{
	cd $SOFTWARE_DIR
	rm .compiled
	cd $SOFTWARE_VER
	make clean
}

distclean()
{
	cd $SOFTWARE_DIR
	rm -rf $SOFTWARE_VER
	rm -rf $INSTALL_DIR
	rm -rf .decompressed .compiled
	rm -rf rootfs-Qt
}

if [ X$1 == Xclean ]; then
	clean
elif [ X$1 == Xdistclean ]; then
	distclean
elif [ X$1 == Xcompile ]; then
	software_compile
else
	rootfs_install
fi
