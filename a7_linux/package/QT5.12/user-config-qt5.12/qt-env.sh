export QTDIR=/usr/qt5.12
export QT_QPA_FONTDIR=$QTDIR/lib/fonts
export LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH
export QT_QPA_PLATFORM_PLUGIN_PATH=$QTDIR/plugins
export QT_QPA_PLATFORM=linuxfb:fb=/dev/fb0
export QT_QPA_GENERIC_PLUGINS=evdevmouse:/dev/input/event0,evdevkeyboard:/dev/input/event1
