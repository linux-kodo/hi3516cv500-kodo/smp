#!/bin/bash

DHCPCD_VER="dhcpcd-6.11.5"
ZLIB_VER="zlib-1.2.11"
OPENSSL_VER="openssl-1.0.2n"
OPENSSH_VER="openssh-7.6p1"

compile()
{
    #build dhcpcd
    cd dhcpcd/
    ./make.sh
    cd ../

    #build ssh
    cd ssh/
    ./make.sh
    cd ../

    #build QT
    # cd QT5.12/
    # ./make.sh
    # cd ../

    #build mpp
    cd ../mpp/sample/
    make clean
    make
    cd ../../package
}

clean()
{
    cd dhcpcd
    rm .compiled
    cd $DHCPCD_VER
    make clean
    cd ../../

    cd ssh
    cd zlib/$ZLIB_VER
    make clean
    cd openssl/$OPENSSL_VER
    make clean
    cd openssh/$OPENSSH_VER
    make clean
    cd ../../

    # cd QT5.12/
    # ./make.sh clean
    # cd ../

    #build mpp
    cd ../mpp/sample/
    make clean
    cd ../../package
}

distclean()
{
    cd dhcpcd/
    rm -rf $DHCPCD_VER
    rm -rf install/
    rm -rf .decompressed .compiled
    cd ../

    cd ssh/zlib/
    rm -rf $ZLIB_VER
    rm -rf zlib-install/
    cd ../openssl/
    rm -rf $OPENSSL_VER
    rm -rf openssl-install/
    cd ../openssh
    rm -rf $OPENSSH_VER
    rm -rf openssh-install
    cd ../../

    # cd QT5.12/
    # ./make.sh distclean
    # cd ../
}

if [ X$1 == Xclean ]; then
    clean
elif [ X$1 == Xdistclean ]; then
    distclean
else
    compile
fi
