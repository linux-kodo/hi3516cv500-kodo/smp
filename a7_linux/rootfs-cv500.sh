#!/bin/bash

A7_LINUX=`pwd`
FS_CUSTOM=$A7_LINUX/rootfs/rootfs

#clean and rebuild
rm -rf $FS_CUSTOM/*

mkdir -p $FS_CUSTOM/root/
mkdir -p $FS_CUSTOM/usr/
mkdir -p $FS_CUSTOM/usr/local/bin
mkdir -p $FS_CUSTOM/usr/komod
#mkdir -p $FS_CUSTOM/etc/wireless

cp -rf $A7_LINUX/package/ssh/openssh/openssh-install/* $FS_CUSTOM/
cp -rf $A7_LINUX/package/rootfs-config/* $FS_CUSTOM/
cp -rf $A7_LINUX/package/dhcpcd/install/* $FS_CUSTOM/
cp -rf $A7_LINUX/package/QT5.12/rootfs-Qt/* $FS_CUSTOM/

# copy wifi
cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/net/wireless/cfg80211.ko $FS_CUSTOM/usr/komod/
cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/net/wireless/realtek/rtl8189FS/8189fs.ko $FS_CUSTOM/usr/komod/
cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/input/touchscreen/gt9xx/goodix_gt9xx.ko $FS_CUSTOM/usr/komod/
cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/input/touchscreen/ft5x06_ts.ko $FS_CUSTOM/usr/komod/
cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/input/touchscreen/ft5x06.ko $FS_CUSTOM/usr/komod/

# usb-uvc
#cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/usb/gadget/function/u_audio.ko $FS_CUSTOM/root/
#cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/usb/gadget/function/usb_f_uac1.ko $FS_CUSTOM/root/
#cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/usb/gadget/function/usb_f_uac2.ko $FS_CUSTOM/root/
#cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/usb/gadget/function/usb_f_uvc.ko $FS_CUSTOM/root/
#cp ../../osdrv/opensource/kernel/linux-4.9.y-smp/drivers/usb/gadget/libcomposite.ko $FS_CUSTOM/root/

# copy mpp/ko
cp -rf $A7_LINUX/mpp/ko $FS_CUSTOM/usr/

# copy lib
cp -rf $A7_LINUX/mpp/lib $FS_CUSTOM/usr/

# copy dump tools
mkdir -p $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/ai_dump              $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/fisheye_calibrate    $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/mipitx_read          $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/mipitx_write         $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/sample_af_bk         $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/vi_bayerdump         $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/vi_chn_dump          $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/vi_pipe_yuvdump      $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/vo_chn_dump          $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/vo_screen_dump       $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/vpss_chn_dump        $FS_CUSTOM/usr/tools
cp $A7_LINUX/mpp/tools/vpss_src_dump        $FS_CUSTOM/usr/tools

# copy sample
mkdir -p $FS_CUSTOM/usr/sample/audio
mkdir -p $FS_CUSTOM/usr/sample/awb_online_calibration
mkdir -p $FS_CUSTOM/usr/sample/calcflicker
mkdir -p $FS_CUSTOM/usr/sample/dis
mkdir -p $FS_CUSTOM/usr/sample/fisheye
mkdir -p $FS_CUSTOM/usr/sample/hifb
mkdir -p $FS_CUSTOM/usr/sample/hifb_en
mkdir -p $FS_CUSTOM/usr/sample/lsc_online_cali
mkdir -p $FS_CUSTOM/usr/sample/region
mkdir -p $FS_CUSTOM/usr/sample/scene_auto
mkdir -p $FS_CUSTOM/usr/sample/snap
mkdir -p $FS_CUSTOM/usr/sample/tde
mkdir -p $FS_CUSTOM/usr/sample/traffic_capture
mkdir -p $FS_CUSTOM/usr/sample/uvc_app
mkdir -p $FS_CUSTOM/usr/sample/vdec
mkdir -p $FS_CUSTOM/usr/sample/venc
mkdir -p $FS_CUSTOM/usr/sample/vgs
mkdir -p $FS_CUSTOM/usr/sample/vio
mkdir -p $FS_CUSTOM/usr/sample/vo
#mkdir -p $FS_CUSTOM/usr/sample/rtsp

cp $A7_LINUX/mpp/sample/audio/sample_audio                              $FS_CUSTOM/usr/sample/audio/
#cp $A7_LINUX/mpp/sample/audio/telephone.aac                            $FS_CUSTOM/usr/sample/audio/
#cp $A7_LINUX/mpp/sample/audio/telephone.aac                            $FS_CUSTOM/usr/sample/audio/audio_chn0.aac
cp $A7_LINUX/mpp/sample/awb_online_calibration/sample_awb_calibration   $FS_CUSTOM/usr/sample/awb_online_calibration
cp $A7_LINUX/mpp/sample/calcflicker/sample_calcflicker                  $FS_CUSTOM/usr/sample/calcflicker
cp $A7_LINUX/mpp/sample/dis/sample_dis                                  $FS_CUSTOM/usr/sample/dis/
cp $A7_LINUX/mpp/sample/fisheye/sample_fisheye                          $FS_CUSTOM/usr/sample/fisheye/
cp $A7_LINUX/mpp/sample/hifb/sample_hifb                                $FS_CUSTOM/usr/sample/hifb/
cp $A7_LINUX/mpp/sample/hifb_en/hifb_en                                 $FS_CUSTOM/usr/sample/hifb_en
cp $A7_LINUX/mpp/sample/lsc_online_cali/lsc_online_cali                 $FS_CUSTOM/usr/sample/lsc_online_cali
cp $A7_LINUX/mpp/sample/region/sample_region                            $FS_CUSTOM/usr/sample/region
cp -R $A7_LINUX/mpp/sample/region/res                                   $FS_CUSTOM/usr/sample/region
cp $A7_LINUX/mpp/sample/scene_auto/sample_scene                         $FS_CUSTOM/usr/sample/scene_auto
cp -R $A7_LINUX/mpp/sample/scene_auto/param                             $FS_CUSTOM/usr/sample/scene_auto
cp $A7_LINUX/mpp/sample/snap/sample_snap                                $FS_CUSTOM/usr/sample/snap
cp $A7_LINUX/mpp/sample/tde/sample_tde                                  $FS_CUSTOM/usr/sample/tde/
cp -R $A7_LINUX/mpp/sample/tde/res                                      $FS_CUSTOM/usr/sample/tde/
cp $A7_LINUX/mpp/sample/traffic_capture/sample_traffic_capture          $FS_CUSTOM/usr/sample/traffic_capture/
cp $A7_LINUX/mpp/sample/uvc_app/uvc_app                                 $FS_CUSTOM/usr/sample/uvc_app/
cp $A7_LINUX/mpp/sample/vdec/sample_vdec                                $FS_CUSTOM/usr/sample/vdec/
cp $A7_LINUX/mpp/sample/venc/sample_venc                                $FS_CUSTOM/usr/sample/venc/
cp $A7_LINUX/mpp/sample/vgs/sample_vgs                                  $FS_CUSTOM/usr/sample/vgs/
cp -R $A7_LINUX/mpp/sample/vgs/data                                     $FS_CUSTOM/usr/sample/vgs/
cp $A7_LINUX/mpp/sample/vio/smp/sample_vio                              $FS_CUSTOM/usr/sample/vio/
cp $A7_LINUX/mpp/sample/vio/res/UsePic_1920_1080_420.yuv                $FS_CUSTOM/usr/sample/vio/
cp $A7_LINUX/mpp/sample/vo/smp/sample_vo                                $FS_CUSTOM/usr/sample/vo
cp -R $A7_LINUX/mpp/sample/vo/res                                       $FS_CUSTOM/usr/sample/vo
#cp $A7_LINUX/mpp/sample/rtsp/bin/*                                     $FS_CUSTOM/usr/sample/rtsp/

# copy script
mkdir -p $FS_CUSTOM/usr/script
cp -rf $A7_LINUX/script/*	$FS_CUSTOM/usr/script/

# mkdir
mkdir -p $FS_CUSTOM/userdata
mkdir -p $FS_CUSTOM/user
