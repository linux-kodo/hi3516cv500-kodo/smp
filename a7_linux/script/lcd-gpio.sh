# lcd enable
LCD_EN_IO_NUM=6
LCD_RST_IO_NUM=5

if [ X$1 == X1 ]; then
	# export io
	if [ ! -e /sys/class/gpio/gpio${LCD_EN_IO_NUM} ];then
		echo ${LCD_EN_IO_NUM}> /sys/class/gpio/export
	fi
	
	if [ ! -e /sys/class/gpio/gpio${LCD_RST_IO_NUM} ];then
		echo ${LCD_RST_IO_NUM} > /sys/class/gpio/export
	fi

	# 1 set io output
	echo out > /sys/class/gpio/gpio${LCD_EN_IO_NUM}/direction
	echo out > /sys/class/gpio/gpio${LCD_RST_IO_NUM}/direction

	# 2 enable
	echo 0      > /sys/class/gpio/gpio${LCD_EN_IO_NUM}/value 
	usleep 100000
	echo 1      > /sys/class/gpio/gpio${LCD_EN_IO_NUM}/value 
	usleep 120000
	
	# 3 reset
	echo 1 >   /sys/class/gpio/gpio${LCD_RST_IO_NUM}/value
	usleep 100000
	echo 0      > /sys/class/gpio/gpio${LCD_RST_IO_NUM}/value
	usleep 120000
	echo 1      > /sys/class/gpio/gpio${LCD_RST_IO_NUM}/value
elif [ X$1 == X0 ];then
	echo 0      > /sys/class/gpio/gpio${LCD_RST_IO_NUM}/value
	echo 0      > /sys/class/gpio/gpio${LCD_EN_IO_NUM}/value 
fi

echo "end of lcd-gpio.sh"
