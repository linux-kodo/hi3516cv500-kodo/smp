# set lcd pwm high and ret high
#echo "55" > /sys/class/gpio/export
#echo out > /sys/class/gpio/gpio55/direction
#echo 1 > /sys/class/gpio/gpio55/value

# pwm gpio num : 55
if [ X$1 == X1 ]; then
	if [ ! -e /sys/class/gpio/gpio55 ];then
		echo 55 > /sys/class/gpio/export
	fi
	
	echo out > /sys/class/gpio/gpio55/direction
	echo 1 > /sys/class/gpio/gpio55/value
elif [ X$1 == X0 ]; then
	echo 0 > /sys/class/gpio/gpio55/value
fi

echo "end of lcd-pwm.sh"
