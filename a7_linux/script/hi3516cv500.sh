# mount SD Card
#mount /dev/mmcblk0p1 /userdata

DAEMON=/sbin/dhcpcd
CONFIG=/etc/dhcpcd.conf
PIDFILE=/var/run/dhcpcd.pid

# touchscreen
TOUCHSCREEN=goodix_gt9xx.ko # insmod goodix_gt9xx.ko
TP_DEBUG=0					# 0:disable(default) 1:enable, printf touchscreen touch point coordinate
LCD_TYPE=0
# network config
ETH0_CFG=dhcp				# static or dhcp
BOARD_IP=192.168.31.200
NETMASK=255.255.255.0
BROADCAST=192.168.31.255 
# Telnetd
RUN_TELNETD=yes				# run telnetd after system run default:yes
# openssh
SSHD_EN=yes					# enable sshd to enable ssh login, default:yes for enalble no:disable

# eth0 ethernet interface config
echo "==========Start of ethernet config=========="
echo "ifconfig eth0 up..."
ifconfig eth0 up
usleep 100000
usleep 100000
echo "============================================"
if [ X$ETH0_CFG == Xstatic ]; then
#	if [ -e /etc/init.d/S40network ]; then
#		mv /etc/init.d/S40network /etc/init.d/backup/
#	fi
#	ifconfig eth0 192.168.31.200 netmask 255.255.255.0 broadcast 192.168.31.255
	ifconfig eth0 $BOARD_IP netmask $NETMASK broadcast $BROADCAST
elif [ X$ETH0_CFG == Xdhcp ]; then
#	if [ -e /etc/init.d/backup/S40network ]; then
#		mv /etc/init.d/backup/S40network /etc/init.d/
#	fi
	chmod +x /lib/dhcpcd/dhcpcd-run-hooks
#   /sbin/dhcpcd eth0
	echo "start dhcpcd..."
    start-stop-daemon -S -x "$DAEMON" -p "$PIDFILE" -- -f "$CONFIG"
else
	ifconfig eth0 192.168.31.200 netmask 255.255.255.0 broadcast 192.168.31.255
fi
echo "===========End of ethernet config==========="

# Start wifi
echo "================Start of wifi==============="
cd /lib/
ln -s libnl-genl.so.2.0.0 libnl-genl.so.2
ln -s libnl.so.2.0.0 libnl.so.2
echo "============================================" 
echo "insmod wifi ko..."
insmod /usr/komod/cfg80211.ko
insmod /usr/komod/8189fs.ko 
echo "============================================"
echo "start wpa_supplicant..."            
cd /sbin
wpa_supplicant -i wlan0 -Dnl80211 -c/etc/wireless/wpa_supplicant.conf&
usleep 100000
echo "============================================"
echo "config wifi IP..."
ifconfig wlan0 192.168.31.201 netmask 255.255.255.0
route add default gw 192.168.31.1 dev wlan0
echo "=================End of wifi================"

# telnetd start
echo "==============Start of telnetd=============="
if [ X$RUN_TELNETD == Xyes ]; then
	telnetd &
	echo "ETH0-IP:192.168.31.200"
	echo "WIFI-IP:192.168.31.201"
	echo "USER:root"
fi
echo "===============End of telnetd==============="

# openssh config
echo "==========Start of openssh config==========="
echo "config openssh..."
if [ X$SSHD_EN != Xyes ]; then
	if [ -e /etc/init.d/S50sshd ]; then
		mv /etc/init.d/S50sshd /etc/init.d/backup/
	fi
else	
	if [ -e /etc/init.d/backup/S50sshd ]; then
		mv /etc/init.d/backup/S50sshd /etc/init.d/
	fi
fi
echo "===========End of openssh config============"

# mount user partition
echo "========Start of mount user partition======="
echo "mount user partition..."
#if [ -e /dev/mmcblk1p0 ]; then
#	mount /dev/mmcblk1p0 /mnt
#	if [ $? != 0 ]; then
#		mkfs.ext4 /dev/mmcblk0p5
#		mount /dev/mmcblk0p5 /user
#	fi
#fi
echo "=========End of mount user partition========"

# UVC
echo "===============Start of USB-UVC=============="
#cd /root/
#insmod libcomposite.ko
#insmod usb_f_uvc.ko
#export VID="0x12d1"
#export PID="0x4321"
#export MANUFACTURER="Huawei"
#export PRODUCT="HiCamera"
#export SERIALNUMBER="12345678"
#export CamControl1=0xa
#export CamControl2=0x0
#export CamControl3=0x0
#export ProcControl1=0x4f
#export ProcControl2=0x14
#export YUV="360p"
#export MJPEG="360p 720p 1080p"
#export H264="360p 720p 1080p "
#cp /usr/script/ConfigUVC.sh /root -rf
#./ConfigUVC.sh
echo "===============Start of USB-UVC=============="

# load3516cv500
echo "===============Start of SENSOR=============="
usleep 100000
cd /usr/ko/
./load3516cv500 -i -sensor0 imx307 -osmem 256
echo "================End of SENSOR==============="

# Turn off lcd-bl
echo "=============Start of OFF-LCD-BL============"
#配置引脚功能
himm 0x111f0024 0x0400
#使用echo命令将要操作的GPIO编号export:例GPIO6-6的编号为6*8+6=54
echo 54 > /sys/class/gpio/export 
#export之后就会生成/sys/class/gpio/gpioN目录,使用echo命令设置GPIO方向：
echo out > /sys/class/gpio/gpio54/direction
#echo in > /sys/class/gpio/gpioN/direction
#使用cat或echo命令查看GPIO输入值或设置GPIO输出值：
#cat /sys/class/gpio/gpioN/value
echo 0 > /sys/class/gpio/gpio54/value
echo "==============End of OFF-LCD-BL============="

# insmod touchscreen driver
echo "========Start of touchscreen driver========="
#if [[ -n $TOUCHSCREEN ]]; then
#        insmod /usr/komod/$TOUCHSCREEN lcd=$LCD_TYPE debug=$TP_DEBUG
#fi
insmod /usr/komod/ft5x06.ko
echo "=========End of touchscreen driver=========="

# audio enable
#sh /usr/script/audio-gpio.sh
# camera enable and reset
#sh /usr/script/camera-gpio.sh 1
# lcd enable and reset
#sh /usr/script/lcd-gpio.sh 1
# lcd backlight enable
#sh /usr/script/lcd-pwm.sh 1

# disable camera lcd backlight 
#sh /usr/script/camera-gpio.sh 0
#sh /usr/script/lcd-gpio.sh 0
#sh /usr/script/lcd-pwm.sh 0

ifconfig wlan0 up
ifconfig eth0 down
usleep 100000
ifconfig -a
#mount -t nfs -o nolock -o tcp -o rsize=32768,wsize=32768 192.168.31.29:/home/kodo/NFS /mnt

# sample
#cd /usr/sample/vio
#./sample_vio 0 &

echo "end of hi3516cv500.sh"
