/*
* module_common.h - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/
#ifndef _MODULE_COMMON_
#define _MODULE_COMMON_

#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>

#include "IPC_msg.h"
#include "IPC_cardvInfo.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

//==============================================================================
//                              MACRO DEFINES
//==============================================================================
#ifndef S8
#define S8 signed char
#endif

#ifndef U8
#define U8 unsigned char
#endif

#ifndef S16
#define S16 signed short
#endif

#ifndef U16
#define U16 unsigned short
#endif

#ifndef S32
#define S32 signed int
#endif

#ifndef U32
#define U32 unsigned int
#endif

#ifndef S64
#define S64 signed long long
#endif

#ifndef U64
#define U64 unsigned long long
#endif

#ifndef MI_RET
#define MI_RET      signed int
#endif

#ifndef BOOL
#define BOOL unsigned char
#endif
// typedef int BOOL;

#ifndef VOID
#define VOID void
#endif
// typedef void VOID;

typedef signed char    CHAR;
typedef unsigned char  BYTE;
typedef signed short   SHORT;
typedef unsigned short WORD;
typedef signed long    LONG;
typedef unsigned int   DWORD;
typedef signed int     INT;
typedef unsigned int   UINT;
// typedef int BOOL;
typedef signed char    INT8;
typedef unsigned short USHORT;
typedef unsigned long  ULONG;
typedef unsigned short WCHAR;
typedef signed int     INTEGER;

#ifndef F32
#define F32 float
#endif

#ifndef HANDLE
#define HANDLE void *
#endif

#ifndef MALLOC
#define MALLOC(s) malloc(s)
#endif

#ifndef FREEIF
#define FREEIF(m) \
    if (m != 0)   \
    {             \
        free(m);  \
        m = NULL; \
    }
#endif

#ifndef CARDV_ALIGN_UP
#define CARDV_ALIGN_UP(value, align) ((value + align - 1) / align * align)
#endif

#ifndef CARDV_ALIGN_DOWN
#define CARDV_ALIGN_DOWN(value, align) (value / align * align)
#endif

#ifndef ExecFuncNoExit
#define ExecFuncNoExit(_func_, _ret_)                                                        \
    do                                                                                       \
    {                                                                                        \
        MI_S32 s32Ret = MI_SUCCESS;                                                          \
        s32Ret        = _func_;                                                              \
        if (s32Ret != _ret_)                                                                 \
        {                                                                                    \
            printf("[%s %d]exec func fail, NOT exit, err:%x\n", __func__, __LINE__, s32Ret); \
        }                                                                                    \
        else                                                                                 \
        {                                                                                    \
            /* printf("[%s %d]exec func pass\n", __func__, __LINE__); */                     \
        }                                                                                    \
    } while (0)
#endif

#ifndef ExecFunc
#define ExecFunc(_func_, _ret_)                                                    \
    do                                                                             \
    {                                                                              \
        MI_S32 s32Ret = MI_SUCCESS;                                                \
        s32Ret        = _func_;                                                    \
        if (s32Ret != _ret_)                                                       \
        {                                                                          \
            printf("[%s %d]exec func fail, err:%x\n", __func__, __LINE__, s32Ret); \
            return s32Ret;                                                         \
        }                                                                          \
        else                                                                       \
        {                                                                          \
            /* printf("[%s %d]exec func pass\n", __func__, __LINE__); */           \
        }                                                                          \
    } while (0)
#endif

#ifndef CARDVCHECKRESULT
#define CARDVCHECKRESULT(_func_)                                                   \
    do                                                                             \
    {                                                                              \
        MI_S32 s32Ret = MI_SUCCESS;                                                \
        s32Ret        = _func_;                                                    \
        if (s32Ret != MI_SUCCESS)                                                  \
        {                                                                          \
            printf("(%s %d)exec func fail, err:%x\n", __func__, __LINE__, s32Ret); \
            return s32Ret;                                                         \
        }                                                                          \
        else                                                                       \
        {                                                                          \
            /* printf("(%s %d)exec func pass\n", __FUNCTION__,__LINE__); */        \
        }                                                                          \
    } while (0)
#endif

#ifndef CarDV_API_ISVALID_POINT
#define CarDV_API_ISVALID_POINT(X)                        \
    {                                                     \
        if (X == NULL)                                    \
        {                                                 \
            printf("cardv input point param is null!\n"); \
            return MI_SUCCESS;                            \
        }                                                 \
    }
#endif

#define CARDVDBG_ENTER()                                            \
    printf("\n");                                                   \
    printf("[IN] [%s:%s:%d] \n", __FILE__, __FUNCTION__, __LINE__); \
    printf("\n");

#define CARDVDBG_LEAVE()                                             \
    printf("\n");                                                    \
    printf("[OUT] [%s:%s:%d] \n", __FILE__, __FUNCTION__, __LINE__); \
    printf("\n");

#define CARDV_RUN()                                                     \
    printf("\n");                                                       \
    printf("[RUN] ok [%s:%s:%d] \n", __FILE__, __FUNCTION__, __LINE__); \
    printf("\n");

#define CARDV_THREAD() /*                                                               \
    printf("\n");                                                                       \
    printf("[THREAD] [%s:%d:pid:%ld] \n", __FUNCTION__, __LINE__, syscall(SYS_gettid)); \
    printf("\n"); \ */

static int g_dbglevel = 4;

#define CARDV_DBGLV_NONE    0 // disable all the debug message
#define CARDV_DBGLV_INFO    1 // information
#define CARDV_DBGLV_NOTICE  2 // normal but significant condition
#define CARDV_DBGLV_DEBUG   3 // debug-level messages
#define CARDV_DBGLV_WARNING 4 // warning conditions
#define CARDV_DBGLV_ERR     5 // error conditions
#define CARDV_DBGLV_CRIT    6 // critical conditions
#define CARDV_DBGLV_ALERT   7 // action must be taken immediately
#define CARDV_DBGLV_EMERG   8 // system is unusable

#ifndef COLOR_NONE
#define COLOR_NONE   "\033[0m"
#endif
#ifndef COLOR_BLACK
#define COLOR_BLACK  "\033[0;30m"
#endif
#ifndef COLOR_BLUE
#define COLOR_BLUE   "\033[0;34m"
#endif
#ifndef COLOR_GREEN
#define COLOR_GREEN  "\033[0;32m"
#endif
#ifndef COLOR_CYAN
#define COLOR_CYAN   "\033[0;36m"
#endif
#ifndef COLOR_RED
#define COLOR_RED    "\033[0;31m"
#endif
#ifndef COLOR_YELLOW
#define COLOR_YELLOW "\033[1;33m"
#endif
#ifndef COLOR_WHITE
#define COLOR_WHITE  "\033[1;37m"
#endif

#define CARDV_NOP(fmt, args...)
#define CARDV_DBG(fmt, args...)                                                      \
    if (g_dbglevel <= CARDV_DBGLV_DEBUG)                                             \
        do                                                                           \
        {                                                                            \
            printf(COLOR_GREEN "[DBG]:%s[%d]: " COLOR_NONE, __FUNCTION__, __LINE__); \
            printf(fmt, ##args);                                                     \
    } while (0)

#define CARDV_WARN(fmt, args...)                                                       \
    if (g_dbglevel <= CARDV_DBGLV_WARNING)                                             \
        do                                                                             \
        {                                                                              \
            printf(COLOR_YELLOW "[WARN]:%s[%d]: " COLOR_NONE, __FUNCTION__, __LINE__); \
            printf(fmt, ##args);                                                       \
    } while (0)

#define CARDV_INFO(fmt, args...)                                 \
    if (g_dbglevel <= CARDV_DBGLV_INFO)                          \
        do                                                       \
        {                                                        \
            printf("[INFO]:%s[%d]: \n", __FUNCTION__, __LINE__); \
            printf(fmt, ##args);                                 \
    } while (0)

#define CARDV_ERR(fmt, args...)                                                    \
    if (g_dbglevel <= CARDV_DBGLV_ERR)                                             \
        do                                                                         \
        {                                                                          \
            printf(COLOR_RED "[ERR]:%s[%d]: " COLOR_NONE, __FUNCTION__, __LINE__); \
            printf(fmt, ##args);                                                   \
    } while (0)

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#endif


#define CONFIG_PATH "/customer/" // "/config"

#define FIFO_NAME "/tmp/cardv_fifo"
#define FIFO_NAME_HIGH_PRIO "/tmp/cardv_internal"

#define REC_STATUS "/tmp/rec_status"
#define MMC_STATUS "/tmp/mmc_status"

#define CARDV_MAX_MSG_BUFFER_SIZE 128
//==============================================================================
//
//                              STRUCT DEFINES
//
//==============================================================================
typedef enum _CarDVCmdIdtype
{
    CMD_VIF         = 0x0100,
    CMD_VPSS        = 0x0200,
    CMD_VENC        = 0x0300,
    CMD_VO          = 0x0400,
    CMD_DISP        = 0x0500,
    CMD_VDEC        = 0x0600,
    CMD_SYSTEM      = 0x0900,
    CARDV_CMD_TYPE  = 0xFF00,
} CarDVCmdType;

typedef enum _CarDVCmdId
{
    CMD_VIF_INIT            = 0x01 | CMD_VIF,
    CMD_VIF_DEINIT          = 0x02 | CMD_VIF,
    CMD_VIF_START           = 0x03 | CMD_VIF,
    CMD_VIF_STOP            = 0x04 | CMD_VIF,
    CMD_VIF_SET_RESOLUTION  = 0x05 | CMD_VIF,

    CMD_VPSS_INIT           = 0x01 | CMD_VPSS,
    CMD_VPSS_DEINIT         = 0x02 | CMD_VPSS,
    CMD_VPSS_START          = 0x03 | CMD_VPSS,
    CMD_VPSS_STOP           = 0x04 | CMD_VPSS,
    CMD_VPSS_BIND           = 0x05 | CMD_VPSS,

    CMD_VENC_INIT           = 0x01 | CMD_VENC,
    CMD_VENC_DEINIT         = 0x02 | CMD_VENC,
    CMD_VENC_START          = 0x03 | CMD_VENC,
    CMD_VENC_STOP           = 0x04 | CMD_VENC,
    CMD_VENC_BIND           = 0x05 | CMD_VENC,
    CMD_VENC_SNAP           = 0x06 | CMD_VENC,

    CMD_VDEC_INIT           = 0x01 | CMD_VDEC,
    CMD_VDEC_DEINIT         = 0x02 | CMD_VDEC,
    CMD_VDEC_START          = 0x03 | CMD_VDEC,
    CMD_VDEC_STOP           = 0x04 | CMD_VDEC,
    CMD_VDEC_BIND           = 0x05 | CMD_VDEC,

    CMD_VO_INIT             = 0x01 | CMD_VO,
    CMD_VO_DEINIT           = 0x02 | CMD_VO,
    CMD_VO_START            = 0x03 | CMD_VO,
    CMD_VO_STOP             = 0x04 | CMD_VO,
    CMD_VO_BIND             = 0x05 | CMD_VO,
    CMD_VO_UNBIND           = 0x06 | CMD_VO,

    CMD_HIFB_START          = 0x01 | CMD_DISP,
    CMD_HIFB_STOP           = 0x02 | CMD_DISP,

    CMD_SYSTEM_WATCHDOG_OPEN         = 0x01 | CMD_SYSTEM,
    CMD_SYSTEM_WATCHDOG_CLOSE        = 0x02 | CMD_SYSTEM,
    CMD_SYSTEM_RTSP_OPEN             = 0x09 | CMD_SYSTEM,
    CMD_SYSTEM_RTSP_CLOSE            = 0x0a | CMD_SYSTEM,
    CMD_SYSTEM_RESET_TO_UBOOT        = 0x0c | CMD_SYSTEM,
    CMD_SYSTEM_EXIT_QUEUE            = 0x10 | CMD_SYSTEM,
    CMD_SYSTEM_WATCHDOG_KEEPALIVE    = 0x1f | CMD_SYSTEM,
    CMD_SYSTEM_INIT                  = 0x11 | CMD_SYSTEM,
    CMD_SYSTEM_UNINIT                = 0x12 | CMD_SYSTEM,
    CMD_SYSTEM_CORE_BACKTRACE        = 0x13 | CMD_SYSTEM,

} CarDVCmdId;

//==============================================================================
//
//                              FUNCTIONS
//
//==============================================================================
int cardv_send_cmd(CarDVCmdId cmdId, MI_S8 *param, MI_S32 paramLen);
int cardv_send_cmd_HP(CarDVCmdId cmdId, MI_S8 *param, MI_S32 paramLen);
int cardv_send_cmd_and_wait(CarDVCmdId cmdId, MI_S8 *param, MI_S32 paramLen);
int cardv_message_queue_init();
int cardv_message_queue_uninit();

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif //_MODULE_COMMON_
