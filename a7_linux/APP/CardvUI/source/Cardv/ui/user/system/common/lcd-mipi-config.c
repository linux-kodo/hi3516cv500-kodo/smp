/*
* lcd-mipi-config.c
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include "lcd-mipi-config.h"

#if defined (MIPI_3IN)
combo_dev_cfg_t *g_mipi_tx_config = &MIPI_TX_3INCH_480X854_60_CONFIG;
VO_SYNC_INFO_S *g_mipi_tx_sync_info = &MIPI_TX_3INCH_480X854_60_SYNC_INFO;
VO_USER_INTFSYNC_INFO_S * g_mipi_tx_user_intfsync_info = &MIPI_TX_3INCH_480X854_60_USER_INTFSYNC_INFO;
lcd_resoluton_t * g_mipi_tx_lcd_resolution = &MIPI_TX_3INCH_480X854_60_LCD_RESOLUTION;
HI_VOID (*g_mipi_lcd_init)(HI_S32 s32fd) = InitScreen_mipi_3INCH_480X854;
#elif defined (MIPI_7IN)
/*============== 7in lcd config start added by KODO ================*/
combo_dev_cfg_t *g_mipi_tx_config = &MIPI_TX_7INCH_1024X600_60_CONFIG;
VO_SYNC_INFO_S *g_mipi_tx_sync_info = &MIPI_TX_7INCH_1024X600_60_SYNC_INFO;
VO_USER_INTFSYNC_INFO_S * g_mipi_tx_user_intfsync_info = &MIPI_TX_7INCH_1024X600_60_USER_INTFSYNC_INFO;
lcd_resoluton_t * g_mipi_tx_lcd_resolution = &MIPI_TX_7INCH_1024X600_60_LCD_RESOLUTION;
HI_VOID (*g_mipi_lcd_init)(HI_S32 s32fd) = InitScreen_mipi_7inch_1024x600;
#elif defined (MIPI_8IN)
/*============== 8in lcd config start added by KODO ================*/
combo_dev_cfg_t *g_mipi_tx_config = &MIPI_TX_8INCH_800X1280_60_CONFIG;
VO_SYNC_INFO_S *g_mipi_tx_sync_info = &MIPI_TX_8INCH_800X1280_60_SYNC_INFO;
VO_USER_INTFSYNC_INFO_S * g_mipi_tx_user_intfsync_info = &MIPI_TX_8INCH_800X1280_60_USER_INTFSYNC_INFO;
lcd_resoluton_t * g_mipi_tx_lcd_resolution = &MIPI_TX_8INCH_800X1280_60_LCD_RESOLUTION;
HI_VOID (*g_mipi_lcd_init)(HI_S32 s32fd) = InitScreen_mipi_8inch_800x1280;
#elif defined (MIPI_10IN)
/*============== 10in lcd config start added by KODO ================*/
combo_dev_cfg_t *g_mipi_tx_config = &MIPI_TX_10INCH_NEW_1024x600_60_CONFIG;
VO_SYNC_INFO_S *g_mipi_tx_sync_info = &MIPI_TX_10INCH_NEW_1024x600_60_SYNC_INFO;
VO_USER_INTFSYNC_INFO_S * g_mipi_tx_user_intfsync_info = &MIPI_TX_10INCH_NEW_1024x600_60_USER_INTFSYNC_INFO;
lcd_resoluton_t * g_mipi_tx_lcd_resolution = &MIPI_TX_10INCH_NEW_1024x600_60_LCD_RESOLUTION;
HI_VOID (*g_mipi_lcd_init)(HI_S32 s32fd) = InitScreen_mipi_10inch_new_1024x600;
#endif
