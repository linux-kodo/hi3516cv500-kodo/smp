/*
* module_video.c - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/prctl.h>

#include "module_mpp.h"

#include "hifb.h"
#include "loadbmp.h"
#include "hi_tde_api.h"

#include "lcd-mipi-config.h"

//==============================================================================
//                              GLOBAL VARIABLES
//==============================================================================
#ifndef __HuaweiLite__
static struct fb_bitfield s_r16 = {10, 5, 0};
static struct fb_bitfield s_g16 = {5, 5, 0};
static struct fb_bitfield s_b16 = {0, 5, 0};
static struct fb_bitfield s_a16 = {15, 1, 0};

static struct fb_bitfield s_a32 = {24, 8, 0};
static struct fb_bitfield s_r32 = {16, 8, 0};
static struct fb_bitfield s_g32 = {8,  8, 0};
static struct fb_bitfield s_b32 = {0,  8, 0};
#endif

#define SAMPLE_IMAGE_WIDTH     300
#define SAMPLE_IMAGE_HEIGHT    150
#define SAMPLE_IMAGE_NUM       20
#define HIFB_RED_1555          0xFC00
#define HIFB_RED_8888          0xFF00ff00

#define GRAPHICS_LAYER_G0      0
#define GRAPHICS_LAYER_G1      1
#define GRAPHICS_LAYER_G3      2

#define SAMPLE_IMAGE1_PATH        "./res/%d.bmp"
#define SAMPLE_IMAGE2_PATH        "./res/1280_720.bits"
#define SAMPLE_CURSOR_PATH        "./res/cursor.bmp"

typedef struct hiPTHREAD_HIFB_SAMPLE
{
    HI_S32           fd;          /* fb's file describ */
    HI_S32           layer;       /* which graphic layer */
    HI_S32           ctrlkey;     /* {0,1,2,3}={1buffer, 2buffer, 0buffer pan display, 0buffer refresh} */
    HI_BOOL          bCompress;   /* image compressed or not */
    HIFB_COLOR_FMT_E enColorFmt;  /* color format. */
} PTHREAD_HIFB_SAMPLE_INFO;

bool             run = true;
pthread_t        g_stHifbThread = 0;

HI_U64           g_Phyaddr      = 0;
HI_U64           g_CanvasAddr   = 0;

HI_VOID*         pBuf;
HI_VOID*         Viraddr        = NULL;

PTHREAD_HIFB_SAMPLE_INFO*  pstInfo;

VO_INTF_TYPE_E   g_enVoIntfType = VO_INTF_BT1120;
OSD_COLOR_FMT_E  g_osdColorFmt  = OSD_COLOR_FMT_RGB1555;

extern SAMPLE_VO_CONFIG_S stVoConfig;

HI_VOID* SAMPLE_HIFB_PANDISPLAY(void* pData,SAMPLE_VI_CONFIG_S *pstVoConfig,VPSS_GRP VpssGrp, HI_BOOL* pabChnEnable)
{
#ifndef __HuaweiLite__
    struct fb_fix_screeninfo fix;
    struct fb_var_screeninfo var;
#endif
    HIFB_POINT_S             stPoint            = {0, 0};
    HI_CHAR                  file[12]           = {0};

    HI_BOOL                  bShow;
    HI_U32                   u32Width;
    HI_U32                   u32Height;
    HIFB_COLOR_FMT_E         enClrFmt = HIFB_FMT_ARGB1555;
    HI_CHAR                  thdname[64];

    if (HI_NULL == pData)
    {
        return HI_NULL;
    }
    pstInfo = (PTHREAD_HIFB_SAMPLE_INFO*)pData;
    snprintf(thdname,sizeof(thdname), "HIFB%d_PANDISPLAY",pstInfo->layer);
    prctl(PR_SET_NAME, thdname, 0,0,0);

    if (VO_INTF_HDMI == g_enVoIntfType)
    {
        u32Width  = 1920;
        u32Height = 1080;
    }
    else
    {
        #if CUSTOM_HIFB
        u32Width  = 480;
        u32Height = 854;
        #else
        u32Width  = 1920;
        u32Height = 1080;
        #endif
    }

    switch (pstInfo->layer)
    {
        case GRAPHICS_LAYER_G0 :
            strncpy(file, "/dev/fb0", 12);
            break;
        case GRAPHICS_LAYER_G1 :
            strncpy(file, "/dev/fb1", 12);
            break;
        case GRAPHICS_LAYER_G3:
            strncpy(file, "/dev/fb2", 12);
            break;
        default:
            strncpy(file, "/dev/fb0", 12);
            break;
    }

    /********************************
    * Step 1. open framebuffer device overlay 0
    **********************************/
    pstInfo->fd = open(file, O_RDWR, 0);
    if (pstInfo->fd < 0)
    {
        SAMPLE_PRT("open %s failed!\n", file);
        return HI_NULL;
    }

    bShow = HI_FALSE;
    if (ioctl(pstInfo->fd, FBIOPUT_SHOW_HIFB, &bShow) < 0)
    {
        SAMPLE_PRT("FBIOPUT_SHOW_HIFB failed!\n");
        return HI_NULL;
    }
    /********************************
    * Step 2. set the screen original position
    **********************************/
    /* 2. set the screen original position */
    switch(pstInfo->ctrlkey)
    {
        case 3:
        {
            stPoint.s32XPos = 150;
            stPoint.s32YPos = 150;
        }
        break;
        default:
        {
            stPoint.s32XPos = 0;
            stPoint.s32YPos = 0;
        }
    }

    if (ioctl(pstInfo->fd, FBIOPUT_SCREEN_ORIGIN_HIFB, &stPoint) < 0)
    {
        SAMPLE_PRT("set screen original show position failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    /********************************
    * Step 3. get the variable screen information
    **********************************/
    if (ioctl(pstInfo->fd, FBIOGET_VSCREENINFO, &var) < 0)
    {
        SAMPLE_PRT("Get variable screen info failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    /* **********************************************************
    *Step 4. modify the variable screen info
    *            the screen size: IMAGE_WIDTH*IMAGE_HEIGHT
    *            the virtual screen size: VIR_SCREEN_WIDTH*VIR_SCREEN_HEIGHT
    *            (which equals to VIR_SCREEN_WIDTH*(IMAGE_HEIGHT*2))
    *            the pixel format: ARGB1555
    **************************************************************/
    SAMPLE_PRT("[Begin]\n");
    SAMPLE_PRT("wait 4 seconds\n");
    usleep(4 * 1000 * 1000);


    switch (enClrFmt = pstInfo->enColorFmt)
    {
        case HIFB_FMT_ARGB8888:
            var.transp = s_a32;
            var.red    = s_r32;
            var.green  = s_g32;
            var.blue   = s_b32;
            var.bits_per_pixel = 32;
            g_osdColorFmt    = OSD_COLOR_FMT_RGB8888;
            break;
        default:
            var.transp = s_a16;
            var.red    = s_r16;
            var.green  = s_g16;
            var.blue   = s_b16;
            var.bits_per_pixel = 16;
            enClrFmt         = HIFB_FMT_ARGB1555;
            break;
    }

    switch(pstInfo->ctrlkey)
    {
        case 3:
        {
            var.xres_virtual = 48;
            var.yres_virtual = 48;
            var.xres = 48;
            var.yres = 48;
        }
        break;
        default:
        {
            var.xres_virtual = u32Width;
            var.yres_virtual = u32Height * 2;
            var.xres         = u32Width;
            var.yres         = u32Height;
        }
    }

    var.activate       = FB_ACTIVATE_NOW;

    /*********************************
    * Step 5. set the variable screen information
    ***********************************/
    if (ioctl(pstInfo->fd, FBIOPUT_VSCREENINFO, &var) < 0)
    {
        SAMPLE_PRT("Put variable screen info failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    /**********************************
    * Step 6. get the fix screen information
    ************************************/
    if (ioctl(pstInfo->fd, FBIOGET_FSCREENINFO, &fix) < 0)
    {
        SAMPLE_PRT("Get fix screen info failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    SAMPLE_COMM_VPSS_Stop(VpssGrp, pabChnEnable);
    SAMPLE_COMM_VI_StopVi(pstVoConfig);

    /* wait until process being killed */
    while(1)
        sleep(5);

    bShow = HI_FALSE;
    if (ioctl(pstInfo->fd, FBIOPUT_SHOW_HIFB, &bShow) < 0)
    {
        SAMPLE_PRT("FBIOPUT_SHOW_HIFB failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }
    close(pstInfo->fd);
    SAMPLE_PRT("[End]\n");

    return HI_NULL;
}

HI_VOID* SAMPLE_HIFB_REFRESH(void* pData)
{
    HI_S32                     s32Ret          = HI_SUCCESS;
    HIFB_LAYER_INFO_S          stLayerInfo     = {0};
    HIFB_BUFFER_S              stCanvasBuf;
    HI_BOOL                    bShow;
    HIFB_POINT_S               stPoint         = {0,0};

    HI_CHAR                    file[12]        = {0};
    HI_U32                     maxW;
    HI_U32                     maxH;
    HIFB_COLORKEY_S            stColorKey;
    HI_U32                     u32BytePerPixel = 2;
    HIFB_COLOR_FMT_E           enClrFmt;
    #if CUSTOM_HIFB
    VO_CSC_S                   stVideoCSC;
    #endif

    prctl(PR_SET_NAME, "HIFB_REFRESH", 0,0,0);

    if (HI_NULL == pData) {
        return HI_NULL;
    }
    pstInfo = (PTHREAD_HIFB_SAMPLE_INFO*)pData;

    switch (pstInfo->layer) {
    case GRAPHICS_LAYER_G0:
        strncpy(file, "/dev/fb0", 12);
        break;
    case GRAPHICS_LAYER_G1:
        strncpy(file, "/dev/fb1", 12);
        break;
    default:
        strncpy(file, "/dev/fb0", 12);
        break;
    }

    /*************************************
    * 1. open framebuffer device overlay 0
    ****************************************/
    pstInfo->fd = open(file, O_RDWR, 0);
    if (pstInfo->fd < 0) {
        SAMPLE_PRT("open %s failed!\n", file);
        return HI_NULL;
    }

    #if CUSTOM_HIFB
    /* set csc enCscMatrix VO_CSC_MATRIX_IDENTITY to make mipi lcd display fine*/
    memset(&stVideoCSC,0,sizeof(VO_CSC_S));
    s32Ret = HI_MPI_VO_GetGraphicLayerCSC(GRAPHICS_LAYER_G0,&stVideoCSC);
    if (HI_SUCCESS != s32Ret) {
        SAMPLE_PRT("HI_MPI_VO_GetGraphicLayerCSC failed with %#x!\n", s32Ret);
    }

    stVideoCSC.enCscMatrix = VO_CSC_MATRIX_IDENTITY;
    s32Ret = HI_MPI_VO_SetGraphicLayerCSC(GRAPHICS_LAYER_G0, &stVideoCSC);
    if (HI_SUCCESS != s32Ret) {
        SAMPLE_PRT("HI_MPI_VO_SetGraphicLayerCSC failed with %#x!\n", s32Ret);
    }
    #endif

    /*all layer surport colorkey*/
    stColorKey.bKeyEnable = HI_TRUE;
    stColorKey.u32Key = 0x0;
    if (ioctl(pstInfo->fd, FBIOPUT_COLORKEY_HIFB, &stColorKey) < 0) {
        SAMPLE_PRT("FBIOPUT_COLORKEY_HIFB!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    struct fb_var_screeninfo   stVarInfo;
    s32Ret = ioctl(pstInfo->fd, FBIOGET_VSCREENINFO, &stVarInfo);
    if (s32Ret < 0) {
        SAMPLE_PRT("GET_VSCREENINFO failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    if (ioctl(pstInfo->fd, FBIOPUT_SCREEN_ORIGIN_HIFB, &stPoint) < 0) {
        SAMPLE_PRT("set screen original show position failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    #if CUSTOM_HIFB
    maxW = g_mipi_tx_lcd_resolution->pu32W;
    maxH = g_mipi_tx_lcd_resolution->pu32H;
    #endif

    switch (enClrFmt = pstInfo->enColorFmt)
    {
        case HIFB_FMT_ARGB8888:
            stVarInfo.transp = s_a32;
            stVarInfo.red    = s_r32;
            stVarInfo.green  = s_g32;
            stVarInfo.blue   = s_b32;
            stVarInfo.bits_per_pixel = 32;
            g_osdColorFmt    = OSD_COLOR_FMT_RGB8888;
            break;
        default:
            stVarInfo.transp = s_a16;
            stVarInfo.red    = s_r16;
            stVarInfo.green  = s_g16;
            stVarInfo.blue   = s_b16;
            stVarInfo.bits_per_pixel = 16;
            enClrFmt         = HIFB_FMT_ARGB1555;
            break;
    }
    u32BytePerPixel    = stVarInfo.bits_per_pixel/8;
    stVarInfo.activate = FB_ACTIVATE_NOW;
    #if CUSTOM_HIFB
    stVarInfo.xres     = maxW;
    stVarInfo.yres     = maxH;
    stVarInfo.xres_virtual = maxW;
    stVarInfo.yres_virtual = maxH;
    #else
    stVarInfo.xres     = stVarInfo.xres_virtual = maxW;
    stVarInfo.yres     = stVarInfo.yres_virtual = maxH;
    #endif

    s32Ret = ioctl(pstInfo->fd, FBIOPUT_VSCREENINFO, &stVarInfo);
    if (s32Ret < 0) {
        SAMPLE_PRT("PUT_VSCREENINFO failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    switch (pstInfo->ctrlkey)
    {
        case 0 : {
            stLayerInfo.BufMode = HIFB_LAYER_BUF_ONE;
            stLayerInfo.u32Mask = HIFB_LAYERMASK_BUFMODE;
            break;
        }

        case 1 : {
            stLayerInfo.BufMode = HIFB_LAYER_BUF_DOUBLE;
            stLayerInfo.u32Mask = HIFB_LAYERMASK_BUFMODE;
            break;
        }

        default: {
            stLayerInfo.BufMode = HIFB_LAYER_BUF_NONE;
            stLayerInfo.u32Mask = HIFB_LAYERMASK_BUFMODE;
        }
    }

    s32Ret = ioctl(pstInfo->fd, FBIOPUT_LAYER_INFO, &stLayerInfo);
    if (s32Ret < 0) {
        SAMPLE_PRT("PUT_LAYER_INFO failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    bShow = HI_TRUE;
    if (ioctl(pstInfo->fd, FBIOPUT_SHOW_HIFB, &bShow) < 0) {
        SAMPLE_PRT("FBIOPUT_SHOW_HIFB failed!\n");
        close(pstInfo->fd);
        return HI_NULL;
    }

    if (HI_FAILURE == HI_MPI_SYS_MmzAlloc(&g_CanvasAddr, ((void**)&pBuf),
                                        NULL, NULL, maxW * maxH * (u32BytePerPixel))) {
        SAMPLE_PRT("allocate memory (maxW*maxH*%d bytes) failed\n",u32BytePerPixel);
        close(pstInfo->fd);
        return HI_NULL;
    }
    stCanvasBuf.stCanvas.u64PhyAddr = g_CanvasAddr;
    stCanvasBuf.stCanvas.u32Height  = maxH;
    stCanvasBuf.stCanvas.u32Width   = maxW;
    stCanvasBuf.stCanvas.u32Pitch   = maxW * (u32BytePerPixel);
    stCanvasBuf.stCanvas.enFmt      = enClrFmt;

    memset(pBuf, 0x00, stCanvasBuf.stCanvas.u32Pitch * stCanvasBuf.stCanvas.u32Height);
    /*change bmp*/
    if (HI_FAILURE == HI_MPI_SYS_MmzAlloc(&g_Phyaddr, ((void**)&Viraddr),
                                        NULL, NULL, SAMPLE_IMAGE_WIDTH * SAMPLE_IMAGE_HEIGHT * u32BytePerPixel)) {
        SAMPLE_PRT("allocate memory (maxW*maxH*%d bytes) failed\n",u32BytePerPixel);
        HI_MPI_SYS_MmzFree(g_CanvasAddr, pBuf);
        g_CanvasAddr = 0;
        close(pstInfo->fd);
        return HI_NULL;
    }

    s32Ret = HI_TDE2_Open();
    if (s32Ret < 0) {
        SAMPLE_PRT("HI_TDE2_Open failed :%d!\n", s32Ret);
        HI_MPI_SYS_MmzFree(g_Phyaddr, Viraddr);
        g_Phyaddr = 0;

        HI_MPI_SYS_MmzFree(g_CanvasAddr, pBuf);
        g_CanvasAddr = 0;

        close(pstInfo->fd);
        return HI_NULL;
    }

    #if CUSTOM_HIFB
    SAMPLE_PRT("wait until process die\n");
    #endif
    /*wait until process die */
    while(run)
    sleep(5);

    return HI_NULL;
}

static void *hifb_thread(void * data)
{
    PTHREAD_HIFB_SAMPLE_INFO stInfo0;

    stInfo0.layer     =  stVoConfig.VoDev;  /* VO device number */
    stInfo0.fd        = -1;
    stInfo0.ctrlkey   =  1;                 /* Double buffer */
    stInfo0.bCompress =  HI_FALSE;          /* Compress opened or not */
    stInfo0.enColorFmt = HIFB_FMT_ARGB8888;

    SAMPLE_HIFB_REFRESH((void*)(&stInfo0));

    return 0;
}

HI_VOID SAMPLE_HIFB_TO_EXIT(HI_VOID)
{
    run = false;

    HI_MPI_SYS_MmzFree(g_Phyaddr, Viraddr);
    g_Phyaddr = 0;

    HI_MPI_SYS_MmzFree(g_CanvasAddr, pBuf);
    g_CanvasAddr = 0;

    close(pstInfo->fd);
    SAMPLE_PRT("[End]\n");
}

HI_S32 CarDV_HiFBModuleStart()
{
    pthread_t pt;
    pthread_create(&pt, NULL, hifb_thread, NULL);
    return 0;
}

HI_S32 CarDV_HiFBModuleStop()
{
    return 0;
}

int hifb_process_cmd(CarDVCmdId id, HI_S8 *param, HI_S32 paramLen)
{
    SAMPLE_PRT("hifb process cmd. id:%d !\n", id);
    switch (id)
    {
    case CMD_HIFB_START:
        CarDV_HiFBModuleStart();
        break;

    case CMD_HIFB_STOP:
        CarDV_HiFBModuleStop();
        break;

    default:
        break;
    }

    return 0;
}
