/*
* module_dcf.h - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/
#ifndef _MODULE_DCF_H_
#define _MODULE_DCF_H_

BOOL DCF_CheckDeleteFilesForSpace(int DB);
void DCF_CheckDeleteFilesForNum(int DB, int nFileNumLimit);
void DCF_CheckDeleteFilesForFormatFreeReserved(int DB, int nReservedCnt);

#endif //#define _MODULE_DCF_H_
