/*
* module_vo.c - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "hi_common.h"
#include "sample_comm.h"

#include "module_mpp.h"
#include "module_common.h"

//==============================================================================
//                              GLOBAL VARIABLES
//==============================================================================
VO_CHN             VoChn          = 0;
SAMPLE_VO_CONFIG_S stVoConfig;

extern VPSS_GRP           VpssGrp;
extern VPSS_CHN           VpssChn[3];
extern HI_BOOL            abChnEnable[VPSS_MAX_PHY_CHN_NUM];

extern VI_PIPE            ViPipe;
extern VI_CHN             ViChn;
extern SAMPLE_VI_CONFIG_S stViConfig;
extern DYNAMIC_RANGE_E    enDynamicRange;
extern PIC_SIZE_E         enPicSize;

//==============================================================================
//                              FUNCTIONS
//==============================================================================
HI_S32 CarDV_VoDevInit()
{
    /*config vo*/
    SAMPLE_COMM_VO_GetDefConfig(&stVoConfig);
    stVoConfig.enDstDynamicRange = enDynamicRange;

    #if CUSTOM_HIFB /* Add By KODO */
    stVoConfig.enVoIntfType = VO_INTF_MIPI;
    stVoConfig.enIntfSync = VO_OUTPUT_USER;
    #else
    stVoConfig.enVoIntfType = VO_INTF_BT1120;
    stVoConfig.enIntfSync   = VO_OUTPUT_1080P25;
    #endif
    stVoConfig.enPicSize = enPicSize;

    return 0;
}

HI_S32 CarDV_VoDevUnInit()
{
    return 0;
}

HI_S32 CarDV_VoModuleStart()
{
    HI_S32 s32Ret = HI_SUCCESS;

    /*start vo*/
    s32Ret = SAMPLE_COMM_VO_StartVO(&stVoConfig);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("start vo failed. s32Ret: 0x%x !\n", s32Ret);
        goto EXIT4;
    }
    return s32Ret;

EXIT4:
    SAMPLE_COMM_VI_UnBind_VPSS(ViPipe, ViChn, VpssGrp);
EXIT2:
    SAMPLE_COMM_VPSS_Stop(VpssGrp, abChnEnable);
EXIT1:
    SAMPLE_COMM_VI_StopVi(&stViConfig);
EXIT:
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

HI_S32 CarDV_VoModuleStop()
{
    HI_S32 s32Ret = HI_SUCCESS;

    /*stop vo*/
    s32Ret = SAMPLE_COMM_VO_StopVO(&stVoConfig);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("stop vo failed. s32Ret: 0x%x !\n", s32Ret);
        goto EXIT4;
    }
    return s32Ret;

EXIT4:
    SAMPLE_COMM_VI_UnBind_VPSS(ViPipe, ViChn, VpssGrp);
EXIT2:
    SAMPLE_COMM_VPSS_Stop(VpssGrp, abChnEnable);
EXIT1:
    SAMPLE_COMM_VI_StopVi(&stViConfig);
EXIT:
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

HI_S32 CarDV_VoModuleBind()
{
    HI_S32 s32Ret = HI_SUCCESS;

    /*vpss bind vo*/
    MPP_CHN_S pstSrcChn;
    MPP_BIND_DEST_S pstBindDest;

    pstSrcChn.enModId  = HI_ID_VPSS;
    pstSrcChn.s32DevId = VpssGrp;
    pstSrcChn.s32ChnId = VpssChn[0];

    s32Ret = HI_MPI_SYS_GetBindbySrc(&pstSrcChn, &pstBindDest);
    if (HI_SUCCESS == s32Ret && pstBindDest.u32Num > 0)
    {
        SAMPLE_PRT("vo already bind vpss. s32Ret: 0x%x !\n", s32Ret);
        return s32Ret;
    }

    s32Ret = SAMPLE_COMM_VPSS_Bind_VO(VpssGrp, VpssChn[0], stVoConfig.VoDev, VoChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("vo bind vpss failed. s32Ret: 0x%x !\n", s32Ret);
        goto EXIT5;
    }
    return s32Ret;

EXIT5:
    SAMPLE_COMM_VO_StopVO(&stVoConfig);
EXIT4:
    SAMPLE_COMM_VI_UnBind_VPSS(ViPipe, ViChn, VpssGrp);
EXIT2:
    SAMPLE_COMM_VPSS_Stop(VpssGrp, abChnEnable);
EXIT1:
    SAMPLE_COMM_VI_StopVi(&stViConfig);
EXIT:
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

HI_S32 CarDV_VoModuleUnBind()
{
    HI_S32 s32Ret = HI_SUCCESS;

    /*vpss bind vo*/
    s32Ret = SAMPLE_COMM_VPSS_UnBind_VO(VpssGrp, VpssChn[0], stVoConfig.VoDev, VoChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("vo unbind vpss failed. s32Ret: 0x%x !\n", s32Ret);
        goto EXIT5;
    }
    return s32Ret;

EXIT5:
    SAMPLE_COMM_VO_StopVO(&stVoConfig);
EXIT4:
    SAMPLE_COMM_VI_UnBind_VPSS(ViPipe, ViChn, VpssGrp);
EXIT2:
    SAMPLE_COMM_VPSS_Stop(VpssGrp, abChnEnable);
EXIT1:
    SAMPLE_COMM_VI_StopVi(&stViConfig);
EXIT:
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

int vo_process_cmd(CarDVCmdId id, MI_S8 *param, MI_S32 paramLen)
{
    SAMPLE_PRT("vo process cmd. id:%d !\n", id);
    switch (id)
    {
    case CMD_VO_INIT:
        CarDV_VoDevInit();
        break;

    case CMD_VO_DEINIT:
        CarDV_VoDevUnInit();
        break;

    case CMD_VO_START:
        CarDV_VoModuleStart();
        break;

    case CMD_VO_STOP:
        CarDV_VoModuleStop();
        break;

    case CMD_VO_BIND:
        CarDV_VoModuleBind();
        break;

    case CMD_VO_UNBIND:
        CarDV_VoModuleUnBind();
        break;

    default:
        break;
    }

    return 0;
}
