#ifndef __MODULE_MPP_H__
#define __MODULE_MPP_H__

#include "hi_common.h"
#include "module_common.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#ifndef SAMPLE_PRT
#define SAMPLE_PRT(fmt...)   \
    do {\
        printf("[%s]-%d: ", __FUNCTION__, __LINE__);\
        printf(fmt);\
    }while(0)
#endif

#ifndef PAUSE
#define PAUSE()  do {\
        printf("---------------press Enter key to exit!---------------\n");\
        getchar();\
    } while (0)
#endif

/* Add By KODO */
#define CUSTOM_HIFB         1

int vif_process_cmd(CarDVCmdId id, HI_S8 *param, HI_S32 paramLen);
int vpss_process_cmd(CarDVCmdId id, MI_S8 *param, MI_S32 paramLen);
int vo_process_cmd(CarDVCmdId id, MI_S8 *param, MI_S32 paramLen);
int hifb_process_cmd(CarDVCmdId id, HI_S8 *param, HI_S32 paramLen);
int venc_process_cmd(CarDVCmdId id, HI_S8 *param, HI_S32 paramLen);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
#endif /* End of #ifndef __MODULE_MPP_H__*/
