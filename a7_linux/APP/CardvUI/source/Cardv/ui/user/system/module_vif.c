/*
* module_vif.c - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "hi_common.h"
#include "sample_comm.h"

#include "module_mpp.h"
#include "module_common.h"

//==============================================================================
//                              GLOBAL VARIABLES
//==============================================================================
HI_S32             s32ViCnt       = 1;
VI_DEV             ViDev          = 0;
VI_PIPE            ViPipe         = 0;
VI_CHN             ViChn          = 0;
HI_S32             s32WorkSnsId   = 0;

SIZE_S             stSize;
SAMPLE_VI_CONFIG_S stViConfig;
PIC_SIZE_E         enPicSize;

DYNAMIC_RANGE_E    enDynamicRange = DYNAMIC_RANGE_SDR8;
VIDEO_FORMAT_E     enVideoFormat  = VIDEO_FORMAT_LINEAR;
COMPRESS_MODE_E    enCompressMode = COMPRESS_MODE_NONE;
PIXEL_FORMAT_E     enPixFormat    = PIXEL_FORMAT_YVU_SEMIPLANAR_420;

//==============================================================================
//                              FUNCTIONS
//==============================================================================
HI_S32 CarDV_VifDevInit()
{
    HI_S32             s32Ret = HI_SUCCESS;

    WDR_MODE_E         enWDRMode      = WDR_MODE_NONE;
    VI_VPSS_MODE_E     enMastPipeMode = VI_ONLINE_VPSS_ONLINE;

    VB_CONFIG_S        stVbConf;
    HI_U32             u32BlkSize;

    /*config vi*/
    SAMPLE_COMM_VI_GetSensorInfo(&stViConfig);

    stViConfig.s32WorkingViNum                                   = s32ViCnt;
    stViConfig.as32WorkingViId[0]                                = 0;
    stViConfig.astViInfo[s32WorkSnsId].stSnsInfo.MipiDev         = ViDev;
    stViConfig.astViInfo[s32WorkSnsId].stSnsInfo.s32BusId        = 0;
    stViConfig.astViInfo[s32WorkSnsId].stDevInfo.ViDev           = ViDev;
    stViConfig.astViInfo[s32WorkSnsId].stDevInfo.enWDRMode       = enWDRMode;
    stViConfig.astViInfo[s32WorkSnsId].stPipeInfo.enMastPipeMode = enMastPipeMode;
    stViConfig.astViInfo[s32WorkSnsId].stPipeInfo.aPipe[0]       = ViPipe;
    stViConfig.astViInfo[s32WorkSnsId].stPipeInfo.aPipe[1]       = -1;
    stViConfig.astViInfo[s32WorkSnsId].stPipeInfo.aPipe[2]       = -1;
    stViConfig.astViInfo[s32WorkSnsId].stPipeInfo.aPipe[3]       = -1;
    stViConfig.astViInfo[s32WorkSnsId].stChnInfo.ViChn           = ViChn;
    stViConfig.astViInfo[s32WorkSnsId].stChnInfo.enPixFormat     = enPixFormat;
    stViConfig.astViInfo[s32WorkSnsId].stChnInfo.enDynamicRange  = enDynamicRange;
    stViConfig.astViInfo[s32WorkSnsId].stChnInfo.enVideoFormat   = enVideoFormat;
    stViConfig.astViInfo[s32WorkSnsId].stChnInfo.enCompressMode  = enCompressMode;

    /*get picture size*/
    s32Ret = SAMPLE_COMM_VI_GetSizeBySensor(stViConfig.astViInfo[s32WorkSnsId].stSnsInfo.enSnsType, &enPicSize);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("get picture size by sensor failed!\n");
        return s32Ret;
    }

    s32Ret = SAMPLE_COMM_SYS_GetPicSize(enPicSize, &stSize);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("get picture size failed!\n");
        return s32Ret;
    }

    /*config vb*/
    memset_s(&stVbConf, sizeof(VB_CONFIG_S), 0, sizeof(VB_CONFIG_S));
    stVbConf.u32MaxPoolCnt              = 2;

    //一般 linear 格式的 YUV 缓存池
    u32BlkSize = COMMON_GetPicBufferSize(stSize.u32Width, stSize.u32Height, SAMPLE_PIXEL_FORMAT, DATA_BITWIDTH_8, COMPRESS_MODE_SEG, DEFAULT_ALIGN);
    stVbConf.astCommPool[0].u64BlkSize  = u32BlkSize;
    stVbConf.astCommPool[0].u32BlkCnt   = 10;

    //VI 写出的 Raw 数据缓存池
    u32BlkSize = VI_GetRawBufferSize(stSize.u32Width, stSize.u32Height, PIXEL_FORMAT_RGB_BAYER_16BPP, COMPRESS_MODE_NONE, DEFAULT_ALIGN);
    stVbConf.astCommPool[1].u64BlkSize  = u32BlkSize;
    stVbConf.astCommPool[1].u32BlkCnt   = 4;

    s32Ret = SAMPLE_COMM_SYS_Init(&stVbConf);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("system init failed with %d!\n", s32Ret);
        return s32Ret;
    }

    return s32Ret;
}

HI_S32 CarDV_VifDevUnInit()
{
    return 0;
}

HI_S32 CarDV_VifModuleStart()
{
    HI_S32 s32Ret = HI_SUCCESS;

    /*start vi*/
    s32Ret = SAMPLE_COMM_VI_StartVi(&stViConfig);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("start vi failed.s32Ret:0x%x !\n", s32Ret);
        goto EXIT;
    }
    return s32Ret;

EXIT:
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

HI_S32 CarDV_VifModuleStop()
{
    SAMPLE_COMM_VI_StopVi(&stViConfig);
    return 0;
}

int vif_process_cmd(CarDVCmdId id, HI_S8 *param, HI_S32 paramLen)
{
    SAMPLE_PRT("vif process cmd. id:%d !\n", id);
    switch (id)
    {
    case CMD_VIF_INIT:
        CarDV_VifDevInit();
        break;

    case CMD_VIF_DEINIT:
        CarDV_VifDevUnInit();
        break;

    case CMD_VIF_START:
        CarDV_VifModuleStart();
        break;

    case CMD_VIF_STOP:
        CarDV_VifModuleStop();
        break;

    case CMD_VIF_SET_RESOLUTION:
        break;

    default:
        break;
    }

    return 0;
}
