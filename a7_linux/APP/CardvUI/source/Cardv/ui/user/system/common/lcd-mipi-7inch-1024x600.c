#include "lcd-mipi-config.h"
#include <sys/ioctl.h>

/*============================= mipi 7 inch 1024x600 lcd config ====================================*/
combo_dev_cfg_t MIPI_TX_7INCH_1024X600_60_CONFIG =
{
	.devno = 0,
	.lane_id = {0, 1, 2, 3},
	.output_mode = OUTPUT_MODE_DSI_VIDEO,
	.output_format = OUT_FORMAT_RGB_24_BIT,
	.video_mode =  BURST_MODE,

	/*
	.sync_info = {
		.vid_pkt_size     = 1024, // hact
		.vid_hsa_pixels   = 4,  // hsa   //20
		.vid_hbp_pixels   = 60,  // hbp  //20
		.vid_hline_pixels = 1224, // hact + hsa + hbp + hfp  //hfb=32 //972
		.vid_vsa_lines    = 2,   // vsa
		.vid_vbp_lines    = 16,  // vbp
		.vid_vfp_lines    = 16,   // vfp
		.vid_active_lines = 634,// vact
		.edpi_cmd_size    = 0,
	},
	.phy_data_rate = 495,
	.pixel_clk = 46561,
	*/
	.sync_info = {
		.vid_pkt_size     = 1024, // hact
		.vid_hsa_pixels   = 10,  // hsa   //20
		.vid_hbp_pixels   = 160,  // hbp  //20
		.vid_hline_pixels = 1354, // hact + hsa + hbp + hfp  //hfb=32 //972
		.vid_vsa_lines    = 1,   // vsa
		.vid_vbp_lines    = 23,  // vbp
		.vid_vfp_lines    = 12,   // vfp
		.vid_active_lines = 600,// vact
		.edpi_cmd_size    = 0,
	},
	.phy_data_rate = 311,
	.pixel_clk = 51669,
};

VO_SYNC_INFO_S MIPI_TX_7INCH_1024X600_60_SYNC_INFO =
{
	.u16Hact	= 1024,
	.u16Hbb		= 170,
	.u16Hfb		= 160,
	.u16Hpw		= 10,
	.u16Vact	= 600,
	.u16Vbb		= 24,
	.u16Vfb		= 12,
	.u16Vpw		= 1,
};

VO_USER_INTFSYNC_INFO_S MIPI_TX_7INCH_1024X600_60_USER_INTFSYNC_INFO =
{
	.stUserIntfSyncAttr =
	{
		.stUserSyncPll	=
		{
			.u32Fbdiv	= 421,
			.u32Frac	= 0xf5e742,
			.u32Refdiv	= 4,
			.u32Postdiv1= 7,
			.u32Postdiv2= 7,
		},
	},
	.u32DevDiv			= 1,
	.u32PreDiv			= 1,
};

lcd_resoluton_t MIPI_TX_7INCH_1024X600_60_LCD_RESOLUTION =
{
	.pu32W	= 1024,
	.pu32H	= 600,
	.pu32Frm= 60,
};

HI_VOID InitScreen_mipi_7inch_1024x600(HI_S32 s32fd)
{
	HI_S32     fd     = s32fd;
	HI_S32     s32Ret;
	cmd_info_t cmd_info = {0};

	SAMPLE_PRT("%s,%d.\n",__FUNCTION__,__LINE__);
	usleep(1000);

#if defined (KODOBoard)
	//LCD-BL-RST
	system("himm 0x111f0024 0x0400");//复用:GPIO6-6
	system("himm 0x112f0094 0x0400");//复用:GPIO0-3
	system("himm 0x120D6400 0x40");  //BL设置输出模式
	system("himm 0x120D0400 0x08");  //RST设置输出模式
	system("himm 0x120D6100 0x40");  //BL复位1
	system("himm 0x120D0020 0x08");  //RST复位1
	usleep(500000);
	system("himm 0x120D0020 0x00");  //RST复位0
	usleep(10000);
	system("himm 0x120D0020 0x08");  //RST置位1
	usleep(500000);
#elif defined (BlueBoard)
	/********HI3516CV500 BlueBoard********/
	//Power-1.8v and 3.3v
	system("himm 0x114f0050 0x0400");//复用
	system("himm 0x114f0054 0x0400");//复用
	system("himm 0x120D3400 0x30");  //模式
	system("himm 0x120D30c0 0x30");  //电平
	usleep(500000);
	//LCD-BL-RST
	system("himm 0x112f0024 0x0404");//复用
	system("himm 0x112f0030 0x0404");//复用
	system("himm 0x120D8400 0x12");  //BL、RST设置输出模式
	system("himm 0x120D807c 0x00");  //BL、RST复位0
	usleep(500000);
	system("himm 0x120D807c 0x12");  //BL、RST置位1
	usleep(500000);
	system("himm 0x120D8040 0x02");  //RST复位0
	usleep(10000);
	system("himm 0x120D8040 0x12");  //RST置位1
	usleep(500000);

#elif defined (GreenBoard)
	//Power
	system("himm 0x120D3400 0x30");
	system("himm 0x120D30c0 0x30");
	usleep(500000);
	usleep(500000);

	system("himm 0x120D8400 0x12");
	system("himm 0x120D807c 0x00");
	usleep(500000);
	usleep(500000);
	//LCD-BL-RST
	system("himm 0x120D807c 0x12");
	usleep(500000);
	system("himm 0x120D8040 0x02");
	usleep(10000);
	system("himm 0x120D8040 0x12");
	usleep(500000);
#endif

	SAMPLE_PRT("%s,%d.\n",__FUNCTION__,__LINE__);
#if 1
	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x8880;
	cmd_info.data_type = 0x15;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x8482;
	cmd_info.data_type = 0x15;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x8883;
	cmd_info.data_type = 0x15;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0xa884;
	cmd_info.data_type = 0x15;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x8385;
	cmd_info.data_type = 0x15;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);

	cmd_info.devno     = 0;
	cmd_info.cmd_size  = 0x8886;
	cmd_info.data_type = 0x15;
	cmd_info.cmd       = NULL;
	s32Ret = ioctl(fd, HI_MIPI_TX_SET_CMD, &cmd_info);
	if (HI_SUCCESS != s32Ret)
	{	
		SAMPLE_PRT("MIPI_TX SET CMD failed\n");
		close(fd);
		return;
	}
	usleep(1000);
#endif
}
