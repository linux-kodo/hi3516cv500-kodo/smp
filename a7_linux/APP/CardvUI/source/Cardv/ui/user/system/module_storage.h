/*
* module_storage.h - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/
#ifndef _MODULE_STORAGE_H_
#define _MODULE_STORAGE_H_

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "module_common.h"

//==============================================================================
//                              MACRO DEFINE
//==============================================================================
#define CARDV_EXFAT_ENABLE (0)

typedef enum _FileSytemType
{
    FS_TYPE_FAT32 = 0,
#if (CARDV_EXFAT_ENABLE)
    FS_TYPE_EXFAT,
#endif
    FS_TYPE_NUM,
} FileSytemType;

//==============================================================================
//                              FUNCTION PROTOTYPES
//==============================================================================
void               StorageStartDetectThread(void);
void               StorageStopDetectThread(void);
BOOL               StorageIsMounted(void);
int                StorageSetVolume(const char *volume);
int                StorageFormat(FileSytemType eFsType);
int                StorageMount(BOOL bFormat);
int                StorageUnMount(BOOL bFormat);
int                StorageReMount(void);
unsigned long long StorageGetTotalSize(void);
unsigned long long StorageGetOtherTotalSize(void);
unsigned long long StorageGetReservedTotalSize(void);

#endif //#define _MODULE_STORAGE_H_
