/*
* module_vpss.c - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "hi_common.h"
#include "sample_comm.h"

#include "module_mpp.h"
#include "module_common.h"
#include "lcd-mipi-config.h"

//==============================================================================
//                              GLOBAL VARIABLES
//==============================================================================
VPSS_GRP           VpssGrp        = 0;
VPSS_CHN           VpssChn[3]     = {VPSS_CHN0, VPSS_CHN1, VPSS_CHN2};
VPSS_GRP_ATTR_S    stVpssGrpAttr;
VPSS_CHN_ATTR_S    astVpssChnAttr[VPSS_MAX_PHY_CHN_NUM];
HI_BOOL            abChnEnable[VPSS_MAX_PHY_CHN_NUM] = {1, 1, 1};

extern VI_PIPE            ViPipe;
extern VI_CHN             ViChn;
extern SIZE_S             stSize;
extern SAMPLE_VI_CONFIG_S stViConfig;
extern PIXEL_FORMAT_E     enPixFormat;
extern DYNAMIC_RANGE_E    enDynamicRange;
extern VIDEO_FORMAT_E     enVideoFormat;
extern COMPRESS_MODE_E    enCompressMode;

extern PAYLOAD_TYPE_E     enPayLoad[2];

//==============================================================================
//                              FUNCTIONS
//==============================================================================
HI_S32 CarDV_VpssDevInit()
{
    HI_S32 i;

    /*config vpss*/
    memset_s(&stVpssGrpAttr, sizeof(VPSS_GRP_ATTR_S), 0, sizeof(VPSS_GRP_ATTR_S));
    stVpssGrpAttr.stFrameRate.s32SrcFrameRate    = -1;
    stVpssGrpAttr.stFrameRate.s32DstFrameRate    = -1;
    stVpssGrpAttr.enDynamicRange                 = DYNAMIC_RANGE_SDR8;
    stVpssGrpAttr.enPixelFormat                  = enPixFormat;
    stVpssGrpAttr.u32MaxW                        = stSize.u32Width;
    stVpssGrpAttr.u32MaxH                        = stSize.u32Height;
    stVpssGrpAttr.bNrEn                          = HI_TRUE;
    stVpssGrpAttr.stNrAttr.enCompressMode        = COMPRESS_MODE_FRAME;
    stVpssGrpAttr.stNrAttr.enNrMotionMode        = NR_MOTION_MODE_NORMAL;

    for(i=0; i<VPSS_MAX_PHY_CHN_NUM; i++)
    {
        if(HI_TRUE == abChnEnable[i])
        {
            if ( i >= 1 && enPayLoad[i - 1] == PT_JPEG) {   //abChnEnable[2] -- jpeg
                SAMPLE_PRT("VpssChn[%d] JPEG!\n", i);
                astVpssChnAttr[i].u32Width                    = stSize.u32Width;
                astVpssChnAttr[i].u32Height                   = stSize.u32Height;
                astVpssChnAttr[i].enChnMode                   = VPSS_CHN_MODE_USER;
                astVpssChnAttr[i].enCompressMode              = enCompressMode;
                astVpssChnAttr[i].enVideoFormat               = enVideoFormat;
                astVpssChnAttr[i].enDynamicRange              = enDynamicRange;
                astVpssChnAttr[i].enPixelFormat               = enPixFormat;
                astVpssChnAttr[i].stFrameRate.s32SrcFrameRate = -1;
                astVpssChnAttr[i].stFrameRate.s32DstFrameRate = -1;
                astVpssChnAttr[i].u32Depth                    = 0;
                astVpssChnAttr[i].bMirror                     = HI_FALSE;
                astVpssChnAttr[i].bFlip                       = HI_FALSE;
                astVpssChnAttr[i].stAspectRatio.enMode        = ASPECT_RATIO_NONE;
            } else {    //abChnEnable[0] -- VO   abChnEnable[1] -- H265   abChnEnable[2] -- H264
                SAMPLE_PRT("VpssChn[%d] H265/H265!\n", i);
                astVpssChnAttr[i].u32Width                    = stSize.u32Width;
                astVpssChnAttr[i].u32Height                   = stSize.u32Height;
                astVpssChnAttr[i].enChnMode                   = VPSS_CHN_MODE_USER;
                astVpssChnAttr[i].enCompressMode              = enCompressMode;
                astVpssChnAttr[i].enDynamicRange              = enDynamicRange;
                astVpssChnAttr[i].enVideoFormat               = enVideoFormat;
                astVpssChnAttr[i].enPixelFormat               = enPixFormat;
                astVpssChnAttr[i].stFrameRate.s32SrcFrameRate = 30;
                astVpssChnAttr[i].stFrameRate.s32DstFrameRate = 30;
                astVpssChnAttr[i].u32Depth                    = 0;
                astVpssChnAttr[i].bMirror                     = HI_FALSE;
                astVpssChnAttr[i].bFlip                       = HI_FALSE;
                astVpssChnAttr[i].stAspectRatio.enMode        = ASPECT_RATIO_NONE;
            }
        }
    }

    return 0;
}

HI_S32 CarDV_VpssDevUnInit()
{
    return 0;
}

HI_S32 CarDV_VpssModuleStart()
{
    HI_S32 s32Ret = HI_SUCCESS;

    /*start vpss*/
    abChnEnable[0] = HI_TRUE;   //VO
    abChnEnable[1] = HI_TRUE;   //Venc H265
    abChnEnable[2] = HI_TRUE;   //Venc H264/JPEG
    s32Ret = SAMPLE_COMM_VPSS_Start(VpssGrp, abChnEnable, &stVpssGrpAttr, astVpssChnAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("start vpss group failed. s32Ret: 0x%x !\n", s32Ret);
        goto EXIT1;
    }

    #if defined LCD_Rotation
    //设置旋转
    s32Ret = HI_MPI_VPSS_SetChnRotation(VpssGrp,VpssChn[0],ROTATION_270);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VPSS_SetChnRotation failed with %d\n", s32Ret);
        goto EXIT1;
    }
    #endif

    return s32Ret;

EXIT1:
    SAMPLE_COMM_VI_StopVi(&stViConfig);
EXIT:
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

HI_S32 CarDV_VpssModuleStop()
{
    SAMPLE_COMM_VPSS_Stop(VpssGrp, abChnEnable);
    return 0;
}

HI_S32 CarDV_VpssModuleBind()
{
    HI_S32 s32Ret = HI_SUCCESS;

    /* vi bind vpss */
    s32Ret = SAMPLE_COMM_VI_Bind_VPSS(ViPipe, ViChn, VpssGrp);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("vpss bind vi failed. s32Ret: 0x%x !\n", s32Ret);
        goto EXIT2;
    }
    return s32Ret;

EXIT2:
    SAMPLE_COMM_VPSS_Stop(VpssGrp, abChnEnable);
EXIT1:
    SAMPLE_COMM_VI_StopVi(&stViConfig);
EXIT:
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

int vpss_process_cmd(CarDVCmdId id, MI_S8 *param, MI_S32 paramLen)
{
    SAMPLE_PRT("vpss process cmd. id:%d !\n", id);
    switch (id)
    {
    case CMD_VPSS_INIT:
        CarDV_VpssDevInit();
        break;

    case CMD_VPSS_DEINIT:
        CarDV_VpssDevUnInit();
        break;

    case CMD_VPSS_START:
        CarDV_VpssModuleStart();
        break;

    case CMD_VPSS_STOP:
        CarDV_VpssModuleStop();
        break;

    case CMD_VPSS_BIND:
        CarDV_VpssModuleBind();
        break;

    default:
        break;
    }

    return 0;
}
