/*
* module_system.h - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/
#ifndef _MODULE_SYSTEM_H_
#define _MODULE_SYSTEM_H_

//==============================================================================
//                              FUNCTIONS
//==============================================================================
void cardv_update_status(const char *status_name, const char *status, size_t size);
int  cardv_get_status(const char *status_name);

#endif //#define _MODULE_SYSTEM_H_
