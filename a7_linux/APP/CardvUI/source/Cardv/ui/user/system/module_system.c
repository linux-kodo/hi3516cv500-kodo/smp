/*
* module_system.c - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <assert.h>
#include <pthread.h>
#include <execinfo.h>

#include "module_system.h"
#include "module_common.h"

//==============================================================================
//                              MACRO DEFINES
//==============================================================================

//==============================================================================
//                              GLOBAL VARIABLES
//==============================================================================

//==============================================================================
//                              FUNCTIONS
//==============================================================================
void cardv_update_status(const char *status_name, const char *status, size_t size)
{
    int pipe_fd_wr = 0;
    pipe_fd_wr     = open(status_name, O_RDWR | O_CREAT, 0666);
    if (pipe_fd_wr >= 0)
    {
        write(pipe_fd_wr, status, size);
        close(pipe_fd_wr);
    }
}

int cardv_get_status(const char *status_name)
{
    int  pipe_fd_rd = 0;
    char status[2]  = {0};
    pipe_fd_rd      = open(status_name, O_RDONLY);
    if (pipe_fd_rd >= 0)
    {
        read(pipe_fd_rd, status, sizeof(status));
        close(pipe_fd_rd);
    }
    return atoi(status);
}
