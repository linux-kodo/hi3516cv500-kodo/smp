/*
 * module_main.h - KODO
 *
 * Copyright (C) 2024 KODO Technology Corp.
 *
 * Author: XiongYingDan
 *
 */
#ifndef _MODULE_MAIN_H_
#define _MODULE_MAIN_H_

#include "IPC_msg.h"
#include "IPC_cardvInfo.h"

#include "module_common.h"
#include "module_system.h"
#include "module_storage.h"

//==============================================================================
//                              FUNCTIONS
//==============================================================================
void app_main_init(void);

#endif //#define _MODULE_MAIN_H_
