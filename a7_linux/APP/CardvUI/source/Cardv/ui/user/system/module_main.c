/*
* module_main.c - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <assert.h>
#include <pthread.h>
#include <execinfo.h>
#include <sys/queue.h>

#include "module_main.h"

//==============================================================================
//                              MACRO DEFINES
//==============================================================================
#ifdef FIFO_CMD_ENABLE
#define MAX_ARGC        (20)
#define FIFO_CMD_SIZE   (16)
#define FIFO_DATA_SIZE  (256)

#define CREATE_FIFO(X_FIFO_NAME)                                                     \
    if (0 == access(X_FIFO_NAME, F_OK))                                              \
    {                                                                                \
        if (0 != remove(X_FIFO_NAME))                                                \
        {                                                                            \
            printf("%s:%d removed %s.", __func__, __LINE__, X_FIFO_NAME);            \
            exit(1);                                                                 \
        }                                                                            \
    }                                                                                \
    if (0 != (mkfifo(X_FIFO_NAME, 0777)))                                            \
    {                                                                                \
        printf("%s:%d Could not create fifo %s\n", __func__, __LINE__, X_FIFO_NAME); \
        exit(1);                                                                     \
    }
#endif

//==============================================================================
//                              GLOBAL VARIABLES
//==============================================================================
struct CarDV_Info carInfo = {0};

#ifdef FIFO_CMD_ENABLE
BOOL       bMessageLoop   = TRUE;
#endif

//==============================================================================
//                              FUNCTIONS
//==============================================================================
#ifdef FIFO_CMD_ENABLE
/*
* FIFO CMD Handler
*/
static void cardv_cmd_handler_exit(MI_U8 u8MaxPara, MI_U8 argc, char **argv)
{
    bMessageLoop = FALSE;
}

U32 cardv_process_display_help(void)
{
    printf("video process useage: echo cmd value > /tmp/cardv_info \n");
    printf("Options: \n");
    /* audio play */
    printf("\t'aplaystop 0/1 dev_id', 0:stop immediately 1:stop wait for complete\n");
    printf("\t'audioplay 0/1 dev_id <live/name>', live:audio live streaming name:play audio file\n");
    /* audio record mute or not */
    printf("\t'audiorec <value[0, 1]>', audio record & rtsp mute or not\n");
    /* resolution */
    printf("\t'res <stream id> <width> <height>',   'res 0 1920 1080' as full HD\n");
    /* bitrate */
    printf("\t'bitrate <channel id> <value>',   'bitrate 0 10000000' set channel 0 10M bps.\n");
    /* record */
    printf("\t'rec <value[0, 1]>', (0-stop, 1-start)\n");
    /* lock file */
    printf("\t'lock <value[0, 1]>', lock file\n");
    /* timelapse record */
    printf("\t'timelapse', timelapse record start\n");
    /* capture */
    printf("\t'capture', capture jpg\n");
    /* mux debug */
    printf("\t'muxdebug', open mux debug log\n");
    /* parkingMonitor */ /*gsensorpwronsens*/
    printf("\t'park <value[0, 1, 2, 3]>', 0:OFF, 1:level1, 2:level2...\n");
    /* voice */
    printf("\t'voice <value[0, 1]>', 0:OFF, 1:ON\n");
    /* gsensor sensitivity*/
    printf("\t'gsensor <value[0, 3]>', 0:OFF, 1:LOW, 2:MID, 3:HIGH\n");
    /* sensor rotate */
    printf("\t'flip <value[0, 1]>', 0:Normal, 1:Rotate\n");
    /* isp */
    printf("\t'bri <value[0, 100]> ', '-bri 60' brightness set 60.\n");
    printf("\t'con <value[0, 100]> ', '-con 60' Contrast set 60.\n");
    printf("\t'sat <value[0, 127]> ', '-sat 60' Saturation set 60.\n");
    printf("\t'sha <value[0, 1023]> ', '-sha 60' Sharpness set 60.\n");
    printf("\t'wb <value[0, 4]> ', '[0] auto, [1] daylight [2] cloudy [3] fluorescent1 [4]incandescent\n");
    printf("\t'ev <value[-6, 6]>' , '-ev 0' EvBiasMode set 0.\n");
    printf("\t'flicker <value[50, 60]> ', '-flicker 60', flicker set 60hz.\n");

    #if (CARDV_LIVE555_ENABLE)
    printf("\t'rtsp', 'rtsp 1' enable live555.\n");
    #endif

    printf("\t lcdbri <value[0, 100]> ', lcdbri 100, set LCD brightness 100.\n");
    /* quit / useage */
    printf("\t, 'help', list useage\n");

    return 0;
}

static void cardv_cmd_handler_help(MI_U8 u8MaxPara, MI_U8 argc, char **argv)
{
    cardv_process_display_help();
}

/*
* Inter-Process Communication -- FIFO
*/
struct fifo_cmd_node{
    char cmd[FIFO_CMD_SIZE];
    unsigned char MaxPara;
    void (*fun)(unsigned char u8MaxPara, unsigned char argc, char **argv);
    TAILQ_ENTRY(fifo_cmd_node) next_cmd;
};

TAILQ_HEAD(tailq_head, fifo_cmd_node) g_CmdQueueHead;

void CarDVInitCmdHandler(void)
{
    TAILQ_INIT(&g_CmdQueueHead);
}

void CarDVRemoveCmdHandler(void)
{
    struct fifo_cmd_node *fileNode;

    while ((fileNode = TAILQ_FIRST(&g_CmdQueueHead))) {
    TAILQ_REMOVE(&g_CmdQueueHead, fileNode, next_cmd);
    FREEIF(fileNode);
}
}

void CarDVAddCmdHandler(char *cmd, unsigned char MaxPara, void *fun)
{
    struct fifo_cmd_node *fileNode = NULL;

    fileNode = (struct fifo_cmd_node *)MALLOC(sizeof(struct fifo_cmd_node));
    fileNode->fun = fun;
    fileNode->MaxPara = MaxPara;
    sprintf(fileNode->cmd, "%s", cmd);
    TAILQ_INSERT_TAIL(&g_CmdQueueHead, fileNode, next_cmd);
}

U8 CarDVParseStringsStart(char *pStart, char *pu8Argv[], char **pNext)
{

}

U32 CarDVExecCmdHandler(char *pu8Argv[], U8 u8Argc)
{

}

void CarDVParseStringsEnd(U8 u8Argc, char *pu8Argv[])
{

}

void cardvInputMessageProcess(char *cmd, MI_U32 len, bool bHighPriority)
{
    U8  u8Argc;
    char * pu8Argv[MAX_ARGC];
    char * pNext;
    char * pStart       = cmd;
    MI_S32 s32ExeMaxCnt = 2;
    MI_S32 s32Ret       = 0;

L_NEXT_CMD:
    s32ExeMaxCnt--;
    u8Argc = CarDVParseStringsStart(pStart, pu8Argv, &pNext);
    if (u8Argc)
    {
        s32Ret = CarDVExecCmdHandler(pu8Argv, u8Argc);
        if (s32Ret < 0)
        {
            printf("\n====\n");
            for (int i = 0; i < u8Argc; i++)
            {
                printf("%s ", pu8Argv[i]);
            }
            printf("\n====\n");
            cardv_process_display_help();
        }
        CarDVParseStringsEnd(u8Argc, pu8Argv);

        if (cmd + len > pNext)
        {
            if (bHighPriority || (!bHighPriority && s32ExeMaxCnt > 0))
            {
                pStart = pNext;
                goto L_NEXT_CMD;
            }
        }
    }
}

U32 cardvInputMessageLoop()
{
    fd_set read_fds;
    char   fifo_data[FIFO_DATA_SIZE] = {0};
    int    ret                       = 0;
    int    fd_out_r = -1, fd_internal_r = -1, maxfds = 0;

    CarDVInitCmdHandler();
    /*                         cmd name    max para num    function */
    CarDVAddCmdHandler((char *)"quit", 1, cardv_cmd_handler_exit);
    CarDVAddCmdHandler((char *)"help", 1, cardv_cmd_handler_help);

    printf("cardv Loop begin\n");

    if (-1 == (fd_out_r = open(FIFO_NAME, O_RDWR)))
    {
        printf("Process %d open fifo error\n", getpid());
        goto L_OPEN_ERR;
    }
    if (-1 == (fd_internal_r = open(FIFO_NAME_HIGH_PRIO, O_RDWR)))
    {
        printf("Process %d open fifo high error\n", getpid());
        close(fd_out_r);
        goto L_OPEN_ERR;
    }

    /*************************************/
    maxfds = MAX(fd_out_r, fd_internal_r);

    while (bMessageLoop)
    {
        FD_ZERO(&read_fds);
        FD_SET(fd_out_r, &read_fds);
        FD_SET(fd_internal_r, &read_fds);

        ret = select(maxfds + 1, &read_fds, NULL, NULL, NULL);
        if (ret > 0)
        {
            // first:FIFO_NAME_HIGH_PRIO
            if (FD_ISSET(fd_internal_r, &read_fds))
            {
                ret = read(fd_internal_r, fifo_data, FIFO_DATA_SIZE);
                if (ret > 0)
                {
                    cardvInputMessageProcess(fifo_data, ret, TRUE);
                    memset(fifo_data, 0x00, FIFO_DATA_SIZE);
                }
            }
            FD_CLR(fd_internal_r, &read_fds);

            // second:FIFO_NAME
            if (FD_ISSET(fd_out_r, &read_fds))
            {
                ret = read(fd_out_r, fifo_data, FIFO_DATA_SIZE);
                if (ret > 0)
                {
                    cardvInputMessageProcess(fifo_data, ret, FALSE);
                    memset(fifo_data, 0x00, FIFO_DATA_SIZE);
                }
            }
            FD_CLR(fd_out_r, &read_fds);
        }
        else if (ret < 0)
        {
            printf("FIFO select failed\n");
            goto L_SELECT_ERR;
        }
        else if (0 == ret)
        {
            printf("FIFO select timeout\n");
            goto L_SELECT_ERR;
        }
    }

L_SELECT_ERR:
    close(fd_out_r);
    close(fd_internal_r);
L_OPEN_ERR:
    CarDVRemoveCmdHandler();
    printf("cardv Loop exit\n");
    return 0;
}
#endif

void cardvExit()
{
    cardv_message_queue_uninit();
    IPC_CarInfo_Close();
}

void cardvResume(MI_BOOL bPowerOn)
{
    //system init
    cardv_send_cmd_HP(CMD_VIF_INIT, NULL, 0);
    cardv_send_cmd_HP(CMD_VIF_START, NULL, 0);

    cardv_send_cmd_HP(CMD_VPSS_INIT, NULL, 0);
    cardv_send_cmd_HP(CMD_VPSS_START, NULL, 0);
    cardv_send_cmd_HP(CMD_VPSS_BIND, NULL, 0);

    cardv_send_cmd_HP(CMD_VENC_INIT, NULL, 0);

    cardv_send_cmd_HP(CMD_VO_INIT, NULL, 0);
    cardv_send_cmd_HP(CMD_VO_START, NULL, 0);
    cardv_send_cmd_HP(CMD_VO_BIND, NULL, 0);

    cardv_send_cmd_HP(CMD_HIFB_START, NULL, 0);
    usleep(1000000);
}

void signalStop(MI_S32 signo)
{
    printf("\nsignalStop(signal code: %d) !!!\n", signo);
    cardvExit();
    printf("cardv app exit\n");
    _exit(0);
}

void *app_main_thread(void)
{
    // register exit function
    struct sigaction sigAction;
    sigAction.sa_handler = signalStop;
    sigemptyset(&sigAction.sa_mask);
    sigAction.sa_flags = 0;
    sigaction(SIGHUP, &sigAction, NULL);  //-1
    sigaction(SIGINT, &sigAction, NULL);  //-2
    sigaction(SIGQUIT, &sigAction, NULL); //-3
    sigaction(SIGKILL, &sigAction, NULL); //-9
    sigaction(SIGTERM, &sigAction, NULL); //-15

    #ifdef FIFO_CMD_ENABLE
    // create cardv_fifo file
    CREATE_FIFO(FIFO_NAME);
    CREATE_FIFO(FIFO_NAME_HIGH_PRIO);
    #endif

    #ifdef FIFO_CMD_ENABLE
    cardvInputMessageLoop();
    #endif
}

void app_main_init(void)
{
    IPC_CarInfo_Open();
    IPC_CarInfo_Write(&carInfo);

    printf("cardv initial message queue!\n");
    if (cardv_message_queue_init())
    {
        printf("cardv initial message queue error!\n");
        exit(1);
    }
    printf("cardv initial mpp!\n");
    cardvResume(TRUE);

    cardv_update_status(REC_STATUS, "0", 2);
    cardv_update_status(MMC_STATUS, "0", 2);
    StorageStartDetectThread();
    usleep(1000000);
}
