/*
* lcd-mipi-config.h
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#ifndef __LCD_MIPI_CONF_H__
#define __LCD_MIPI_CONF_H__

#include <stdio.h>
#include <unistd.h>

#include "hi_comm_vo.h"
#include "hi_mipi_tx.h"
#include "sample_comm.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

/* ================ add lcd config by kodo start ============= */
#define LCD_DRIVER_ENABLE 1

#define MIPI_3IN
#define LCD_Rotation
#define KODOBoard

/* Added KODO for multi user lcd config */
typedef struct lcd_resoluton {
    HI_U32 pu32W;
    HI_U32 pu32H;
    HI_U32 pu32Frm;
} lcd_resoluton_t;

/*============================= mipi 3 inch 480x854 lcd config ====================================*/
extern combo_dev_cfg_t          MIPI_TX_3INCH_480X854_60_CONFIG;
extern VO_SYNC_INFO_S           MIPI_TX_3INCH_480X854_60_SYNC_INFO;
extern VO_USER_INTFSYNC_INFO_S  MIPI_TX_3INCH_480X854_60_USER_INTFSYNC_INFO;
extern lcd_resoluton_t          MIPI_TX_3INCH_480X854_60_LCD_RESOLUTION;
extern HI_VOID                  InitScreen_mipi_3INCH_480X854(HI_S32 s32fd);

/*============================= mipi 7 inch 1024x600 lcd config ====================================*/
extern combo_dev_cfg_t          MIPI_TX_7INCH_1024X600_60_CONFIG;
extern VO_SYNC_INFO_S           MIPI_TX_7INCH_1024X600_60_SYNC_INFO;
extern VO_USER_INTFSYNC_INFO_S  MIPI_TX_7INCH_1024X600_60_USER_INTFSYNC_INFO;
extern lcd_resoluton_t          MIPI_TX_7INCH_1024X600_60_LCD_RESOLUTION;
extern HI_VOID                  InitScreen_mipi_7inch_1024x600(HI_S32 s32fd);

/*============================= mipi 8 inch 800x1280 lcd config ====================================*/
extern combo_dev_cfg_t          MIPI_TX_8INCH_800X1280_60_CONFIG;
extern VO_SYNC_INFO_S           MIPI_TX_8INCH_800X1280_60_SYNC_INFO;
extern VO_USER_INTFSYNC_INFO_S  MIPI_TX_8INCH_800X1280_60_USER_INTFSYNC_INFO;
extern lcd_resoluton_t          MIPI_TX_8INCH_800X1280_60_LCD_RESOLUTION;
extern HI_VOID                  InitScreen_mipi_8inch_800x1280(HI_S32 s32fd);

/*============================= mipi 10 inchi new 800x1280 lcd config ====================================*/
extern combo_dev_cfg_t          MIPI_TX_10INCH_NEW_800X1280_60_CONFIG;
extern VO_SYNC_INFO_S           MIPI_TX_10INCH_NEW_800X1280_60_SYNC_INFO;
extern VO_USER_INTFSYNC_INFO_S  MIPI_TX_10INCH_NEW_800X1280_60_USER_INTFSYNC_INFO;
extern lcd_resoluton_t          MIPI_TX_10INCH_NEW_800X1280_60_LCD_RESOLUTION;
extern HI_VOID                  InitScreen_mipi_10inch_new_800x1280(HI_S32 s32fd);

extern combo_dev_cfg_t *g_mipi_tx_config;
extern VO_SYNC_INFO_S *g_mipi_tx_sync_info;
extern VO_USER_INTFSYNC_INFO_S * g_mipi_tx_user_intfsync_info;
extern lcd_resoluton_t * g_mipi_tx_lcd_resolution;
extern HI_VOID (*g_mipi_lcd_init)(HI_S32 s32fd);
/* ================================ kodo config end ====================================== */

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

#endif /* End of #ifndef __SAMPLE_COMMON_H__ */
