/*
* module_venc.c - KODO
*
* Copyright (C) 2024 KODO Technology Corp.
*
* Author: XiongYingDan
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "hi_common.h"
#include "sample_comm.h"

#include "module_mpp.h"

//==============================================================================
//                              GLOBAL VARIABLES
//==============================================================================
VENC_GOP_ATTR_S stGopAttr;
SAMPLE_RC_E     enRcMode;

HI_S32          s32ChnNum     = 2;
VENC_CHN        VencChn[2]    = {0,1};
HI_U32          u32Profile[2] = {0,0};

HI_BOOL         bSupportDcf   = HI_FALSE;
HI_BOOL         bRcnRefShareBuf = HI_TRUE;
PAYLOAD_TYPE_E  enPayLoad[2]  = {PT_H265, PT_JPEG};
PIC_SIZE_E      enSize[2]     = {PIC_1080P, PIC_1080P};

extern VPSS_GRP           VpssGrp;
extern VPSS_CHN           VpssChn[3];
extern HI_BOOL            abChnEnable[VPSS_MAX_PHY_CHN_NUM];

extern SIZE_S             stSize;
extern VI_PIPE            ViPipe;
extern VI_CHN             ViChn;
extern SAMPLE_VI_CONFIG_S stViConfig;

extern struct CarDV_Info carInfo;

//==============================================================================
//                              FUNCTIONS
//==============================================================================
VENC_GOP_MODE_E SAMPLE_VENC_GetGopMode(void)
{
    char c;
    VENC_GOP_MODE_E enGopMode = 0;

Begin_Get:

    printf("please input choose gop mode!\n");
    printf("\t 0) NORMALP.\n");
    printf("\t 1) DUALP.\n");
    printf("\t 2) SMARTP.\n");

    while((c = getchar()) != '\n' && c != EOF)
    switch(c)
    {
        case '0':
            enGopMode = VENC_GOPMODE_NORMALP;
            break;
        case '1':
            enGopMode = VENC_GOPMODE_DUALP;
            break;
        case '2':
            enGopMode = VENC_GOPMODE_SMARTP;
            break;
        default:
            SAMPLE_PRT("input rcmode: %c, is invaild!\n",c);
            goto Begin_Get;
    }

    return enGopMode;
}

SAMPLE_RC_E SAMPLE_VENC_GetRcMode(void)
{
    char c;
    SAMPLE_RC_E  enRcMode = 0;

Begin_Get:

    printf("please input choose rc mode!\n");
    printf("\t c) cbr.\n");
    printf("\t v) vbr.\n");
    printf("\t a) avbr.\n");
    printf("\t x) cvbr.\n");
    printf("\t q) qvbr.\n");
    printf("\t f) fixQp\n");

    while((c = getchar()) != '\n' && c != EOF)
    switch(c)
    {
        case 'c':
            enRcMode = SAMPLE_RC_CBR;
            break;
        case 'v':
            enRcMode = SAMPLE_RC_VBR;
            break;
        case 'a':
            enRcMode = SAMPLE_RC_AVBR;
            break;
        case 'q':
            enRcMode = SAMPLE_RC_QVBR;
            break;
        case 'x':
            enRcMode = SAMPLE_RC_CVBR;
            break;
        case 'f':
            enRcMode = SAMPLE_RC_FIXQP;
            break;
        default:
            SAMPLE_PRT("input rcmode: %c, is invaild!\n",c);
            goto Begin_Get;
    }
    return enRcMode;
}

HI_VOID SAMPLE_VENC_SetDCFInfo(VI_PIPE ViPipe)
{
    ISP_DCF_INFO_S stIspDCF;

    HI_MPI_ISP_GetDCFInfo(ViPipe, &stIspDCF);

    //description: Thumbnail test
    strncpy((char *)stIspDCF.stIspDCFConstInfo.au8ImageDescription,"Thumbnail test",DCF_DRSCRIPTION_LENGTH);
    //manufacturer: Hisilicon
    strncpy((char *)stIspDCF.stIspDCFConstInfo.au8Make,"Hisilicon",DCF_DRSCRIPTION_LENGTH);
    //model number: Hisilicon IP Camera
    strncpy((char *)stIspDCF.stIspDCFConstInfo.au8Model,"Hisilicon IP Camera",DCF_DRSCRIPTION_LENGTH);
    //firmware version: v.1.1.0
    strncpy((char *)stIspDCF.stIspDCFConstInfo.au8Software,"v.1.1.0",DCF_DRSCRIPTION_LENGTH);


    stIspDCF.stIspDCFConstInfo.u32FocalLength             = 0x00640001;
    stIspDCF.stIspDCFConstInfo.u8Contrast                 = 5;
    stIspDCF.stIspDCFConstInfo.u8CustomRendered           = 0;
    stIspDCF.stIspDCFConstInfo.u8FocalLengthIn35mmFilm    = 1;
    stIspDCF.stIspDCFConstInfo.u8GainControl              = 1;
    stIspDCF.stIspDCFConstInfo.u8LightSource              = 1;
    stIspDCF.stIspDCFConstInfo.u8MeteringMode             = 1;
    stIspDCF.stIspDCFConstInfo.u8Saturation               = 1;
    stIspDCF.stIspDCFConstInfo.u8SceneCaptureType         = 1;
    stIspDCF.stIspDCFConstInfo.u8SceneType                = 0;
    stIspDCF.stIspDCFConstInfo.u8Sharpness                = 5;
    stIspDCF.stIspDCFUpdateInfo.u32ISOSpeedRatings         = 500;
    stIspDCF.stIspDCFUpdateInfo.u32ExposureBiasValue       = 5;
    stIspDCF.stIspDCFUpdateInfo.u32ExposureTime            = 0x00010004;
    stIspDCF.stIspDCFUpdateInfo.u32FNumber                 = 0x0001000f;
    stIspDCF.stIspDCFUpdateInfo.u8WhiteBalance             = 1;
    stIspDCF.stIspDCFUpdateInfo.u8ExposureMode             = 0;
    stIspDCF.stIspDCFUpdateInfo.u8ExposureProgram          = 1;
    stIspDCF.stIspDCFUpdateInfo.u32MaxApertureValue        = 0x00010001;

    HI_MPI_ISP_SetDCFInfo(ViPipe, &stIspDCF);

    return;
}

HI_S32 CarDV_VencDevInit()
{
    HI_S32 s32Ret;

    VENC_GOP_MODE_E enGopMode;

    /******************************************
    start stream venc
    ******************************************/
    //设置编码通道码率控制器 RC 属性
    /* enRcMode = SAMPLE_VENC_GetRcMode(); */
    enRcMode = SAMPLE_RC_CBR;
    //设置编码器 GOP 属性
    /* enGopMode = SAMPLE_VENC_GetGopMode(); */
    enGopMode = VENC_GOPMODE_NORMALP;
    s32Ret = SAMPLE_COMM_VENC_GetGopAttr(enGopMode,&stGopAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Venc Get GopAttr for %#x!\n", s32Ret);
        goto EXIT_VI_VPSS_UNBIND;
    }

    carInfo.stRecInfo.bMuxing      = FALSE;
    carInfo.stRecInfo.bMuxingEmerg = FALSE;
    carInfo.stRecInfo.bMuxingShare = FALSE;
    carInfo.stRecInfo.bMuxingMD    = FALSE;
    IPC_CarInfo_Write_RecInfo(&carInfo.stRecInfo);
    return s32Ret;

EXIT_VENC_H264_UnBind:
    SAMPLE_COMM_VPSS_UnBind_VENC(VpssGrp,VpssChn[2],VencChn[1]);
EXIT_VENC_H264_STOP:
    SAMPLE_COMM_VENC_Stop(VencChn[1]);
EXIT_VENC_H265_UnBind:
    SAMPLE_COMM_VPSS_UnBind_VENC(VpssGrp,VpssChn[1],VencChn[0]);
EXIT_VENC_H265_STOP:
    SAMPLE_COMM_VENC_Stop(VencChn[0]);
EXIT_VI_VPSS_UNBIND:
    SAMPLE_COMM_VI_UnBind_VPSS(ViPipe,ViChn,VpssGrp);
EXIT_VPSS_STOP:
    SAMPLE_COMM_VPSS_Stop(VpssGrp,abChnEnable);
EXIT_VI_STOP:
    SAMPLE_COMM_VI_StopVi(&stViConfig);
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

HI_S32 CarDV_VencDevUnInit()
{
    return HI_SUCCESS;
}

HI_S32 CarDV_VencModuleStart()
{
    HI_S32 s32Ret;

    if (enPayLoad[0] == PT_H264 || enPayLoad[0] == PT_H265) {
        /***encode h.265 **/
        SAMPLE_PRT("VencChn[%d] encode h.265!\n", VencChn[0]);
        s32Ret = SAMPLE_COMM_VENC_Start(VencChn[0], enPayLoad[0],enSize[0], enRcMode,u32Profile[0],bRcnRefShareBuf,&stGopAttr);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("Venc Start failed for %#x!\n", s32Ret);
            goto EXIT_VI_VPSS_UNBIND;
        }
    } else if (enPayLoad[0] == PT_JPEG) {
        /***encode Jpege **/
        SAMPLE_PRT("VencChn[%d] encode Jpege!\n", VencChn[0]);
        s32Ret = SAMPLE_COMM_VENC_SnapStart(VencChn[0], &stSize, bSupportDcf);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("Venc Start failed for %#x!\n", s32Ret);
            goto EXIT_VI_VPSS_UNBIND;
        }
    }

    s32Ret = SAMPLE_COMM_VPSS_Bind_VENC(VpssGrp, VpssChn[1],VencChn[0]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Venc Get GopAttr failed for %#x!\n", s32Ret);
        goto EXIT_VENC_H265_STOP;
    }

    if (enPayLoad[1] == PT_H264 || enPayLoad[1] == PT_H265) {
        /***encode h.264 **/
        SAMPLE_PRT("VencChn[%d] encode h.264!\n", VencChn[1]);
        s32Ret = SAMPLE_COMM_VENC_Start(VencChn[1], enPayLoad[1], enSize[1], enRcMode,u32Profile[1],bRcnRefShareBuf,&stGopAttr);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("Venc Start failed for %#x!\n", s32Ret);
            goto EXIT_VENC_H265_UnBind;
        }
    } else if (enPayLoad[1] == PT_JPEG) {
        /***encode Jpege **/
        SAMPLE_PRT("VencChn[%d] encode Jpege!\n", VencChn[1]);
        s32Ret = SAMPLE_COMM_VENC_SnapStart(VencChn[1], &stSize, bSupportDcf);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("Venc Start failed for %#x!\n", s32Ret);
            goto EXIT_VENC_H265_UnBind;
        }
    }

    s32Ret = SAMPLE_COMM_VPSS_Bind_VENC(VpssGrp, VpssChn[2],VencChn[1]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Venc bind Vpss failed for %#x!\n", s32Ret);
        goto EXIT_VENC_H264_STOP;
    }

    /******************************************
     stream save process
    ******************************************/
    HI_S8 i = 0;
    HI_S8 vencChnNum = 0;
    for(i = 0; i < s32ChnNum; i++) {
        if(enPayLoad[i] == PT_H264 || enPayLoad[i] == PT_H265) {
            vencChnNum++;
        }
    }
    SAMPLE_PRT("vencChnNum:%d!\n", vencChnNum);
    s32Ret = SAMPLE_COMM_VENC_StartGetStream(VencChn,vencChnNum);
    /*
    s32Ret = SAMPLE_COMM_VENC_StartGetStream(VencChn,s32ChnNum);
    */
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start Venc failed!\n");
        goto EXIT_VENC_H264_UnBind;
    }
    SAMPLE_PRT("Start Venc!\n");
    carInfo.stRecInfo.bMuxing      = TRUE;
    IPC_CarInfo_Write_RecInfo(&carInfo.stRecInfo);
    return s32Ret;

EXIT_VENC_H264_UnBind:
    SAMPLE_COMM_VPSS_UnBind_VENC(VpssGrp,VpssChn[2],VencChn[1]);
EXIT_VENC_H264_STOP:
    SAMPLE_COMM_VENC_Stop(VencChn[1]);
EXIT_VENC_H265_UnBind:
    SAMPLE_COMM_VPSS_UnBind_VENC(VpssGrp,VpssChn[1],VencChn[0]);
EXIT_VENC_H265_STOP:
    SAMPLE_COMM_VENC_Stop(VencChn[0]);
EXIT_VI_VPSS_UNBIND:
    SAMPLE_COMM_VI_UnBind_VPSS(ViPipe,ViChn,VpssGrp);
EXIT_VPSS_STOP:
    SAMPLE_COMM_VPSS_Stop(VpssGrp,abChnEnable);
EXIT_VI_STOP:
    SAMPLE_COMM_VI_StopVi(&stViConfig);
    SAMPLE_COMM_SYS_Exit();

    return HI_FAILURE;
}

HI_S32 CarDV_VencModuleStop()
{
    HI_S32 s32Ret;

    /******************************************
     exit process
    ******************************************/
    SAMPLE_COMM_VENC_StopGetStream();

    /***stop encode h.265 **/
    s32Ret = SAMPLE_COMM_VPSS_UnBind_VENC(VpssGrp, VpssChn[1],VencChn[0]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Venc Get GopAttr failed for %#x!\n", s32Ret);
        return s32Ret;
    }

    s32Ret = SAMPLE_COMM_VENC_Stop(VencChn[0]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Venc Stop failed for %#x!\n", s32Ret);
        return s32Ret;
    }

    /***stop encode h.264 **/
    s32Ret = SAMPLE_COMM_VPSS_UnBind_VENC(VpssGrp, VpssChn[2],VencChn[1]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Venc bind Vpss failed for %#x!\n", s32Ret);
        return s32Ret;
    }

    s32Ret = SAMPLE_COMM_VENC_Stop(VencChn[1]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Venc Start failed for %#x!\n", s32Ret);
        return s32Ret;
    }
    SAMPLE_PRT("Stop Venc!\n");
    carInfo.stRecInfo.bMuxing      = FALSE;
    IPC_CarInfo_Write_RecInfo(&carInfo.stRecInfo);
    return s32Ret;
}

HI_S32 CarDV_VencModuleSnap()
{
    HI_S32 s32Ret;

    s32Ret = SAMPLE_COMM_VENC_SnapProcess(VencChn[1], 1, HI_TRUE, HI_FALSE);
    if (HI_SUCCESS != s32Ret)
    {
        printf("%s: sanp process failed!\n", __FUNCTION__);
        return s32Ret;
    }
    //IPC_MsgToUI_SendMsg(IPC_MSG_UI_TAKEPHOTO, NULL, 0);
    printf("snap success!\n");
    return s32Ret;
}

int venc_process_cmd(CarDVCmdId id, HI_S8 *param, HI_S32 paramLen)
{
    SAMPLE_PRT("venc process cmd. id:%d !\n", id);
    switch (id)
    {
    case CMD_VENC_INIT:
        CarDV_VencDevInit();
        break;

    case CMD_VENC_DEINIT:
        CarDV_VencDevUnInit();
        break;

    case CMD_VENC_START:
        CarDV_VencModuleStart();
        break;

    case CMD_VENC_STOP:
        CarDV_VencModuleStop();
        break;

    case CMD_VENC_SNAP:
        CarDV_VencModuleSnap();
        break;
    
    default:
        break;
    }

    return 0;
}
