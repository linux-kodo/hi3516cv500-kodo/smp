/*
* TimerEventContext.h
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#ifndef _TIMER_EVENT_CONTEXT_H_
#define _TIMER_EVENT_CONTEXT_H_

#include <stdint.h>
#include <stdbool.h>

typedef void (*on_timer_event_callback)(int id);

typedef struct {
	int id; // 定时器ID ， 不能重复
	int time; // 定时器  时间间隔  单位 毫秒
	int time_count; // 定时器  计数  单位 次
}S_ACTIVITY_TIMEER;

bool callback_run_flag;

void cardv_timer_init(void);
void cardv_timer_uninit(void);
void register_timer_callback(on_timer_event_callback cb, S_ACTIVITY_TIMEER *timer, uint32_t num);

#endif /* _TIMER_EVENT_CONTEXT_H_ */
