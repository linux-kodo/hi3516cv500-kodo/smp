#ifndef _MENU_CONFIG_SDK_H_
#define _MENU_CONFIG_SDK_H_

/* Movie */
#define MENU_MOVIE_PAGE_EN                          (1)
#define MENU_MOVIE_EN                               (MENU_MOVIE_PAGE_EN)

#define MENU_MOVIE_SIZE_EN                          (1)
#define MENU_MOVIE_SIZE_4K_25P_EN                   (1)
#define MENU_MOVIE_SIZE_1440_30P_EN                 (1)
#define MENU_MOVIE_SIZE_1080_60P_EN                 (0)
#define MENU_MOVIE_SIZE_1080P_EN                    (1)
#define MENU_MOVIE_SIZE_1080_30P_EN                 (MENU_MOVIE_SIZE_1080P_EN)
#define MENU_MOVIE_SIZE_1080P_30_HDR_EN             (0)   // for HDR
#define MENU_MOVIE_SIZE_1080_25P_EN                 (0)
#define MENU_MOVIE_SIZE_SHD_30P_EN                  (0)
#define MENU_MOVIE_SIZE_SHD_25P_EN                  (0)
#define MENU_MOVIE_SIZE_960P_30P_EN                 (0)
#define MENU_MOVIE_SIZE_900P_30P_EN                 (0)
#define MENU_MOVIE_SIZE_720P_EN                     (0)
#define MENU_MOVIE_SIZE_720_120P_EN                 (0)
#define MENU_MOVIE_SIZE_720_30P_EN                  (MENU_MOVIE_SIZE_720P_EN)
#define MENU_MOVIE_SIZE_720_60P_EN                  (0)
#define MENU_MOVIE_SIZE_720_50P_EN                  (0)
#define MENU_MOVIE_SIZE_VGA30P_EN                   (0)

#define MENU_MOVIE_SOUND_RECORD_EN                  (0)
#define MENU_MOVIE_SOUND_RECORD_ON_EN               (1)
#define MENU_MOVIE_SOUND_RECORD_OFF_EN              (1)

#define MENU_MOVIE_CLIP_TIME_EN                     (1)
#define MENU_MOVIE_CLIP_TIME_OFF_EN                 (0) // Not Support
#define MENU_MOVIE_CLIP_TIME_1MIN_EN                (1)
#define MENU_MOVIE_CLIP_TIME_2MIN_EN                (1)
#define MENU_MOVIE_CLIP_TIME_3MIN_EN                (1)
#define MENU_MOVIE_CLIP_TIME_5MIN_EN                (0)
#define MENU_MOVIE_CLIP_TIME_10MIN_EN               (0)
#define MENU_MOVIE_CLIP_TIME_30MIN_EN               (0)

/* General */
#define MENU_GENERAL_PAGE_EN                        (1)
#define MENU_SYSTEM_PAGE_EN                         (1)
#define MENU_GENERAL_EN                             (MENU_GENERAL_PAGE_EN)

#define MENU_SYSTEM_VOLUME_EN                       (1)
#define MENU_SYSTEM_VOLUME_LV0_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV1_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV2_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV3_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV4_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV5_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV6_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV7_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV8_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV9_EN                   (1)
#define MENU_SYSTEM_VOLUME_LV10_EN                  (1)

#define MENU_SYSTEM_BACKLIGHT_EN                    (1)
#define MENU_SYSTEM_BACKLIGHT_LV0_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV1_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV2_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV3_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV4_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV5_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV6_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV7_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV8_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV9_EN                (1)
#define MENU_SYSTEM_BACKLIGHT_LV10_EN               (1)

#define MENU_GENERAL_CLOCK_SETTING_EN               (1)

#define MENU_GENERAL_DATE_FORMAT_EN                 (1)
#define MENU_GENERAL_DATE_FORMAT_NONE_EN            (0)
#define MENU_GENERAL_DATE_FORMAT_YMD_EN             (1)
#define MENU_GENERAL_DATE_FORMAT_MDY_EN             (1)
#define MENU_GENERAL_DATE_FORMAT_DMY_EN             (1)

#define MENU_GENERAL_LANGUAGE_EN                    (1)
#define MENU_GENERAL_LANGUAGE_ENGLISH_EN            (1)
#define MENU_GENERAL_LANGUAGE_SPANISH_EN            (0)
#define MENU_GENERAL_LANGUAGE_PORTUGUESE_EN         (0)
#define MENU_GENERAL_LANGUAGE_RUSSIAN_EN            (0)
#define MENU_GENERAL_LANGUAGE_SCHINESE_EN           (1)
#define MENU_GENERAL_LANGUAGE_TCHINESE_EN           (1)
#define MENU_GENERAL_LANGUAGE_GERMAN_EN             (0)
#define MENU_GENERAL_LANGUAGE_ITALIAN_EN            (0)
#define MENU_GENERAL_LANGUAGE_LATVIAN_EN            (0)
#define MENU_GENERAL_LANGUAGE_POLISH_EN             (0)
#define MENU_GENERAL_LANGUAGE_ROMANIAN_EN           (0)
#define MENU_GENERAL_LANGUAGE_SLOVAK_EN             (0)
#define MENU_GENERAL_LANGUAGE_UKRANINIAN_EN         (0)
#define MENU_GENERAL_LANGUAGE_FRENCH_EN             (0)
#define MENU_GENERAL_LANGUAGE_JAPANESE_EN           (0)
#define MENU_GENERAL_LANGUAGE_KOREAN_EN             (0)
#define MENU_GENERAL_LANGUAGE_CZECH_EN              (0)
#define MENU_GENERAL_LANGUAGE_TURKISH_EN            (0) // TBD
#define MENU_GENERAL_LANGUAGE_DUTCH_EN              (0) // TBD
#define MENU_GENERAL_LANGUAGE_DANISH_EN             (0) // TBD
#define MENU_GENERAL_LANGUAGE_GREEK_EN              (0) // TBD
#define MENU_GENERAL_LANGUAGE_ARABIC_EN             (0) // TBD
#define MENU_GENERAL_LANGUAGE_THAI_EN               (0) // TBD
#define MENU_GENERAL_LANGUAGE_VIETNAMESE_EN         (0)

#define MENU_MEDIA_FORMAT_SD_EN                     (1)
#define MENU_MEDIA_FORMAT_REMINDER_EN               (0)

#define MENU_GENERAL_RESET_SETUP_EN                 (1)
#define MENU_GENERAL_RESET_SETUP_YES_EN             (1)
#define MENU_GENERAL_RESET_SETUP_NO_EN              (1)

#define MENU_GENERAL_FLICKER_EN                     (1)
#define MENU_GENERAL_FLICKER_50HZ_EN                (1)
#define MENU_GENERAL_FLICKER_60HZ_EN                (1)

#define MENU_GENERAL_LCD_POWER_SAVE_EN              (1) // TBD
#define MENU_GENERAL_LCD_POWER_SAVE_OFF_EN          (1)
#define MENU_GENERAL_LCD_POWER_SAVE_10SEC_EN        (0)
#define MENU_GENERAL_LCD_POWER_SAVE_30SEC_EN        (0)
#define MENU_GENERAL_LCD_POWER_SAVE_1MIN_EN         (1)
#define MENU_GENERAL_LCD_POWER_SAVE_3MIN_EN         (1)
#define MENU_GENERAL_LCD_POWER_SAVE_5MIN_EN         (1)

#define GSENSOR_5_LEVEL                             (1) // OFF, Level0 ~ 4
#define GSENSOR_3_LEVEL                             (2) // OFF, HIGH,MIDDLE,LOW
#define GSENSOR_4_LEVEL                             (3) // OFF, HIGH,MIDDLE,LOW,STANDARD
#define MENU_GENERAL_GSENSOR_EN                     (2) // TBD

#define MENU_GENERAL_GSENSOR_OFF_EN                 (1) // TBD
#if (MENU_GENERAL_GSENSOR_EN==GSENSOR_5_LEVEL)
#define MENU_GENERAL_GSENSOR_LEVEL0_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL1_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL2_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL3_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL4_EN              (1)
#elif (MENU_GENERAL_GSENSOR_EN==GSENSOR_4_LEVEL)
#define MENU_GENERAL_GSENSOR_LEVEL0_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL1_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL2_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL3_EN              (0)
#define MENU_GENERAL_GSENSOR_LEVEL4_EN              (1)
#else
#define MENU_GENERAL_GSENSOR_LEVEL0_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL1_EN              (0)
#define MENU_GENERAL_GSENSOR_LEVEL2_EN              (1)
#define MENU_GENERAL_GSENSOR_LEVEL3_EN              (0)
#define MENU_GENERAL_GSENSOR_LEVEL4_EN              (1)
#endif

#define MENU_GENERAL_FW_VERSION_EN                  (1)
#define MENU_GENERAL_QR_CODE_EN                     (1)

#define MENU_GENERAL_WIFI_EN                        (1)
#define MENU_GENERAL_WIFI_OFF_EN                    (1)
#define MENU_GENERAL_WIFI_ON_EN                     (1)
#define MENU_GENERAL_WIFI_2_4G_EN                   (0)
#define MENU_GENERAL_WIFI_5G_EN                     (0)

#define MENU_GENERAL_SHUTDOWN_TONE_EN               (1)
#define MENU_GENERAL_SHUTDOWN_TONE_ON_EN            (0)
#define MENU_GENERAL_SHUTDOWN_TONE_OFF_EN           (1)

#define MENU_GENERAL_KEY_TONE_EN                    (1)
#define MENU_GENERAL_KEY_TONE_ON_EN                 (0)
#define MENU_GENERAL_KEY_TONE_OFF_EN                (1)

#define MENU_GENERAL_REVERSING_LINE_EN              (1)
#define MENU_GENERAL_REVERSING_LINE_ON_EN           (1)
#define MENU_GENERAL_REVERSING_LINE_OFF_EN          (1)

#define MENU_GENERAL_DATE_LOGO_DISPLAY_EN           (1)
#define MENU_GENERAL_DATE_LOGO_DISPLAY_ON_EN        (1)
#define MENU_GENERAL_DATE_LOGO_DISPLAY_OFF_EN       (1)

/* Playback */
#define MENU_PLAYBACK_VIDEO_TYPE_EN                 (0) //Note: Make sure (DCF_DB_COUNT > 1) before enable it
#define MENU_PLAYBACK_VIDEO_TYPE_NORMAL_EN          (1)
#define MENU_PLAYBACK_VIDEO_TYPE_PARKING_EN         (1)
#define MENU_PLAYBACK_VIDEO_TYPE_EMERGENCY_EN       (1)

#endif//_MENU_CONFIG_SDK_H_
