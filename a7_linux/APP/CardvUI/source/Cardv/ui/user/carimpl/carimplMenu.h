/*
 * carimplMenu.h
 *
 *  Created on: Mar 1, 2024
 *      Author: XiongYingDan
 */
#ifndef _CARIMPL_MENU_H_
#define _CARIMPL_MENU_H_

//==============================================================================
//                              MACRO DEFINES
//==============================================================================

//==============================================================================
//                              STRUCT / ENUM 
//==============================================================================
typedef enum _E_CARIMMPL_MENU_ID
{
	CARIMMPL_MENU_SOUND_RECORD =0,
	CARIMMPL_MENU_LCD_POWER_SAVE,
	CARIMMPL_MENU_LCD_GSENSOR_SEN,
	CARIMMPL_MENU_LCD_LOOPVIDEO,
	CARIMMPL_MENU_NIGHT_MODE,
	CARIMMPL_MENU_WIFI,
	CARIMMPL_MENU_DATE_TIME_FORMAT,
	CARIMMPL_MENU_MOV_SIZE,
	CARIMMPL_MENU_MAX,
}E_CARIMMPL_MENU_ID;

typedef struct _CARIMMPL_MENU_ST
{
	E_CARIMMPL_MENU_ID id;
	const char * cmd;
}CARIMMPL_MENU_ST;

//==============================================================================
//                              FUNCTIONS
//==============================================================================
void carimpl_menu_hander(char *msgBuf, MI_S32 msgBufLen);

#endif //#define _CARIMPL_MENU_H_
