/*===========================================================================
* Include file
*===========================================================================*/
#include <string.h>
#include "MenuCommon.h"

/*===========================================================================
* Local function
*===========================================================================*/
static int MenuSetting_Get_General(int icur_val, va_list *ap);
static int ParamFun(SETTING_ATOM *pa, int idx, char *szVal, void *arg);
static int ParamFun_adjustParam(SETTING_ATOM *pa, int idx, char *szVal, void *arg);
/*===========================================================================
* Extern varible
*===========================================================================*/

/*===========================================================================
* Global varible :
*===========================================================================*/
static MenuInfo gCurMenuInfo = {0};

static MENU_ATOMS  temp_menu_atoms;
static MENU_ATOMS  menu_atoms =
{
	MAGIC_NUM,
	HDRTAIL_NUM,
	{
		// Movie
		{/* 001 */COMMON_KEY_MOVIE_SIZE, 0, MENU_MOVIE_SIZE_EN, ParamFun, MenuSetting_Get_General,
			{
			#if MENU_MOVIE_SIZE_4K_25P_EN
			"4K", MOVIE_SIZE_4K_25P,
			#endif
			#if (MENU_MOVIE_SIZE_1440_30P_EN)
			"2K", MOVIE_SIZE_1440_30P,
			#endif
			#if MENU_MOVIE_SIZE_SHD_30P_EN
			"1296P30fps", MOVIE_SIZE_SHD_30P,
			#endif
			#if MENU_MOVIE_SIZE_SHD_25P_EN
			"1296P25fps", MOVIE_SIZE_SHD_25P,
			#endif
			#if MENU_MOVIE_SIZE_1080P_30_HDR_EN
			"1080P30fps_hdr", MOVIE_SIZE_1080_30P_HDR,
			#endif
			#if MENU_MOVIE_SIZE_1080_60P_EN
			"1080P60fps", MOVIE_SIZE_1080_60P,
			#endif
			#if MENU_MOVIE_SIZE_1080_25P_EN
			"1080P25fps", MOVIE_SIZE_1080_25P,
			#endif
			#if MENU_MOVIE_SIZE_1080P_EN
			"1080P", MOVIE_SIZE_1080P,
			#endif
			#if (MENU_MOVIE_SIZE_960P_30P_EN)
			"960P30fps", MOVIE_SIZE_960P_30P,
			#endif
			#if MENU_MOVIE_SIZE_900P_30P_EN
			"900P30fps", MOVIE_SIZE_900P_30P,
			#endif
			#if MENU_MOVIE_SIZE_720P_EN
			"720P30fps", MOVIE_SIZE_720P,
			#endif
			#if MENU_MOVIE_SIZE_720_120P_EN
			"720P120fps", MOVIE_SIZE_720_120P,
			#endif
			#if MENU_MOVIE_SIZE_720_60P_EN
			"720P60fps", MOVIE_SIZE_720_60P,
			#endif
			#if MENU_MOVIE_SIZE_720_50P_EN
			"720P50fps", MOVIE_SIZE_720_50P,
			#endif
			#if MENU_MOVIE_SIZE_VGA30P_EN
			"360P30fps", MOVIE_SIZE_VGA30P,
			#endif
			NULL, 0,
			}
		},
		{/* 002 */COMMON_KEY_VR_CLIP_TIME, 2, MENU_MOVIE_CLIP_TIME_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_MOVIE_CLIP_TIME_OFF_EN)
			"OFF", MOVIE_CLIP_TIME_OFF,
			#endif
			#if (MENU_MOVIE_CLIP_TIME_1MIN_EN)
			"1MIN", MOVIE_CLIP_TIME_1MIN,
			#endif
			#if (MENU_MOVIE_CLIP_TIME_2MIN_EN)
			"2MIN", MOVIE_CLIP_TIME_2MIN,
			#endif
			#if (MENU_MOVIE_CLIP_TIME_3MIN_EN)
			"3MIN", MOVIE_CLIP_TIME_3MIN,
			#endif
			#if (MENU_MOVIE_CLIP_TIME_5MIN_EN)
			"5MIN", MOVIE_CLIP_TIME_5MIN,
			#endif
			#if (MENU_MOVIE_CLIP_TIME_10MIN_EN)
			"10MIN", MOVIE_CLIP_TIME_10MIN,
			#endif
			#if (MENU_MOVIE_CLIP_TIME_30MIN_EN)
			"30MIN", MOVIE_CLIP_TIME_30MIN,
			#endif
			NULL, 0
			},
		},
		{/* 003 */COMMON_KEY_RECD_SOUND, 0, MENU_MOVIE_SOUND_RECORD_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_MOVIE_SOUND_RECORD_ON_EN)
			"ON", MOVIE_SOUND_RECORD_ON,
			#endif
			#if (MENU_MOVIE_SOUND_RECORD_OFF_EN)
			"OFF", MOVIE_SOUND_RECORD_OFF,
			#endif
			NULL, 0
			},
		},
		// General
		{/* 004 */COMMON_DATE_TIME_FMT, 1, MENU_GENERAL_DATE_FORMAT_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_DATE_FORMAT_NONE_EN)
			"NONE", DATETIME_SETUP_NONE,
			#endif
			#if (MENU_GENERAL_DATE_FORMAT_YMD_EN)
			"YMD", DATETIME_SETUP_YMD,
			#endif
			#if (MENU_GENERAL_DATE_FORMAT_MDY_EN)
			"MDY", DATETIME_SETUP_MDY,
			#endif
			#if (MENU_GENERAL_DATE_FORMAT_DMY_EN)
			"DMY", DATETIME_SETUP_DMY,
			#endif
			NULL, 0
			},
		},
		{/* 005 */COMMON_KEY_LANGUAGE,       0, MENU_GENERAL_LANGUAGE_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
			"English", LANGUAGE_ENGLISH,
			#endif
			#if (MENU_GENERAL_LANGUAGE_SPANISH_EN)
			"Spanish", LANGUAGE_SPANISH,
			#endif
			#if (MENU_GENERAL_LANGUAGE_PORTUGUESE_EN)
			"Portuguese", LANGUAGE_PORTUGUESE,
			#endif
			#if (MENU_GENERAL_LANGUAGE_RUSSIAN_EN)
			"Russian", LANGUAGE_RUSSIAN,
			#endif
			#if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
			"Simplified", LANGUAGE_SCHINESE,
			#endif
			#if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
			"Traditional", LANGUAGE_TCHINESE,
			#endif
			#if (MENU_GENERAL_LANGUAGE_GERMAN_EN)
			"German", LANGUAGE_GERMAN,
			#endif
			#if (MENU_GENERAL_LANGUAGE_ITALIAN_EN)
			"Italian", LANGUAGE_ITALIAN,
			#endif
			#if (MENU_GENERAL_LANGUAGE_LATVIAN_EN)
			"Latvian", LANGUAGE_LATVIAN,
			#endif
			#if (MENU_GENERAL_LANGUAGE_POLISH_EN)
			"Polish", LANGUAGE_POLISH,
			#endif
			#if (MENU_GENERAL_LANGUAGE_ROMANIAN_EN)
			"Romanian", LANGUAGE_ROMANIAN,
			#endif
			#if (MENU_GENERAL_LANGUAGE_SLOVAK_EN)
			"Slovak", LANGUAGE_SLOVAK,
			#endif
			#if (MENU_GENERAL_LANGUAGE_UKRANINIAN_EN)
			"Ukraninian", LANGUAGE_UKRANINIAN,
			#endif
			#if (MENU_GENERAL_LANGUAGE_FRENCH_EN)
			"French", LANGUAGE_FRENCH,
			#endif
			#if (MENU_GENERAL_LANGUAGE_JAPANESE_EN)
			"Japanese", LANGUAGE_JAPANESE,
			#endif
			#if (MENU_GENERAL_LANGUAGE_KOREAN_EN)
			"Korean", LANGUAGE_KOREAN,
			#endif
			#if (MENU_GENERAL_LANGUAGE_CZECH_EN)
			"Czech", LANGUAGE_CZECH,
			#endif
			#if (MENU_GENERAL_LANGUAGE_VIETNAMESE_EN)
			"Vietnamese", LANGUAGE_VIETNAMESE,
			#endif
			NULL, 0
			},
		},
		{/* 006 */COMMON_KEY_RESET,          0, MENU_GENERAL_RESET_SETUP_EN, ParamFun, NULL,
			"NO", RESET_NO  ,
			"YES", RESET_YES  ,
			{NULL, 0},
		},
		{/* 007 */COMMON_KEY_FLICKER,          0, MENU_GENERAL_FLICKER_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_FLICKER_50HZ_EN)
			"50HZ", FLICKER_50HZ  ,
			#endif
			#if (MENU_GENERAL_FLICKER_60HZ_EN)
			"60HZ", FLICKER_60HZ  ,
			#endif
			NULL, 0
			},
		},
		{/* 008 */COMMON_KEY_LCD_POWERS,          0, MENU_GENERAL_LCD_POWER_SAVE_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_LCD_POWER_SAVE_OFF_EN)
			"OFF", LCD_POWER_SAVE_OFF,
			#endif
			#if (MENU_GENERAL_LCD_POWER_SAVE_10SEC_EN)
			"10SEC", LCD_POWER_SAVE_10SEC,
			#endif
			#if (MENU_GENERAL_LCD_POWER_SAVE_30SEC_EN)
			"30SEC", LCD_POWER_SAVE_30SEC,
			#endif
			#if (MENU_GENERAL_LCD_POWER_SAVE_1MIN_EN)
			"1MIN", LCD_POWER_SAVE_1MIN,
			#endif
			#if (MENU_GENERAL_LCD_POWER_SAVE_3MIN_EN)
			"3MIN", LCD_POWER_SAVE_3MIN,
			#endif
			#if (MENU_GENERAL_LCD_POWER_SAVE_5MIN_EN)
			"5MIN", LCD_POWER_SAVE_5MIN,
			#endif
			NULL, 0
			},
		},
		{/* 009 */COMMON_KEY_BEEP,  0, MENU_GENERAL_KEY_TONE_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_KEY_TONE_ON_EN)
			"ON", BEEP_ON,
			#endif
			#if (MENU_GENERAL_KEY_TONE_OFF_EN)
			"OFF", BEEP_OFF,
			#endif
			"LOW", BEEP_LOW,
			"MID", BEEP_MID,
			"HIGH", BEEP_HIGH,
			NULL, 0
			},
		},
		{/* 010 */COMMON_KEY_SHUTDOWN_TONE,  0, MENU_GENERAL_SHUTDOWN_TONE_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_SHUTDOWN_TONE_ON_EN)
			"ON", SHUTDOWN_TONE_ON,
			#endif
			#if (MENU_GENERAL_SHUTDOWN_TONE_OFF_EN)
			"OFF", SHUTDOWN_TONE_OFF,
			#endif
			"LOW", SHUTDOWN_TONE_LOW,
			"MID", SHUTDOWN_TONE_MID,
			"HIGH", SHUTDOWN_TONE_HIGH,
			NULL, 0
			},
		},
		{/* 011 */COMMON_KEY_GSENSOR_SENS, 3, MENU_GENERAL_GSENSOR_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_GSENSOR_OFF_EN)
			"OFF", GSENSOR_SENSITIVITY_OFF,
			#endif
			#if (MENU_GENERAL_GSENSOR_LEVEL0_EN)
			#if (MENU_GENERAL_GSENSOR_EN == GSENSOR_3_LEVEL)
			"LOW", GSENSOR_SENSITIVITY_L0,
			#else
			"LEVEL0", GSENSOR_SENSITIVITY_L0,
			#endif
			#endif
			#if (MENU_GENERAL_GSENSOR_LEVEL1_EN)
			"LEVEL1", GSENSOR_SENSITIVITY_L1,
			#endif
			#if (MENU_GENERAL_GSENSOR_LEVEL2_EN)
			#if (MENU_GENERAL_GSENSOR_EN == GSENSOR_3_LEVEL)
			"MID", GSENSOR_SENSITIVITY_L2,
			#else
			"LEVEL2", GSENSOR_SENSITIVITY_L2,
			#endif
			#endif
			#if (MENU_GENERAL_GSENSOR_LEVEL3_EN)
			"LEVEL3", GSENSOR_SENSITIVITY_L3,
			#endif
			#if (MENU_GENERAL_GSENSOR_LEVEL4_EN)
			#if (MENU_GENERAL_GSENSOR_EN == GSENSOR_3_LEVEL)
			"HIGH", GSENSOR_SENSITIVITY_L4,
			#else
			"LEVEL4", GSENSOR_SENSITIVITY_L4,
			#endif
			#endif
			NULL, 0
			},
		},
		{/* 012 */COMMON_KEY_DATE_LOGO_DISPLAY, 0, MENU_GENERAL_DATE_LOGO_DISPLAY_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_DATE_LOGO_DISPLAY_ON_EN)
			"ON", DATE_LOGO_DISPLAY_ON,
			#endif
			#if (MENU_GENERAL_DATE_LOGO_DISPLAY_OFF_EN)
			"OFF", DATE_LOGO_DISPLAY_OFF,
			#endif
			NULL, 0
			},
		},
		{/* 013 */COMMON_KEY_WIFI_EN,       0, MENU_GENERAL_WIFI_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_WIFI_OFF_EN)
			"OFF", WIFI_MODE_OFF,
			#endif
			#if (MENU_GENERAL_WIFI_ON_EN)
			//"ON", WIFI_MODE_ON,
			"2.4G", WIFI_MODE_ON,
			#endif
			#if (MENU_GENERAL_WIFI_2_4G_EN)
			"2.4G", WIFI_MODE_2_4G,
			#endif
			#if (MENU_GENERAL_WIFI_5G_EN)
			"5G", WIFI_MODE_5G,
			#endif
			NULL, 0
			},
		},
		{/* 014 */COMMON_KEY_STSTEM_VOLUME, 0, MENU_SYSTEM_VOLUME_EN, ParamFun_adjustParam, MenuSetting_Get_General,
			{
			#if (MENU_SYSTEM_VOLUME_LV0_EN)
			"00",VOLUME_00,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV1_EN)
			"01",VOLUME_01,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV2_EN)
			"02",VOLUME_02,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV3_EN)
			"03",VOLUME_03,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV4_EN)
			"04",VOLUME_04,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV5_EN)
			"05",VOLUME_05,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV6_EN)
			"06",VOLUME_06,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV7_EN)
			"07",VOLUME_07,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV8_EN)
			"08",VOLUME_08,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV9_EN)
			"09",VOLUME_09,
			#endif
			#if (MENU_SYSTEM_VOLUME_LV10_EN)
			"10",VOLUME_10,
			#endif
			NULL, 0},
		},
		{/* 015 */COMMON_KEY_LCD_BACKLIGHT, 0, MENU_SYSTEM_BACKLIGHT_EN, ParamFun_adjustParam, MenuSetting_Get_General,
			{
			#if (MENU_SYSTEM_BACKLIGHT_LV0_EN)
			"00",BACKLIGHT_00,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV1_EN)
			"01",BACKLIGHT_01,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV2_EN)
			"02",BACKLIGHT_02,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV3_EN)
			"03",BACKLIGHT_03,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV4_EN)
			"04",BACKLIGHT_04,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV5_EN)
			"05",BACKLIGHT_05,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV6_EN)
			"06",BACKLIGHT_06,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV7_EN)
			"07",BACKLIGHT_07,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV8_EN)
			"08",BACKLIGHT_08,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV9_EN)
			"09",BACKLIGHT_09,
			#endif
			#if (MENU_SYSTEM_BACKLIGHT_LV10_EN)
			"10",BACKLIGHT_10,
			#endif
			NULL, 0},
		},
		// ReversingLine
		{/* 016 */COMMON_KEY_REVERSING_LINE, 0, MENU_GENERAL_REVERSING_LINE_EN, ParamFun, MenuSetting_Get_General,
			{
			#if (MENU_GENERAL_REVERSING_LINE_ON_EN)
			"ON", REVERSING_LINE_ON,
			#endif
			#if (MENU_GENERAL_REVERSING_LINE_OFF_EN)
			"OFF", REVERSING_LINE_OFF,
			#endif
			NULL, 0
			},
		},
		{/* 017 */COMMON_REVERSING_LEFT_TOP_X, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 018 */COMMON_REVERSING_LEFT_TOP_Y, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 019 */COMMON_REVERSING_LEFT_BOTTOM_X, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 020 */COMMON_REVERSING_LEFT_BOTTOM_Y, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 021 */COMMON_REVERSING_LEFT_MIDDLE1_RATIO, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 022 */COMMON_REVERSING_LEFT_MIDDLE2_RATIO, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 023 */COMMON_REVERSING_RIGHT_TOP_X, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 024 */COMMON_REVERSING_RIGHT_TOP_Y, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 025 */COMMON_REVERSING_RIGHT_BOTTOM_X, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 026 */COMMON_REVERSING_RIGHT_BOTTOM_Y, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 027 */COMMON_REVERSING_RIGHT_MIDDLE1_RATIO, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		{/* 028 */COMMON_REVERSING_RIGHT_MIDDLE2_RATIO, 0, 1, NULL, NULL,
			{NULL, 0},
		},
		//version
		{/* 029 */"version",       0, MENU_GENERAL_FW_VERSION_EN, NULL, NULL,
			{
			"v1.0.0", 0,
			NULL, 0
			},
		},
		// Playback
		{/* 031 */"PlaybackVideoType", 0, MENU_PLAYBACK_VIDEO_TYPE_EN, ParamFun, MenuSetting_Get_General,
			{NULL, 0},
		},
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* NEW!! Extension setting */

		/* END OF MARK */
		{NULL,                      0, 0, NULL,       NULL,
			{NULL, 0},
		}
	}
};

/*===========================================================================
* Main body
*===========================================================================*/
bool Menu_General_EnGet(char *key, char *value, int len)
{
	FILE * fp = NULL;
	char str[64] = {0};
	bool ret = true;

	sprintf(str, "%s%s", GET_CONFIG_SETTING, key);
	printf("get [%s]\n", str);
	fp = popen(str, "r");
	if (fp == NULL)
		return false;

	if (value != NULL) {
		while (fgets(value, len, fp) != NULL) {
			if (value[strlen(value) - 1] == '\n')
				value[strlen(value) - 1] = '\0';
		}
	}
	pclose(fp);
	return ret;
}

bool Menu_General_EnSet(const char *key, char *value)
{
	bool ret = true;
	int status = 0;
	char str[64] = {0};
	sprintf(str, "%s%s %s", SET_CONFIG_SETTING, key, value);
	printf("set [%s]\n", str);
	status = system(str);
	if (status < 0) {
		printf("[%s]fail\n", str);
		ret = false;
	}
	return ret;
}

static int MenuSetting_Get_General(int icur_val, va_list *ap)
{
	int* pSetting;

	pSetting = va_arg(*ap, int *); // Get next argument (note: typedef must be correct)
	*pSetting = icur_val;
	return 0;
}

//menu_atoms.sAtom[i].fnSet(&menu_atoms.sAtom[0], i, val, pmi);
static int ParamFun(SETTING_ATOM *pa, int idx, char *szVal, void *arg)
{
	int    *p; // point to MenuInfo
	int     v = 0;
	int     i = 0;

	/* pmi is menu struct (MenuInfo), here use it as char* to set value.
	* BE CAREFUL the order of MenuInfo and SETTING_ATOM must be same! */
	p = (int *) arg;

	for (i = 0; i < NUMBER_ITEM_VALS; i++) {
		if ((pa+idx)->szItemMaps[i].szItemKey != NULL) {
			if (strcmp(szVal, (pa+idx)->szItemMaps[i].szItemKey) == 0) {
				v = (pa+idx)->szItemMaps[i].nItemValue;
				break;
			} else
				v = 0;
		} else
			v = 0;
	}

	(pa + idx)->nSVal = v;

	if (idx  < (int)sizeof(MenuInfo)/4)
	{
		*(p + idx) = (int)(pa + idx)->nSVal;
	}

	return  0;
}

#define MENU_MANUAL_ADJUST_PARAM_MAX    100
#define MENU_MANUAL_ADJUST_PARAM_MIN    0
static int ParamFun_adjustParam(SETTING_ATOM *pa, int idx, char *szVal, void *arg)
{
	int    *p; // point to MenuInfo
	int     v = 0;

	/* pmi is menu struct (MenuInfo), here use it as char* to set value.
	* BE CAREFUL the order of MenuInfo and SETTING_ATOM must be same! */
	p = (int *) arg;

	v = atoi(szVal);
	if (v < MENU_MANUAL_ADJUST_PARAM_MIN) v = MENU_MANUAL_ADJUST_PARAM_MIN;
	if (v > MENU_MANUAL_ADJUST_PARAM_MAX) v = MENU_MANUAL_ADJUST_PARAM_MAX;

	(pa + idx)->nSVal = v;

	if (idx  < (int)sizeof(MenuInfo)/4)
	{
		*(p + idx) = (int)(pa + idx)->nSVal;
	}

	return  0;
}

/* Read cgi_config.bin  File Setting to MENU_ATOMS and MenuInfo */
int ParseMenuSet(const char *file, MenuInfo* pmi /*out*/)
{
	FILE *fp = NULL;
	char file_buf[128] = {0};
	
	//printf("open:%s\n",file);
	fp = fopen(file, "r");
	if (fp == NULL)
		return -1;

	while (!feof(fp)) {
		memset(file_buf, 0, sizeof(file_buf));
		if (fgets(file_buf, sizeof(file_buf), fp) != NULL) {
			char    *key, *val;
			int     i;

			if (strstr(file_buf, ";;\n") == NULL)
				continue;
			key = strtok(file_buf, "=");
			val = strtok(NULL, ";;\n");
			if (key == NULL)
				continue;
			if (val == NULL)
				continue;
			
			for (i = 0; i< NUMBER_SETTING && menu_atoms.sAtom[i].szSKey != NULL; i++) {
				if (strcmp(key, menu_atoms.sAtom[i].szSKey) == 0) {
					//printf("%d:%s=%s\n",i,key,val);
					if (menu_atoms.sAtom[i].fnSet != NULL)
						menu_atoms.sAtom[i].fnSet(&menu_atoms.sAtom[0], i, val, pmi);
					break;
				}
			}
		}
	}
	fclose(fp);
	return 0;
}

/*Get MENU_ATOMS value*/
bool MenuSetting_GetCB(char *cmenu_string, ...)
{
	bool retVal = false;
	int uloop;
	va_list ap;

	for (uloop = 0; menu_atoms.sAtom[uloop].szSKey != NULL; uloop++){
		//case sensitive
		if (/*strcmpi*/strcmp(cmenu_string, menu_atoms.sAtom[uloop].szSKey) == 0){
			va_start(ap, cmenu_string);     // Set pointer to 1st argument of "Varaible List"
			if (menu_atoms.sAtom[uloop].fnGet != NULL){
				retVal = menu_atoms.sAtom[uloop].fnGet(menu_atoms.sAtom[uloop].nSVal, &ap);
			}
			va_end(ap);
			return retVal;
		}
	}

	return false;
}

bool ImportMenuInfo(MenuInfo *pmi)
{
	int *p;
	unsigned int i = 0;

	p = (int *)pmi;

	for (i = 0; i < sizeof(MenuInfo)/4; i++) {
		menu_atoms.sAtom[i].nSVal = *(p + i);
	}
	return true;
}

int ExportMenuInfo(const char *file)
{
	unsigned int i = 0;
	int nValue = 0;
	char szValue[20] = {0};
	for (i = 0; i < sizeof(MenuInfo)/4; i++) {
		if (menu_atoms.sAtom[i].szSKey != NULL &&
				menu_atoms.sAtom[i].nSVal != temp_menu_atoms.sAtom[i].nSVal) {
			nValue = menu_atoms.sAtom[i].nSVal;

			//Caution:set gamma will make a "Segmentation fault"
			if ((nValue < NUMBER_ITEM_VALS) && menu_atoms.sAtom[i].szItemMaps[nValue].szItemKey != NULL) {
				strcpy(szValue, menu_atoms.sAtom[i].szItemMaps[nValue].szItemKey);
			}
			else {
				sprintf(szValue, "%d", nValue);
			}
			Menu_General_EnSet(menu_atoms.sAtom[i].szSKey, szValue);
			break;
		}
	}
	return 0;
}

bool MenuSetting_CheckMenuAtoms(void)
{
	if (memcmp((char *) &temp_menu_atoms, (char *) &menu_atoms, sizeof(MENU_ATOMS)) == 0) {
		return true;
	}
	printf("Menu setting is changed\r\n");
	return false;
}

void MenuSetting_BackupMenuAtoms(void)
{
	memcpy((char *) &temp_menu_atoms, (char *) &menu_atoms, sizeof(MENU_ATOMS));
}

void Menu_WriteSetting(void)
{
	ImportMenuInfo(&gCurMenuInfo);

	if (MenuSetting_CheckMenuAtoms() == false) {
		//ImportMenuInfo(&gCurMenuInfo);
		ExportMenuInfo(MenuCfgFile);
		MenuSetting_BackupMenuAtoms();
	}
}

pMenuInfo MenuSettingConfig(void)
{
	return &gCurMenuInfo;
}

void DefaultMenuSettingInit(void)
{
	#if MENU_MOVIE_SIZE_EN
	gCurMenuInfo.uiMOVSize              = MOVIE_SIZE_1080_30P;
	#endif
	#if MENU_MOVIE_CLIP_TIME_EN
	gCurMenuInfo.uiMOVClipTime          = MOVIE_CLIP_TIME_1MIN;
	#endif
	#if MENU_MOVIE_SOUND_RECORD_EN
	gCurMenuInfo.uiMOVSoundRecord       = MOVIE_SOUND_RECORD_ON;
	#endif
	#if MENU_GENERAL_BEEP_EN
	gCurMenuInfo.uiBeep                 = BEEP_LOW;
	#endif
	#if MENU_SYSTEM_BACKLIGHT_EN
	gCurMenuInfo.uiLCDBacklight         = BACKLIGHT_08;
	#endif
	#if MENU_GENERAL_DATE_FORMAT_EN
	gCurMenuInfo.uiDateTimeFormat       = DATETIME_SETUP_YMD;
	#endif
	#if MENU_GENERAL_LANGUAGE_EN
	gCurMenuInfo.uiLanguage             = LANGUAGE_ENGLISH;
	#endif
	#if MENU_GENERAL_RESET_SETUP_EN
	gCurMenuInfo.uiResetSetting         = 0;
	#endif
	#if MENU_GENERAL_FLICKER_EN
	gCurMenuInfo.uiFlickerHz            = FLICKER_50HZ;
	#endif
	#if MENU_GENERAL_LCD_POWER_SAVE_EN
	gCurMenuInfo.uiLCDPowerSave         = LCD_POWER_SAVE_OFF;
	#endif
	#if MENU_GENERAL_GSENSOR_OFF_EN
	gCurMenuInfo.uiGsensorSensitivity   = GSENSOR_SENSITIVITY_OFF;
	#endif
	#if MENU_GENERAL_WIFI_EN
	gCurMenuInfo.uiWifi                 = WIFI_MODE_OFF;
	#endif
}

void MenuSettingInit(void)
{
	if (ParseMenuSet(MenuCfgFile, MenuSettingConfig()) != 0) {
		printf("###############################\r\n"
				"--E-- No Default Settings\r\n"
				"###############################\r\n");
		DefaultMenuSettingInit();
	}
	ImportMenuInfo(&gCurMenuInfo);
	MenuSetting_BackupMenuAtoms();
	ListAllMenuSetting(&gCurMenuInfo);
}

void ListAllMenuSetting(MenuInfo *Info)
{
	printf("---------------------------------------------------\n");
	printf(" uiMOVSize              = %d \n", Info->uiMOVSize          );
	printf(" uiMOVClipTime          = %d \n", Info->uiMOVClipTime      );
	printf(" uiMOVSoundRecord       = %d \n", Info->uiMOVSoundRecord   );

	printf(" uiBeep                 = %d \n", Info->uiBeep             );
	printf(" uiLCDBacklight         = %d \n", Info->uiLCDBacklight     );
	printf(" uiDateTimeFormat       = %d \n", Info->uiDateTimeFormat   );
	printf(" uiLanguage             = %d \n", Info->uiLanguage         );
	printf(" uiResetSetting         = %d \n", Info->uiResetSetting     );
	printf(" uiFlickerHz            = %d \n", Info->uiFlickerHz        );
	printf(" uiLCDPowerSave         = %d \n", Info->uiLCDPowerSave     );
	printf(" uiGsensorSensitivity   = %d \n", Info->uiGsensorSensitivity);
	printf(" uiWifi                 = %d \n", Info->uiWifi);
	printf(" uiVersion              = %d \n", Info->uiVersion);
	printf("---------------------------------------------------\n");
}

int MenuSetting_GetStringPool(const char *file)
{
	FILE *fp = NULL;
	char stringpool[32][IDS_DS_END] = {0};
	int i = 0;

	fp = fopen(file, "r+");
	if (fp == NULL)
		return -1;

	while (!feof(fp)) {
		fgets(stringpool[i], 32, fp);
		printf("%d=%s\n", i, stringpool[i]);
		i++;
	}
	fclose(fp);
	return 0;
}
