/*
* OSDStringCommon.c
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#ifndef _OSD_STRING_COMMON_H_
#define _OSD_STRING_COMMON_H_

#include "lvgl/lvgl.h"
#include "ui_common.h"

typedef struct _OSD_STRING {
    int       nItemValue;
    char*     osd_strings;
} OSD_STRING;

OSD_STRING *osdstringpool;

const lv_font_t *font_montserrat_14;
const lv_font_t *font_montserrat_16;
const lv_font_t *font_montserrat_18;
const lv_font_t *font_montserrat_20;
const lv_font_t *font_montserrat_22;
const lv_font_t *font_montserrat_24;
const lv_font_t *font_montserrat_26;
const lv_font_t *font_montserrat_28;
const lv_font_t *font_montserrat_30;
const lv_font_t *font_montserrat_40;

void updata_font(int type);
void updata_osd_string(int type);

#endif //_OSD_STRING_COMMON_H_
