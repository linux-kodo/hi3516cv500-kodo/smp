/*
* OSDStringCommon.c
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include <string.h>
#include "MenuCommon.h"
#include "OSDStringCommon.h"

OSD_STRING osdstringpoolEnglish[] = {
IDS_DS_EMPTY," ",
IDS_DS_NO,"NO",
IDS_DS_YES,"YES",
IDS_DS_ON,"On",
IDS_DS_OFF,"Off",
IDS_DS_LOW,"Low",
IDS_DS_MIDDLE,"Middle",
IDS_DS_HIGH,"High",
IDS_DS_STANDARD,"Standard",
IDS_DS_NONE,"None",
IDS_DS_OK,"OK",
IDS_DS_CANCEL,"Cancel",
IDS_TIME_10SEC,"10 sec",
IDS_TIME_30SEC,"30 sec",
IDS_TIME_1MIN,"1Minute",
IDS_TIME_2MIN,"2Minutes",
IDS_TIME_3MIN,"3Minutes",
IDS_TIME_5MIN,"5Minutes",
IDS_TIME_10MIN,"10Minutes",
IDS_TIME_30MIN,"30Minutes",

IDS_DS_MOVIE_MODE,"Video Resolution",
IDS_DS_MOVIE_MODE_4K_25,"4K",
IDS_DS_MOVIE_MODE_1440_30,"2K",
IDS_DS_MOVIE_MODE_SHD,"SHD 30fps",
IDS_DS_MOVIE_MODE_SHD_25P,"SHD 25fps",
IDS_DS_MOVIE_MODE_FHD_60P,"FHD 60fps",
IDS_DS_MOVIE_MODE_FHD,"1080P",
IDS_DS_MOVIE_MODE_FHD_HDR,"FHD HDR 30fps",
IDS_DS_MOVIE_MODE_HD,"HD 30fps",
IDS_DS_MOVIE_MODE_HD_60P,"HD 60fps",
IDS_DS_MOVIE_MODE_VGA_30P,"VGA 30fps",

IDS_MOVIE_CLIPTIME,"Loop Recording",

IDS_MOVIE_SOUND_RECORD,"Sound Record",

IDS_DS_CLOCK_SETTINGS,"Data/Time",
IDS_DS_DATETIME_Y,"Y",
IDS_DS_DATETIME_M,"M",
IDS_DS_DATETIME_D,"D",
IDS_DS_DATETIME_H,"H",
IDS_DS_DATETIME_MM,"M",
IDS_DS_DATETIME_S,"S",

IDS_DS_DATETIME_FORMAT,"Data/Time Format",
IDS_DS_DATETIME_YMD,"YY/MM/DD",
IDS_DS_DATETIME_MDY,"MM/DD/YY",
IDS_DS_DATETIME_DMY,"DD/MM/YY",

IDS_DS_LANGUAGE,"Language",
IDS_DS_LANGUAGES_ENGLISH,"English",
IDS_DS_LANGUAGES_SPANISH,"Español",
IDS_DS_LANGUAGES_PORTUGUESE,"Português",
IDS_DS_LANGUAGES_RUSSIAN,"Русский",
IDS_DS_LANGUAGES_SCHINESE,"简体中文",
IDS_DS_LANGUAGES_TCHINESE,"繁体中文",
IDS_DS_LANGUAGES_GERMAN,"Deutsch",
IDS_DS_LANGUAGES_ITALIAN,"Italiano",
IDS_DS_LANGUAGES_LATVIAN,"Latviski",
IDS_DS_LANGUAGES_POLISH,"Polski",
IDS_DS_LANGUAGES_ROMANIAN,"Roma",
IDS_DS_LANGUAGES_SLOVAK,"slovenčina",
IDS_DS_LANGUAGES_UKRANINIAN,"українська",
IDS_DS_LANGUAGES_FRENCH,"Français",
IDS_DS_LANGUAGES_KOREAN,"한국어",
IDS_DS_LANGUAGES_JAPANESE,"日本語",
IDS_DS_LANGUAGES_CZECH,"Čeština",
IDS_DS_LANGUAGES_VIETNAMESE,"việt nam",
IDS_DS_LANGUAGE_DESCRIPTION,"Select language",

IDS_DS_RESET_SETUP,"Default Settings",
IDS_DS_RESET_SETUP_CONFIRM,"Restore Default Settins?",
IDS_DS_RESET_INFO,"All setting will be reset to default, And reboot system!",

IDS_DS_FW_VERSION_INFO,"Version Info",
IDS_DS_FW_VERSION,"Firmware Version",

IDS_DS_QR_CODE_INFO,"QR-Code",
IDS_DS_QR_CODE,"QR-Code",

IDS_DS_FLICKER,"Frequency",
IDS_FLICKER_50HZ,"50 Hz",
IDS_FLICKER_60HZ,"60 Hz",

IDS_DS_REVERSING_LINE,"Reversing Line",
IDS_REVERSING_LINE_ON,"ON",
IDS_REVERSING_LINE_OFF,"OFF",

IDS_DS_LCD_POWER_SAVE,"Screen Saver",

IDS_DS_KEY_TONE,"Key Tone",
IDS_DS_SHUTDOWN_TONE,"Boot Ringtone",

IDS_DS_GSENSOR_SENSITIVETY,"G-Sensor",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL0,"Level 0",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL1,"Level 1",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL2,"Level 2",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL3,"Level 3",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL4,"Level 4",

IDS_DS_DATE_LOGO_DISPLAY,"Data/Time/Logo Dispaly",

IDS_DS_FORMAT_SD_CARD,"Format SD-Card",
IDS_DS_FORMAT_CARD_CONFIRM,"Format Card?",
IDS_DS_DATA_DELETED,"All data will be deleted.",
IDS_DS_MSG_LOCK_CURRENT_FILE,"Lock Current File",
IDS_DS_MSG_UNLOCK_FILE,"Unlock files",
IDS_DS_MSG_DELETE_FILE_OK,"Delete File Complete",
IDS_DS_MSG_PROTECT_FILE_OK,"Protect File Complete",
IDS_DS_MSG_NO_FILE,"No file",
IDS_DS_MSG_SURE_TO_DELETE_SELETED,"Delete Selected File?",
IDS_DS_MSG_DELETE_SELETED_ERROR,"Delete Selected File Error!",
IDS_DS_MSG_CANNOT_DELETE,"Cannot delete protected files!",
IDS_DS_MSG_SURE_TO_PROTECT_SELETED,"Protect Selected File?",
IDS_DS_MSG_SURE_TO_UNPROTECT_SELETED,"UnProtect Selected File?",
IDS_DS_MSG_CARD_ERROR,"SD card is not formatted correctly.",
IDS_DS_MSG_STORAGE_FULL,"This storage is full!",
IDS_DS_MSG_NO_CARD,"SD card is not inserted.",
IDS_DS_MSG_PLUG_OUT_SD,"SD Card Is Plug Out",
IDS_DS_MSG_WAIT_INITIAL_DONE,"Wait SD Card Initialization Done",
IDS_DS_MSG_INSERT_SD_AGAIN,"Please Insert SD Card Again",
IDS_DS_MSG_FORMAT_SD_PROCESSING,"SD Card Format is Processing",
IDS_DS_MSG_FORMAT_SD_CARD_OK,"Format SD Card Complete",
IDS_DS_MSG_FORMAT_SD_CARD_FAIL,"Format SD Card Fail",
IDS_DS_MSG_FORMAT_SD_CARD,"Please Format SD Card",
IDS_DS_MSG_GOTO_POWER_OFF,"Going Power Off",
IDS_DS_MSG_OPENFILE_WAIT,"Please Wait ...",
IDS_DS_MSG_ACC_OFF_TIMELAPSE_REC,"After 5 seconds, Startup of video recording",
IDS_DS_MSG_ACC_ON_NORMAL_REC,"Normal video startup",
IDS_DS_15DAY,"15 Days",
IDS_DS_30DAY,"30 Days",
IDS_DS_60DAY,"60 Days",
IDS_DS_GENERAL_SETTINGS,"General Settings",
IDS_DS_SYSTEM_SETTINGS,"System Settings",
IDS_DS_VIDEO_SETTINGS,"Video Settings",
IDS_DS_CAMID_AND_VIDEO_TYPE,"Front/Rear Video",
IDS_DS_VIDEO_TYPE_NORMAL_1ST,"Front Normal Video",
IDS_DS_VIDEO_TYPE_NORMAL_2ST,"Rear Normal Video",
IDS_DS_VIDEO_TYPE_PARKING_1ST,"Front Parking Video",
IDS_DS_VIDEO_TYPE_PARKING_2ST,"Rear Parking Video",
IDS_DS_VIDEO_TYPE_EMERGENCY_1ST,"Front Emergency Video",
IDS_DS_VIDEO_TYPE_EMERGENCY_2ST,"Rear Emergency Video",
IDS_DS_VIDEO_TYPE_PHOTO_1ST,"Front Photo",
IDS_DS_VIDEO_TYPE_PHOTO_2ST,"Rear Photo",
IDS_DS_2_4G,"2.4G",
IDS_DS_5G,"5G",
IDS_DS_WIFI,"Wifi Mode",
IDS_DS_PASSWORD_INFO,"PassWord:",
IDS_DS_WIFI_SSID_INFO,"WIFI SSID:",
IDS_DS_NOTE,"Notice",
IDS_DS_WEEK_SUNDAY,"Sunday",
IDS_DS_WEEK_MONDAY,"Monday",
IDS_DS_WEEK_TUESDAY,"Tuesday",
IDS_DS_WEEK_WEDNESDAY,"Wednesday",
IDS_DS_WEEK_THURSDAY,"Thursday",
IDS_DS_WEEK_FRIDAY,"Friday",
IDS_DS_WEEK_STATURDAY,"Saturday",
IDS_DS_UP_PAGE,"Up Page",
};

OSD_STRING osdstringpoolTChinese[] = {
IDS_DS_EMPTY," ",
IDS_DS_NO,"NO",
IDS_DS_YES,"YES",
IDS_DS_ON,"On",
IDS_DS_OFF,"Off",
IDS_DS_LOW,"Low",
IDS_DS_MIDDLE,"Middle",
IDS_DS_HIGH,"High",
IDS_DS_STANDARD,"Standard",
IDS_DS_NONE,"None",
IDS_DS_OK,"OK",
IDS_DS_CANCEL,"Cancel",
IDS_TIME_10SEC,"10 sec",
IDS_TIME_30SEC,"30 sec",
IDS_TIME_1MIN,"1Minute",
IDS_TIME_2MIN,"2Minutes",
IDS_TIME_3MIN,"3Minutes",
IDS_TIME_5MIN,"5Minutes",
IDS_TIME_10MIN,"10Minutes",
IDS_TIME_30MIN,"30Minutes",

IDS_DS_MOVIE_MODE,"Video Resolution",
IDS_DS_MOVIE_MODE_4K_25,"4K",
IDS_DS_MOVIE_MODE_1440_30,"2K",
IDS_DS_MOVIE_MODE_SHD,"SHD 30fps",
IDS_DS_MOVIE_MODE_SHD_25P,"SHD 25fps",
IDS_DS_MOVIE_MODE_FHD_60P,"FHD 60fps",
IDS_DS_MOVIE_MODE_FHD,"1080P",
IDS_DS_MOVIE_MODE_FHD_HDR,"FHD HDR 30fps",
IDS_DS_MOVIE_MODE_HD,"HD 30fps",
IDS_DS_MOVIE_MODE_HD_60P,"HD 60fps",
IDS_DS_MOVIE_MODE_VGA_30P,"VGA 30fps",

IDS_MOVIE_CLIPTIME,"Loop Recording",

IDS_MOVIE_SOUND_RECORD,"Sound Record",

IDS_DS_CLOCK_SETTINGS,"Data/Time",
IDS_DS_DATETIME_Y,"Y",
IDS_DS_DATETIME_M,"M",
IDS_DS_DATETIME_D,"D",
IDS_DS_DATETIME_H,"H",
IDS_DS_DATETIME_MM,"M",
IDS_DS_DATETIME_S,"S",

IDS_DS_DATETIME_FORMAT,"Data/Time Format",
IDS_DS_DATETIME_YMD,"YY/MM/DD",
IDS_DS_DATETIME_MDY,"MM/DD/YY",
IDS_DS_DATETIME_DMY,"DD/MM/YY",

IDS_DS_LANGUAGE,"Language",
IDS_DS_LANGUAGES_ENGLISH,"English",
IDS_DS_LANGUAGES_SPANISH,"Español",
IDS_DS_LANGUAGES_PORTUGUESE,"Português",
IDS_DS_LANGUAGES_RUSSIAN,"Русский",
IDS_DS_LANGUAGES_SCHINESE,"简体中文",
IDS_DS_LANGUAGES_TCHINESE,"繁体中文",
IDS_DS_LANGUAGES_GERMAN,"Deutsch",
IDS_DS_LANGUAGES_ITALIAN,"Italiano",
IDS_DS_LANGUAGES_LATVIAN,"Latviski",
IDS_DS_LANGUAGES_POLISH,"Polski",
IDS_DS_LANGUAGES_ROMANIAN,"Roma",
IDS_DS_LANGUAGES_SLOVAK,"slovenčina",
IDS_DS_LANGUAGES_UKRANINIAN,"українська",
IDS_DS_LANGUAGES_FRENCH,"Français",
IDS_DS_LANGUAGES_KOREAN,"한국어",
IDS_DS_LANGUAGES_JAPANESE,"日本語",
IDS_DS_LANGUAGES_CZECH,"Čeština",
IDS_DS_LANGUAGES_VIETNAMESE,"việt nam",
IDS_DS_LANGUAGE_DESCRIPTION,"Select language",

IDS_DS_RESET_SETUP,"Default Settings",
IDS_DS_RESET_SETUP_CONFIRM,"Restore Default Settins?",
IDS_DS_RESET_INFO,"All setting will be reset to default, And reboot system!",

IDS_DS_FW_VERSION_INFO,"Version Info",
IDS_DS_FW_VERSION,"Firmware Version",

IDS_DS_QR_CODE_INFO,"QR-Code",
IDS_DS_QR_CODE,"QR-Code",

IDS_DS_FLICKER,"Frequency",
IDS_FLICKER_50HZ,"50 Hz",
IDS_FLICKER_60HZ,"60 Hz",

IDS_DS_REVERSING_LINE,"Reversing Line",
IDS_REVERSING_LINE_ON,"ON",
IDS_REVERSING_LINE_OFF,"OFF",

IDS_DS_LCD_POWER_SAVE,"Screen Saver",

IDS_DS_KEY_TONE,"Key Tone",
IDS_DS_SHUTDOWN_TONE,"Boot Ringtone",

IDS_DS_GSENSOR_SENSITIVETY,"G-Sensor",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL0,"Level 0",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL1,"Level 1",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL2,"Level 2",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL3,"Level 3",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL4,"Level 4",

IDS_DS_DATE_LOGO_DISPLAY,"Data/Time/Logo Dispaly",

IDS_DS_FORMAT_SD_CARD,"Format SD-Card",
IDS_DS_FORMAT_CARD_CONFIRM,"Format Card?",
IDS_DS_DATA_DELETED,"All data will be deleted.",
IDS_DS_MSG_LOCK_CURRENT_FILE,"Lock Current File",
IDS_DS_MSG_UNLOCK_FILE,"Unlock files",
IDS_DS_MSG_DELETE_FILE_OK,"Delete File Complete",
IDS_DS_MSG_PROTECT_FILE_OK,"Protect File Complete",
IDS_DS_MSG_NO_FILE,"No file",
IDS_DS_MSG_SURE_TO_DELETE_SELETED,"Delete Selected File?",
IDS_DS_MSG_DELETE_SELETED_ERROR,"Delete Selected File Error!",
IDS_DS_MSG_CANNOT_DELETE,"Cannot delete protected files!",
IDS_DS_MSG_SURE_TO_PROTECT_SELETED,"Protect Selected File?",
IDS_DS_MSG_SURE_TO_UNPROTECT_SELETED,"UnProtect Selected File?",
IDS_DS_MSG_CARD_ERROR,"SD card is not formatted correctly.",
IDS_DS_MSG_STORAGE_FULL,"This storage is full!",
IDS_DS_MSG_NO_CARD,"SD card is not inserted.",
IDS_DS_MSG_PLUG_OUT_SD,"SD Card Is Plug Out",
IDS_DS_MSG_WAIT_INITIAL_DONE,"Wait SD Card Initialization Done",
IDS_DS_MSG_INSERT_SD_AGAIN,"Please Insert SD Card Again",
IDS_DS_MSG_FORMAT_SD_PROCESSING,"SD Card Format is Processing",
IDS_DS_MSG_FORMAT_SD_CARD_OK,"Format SD Card Complete",
IDS_DS_MSG_FORMAT_SD_CARD_FAIL,"Format SD Card Fail",
IDS_DS_MSG_FORMAT_SD_CARD,"Please Format SD Card",
IDS_DS_MSG_GOTO_POWER_OFF,"Going Power Off",
IDS_DS_MSG_OPENFILE_WAIT,"Please Wait ...",
IDS_DS_MSG_ACC_OFF_TIMELAPSE_REC,"After 5 seconds, Startup of video recording",
IDS_DS_MSG_ACC_ON_NORMAL_REC,"Normal video startup",
IDS_DS_15DAY,"15 Days",
IDS_DS_30DAY,"30 Days",
IDS_DS_60DAY,"60 Days",
IDS_DS_GENERAL_SETTINGS,"General Settings",
IDS_DS_SYSTEM_SETTINGS,"System Settings",
IDS_DS_VIDEO_SETTINGS,"Video Settings",
IDS_DS_CAMID_AND_VIDEO_TYPE,"Front/Rear Video",
IDS_DS_VIDEO_TYPE_NORMAL_1ST,"Front Normal Video",
IDS_DS_VIDEO_TYPE_NORMAL_2ST,"Rear Normal Video",
IDS_DS_VIDEO_TYPE_PARKING_1ST,"Front Parking Video",
IDS_DS_VIDEO_TYPE_PARKING_2ST,"Rear Parking Video",
IDS_DS_VIDEO_TYPE_EMERGENCY_1ST,"Front Emergency Video",
IDS_DS_VIDEO_TYPE_EMERGENCY_2ST,"Rear Emergency Video",
IDS_DS_VIDEO_TYPE_PHOTO_1ST,"Front Photo",
IDS_DS_VIDEO_TYPE_PHOTO_2ST,"Rear Photo",
IDS_DS_2_4G,"2.4G",
IDS_DS_5G,"5G",
IDS_DS_WIFI,"Wifi Mode",
IDS_DS_PASSWORD_INFO,"PassWord:",
IDS_DS_WIFI_SSID_INFO,"WIFI SSID:",
IDS_DS_NOTE,"Notice",
IDS_DS_WEEK_SUNDAY,"Sunday",
IDS_DS_WEEK_MONDAY,"Monday",
IDS_DS_WEEK_TUESDAY,"Tuesday",
IDS_DS_WEEK_WEDNESDAY,"Wednesday",
IDS_DS_WEEK_THURSDAY,"Thursday",
IDS_DS_WEEK_FRIDAY,"Friday",
IDS_DS_WEEK_STATURDAY,"Saturday",
IDS_DS_UP_PAGE,"Up Page",
};

OSD_STRING osdstringpoolSChinese[] = {
IDS_DS_EMPTY," ",
IDS_DS_NO,"NO",
IDS_DS_YES,"YES",
IDS_DS_ON,"On",
IDS_DS_OFF,"Off",
IDS_DS_LOW,"Low",
IDS_DS_MIDDLE,"Middle",
IDS_DS_HIGH,"High",
IDS_DS_STANDARD,"Standard",
IDS_DS_NONE,"None",
IDS_DS_OK,"OK",
IDS_DS_CANCEL,"Cancel",
IDS_TIME_10SEC,"10 sec",
IDS_TIME_30SEC,"30 sec",
IDS_TIME_1MIN,"1Minute",
IDS_TIME_2MIN,"2Minutes",
IDS_TIME_3MIN,"3Minutes",
IDS_TIME_5MIN,"5Minutes",
IDS_TIME_10MIN,"10Minutes",
IDS_TIME_30MIN,"30Minutes",

IDS_DS_MOVIE_MODE,"Video Resolution",
IDS_DS_MOVIE_MODE_4K_25,"4K",
IDS_DS_MOVIE_MODE_1440_30,"2K",
IDS_DS_MOVIE_MODE_SHD,"SHD 30fps",
IDS_DS_MOVIE_MODE_SHD_25P,"SHD 25fps",
IDS_DS_MOVIE_MODE_FHD_60P,"FHD 60fps",
IDS_DS_MOVIE_MODE_FHD,"1080P",
IDS_DS_MOVIE_MODE_FHD_HDR,"FHD HDR 30fps",
IDS_DS_MOVIE_MODE_HD,"HD 30fps",
IDS_DS_MOVIE_MODE_HD_60P,"HD 60fps",
IDS_DS_MOVIE_MODE_VGA_30P,"VGA 30fps",

IDS_MOVIE_CLIPTIME,"Loop Recording",

IDS_MOVIE_SOUND_RECORD,"Sound Record",

IDS_DS_CLOCK_SETTINGS,"Data/Time",
IDS_DS_DATETIME_Y,"Y",
IDS_DS_DATETIME_M,"M",
IDS_DS_DATETIME_D,"D",
IDS_DS_DATETIME_H,"H",
IDS_DS_DATETIME_MM,"M",
IDS_DS_DATETIME_S,"S",

IDS_DS_DATETIME_FORMAT,"Data/Time Format",
IDS_DS_DATETIME_YMD,"YY/MM/DD",
IDS_DS_DATETIME_MDY,"MM/DD/YY",
IDS_DS_DATETIME_DMY,"DD/MM/YY",

IDS_DS_LANGUAGE,"Language",
IDS_DS_LANGUAGES_ENGLISH,"English",
IDS_DS_LANGUAGES_SPANISH,"Español",
IDS_DS_LANGUAGES_PORTUGUESE,"Português",
IDS_DS_LANGUAGES_RUSSIAN,"Русский",
IDS_DS_LANGUAGES_SCHINESE,"简体中文",
IDS_DS_LANGUAGES_TCHINESE,"繁体中文",
IDS_DS_LANGUAGES_GERMAN,"Deutsch",
IDS_DS_LANGUAGES_ITALIAN,"Italiano",
IDS_DS_LANGUAGES_LATVIAN,"Latviski",
IDS_DS_LANGUAGES_POLISH,"Polski",
IDS_DS_LANGUAGES_ROMANIAN,"Roma",
IDS_DS_LANGUAGES_SLOVAK,"slovenčina",
IDS_DS_LANGUAGES_UKRANINIAN,"українська",
IDS_DS_LANGUAGES_FRENCH,"Français",
IDS_DS_LANGUAGES_KOREAN,"한국어",
IDS_DS_LANGUAGES_JAPANESE,"日本語",
IDS_DS_LANGUAGES_CZECH,"Čeština",
IDS_DS_LANGUAGES_VIETNAMESE,"việt nam",
IDS_DS_LANGUAGE_DESCRIPTION,"Select language",

IDS_DS_RESET_SETUP,"Default Settings",
IDS_DS_RESET_SETUP_CONFIRM,"Restore Default Settins?",
IDS_DS_RESET_INFO,"All setting will be reset to default, And reboot system!",

IDS_DS_FW_VERSION_INFO,"Version Info",
IDS_DS_FW_VERSION,"Firmware Version",

IDS_DS_QR_CODE_INFO,"QR-Code",
IDS_DS_QR_CODE,"QR-Code",

IDS_DS_FLICKER,"Frequency",
IDS_FLICKER_50HZ,"50 Hz",
IDS_FLICKER_60HZ,"60 Hz",

IDS_DS_REVERSING_LINE,"Reversing Line",
IDS_REVERSING_LINE_ON,"ON",
IDS_REVERSING_LINE_OFF,"OFF",

IDS_DS_LCD_POWER_SAVE,"Screen Saver",

IDS_DS_KEY_TONE,"Key Tone",
IDS_DS_SHUTDOWN_TONE,"Boot Ringtone",

IDS_DS_GSENSOR_SENSITIVETY,"G-Sensor",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL0,"Level 0",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL1,"Level 1",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL2,"Level 2",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL3,"Level 3",
IDS_DS_GSENSOR_SENSITIVETY_LEVEL4,"Level 4",

IDS_DS_DATE_LOGO_DISPLAY,"Data/Time/Logo Dispaly",

IDS_DS_FORMAT_SD_CARD,"Format SD-Card",
IDS_DS_FORMAT_CARD_CONFIRM,"Format Card?",
IDS_DS_DATA_DELETED,"All data will be deleted.",
IDS_DS_MSG_LOCK_CURRENT_FILE,"Lock Current File",
IDS_DS_MSG_UNLOCK_FILE,"Unlock files",
IDS_DS_MSG_DELETE_FILE_OK,"Delete File Complete",
IDS_DS_MSG_PROTECT_FILE_OK,"Protect File Complete",
IDS_DS_MSG_NO_FILE,"No file",
IDS_DS_MSG_SURE_TO_DELETE_SELETED,"Delete Selected File?",
IDS_DS_MSG_DELETE_SELETED_ERROR,"Delete Selected File Error!",
IDS_DS_MSG_CANNOT_DELETE,"Cannot delete protected files!",
IDS_DS_MSG_SURE_TO_PROTECT_SELETED,"Protect Selected File?",
IDS_DS_MSG_SURE_TO_UNPROTECT_SELETED,"UnProtect Selected File?",
IDS_DS_MSG_CARD_ERROR,"SD card is not formatted correctly.",
IDS_DS_MSG_STORAGE_FULL,"This storage is full!",
IDS_DS_MSG_NO_CARD,"SD card is not inserted.",
IDS_DS_MSG_PLUG_OUT_SD,"SD Card Is Plug Out",
IDS_DS_MSG_WAIT_INITIAL_DONE,"Wait SD Card Initialization Done",
IDS_DS_MSG_INSERT_SD_AGAIN,"Please Insert SD Card Again",
IDS_DS_MSG_FORMAT_SD_PROCESSING,"SD Card Format is Processing",
IDS_DS_MSG_FORMAT_SD_CARD_OK,"Format SD Card Complete",
IDS_DS_MSG_FORMAT_SD_CARD_FAIL,"Format SD Card Fail",
IDS_DS_MSG_FORMAT_SD_CARD,"Please Format SD Card",
IDS_DS_MSG_GOTO_POWER_OFF,"Going Power Off",
IDS_DS_MSG_OPENFILE_WAIT,"Please Wait ...",
IDS_DS_MSG_ACC_OFF_TIMELAPSE_REC,"After 5 seconds, Startup of video recording",
IDS_DS_MSG_ACC_ON_NORMAL_REC,"Normal video startup",
IDS_DS_15DAY,"15 Days",
IDS_DS_30DAY,"30 Days",
IDS_DS_60DAY,"60 Days",
IDS_DS_GENERAL_SETTINGS,"General Settings",
IDS_DS_SYSTEM_SETTINGS,"System Settings",
IDS_DS_VIDEO_SETTINGS,"Video Settings",
IDS_DS_CAMID_AND_VIDEO_TYPE,"Front/Rear Video",
IDS_DS_VIDEO_TYPE_NORMAL_1ST,"Front Normal Video",
IDS_DS_VIDEO_TYPE_NORMAL_2ST,"Rear Normal Video",
IDS_DS_VIDEO_TYPE_PARKING_1ST,"Front Parking Video",
IDS_DS_VIDEO_TYPE_PARKING_2ST,"Rear Parking Video",
IDS_DS_VIDEO_TYPE_EMERGENCY_1ST,"Front Emergency Video",
IDS_DS_VIDEO_TYPE_EMERGENCY_2ST,"Rear Emergency Video",
IDS_DS_VIDEO_TYPE_PHOTO_1ST,"Front Photo",
IDS_DS_VIDEO_TYPE_PHOTO_2ST,"Rear Photo",
IDS_DS_2_4G,"2.4G",
IDS_DS_5G,"5G",
IDS_DS_WIFI,"Wifi Mode",
IDS_DS_PASSWORD_INFO,"PassWord:",
IDS_DS_WIFI_SSID_INFO,"WIFI SSID:",
IDS_DS_NOTE,"Notice",
IDS_DS_WEEK_SUNDAY,"Sunday",
IDS_DS_WEEK_MONDAY,"Monday",
IDS_DS_WEEK_TUESDAY,"Tuesday",
IDS_DS_WEEK_WEDNESDAY,"Wednesday",
IDS_DS_WEEK_THURSDAY,"Thursday",
IDS_DS_WEEK_FRIDAY,"Friday",
IDS_DS_WEEK_STATURDAY,"Saturday",
IDS_DS_UP_PAGE,"Up Page",
};

void updata_font(int type)
{
	printf("Language type is:%d !!", type);
	switch (type)
	{
		#if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
		case LANGUAGE_ENGLISH:
			font_montserrat_14 = &ui_font_English14;
			font_montserrat_16 = &ui_font_English16;
			font_montserrat_18 = &ui_font_English18;
			font_montserrat_20 = &ui_font_English20;
			font_montserrat_22 = &ui_font_English22;
			font_montserrat_24 = &ui_font_English24;
			font_montserrat_26 = &ui_font_English26;
			font_montserrat_28 = &ui_font_English28;
			font_montserrat_30 = &ui_font_English30;
			break;
		#endif
		#if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
		case LANGUAGE_SCHINESE:
			// font_montserrat_14 = &ui_font_chinese14;
			// font_montserrat_16 = &ui_font_chinese16;
			// font_montserrat_18 = &ui_font_chinese18;
			// font_montserrat_20 = &ui_font_chinese20;
			// font_montserrat_22 = &ui_font_chinese22;
			// font_montserrat_24 = &ui_font_chinese24;
			// font_montserrat_26 = &ui_font_chinese26;
			// font_montserrat_28 = &ui_font_chinese28;
			// font_montserrat_40 = &ui_font_chinese40;
			break;
		#endif
		#if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
		case LANGUAGE_TCHINESE:
			// font_montserrat_14 = &ui_font_Tchinese14;
			// font_montserrat_16 = &ui_font_Tchinese16;
			// font_montserrat_18 = &ui_font_Tchinese18;
			// font_montserrat_20 = &ui_font_Tchinese20;
			// font_montserrat_22 = &ui_font_Tchinese22;
			// font_montserrat_24 = &ui_font_Tchinese24;
			// font_montserrat_26 = &ui_font_Tchinese26;
			// font_montserrat_28 = &ui_font_Tchinese28;
			// font_montserrat_40 = &ui_font_Tchinese40;
			break;
		#endif
		default:
			break;
	}
}

void updata_osd_string(int type)
{
	updata_font(type);
	switch (type)
	{
		#if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
		case LANGUAGE_ENGLISH:
			osdstringpool = osdstringpoolEnglish;
			break;
		#endif
		#if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
		case LANGUAGE_SCHINESE:
			osdstringpool = osdstringpoolSChinese;
			break;
		#endif
		#if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
		case LANGUAGE_TCHINESE:
			osdstringpool = osdstringpoolTChinese;
			break;
		#endif
		default:
			break;
	}
}
