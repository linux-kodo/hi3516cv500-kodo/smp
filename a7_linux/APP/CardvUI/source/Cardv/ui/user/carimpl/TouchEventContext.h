/*
* TouchEventContext.h
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#ifndef _TOUCH_EVENT_CONTEXT_H_
#define _TOUCH_EVENT_CONTEXT_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum {
	E_ACTION_UP,
	E_ACTION_DOWN,
	E_ACTION_MOVE,
	E_ACTION_NONE,
} EActionStatus;

typedef struct _MOTION_EVENT{
	int mX;
	int mY;
	EActionStatus mActionStatus;
} MOTION_EVENT;

typedef struct _LVGL_POINT{
	float x;
	float y;
} LVGL_Point;

typedef bool (*touch_event_fun)(MOTION_EVENT ev);

void reset(MOTION_EVENT *p);
bool run_touch_callback(MOTION_EVENT ev);
void register_touch_callback(touch_event_fun cb);

#endif /* _TOUCH_EVENT_CONTEXT_H_ */
