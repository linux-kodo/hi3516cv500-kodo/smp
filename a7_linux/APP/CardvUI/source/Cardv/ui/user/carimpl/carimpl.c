/*
* Carimpl.cpp
*
* This file use to exchange data between UI with CARDV.
*
*  Created on: 2024.06.03
*      Author: XiongYingDan
*/
#include <string.h>
#include <stdlib.h>
#include <sys/statfs.h>
#include <sys/stat.h>
#include "fcntl.h"

#include "DCF.h"
#include "carimpl.h"
#include "ui_common.h"
#include "OSDStringCommon.h"

/*===========================================================================
* Macro define
*===========================================================================*/

#define DCF_FROM_TAIL (1)

/*===========================================================================
* Global variable
*===========================================================================*/

struct CarDV_Info carimpl = {0};
bool bDcfMount = FALSE;

bool format_sd_card_flag = false;
MI_U8 gu8LastUIMode = UI_VIDEO_MODE;

static int m_uiPlaybackVideoCamId = 0;
static long	gulLCDPowerSaveRtcTime = 0; //Unit:seconds
static char gCmdBuf[256] = {0};
static WMSG_INFO m_WMSGInfo = WMSG_NONE;

static bool carimpl_ExecCmdHandler(CARIMPL_CMD_TYPE eCmdType, const char *szCmd, const char *szCmdParam);

bool Carimpl_SyncAllSetting(void) {
    //carimpl maybe need to maintain all of user`s setting.
    /* memcpy(&carimpl.stMenuInfo, MenuSettingConfig(), sizeof(MenuInfo));
    IPC_CarInfo_Write_MenuInfo(&carimpl.stMenuInfo); */

    /* if(MenuSettingConfig()->uiResetSetting){
        MenuSettingInit();
        MenuSettingConfig()->uiResetSetting = 0;
    } */
    return true;
}

bool Carimpl_Reset(void)
{
    carimpl.stBrowserInfo.u8CamId = 0;     //fron cam
    carimpl.stBrowserInfo.u8DBId = 0;      //normal folder
    printf("%s %d\n", __FUNCTION__, __LINE__);
    for (int i = 0; i < DB_NUM; i++)
        for (int j = 0; j < CAM_NUM; j++)
            carimpl.stBrowserInfo.u32CurFileIdx[i][j] = 0;
    return true;
}

void Carimpl_Send2Fifo(const void *cmd, size_t size)
{
    int pipe_fd_w = -1;
    pipe_fd_w = open(FIFO_NAME, O_WRONLY);
    if (pipe_fd_w >= 0) {
        write(pipe_fd_w, cmd, size);
        close(pipe_fd_w);
    }
    else {
        printf("Error: failed to open %s\n", FIFO_NAME);
    }
}

/**********************************************************/
/********************* browser ****************************/
/**********************************************************/
int Carimpl_DcfMount(void)
{
    int ret = 0;

    DCF_Lock();
    if (bDcfMount)
        goto EXIT;

    ret = DCF_Mount();
    if (ret == 0)
        bDcfMount = TRUE;

EXIT:
    DCF_UnLock();
    return ret;
}

int Carimpl_DcfUnmount(void)
{
    int ret = 0;

    DCF_Lock();
    if (bDcfMount == FALSE)
        goto EXIT;

    bDcfMount = FALSE;
    DCF_UnMount();
EXIT:
    DCF_UnLock();
    return ret;
}

bool Carimpl_IsSubFileEnable(void)
{
    return DCF_IsSubFileEnable(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId);
}

unsigned int Carimpl_GetTotalFilesByParameter(unsigned char u8DB, unsigned char u8CamId)
{
    return DCF_GetDBFileCnt(u8DB, u8CamId);
}

unsigned int Carimpl_GetTotalFiles(void)
{
    return DCF_GetDBFileCnt(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId);
}

unsigned int Carimpl_GetCurFileIdxByParameter(unsigned char u8DB, unsigned char u8CamId)
{
    return carimpl.stBrowserInfo.u32CurFileIdx[u8DB][u8CamId];
}

unsigned int Carimpl_GetCurFileIdx(void)
{
    return carimpl.stBrowserInfo.u32CurFileIdx[carimpl.stBrowserInfo.u8DBId][carimpl.stBrowserInfo.u8CamId];
}

bool Carimpl_SetCurFileIdxByParameter(unsigned char u8DB, unsigned char u8CamId, unsigned int u32FileIdx)
{
    // Idx from tail or head
    carimpl.stBrowserInfo.u32CurFileIdx[u8DB][u8CamId] = u32FileIdx;
    IPC_CarInfo_Write_BrowserInfo(&carimpl.stBrowserInfo);
    return true;
}

bool Carimpl_SetCurFileIdx(unsigned int u32FileIdx)
{
    // Idx from tail or head
    carimpl.stBrowserInfo.u32CurFileIdx[carimpl.stBrowserInfo.u8DBId][carimpl.stBrowserInfo.u8CamId] = u32FileIdx;
    IPC_CarInfo_Write_BrowserInfo(&carimpl.stBrowserInfo);
    return true;
}

bool Carimpl_SetCurFolder(unsigned char u8DB, unsigned char u8CamId)
{
    if (u8DB >= DB_NUM || u8CamId >= CAM_NUM) {
        printf("%s err %d\n",__FUNCTION__, __LINE__);
        return FALSE;
    }

    carimpl.stBrowserInfo.u8DBId = u8DB;
    carimpl.stBrowserInfo.u8CamId = u8CamId;
    IPC_CarInfo_Write_BrowserInfo(&carimpl.stBrowserInfo);
    return TRUE;
}

unsigned char Carimpl_GetCurDB(void)
{
    return carimpl.stBrowserInfo.u8DBId;
}

unsigned char Carimpl_GetCurCamId(void)
{
    return carimpl.stBrowserInfo.u8CamId;
}

char* Carimpl_GetCurFileName(unsigned int u32FileIdx)
{
    #if (DCF_FROM_TAIL)
    return DCF_GetFileNameFromTailByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #else
    return DCF_GetFileNameFromHeadByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #endif
}

char* Carimpl_GetCurSubFileName(unsigned int u32FileIdx)
{
    #if (DCF_FROM_TAIL)
    return DCF_GetSubFileNameFromTailByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #else
    return DCF_GetSubFileNameFromHeadByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #endif
}

char* Carimpl_GetPairFileName(char* pszFileName, unsigned char u8CamId)
{
    char* strFile = NULL;
    if (DCF_IsSubFileEnable(carimpl.stBrowserInfo.u8DBId, u8CamId)){
        strFile = DCF_GetPairSubFileName(pszFileName, u8CamId);
        if(strFile)
            return strFile;
    }
    return DCF_GetPairFileName(pszFileName, u8CamId);
}

int Carimpl_DeleteFile(unsigned int u32FileIdx)
{
    #if (DCF_FROM_TAIL)
    return DCF_DeleteFileFromTailByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #else
    return DCF_DeleteFileFromHeadByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #endif
}

int Carimpl_ProtectFile(unsigned int u32FileIdx)
{
    #if (DCF_FROM_TAIL)
    return DCF_ProtectFileFromTailByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #else
    return DCF_ProtectFileFromHeadByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #endif
}

int Carimpl_UnProtectFile(unsigned int u32FileIdx)
{
    #if (DCF_FROM_TAIL)
    return DCF_UnProtectFileFromTailByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #else
    return DCF_UnProtectFileFromHeadByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #endif
}

bool Carimpl_IsProtectFile(unsigned int u32FileIdx)
{
    #if (DCF_FROM_TAIL)
    return DCF_IsProtectFileFromTailByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #else
    return DCF_IsProtectFileFromHeadByIdx(carimpl.stBrowserInfo.u8DBId, carimpl.stBrowserInfo.u8CamId, u32FileIdx);
    #endif
}

void carimpl_SyncDCF(void)
{
    carimpl_ExecCmdHandler(FIFO_CMD_TYPE, CARDV_CMD_SYNC_DCF, "");
}

int Carimpl_GetFileThumbnail(char *pFileName, char *pThumbFileName)
{

}

int Carimpl_GetJpegFileResolution(const char *szFileName, unsigned int *pu32Width, unsigned int *pu32Height)
{
#define MAKEUS(a, b)    ((unsigned short) (((unsigned short)(a)) << 8 | ((unsigned short)(b))))

#define M_DATA  0x00
#define M_SOF0  0xc0
#define M_DHT   0xc4
#define M_SOI   0xd8
#define M_EOI   0xd9
#define M_SOS   0xda
#define M_DQT   0xdb
#define M_DNL   0xdc
#define M_DRI   0xdd
#define M_APP0  0xe0
#define M_APPF  0xef
#define M_COM   0xfe

    int Finished = 0;
    unsigned char id, u8High, u8Low;
    FILE *pFile = NULL;

    *pu32Width = 0;
    *pu32Height = 0;

    pFile = fopen(szFileName, "rb");
    if (pFile == NULL)
        return -1;

    while (!Finished) {
        if (!fread(&id, sizeof(char), 1, pFile) ||
            id != 0xff ||
            !fread(&id, sizeof(char), 1, pFile)) {
            Finished = -2;
            break;
        }

        if (id >= M_APP0 && id <= M_APPF) {
            fread(&u8High, sizeof(char), 1, pFile);
            fread(&u8Low, sizeof(char), 1, pFile);
            fseek(pFile, (long)(MAKEUS(u8High, u8Low) - 2), SEEK_CUR);
            continue;
        }

        switch (id) {
        case M_SOI:
            break;

        case M_COM:
        case M_DQT:
        case M_DHT:
        case M_DNL:
        case M_DRI:
            fread(&u8High, sizeof(char), 1, pFile);
            fread(&u8Low, sizeof(char), 1, pFile);
            fseek(pFile, (long)(MAKEUS(u8High, u8Low) - 2), SEEK_CUR);
            break;

        case M_SOF0:
            fseek(pFile, 3L, SEEK_CUR);
            fread(&u8High, sizeof(char), 1, pFile);
            fread(&u8Low, sizeof(char), 1, pFile);
            *pu32Height = (unsigned int)MAKEUS(u8High, u8Low);
            fread(&u8High, sizeof(char), 1, pFile);
            fread(&u8Low, sizeof(char), 1, pFile);
            *pu32Width = (unsigned int)MAKEUS(u8High, u8Low);
            fclose(pFile);
            return 0;

        case M_SOS:
        case M_EOI:
        case M_DATA:
            Finished = -1;
            break;

        default:
            fread(&u8High, sizeof(char), 1, pFile);
            fread(&u8Low, sizeof(char), 1, pFile);
            printf("unknown id [%x] length [%hd]\n", id, MAKEUS(u8High, u8Low));
            if (fseek(pFile, (long)(MAKEUS(u8High, u8Low) - 2), SEEK_CUR) != 0)
                Finished = -2;
            break;
        }
    }

    if (Finished == -1)
        printf("can't find SOF0\n");
    else if (Finished == -2)
        printf("jpeg format error\n");
    fclose(pFile);
    return -1;
}

int64_t Carimpl_GetFileDuration(char *pFileName)
{

}

/**********************************************************/
/********************** setting ***************************/
/**********************************************************/
static bool carimpl_ExecCmdHandler(CARIMPL_CMD_TYPE eCmdType, const char *szCmd, const char *szCmdParam)
{
    char cmdstr[256] = {0};
    if (eCmdType == CGI_CMD_TYPE)
    {
        int  cmdstatus = 0;
        sprintf(cmdstr, "%s %s %s &", CGI_PROCESS_PATH, szCmd, szCmdParam);
        if (strstr(szCmd, "set"))
        {
            cmdstatus = system(cmdstr);
            if (cmdstatus < 0) {
                printf("[%s]fail\n", cmdstr);
                return false;
            }
        }
        else if (strstr(szCmd, "get"))
        {
            memset(gCmdBuf, 0, sizeof(gCmdBuf));
            FILE *fp = popen(cmdstr, "r");
            if (!fp)
            {
                printf("CGI_CMD_TYPE: popen errno\n");
                return false;
            }
            while (fgets(gCmdBuf, sizeof(gCmdBuf), fp) != NULL)
            {
                //printf("CGI_CMD_TYPE: %s\n",gCmdBuf);
            }
            pclose(fp);
        }
    }
    else if (eCmdType == FIFO_CMD_TYPE)
    {
        sprintf(cmdstr, "%s %s\r\n", szCmd, szCmdParam);
        printf("%s", cmdstr);
        Carimpl_Send2Fifo(cmdstr, strlen(cmdstr));
    }
    else if (eCmdType == SCRIPT_CMD_TYPE)
    {
        int  cmdstatus = 0;
        sprintf(cmdstr, "%s %s", szCmd, szCmdParam);
        cmdstatus = system(cmdstr);
        if (cmdstatus < 0) {
            printf("[%s]fail\n", cmdstr);
            return false;
        }
    }
    return true;
}

char* carimpl_GetSettingCmdBuf(void)
{
    //Cmdbuf format key=value
    char *bufval = NULL;
    bufval = strchr(gCmdBuf, '=');
    if (bufval != NULL)
        return bufval+1;
    else
        return gCmdBuf;
}

/*===========================================================================
* Video related
*===========================================================================*/
void carimpl_VideoFunc_SetResolution(void)
{
    if (carimpl.stUIModeInfo.u8Mode == UI_MENU_MODE &&
        gu8LastUIMode != UI_VIDEO_MODE)
        return;

    switch (MenuSettingConfig()->uiMOVSize)
    {
    #if (MENU_MOVIE_SIZE_4K_25P_EN)
    case MOVIE_SIZE_4K_25P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "4K");
        break; 
    #endif
    #if (MENU_MOVIE_SIZE_1440_30P_EN)
    case MOVIE_SIZE_1440_30P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "2K");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_SHD_30P_EN)
    case MOVIE_SIZE_SHD_30P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "1296P30fps");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_SHD_25P_EN)
    case MOVIE_SIZE_SHD_25P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "1296P25fps");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_1080_60P_EN)
    case MOVIE_SIZE_1080_60P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "1080P60fps");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_1080P_EN)
    case MOVIE_SIZE_1080P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "1080P");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_1080P_30_HDR_EN)
    case MOVIE_SIZE_1080_30P_HDR:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "1080P30fpsHDR");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_900P_30P_EN)
    case MOVIE_SIZE_900P_30P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "900P30fps");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_960P_30P_EN)
    case MOVIE_SIZE_960P_30P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "960P30fps");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_720P_EN)
    case MOVIE_SIZE_720P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "720P30fps");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_720_60P_EN)
    case MOVIE_SIZE_720_60P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "720P60fps");
        break;
    #endif
    #if (MENU_MOVIE_SIZE_VGA30P_EN)
    case MOVIE_SIZE_VGA_30P:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set VideoRes", "VGA");
        break;
    #endif
    default:
        printf("Unsupported resolution/frame rate!\n");
        break;
    }
}

void carimpl_VideoFunc_SetClipTime(void)
{
    switch (MenuSettingConfig()->uiMOVClipTime)
    {
    #if (MENU_MOVIE_CLIP_TIME_OFF_EN)
    case MOVIE_CLIP_TIME_OFF:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LoopingVideo", "OFF");
        break;
    #endif
    #if (MENU_MOVIE_CLIP_TIME_1MIN_EN)
    case MOVIE_CLIP_TIME_1MIN:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LoopingVideo", "1MIN");
        break;
    #endif
    #if (MENU_MOVIE_CLIP_TIME_2MIN_EN)
    case MOVIE_CLIP_TIME_2MIN:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LoopingVideo", "2MIN");
        break;
    #endif
    #if (MENU_MOVIE_CLIP_TIME_3MIN_EN)
    case MOVIE_CLIP_TIME_3MIN:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LoopingVideo", "3MIN");
        break;
    #endif
    #if (MENU_MOVIE_CLIP_TIME_5MIN_EN)
    case MOVIE_CLIP_TIME_5MIN:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LoopingVideo", "5MIN");
        break;
    #endif
    #if (MENU_MOVIE_CLIP_TIME_10MIN_EN)
    case MOVIE_CLIP_TIME_10MIN:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LoopingVideo", "10MIN");
        break;
    #endif
    #if (MENU_MOVIE_CLIP_TIME_30MIN_EN)
    case MOVIE_CLIP_TIME_30MIN:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LoopingVideo", "30MIN");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_VideoFunc_SetSoundRecord(void)
{
    switch (MenuSettingConfig()->uiMOVSoundRecord)
    {
    #if (MENU_MOVIE_SOUND_RECORD_ON_EN)
    case MOVIE_SOUND_RECORD_ON:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set SoundRecord", "ON");
        break;
    #endif
    #if (MENU_MOVIE_SOUND_RECORD_OFF_EN)
    case MOVIE_SOUND_RECORD_OFF:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set SoundRecord", "OFF");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_VideoFunc_StartRecording(bool bStart)
{
    #if ACT_SAME_AS_CGI_CMD
    carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Video", "record");
    #else
    if (bStart)
    carimpl_ExecCmdHandler(FIFO_CMD_TYPE, CARDV_CMD_START_REC, "");
    else
    carimpl_ExecCmdHandler(FIFO_CMD_TYPE, CARDV_CMD_STOP_REC, "");
    #endif
}

void carimpl_VideoFunc_StartEmergRecording(void)
{
    carimpl_ExecCmdHandler(FIFO_CMD_TYPE, CARDV_CMD_EMERG_REC, "");
    carimpl_show_wmsg(true, WMSG_LOCK_CURRENT_FILE); // TBD : warning message
}

void carimpl_VideoFunc_Capture(void)
{
    #if ACT_SAME_AS_CGI_CMD
    carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Video", "capture");
    #else
    carimpl_ExecCmdHandler(FIFO_CMD_TYPE, CARDV_CMD_CAPTURE, "");
    #endif
}

void carimpl_IQFunc_SetFlicker(void)
{
    switch (MenuSettingConfig()->uiFlickerHz)
    {
    #if (MENU_GENERAL_FLICKER_50HZ_EN)
    case FLICKER_50HZ:
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Flicker", "50HZ");
        break;
    #endif
    #if (MENU_GENERAL_FLICKER_60HZ_EN)
    case FLICKER_60HZ:
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Flicker", "60HZ");
        break;
    #endif
    default:
        break;
    }
}

/*===========================================================================
* Playback related
*===========================================================================*/
void carimpl_PlaybackFunc_setCamID(char value)
{
    m_uiPlaybackVideoCamId = value;
}

void carimpl_PlaybackFunc_setVideoType(void)
{
    int iType = m_uiPlaybackVideoCamId;
    switch (MenuSettingConfig()->uiPlaybackVideoType)
    {
    #if (MENU_PLAYBACK_VIDEO_TYPE_NORMAL_EN)
    case 0:
        Carimpl_SetCurFolder(DB_NORMAL, iType);
        break;
    #endif
    #if (MENU_PLAYBACK_VIDEO_TYPE_PARKING_EN)
    case 1:
        Carimpl_SetCurFolder(DB_PARKING, iType);
        break;
    #endif
    #if (MENU_PLAYBACK_VIDEO_TYPE_EMERGENCY_EN)
    case 2:
        Carimpl_SetCurFolder(DB_EVENT, iType);
        break;
    #endif
    case 3:
        Carimpl_SetCurFolder(DB_SHARE, iType);
        break;
    default:
        Carimpl_SetCurFolder(DB_NORMAL, iType);
        break;
    }
}

void carimpl_PlaybackFunc_setImageType(void)
{
    int iType = m_uiPlaybackVideoCamId;
    Carimpl_SetCurFolder(DB_PHOTO, iType);
}

void carimpl_PlaybackFunc_DeleteOne(void)
{
    int MaxDcfObj, CurObjIdx;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    MaxDcfObj = Carimpl_GetTotalFiles();
    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        return;
    }
    CurObjIdx = Carimpl_GetCurFileIdx();
    if (Carimpl_DeleteFile(CurObjIdx) == 0)
        carimpl_show_wmsg(true, WMSG_DELETE_FILE_OK);
    else
        carimpl_show_wmsg(true, WMSG_CANNOT_DELETE);

    carimpl.stCapInfo.u32FileCnt = DCF_GetDBFileCntByDB(DB_PHOTO);
    IPC_CarInfo_Write_CapInfo(&carimpl.stCapInfo);
}

void carimpl_PlaybackFunc_DeleteAll_Video(void)
{
    int MaxDcfObj, CurObjIdx, DeleteCnt = 0;
    int usedIdx = 0;
    int i;
    int iType = m_uiPlaybackVideoCamId;
    bool bDBSwitch = false;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    if (Carimpl_GetCurDB() == DB_PHOTO)
        bDBSwitch = true;
    if (bDBSwitch)
        carimpl_PlaybackFunc_setVideoType();
    MaxDcfObj = Carimpl_GetTotalFiles();

    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        if (bDBSwitch)
            Carimpl_SetCurFolder(DB_PHOTO, iType);
        return;
    }
    usedIdx = Carimpl_GetCurFileIdx();
    Carimpl_SetCurFileIdx(0);
    for (i = 0; i < MaxDcfObj; i++) {
        CurObjIdx = Carimpl_GetCurFileIdx();
        if (Carimpl_DeleteFile(CurObjIdx) != 0) {
            CurObjIdx++;
            Carimpl_SetCurFileIdx(CurObjIdx);
        } else {
            DeleteCnt++;
        }
    }

    if (DeleteCnt)
        carimpl_show_wmsg(true, WMSG_DELETE_FILE_OK);
    else {
        carimpl_show_wmsg(true, WMSG_CANNOT_DELETE);
        Carimpl_SetCurFileIdx(usedIdx);
    }
    if (bDBSwitch)
        Carimpl_SetCurFolder(DB_PHOTO, iType);
    return;
}

void carimpl_PlaybackFunc_DeleteAll_Image(void)
{
    int MaxDcfObj, CurObjIdx, DeleteCnt = 0;
    int usedIdx = 0;
    int i;
    int iType = m_uiPlaybackVideoCamId;
    bool bDBSwitch = false;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    if (Carimpl_GetCurDB() != DB_PHOTO)
        bDBSwitch = true;
    if (bDBSwitch)
        Carimpl_SetCurFolder(DB_PHOTO, iType);
    MaxDcfObj = Carimpl_GetTotalFiles();
    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        if (bDBSwitch)
            carimpl_PlaybackFunc_setVideoType();
        return;
    }
    usedIdx = Carimpl_GetCurFileIdx();
    Carimpl_SetCurFileIdx(0);
    for (i = 0; i < MaxDcfObj; i++) {
        CurObjIdx = Carimpl_GetCurFileIdx();
        if (Carimpl_DeleteFile(CurObjIdx) != 0) {
            CurObjIdx++;
            Carimpl_SetCurFileIdx(CurObjIdx);
        } else {
            DeleteCnt++;
        }
    }

    if (DeleteCnt)
        carimpl_show_wmsg(true, WMSG_DELETE_FILE_OK);
    else {
        carimpl_show_wmsg(true, WMSG_CANNOT_DELETE);
        Carimpl_SetCurFileIdx(usedIdx);
    }
    carimpl.stCapInfo.u32FileCnt = DCF_GetDBFileCntByDB(DB_PHOTO);
    IPC_CarInfo_Write_CapInfo(&carimpl.stCapInfo);
    if (bDBSwitch)
        carimpl_PlaybackFunc_setVideoType();
    return;
}

void carimpl_PlaybackFunc_ProtectOne(void)
{
    int MaxDcfObj, CurObjIdx;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    MaxDcfObj = Carimpl_GetTotalFiles();
    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        return;
    }
    CurObjIdx = Carimpl_GetCurFileIdx();
    if (Carimpl_ProtectFile(CurObjIdx) == 0)
        carimpl_show_wmsg(true, WMSG_PROTECT_FILE_OK);
    else
        carimpl_show_wmsg(true, WMSG_INVALID_OPERATION);
}

void carimpl_PlaybackFunc_ProtectAll_Video(void)
{
    int MaxDcfObj;
    int i;
    int iType = m_uiPlaybackVideoCamId;
    bool bDBSwitch = false;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    if (Carimpl_GetCurDB() == DB_PHOTO)
        bDBSwitch = true;
    if (bDBSwitch)
        carimpl_PlaybackFunc_setVideoType();
    MaxDcfObj = Carimpl_GetTotalFiles();
    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        if (bDBSwitch)
            Carimpl_SetCurFolder(DB_PHOTO, iType);
        return;
    }
    for (i = 0; i < MaxDcfObj; i++)
    {
        if (Carimpl_ProtectFile(i) != 0)
        {
            carimpl_show_wmsg(true, WMSG_INVALID_OPERATION);
            continue;
        }
        //DrawProgressBar
    }
    carimpl_show_wmsg(true, WMSG_PROTECT_FILE_OK);
    if (bDBSwitch)
        Carimpl_SetCurFolder(DB_PHOTO, iType);
}

void carimpl_PlaybackFunc_ProtectAll_Image(void)
{
    int MaxDcfObj;
    int i;
    int iType = m_uiPlaybackVideoCamId;
    bool bDBSwitch = false;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    if (Carimpl_GetCurDB() != DB_PHOTO)
        bDBSwitch = true;
    if (bDBSwitch)
        Carimpl_SetCurFolder(DB_PHOTO, iType);
    MaxDcfObj = Carimpl_GetTotalFiles();
    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        if (bDBSwitch)
            carimpl_PlaybackFunc_setVideoType();
        return;
    }
    for (i = 0; i < MaxDcfObj; i++)
    {
        if (Carimpl_ProtectFile(i) != 0)
        {
            carimpl_show_wmsg(true, WMSG_INVALID_OPERATION);
            continue;
        }
        //DrawProgressBar
    }
    carimpl_show_wmsg(true, WMSG_PROTECT_FILE_OK);
    if (bDBSwitch)
        carimpl_PlaybackFunc_setVideoType();
}

void carimpl_PlaybackFunc_UnProtectOne(void)
{
    int MaxDcfObj, CurObjIdx;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    MaxDcfObj = Carimpl_GetTotalFiles();
    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        return;
    }
    CurObjIdx = Carimpl_GetCurFileIdx();
    if (Carimpl_UnProtectFile(CurObjIdx) != 0)
        carimpl_show_wmsg(true, WMSG_INVALID_OPERATION);
}

void carimpl_PlaybackFunc_UnProtectAll_Video(void)
{
    int MaxDcfObj;
    int i;
    int iType = m_uiPlaybackVideoCamId;
    bool bDBSwitch = false;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    if (Carimpl_GetCurDB() == DB_PHOTO)
        bDBSwitch = true;
    if (bDBSwitch)
        carimpl_PlaybackFunc_setVideoType();
    MaxDcfObj = Carimpl_GetTotalFiles();
    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        if (bDBSwitch)
            Carimpl_SetCurFolder(DB_PHOTO, iType);
        return;
    }
    for (i = 0; i < MaxDcfObj; i++)
    {
        if (Carimpl_UnProtectFile(i) != 0)
        {
            carimpl_show_wmsg(true, WMSG_INVALID_OPERATION);
            continue;
        }
        //DrawProgressBar
    }
    if (bDBSwitch)
        Carimpl_SetCurFolder(DB_PHOTO, iType);
}

void carimpl_PlaybackFunc_UnProtectAll_Image(void)
{
    int MaxDcfObj;
    int i;
    int iType = m_uiPlaybackVideoCamId;
    bool bDBSwitch = false;

    if (!IMPL_SD_ISMOUNT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }
    if (Carimpl_GetCurDB() != DB_PHOTO)
        bDBSwitch = true;
    if (bDBSwitch)
        Carimpl_SetCurFolder(DB_PHOTO, iType);
    MaxDcfObj = Carimpl_GetTotalFiles();
    if (MaxDcfObj == 0) {
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        if (bDBSwitch)
            carimpl_PlaybackFunc_setVideoType();
        return;
    }
    for (i = 0; i < MaxDcfObj; i++)
    {
        if (Carimpl_UnProtectFile(i) != 0)
        {
            carimpl_show_wmsg(true, WMSG_INVALID_OPERATION);
            continue;
        }
        //DrawProgressBar
    }
    if (bDBSwitch)
        carimpl_PlaybackFunc_setVideoType();
}

/*===========================================================================
* Media tool related
*===========================================================================*/
void carimpl_MediaToolFunc_FormatSDCard(void)
{
    if (!IMPL_SD_ISINSERT) {
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        return;
    }

    if (gu8LastUIMode == UI_PLAYBACK_MODE) {
        carimpl_show_wmsg(true, WMSG_INVALID_OPERATION);
        return;
    }

    if (gu8LastUIMode == UI_BROWSER_MODE)
        Carimpl_DcfUnmount();

    carimpl_show_wmsg(true, WMSG_FORMAT_SD_PROCESSING);
    carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set SD0", "");
}

/*===========================================================================
* Genera setting related
*===========================================================================*/
void carimpl_GeneralFunc_SetShutdownTone(void)
{
    switch (MenuSettingConfig()->uiShutdownTone)
    {
    #if (MENU_GENERAL_BEEP_ON_EN)
    case BEEP_ON:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ShutdownTone", "ON");
        break;
    #endif
    #if (MENU_GENERAL_BEEP_OFF_EN)
    case BEEP_OFF:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ShutdownTone", "OFF");
        break;
    #endif
    case BEEP_LOW:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ShutdownTone", "LOW");
        break;
    case BEEP_MID:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ShutdownTone", "MID");
        break;
    case BEEP_HIGH:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ShutdownTone", "HIGH");
        break;
    default:
        break;
    }
}

void carimpl_GeneralFunc_SetBeep(void)
{
    switch (MenuSettingConfig()->uiBeep)
    {
    #if (MENU_GENERAL_BEEP_ON_EN)
    case BEEP_ON:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Beep", "ON");
        break;
    #endif
    #if (MENU_GENERAL_BEEP_OFF_EN)
    case BEEP_OFF:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Beep", "OFF");
        break;
    #endif
    case BEEP_LOW:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Beep", "LOW");
        break;
    case BEEP_MID:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Beep", "MID");
        break;
    case BEEP_HIGH:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Beep", "HIGH");
        break;
    default:
        break;
    }
}

/*
* author:Xiong
* purpose:Set LCD Backlight
*/
void carimpl_GeneralFunc_SetLCDBacklight(char value)
{
    MenuSettingConfig()->uiLCDBacklight = value;
    switch (MenuSettingConfig()->uiLCDBacklight)
    {
    #if (MENU_SYSTEM_BACKLIGHT_LV0_EN)
    case BACKLIGHT_00:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 0", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV1_EN)
    case BACKLIGHT_01:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 1", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV2_EN)
    case BACKLIGHT_02:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 2", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV3_EN)
    case BACKLIGHT_03:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 3", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV4_EN)
    case BACKLIGHT_04:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 4", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV5_EN)
    case BACKLIGHT_05:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 5", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV6_EN)
    case BACKLIGHT_06:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 6", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV7_EN)
    case BACKLIGHT_07:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 7", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV8_EN)
    case BACKLIGHT_08:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 8", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV9_EN)
    case BACKLIGHT_09:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 9", "");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV10_EN)
    case BACKLIGHT_10:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Backlight 10", "");
        break;
    #endif
    default:
        break;
    }
}

/*
* author:Xiong
* purpose:Set Volume
*/
void carimpl_GeneralFunc_SetVolume(char value)
{
    MenuSettingConfig()->uiSystemVolume = value;
    switch (MenuSettingConfig()->uiSystemVolume)
    {
    #if (MENU_SYSTEM_VOLUME_LV0_EN)
    case VOLUME_00:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 0", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV1_EN)
    case VOLUME_01:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 1", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV2_EN)
    case VOLUME_02:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 2", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV3_EN)
    case VOLUME_03:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 3", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV4_EN)
    case VOLUME_04:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 4", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV5_EN)
    case VOLUME_05:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 5", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV6_EN)
    case VOLUME_06:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 6", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV7_EN)
    case VOLUME_07:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 7", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV8_EN)
    case VOLUME_08:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 8", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV9_EN)
    case VOLUME_09:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 9", "");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV10_EN)
    case VOLUME_10:
        carimpl_ExecCmdHandler(FIFO_CMD_TYPE, "Volume 10", "");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_GeneralFunc_setSysVolume(void)
{
    switch (MenuSettingConfig()->uiSystemVolume)
    {
    #if (MENU_SYSTEM_VOLUME_LV0_EN)
    case VOLUME_00:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "00");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV1_EN)
    case VOLUME_01:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "01");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV2_EN)
    case VOLUME_02:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "02");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV3_EN)
    case VOLUME_03:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "03");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV4_EN)
    case VOLUME_04:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "04");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV5_EN)
    case VOLUME_05:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "05");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV6_EN)
    case VOLUME_06:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "06");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV7_EN)
    case VOLUME_07:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "07");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV8_EN)
    case VOLUME_08:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "08");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV9_EN)
    case VOLUME_09:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "09");
        break;
    #endif
    #if (MENU_SYSTEM_VOLUME_LV10_EN)
    case VOLUME_10:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Volume", "10");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_GeneralFunc_SetSysLCDBacklight(void)
{
    switch (MenuSettingConfig()->uiLCDBacklight)
    {
    #if (MENU_SYSTEM_BACKLIGHT_LV0_EN)
    case BACKLIGHT_00:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "00");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV1_EN)
    case BACKLIGHT_01:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "01");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV2_EN)
    case BACKLIGHT_02:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "02");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV3_EN)
    case BACKLIGHT_03:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "03");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV4_EN)
    case BACKLIGHT_04:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "04");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV5_EN)
    case BACKLIGHT_05:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "05");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV6_EN)
    case BACKLIGHT_06:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "06");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV7_EN)
    case BACKLIGHT_07:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "07");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV8_EN)
    case BACKLIGHT_08:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "08");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV9_EN)
    case BACKLIGHT_09:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "09");
        break;
    #endif
    #if (MENU_SYSTEM_BACKLIGHT_LV10_EN)
    case BACKLIGHT_10:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set backlight", "10");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_GeneralFunc_SetCloseBacklight(void)
{
    carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set CloseBacklight", "OFF");
}

void carimpl_GeneralFunc_SetDatetimeFormat(void)
{
    switch (MenuSettingConfig()->uiDateTimeFormat)
    {
    #if (MENU_GENERAL_DATE_FORMAT_NONE_EN)
    case DATETIME_SETUP_NONE:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set DateTimeFormat", "NONE");
        break;
    #endif
    #if (MENU_GENERAL_DATE_FORMAT_YMD_EN)
    case DATETIME_SETUP_YMD:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set DateTimeFormat", "YMD");
        break;
    #endif
    #if (MENU_GENERAL_DATE_FORMAT_MDY_EN)
    case DATETIME_SETUP_MDY:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set DateTimeFormat", "MDY");
        break;
    #endif
    #if (MENU_GENERAL_DATE_FORMAT_DMY_EN)
    case DATETIME_SETUP_DMY:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set DateTimeFormat", "DMY");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_GeneralFunc_SetLanguage(void)
{
    switch (MenuSettingConfig()->uiLanguage)
    {
    #if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
    case LANGUAGE_ENGLISH:
        updata_osd_string(LANGUAGE_ENGLISH);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "English");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_SPANISH_EN)
    case LANGUAGE_SPANISH:
        updata_osd_string(LANGUAGE_SPANISH);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Spanish");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_PORTUGUESE_EN)
    case LANGUAGE_PORTUGUESE:
        updata_osd_string(LANGUAGE_PORTUGUESE);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Portuguese");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_RUSSIAN_EN)
    case LANGUAGE_RUSSIAN:
        updata_osd_string(LANGUAGE_RUSSIAN);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Russian");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
    case LANGUAGE_SCHINESE:
        updata_osd_string(LANGUAGE_SCHINESE);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Simplified");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
    case LANGUAGE_TCHINESE:
        updata_osd_string(LANGUAGE_TCHINESE);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Traditional");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_GERMAN_EN)
    case LANGUAGE_GERMAN:
        updata_osd_string(LANGUAGE_GERMAN);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "German");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_ITALIAN_EN)
    case LANGUAGE_ITALIAN:
        updata_osd_string(LANGUAGE_ITALIAN);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Italian");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_LATVIAN_EN)
    case LANGUAGE_LATVIAN:
        updata_osd_string(LANGUAGE_LATVIAN);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Latvian");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_POLISH_EN)
    case LANGUAGE_POLISH:
        updata_osd_string(LANGUAGE_POLISH);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Polish");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_ROMANIAN_EN)
    case LANGUAGE_ROMANIAN:
        updata_osd_string(LANGUAGE_ROMANIAN);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Romanian");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_SLOVAK_EN)
    case LANGUAGE_SLOVAK:
        updata_osd_string(LANGUAGE_SLOVAK);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Slovak");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_UKRANINIAN_EN)
    case LANGUAGE_UKRANINIAN:
        updata_osd_string(LANGUAGE_UKRANINIAN);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "UKRomanian");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_FRENCH_EN)
    case LANGUAGE_FRENCH:
        updata_osd_string(LANGUAGE_FRENCH);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "French");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_JAPANESE_EN)
    case LANGUAGE_JAPANESE:
        updata_osd_string(LANGUAGE_JAPANESE);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Japanese");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_KOREAN_EN)
    case LANGUAGE_KOREAN:
        updata_osd_string(LANGUAGE_KOREAN);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Korean");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_CZECH_EN)
    case LANGUAGE_CZECH:
        updata_osd_string(LANGUAGE_CZECH);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Czech");
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_VIETNAMESE_EN)
    case LANGUAGE_VIETNAMESE:
        updata_osd_string(LANGUAGE_VIETNAMESE);
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set Language", "Vietnamese");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_GeneralFunc_UpdateUILocalesCode(void)
{
    switch (MenuSettingConfig()->uiLanguage)
    {
    #if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
    case LANGUAGE_ENGLISH:
        updata_osd_string(LANGUAGE_ENGLISH);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_SPANISH_EN)
    case LANGUAGE_SPANISH:
        updata_osd_string(LANGUAGE_SPANISH);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_PORTUGUESE_EN)
    case LANGUAGE_PORTUGUESE:
        updata_osd_string(LANGUAGE_PORTUGUESE);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_RUSSIAN_EN)
    case LANGUAGE_RUSSIAN:
        updata_osd_string(LANGUAGE_RUSSIAN);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
    case LANGUAGE_SCHINESE:
        updata_osd_string(LANGUAGE_SCHINESE);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
    case LANGUAGE_TCHINESE:
        updata_osd_string(LANGUAGE_TCHINESE);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_GERMAN_EN)
    case LANGUAGE_GERMAN:
        updata_osd_string(LANGUAGE_GERMAN);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_ITALIAN_EN)
    case LANGUAGE_ITALIAN:
        updata_osd_string(LANGUAGE_ITALIAN);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_LATVIAN_EN)
    case LANGUAGE_LATVIAN:
        updata_osd_string(LANGUAGE_LATVIAN);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_POLISH_EN)
    case LANGUAGE_POLISH:
        updata_osd_string(LANGUAGE_POLISH);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_ROMANIAN_EN)
    case LANGUAGE_ROMANIAN:
        updata_osd_string(LANGUAGE_ROMANIAN);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_SLOVAK_EN)
    case LANGUAGE_SLOVAK:
        updata_osd_string(LANGUAGE_SLOVAK);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_UKRANINIAN_EN)
    case LANGUAGE_UKRANINIAN:
        printf("err Has NO UKRomanian!! \n");
        updata_osd_string(LANGUAGE_UKRANINIAN);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_FRENCH_EN)
    case LANGUAGE_FRENCH:
        updata_osd_string(LANGUAGE_FRENCH);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_JAPANESE_EN)
    case LANGUAGE_JAPANESE:
        updata_osd_string(LANGUAGE_JAPANESE);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_KOREAN_EN)
    case LANGUAGE_KOREAN:
        updata_osd_string(LANGUAGE_KOREAN);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_CZECH_EN)
    case LANGUAGE_CZECH:
        updata_osd_string(LANGUAGE_CZECH);
        break;
    #endif
    #if (MENU_GENERAL_LANGUAGE_VIETNAMESE_EN)
    case LANGUAGE_VIETNAMESE:
        updata_osd_string(LANGUAGE_VIETNAMESE);
        break;
    #endif
    default:
        updata_osd_string(LANGUAGE_ENGLISH);  // set to default language
        break;
    }
}

void carimpl_GeneralFunc_SetResetSetting(void)
{
    if (MenuSettingConfig()->uiResetSetting != 0)
    {
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ResetSetting", "YES");
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set FactoryReset", "0");
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set RebootSystem", "0");
        MenuSettingConfig()->uiResetSetting = 0;
    } else {
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ResetSetting", "NO");
    }
}

void carimpl_GeneralFunc_SetLcdPowerSave(void)
{
    switch (MenuSettingConfig()->uiLCDPowerSave)
    {
        #if (MENU_GENERAL_LCD_POWER_SAVE_OFF_EN)
        case LCD_POWER_SAVE_OFF:
            gulLCDPowerSaveRtcTime = 0;
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LcdPowerSave", "OFF");
            break;
        #endif
        #if (MENU_GENERAL_LCD_POWER_SAVE_10SEC_EN)
        case LCD_POWER_SAVE_10SEC:
            gulLCDPowerSaveRtcTime = 10;
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LcdPowerSave", "10SEC");
            break;
        #endif
        #if (MENU_GENERAL_LCD_POWER_SAVE_30SEC_EN)
        case LCD_POWER_SAVE_30SEC:
            gulLCDPowerSaveRtcTime = 30;
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LcdPowerSave", "30SEC");
            break;
        #endif
        #if (MENU_GENERAL_LCD_POWER_SAVE_1MIN_EN)
        case LCD_POWER_SAVE_1MIN:
            gulLCDPowerSaveRtcTime = 60;
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LcdPowerSave", "1MIN");
            break;
        #endif
        #if (MENU_GENERAL_LCD_POWER_SAVE_3MIN_EN)
        case LCD_POWER_SAVE_3MIN:
            gulLCDPowerSaveRtcTime = 180;
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LcdPowerSave", "3MIN");
            break;
        #endif
        #if (MENU_GENERAL_LCD_POWER_SAVE_5MIN_EN)
        case LCD_POWER_SAVE_5MIN:
            gulLCDPowerSaveRtcTime = 300;
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LcdPowerSave", "5MIN");
            break;
        #endif
        default:
            break;
    }
}

void carimpl_GeneralFunc_setGSensorSensitivity(void)
{
    switch (MenuSettingConfig()->uiGsensorSensitivity)
    {
        #if (MENU_GENERAL_GSENSOR_OFF_EN)
        case GSENSOR_SENSITIVITY_OFF:
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "OFF");
            break;
        #endif
        #if (MENU_GENERAL_GSENSOR_LEVEL0_EN)
        case GSENSOR_SENSITIVITY_L0:
            #if (MENU_GENERAL_GSENSOR_EN == GSENSOR_3_LEVEL)
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "LOW");
            #else
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "LEVEL0");
            #endif
            break;
        #endif
        #if (MENU_GENERAL_GSENSOR_LEVEL1_EN)
        case GSENSOR_SENSITIVITY_L1:
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "LEVEL1");
            break;
        #endif
        #if (MENU_GENERAL_GSENSOR_LEVEL2_EN)
        case GSENSOR_SENSITIVITY_L2:
            #if (MENU_GENERAL_GSENSOR_EN == GSENSOR_3_LEVEL)
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "MID");
            #else
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "LEVEL2");
            #endif
            break;
        #endif
        #if (MENU_GENERAL_GSENSOR_LEVEL3_EN)
        case GSENSOR_SENSITIVITY_L3:
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "LEVEL3");
            break;
        #endif
        #if (MENU_GENERAL_GSENSOR_LEVEL4_EN)
        case GSENSOR_SENSITIVITY_L4:
            #if (MENU_GENERAL_GSENSOR_EN == GSENSOR_3_LEVEL)
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "HIGH");
            #else
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set GSensor", "LEVEL4");
            #endif
            break;
        #endif
    default:
        break;
    }
}

void carimpl_GeneralFunc_SetReversingLine(void)
{
    switch (MenuSettingConfig()->uiReversingLine)
    {
    #if (MENU_GENERAL_REVERSING_LINE_ON_EN)
    case REVERSING_LINE_ON:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ReversingLine", "ON");
        break;
    #endif
    #if (MENU_GENERAL_REVERSING_LINE_OFF_EN)
    case REVERSING_LINE_OFF:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set ReversingLine", "OFF");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_GeneralFunc_SetReversingLinePoint(char type)
{
    char strValue[16] = {0};
    switch(type){
        case REVERSING_LEFT_TOP_X :
            sprintf(strValue,"%d",MenuSettingConfig()->uiLeftTopX);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LeftTopX", strValue);
            break;
        case REVERSING_LEFT_TOP_Y :
            sprintf(strValue,"%d",MenuSettingConfig()->uiLeftTopY);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LeftTopY", strValue);
            break;
        case REVERSING_LEFT_BOTTOM_X :
            sprintf(strValue,"%d",MenuSettingConfig()->uiLeftBottomX);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LeftBottomX", strValue);
            break;
        case REVERSING_LEFT_BOTTOM_Y :
            sprintf(strValue,"%d",MenuSettingConfig()->uiLeftBottomY);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LeftBottomY", strValue);
            break;
        case REVERSING_LEFT_MIDDLE1_RATIO :
            sprintf(strValue,"%d",MenuSettingConfig()->uiLeftMid1Ratio);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LeftMid1Ratio", strValue);
            break;
        case REVERSING_LEFT_MIDDLE2_RATIO :
            sprintf(strValue,"%d",MenuSettingConfig()->uiLeftMid2Ratio);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set LeftMid2Ratio", strValue);
            break;
        case REVERSING_RIGHT_TOP_X :
            sprintf(strValue,"%d",MenuSettingConfig()->uiRightTopX);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set RightTopX", strValue);
            break;
        case REVERSING_RIGHT_TOP_Y :
            sprintf(strValue,"%d",MenuSettingConfig()->uiRightTopY);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set RightTopY", strValue);
            break;
        case REVERSING_RIGHT_BOTTOM_X :
            sprintf(strValue,"%d",MenuSettingConfig()->uiRightBottomX);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set RightBottomX", strValue);
            break;
        case REVERSING_RIGHT_BOTTOM_Y :
            sprintf(strValue,"%d",MenuSettingConfig()->uiRightBottomY);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set RightBottomY", strValue);
            break;
        case REVERSING_RIGHT_MIDDLE1_RATIO :
            sprintf(strValue,"%d",MenuSettingConfig()->uiRightMid1Ratio);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set RightMid1Ratio", strValue);
            break;
        case REVERSING_RIGHT_MIDDLE2_RATIO :
            sprintf(strValue,"%d",MenuSettingConfig()->uiRightMid2Ratio);
            carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set RightMid2Ratio", strValue);
            break;
        default:
            break;
    }
}

void carimpl_GeneralFunc_SetDateLogoDisplay(void)
{
    switch (MenuSettingConfig()->uiDateLogoDisplay)
    {
    #if (MENU_GENERAL_DATE_LOGO_DISPLAY_ON_EN)
    case DATE_LOGO_DISPLAY_ON:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set DateLogoDisplay", "ON");
        break;
    #endif
    #if (MENU_GENERAL_DATE_LOGO_DISPLAY_OFF_EN)
    case DATE_LOGO_DISPLAY_OFF:
        carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set DateLogoDisplay", "OFF");
        break;
    #endif
    default:
        break;
    }
}

void carimpl_GeneralFunc_SetWifiOnOff(void)
{
    switch (MenuSettingConfig()->uiWifi)
    {
        #if (MENU_GENERAL_WIFI_OFF_EN)
        case WIFI_MODE_OFF:
            if(carimpl_ExecCmdHandler(SCRIPT_CMD_TYPE, "net_toggle.sh", "OFF")){
                MenuSettingConfig()->uiWifi = WIFI_MODE_OFF;
                carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set WiFi", "OFF");
            }
            break;
        #endif
        #if (MENU_GENERAL_WIFI_ON_EN)
        case WIFI_MODE_ON:
            if(carimpl_ExecCmdHandler(SCRIPT_CMD_TYPE, "net_toggle.sh", "2.4G")){
                MenuSettingConfig()->uiWifi = WIFI_MODE_ON;
                carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set WiFi", "2.4G");
            }
            break;
        #endif
        #if (MENU_GENERAL_WIFI_2_4G_EN)
        case WIFI_MODE_2_4G:
            if(carimpl_ExecCmdHandler(SCRIPT_CMD_TYPE, "net_toggle.sh", "2.4G")){
                MenuSettingConfig()->uiWifi = WIFI_MODE_2_4G;
                carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set WiFi", "2.4G");
            }
            break;
        #endif
        #if (MENU_GENERAL_WIFI_5G_EN)
        case WIFI_MODE_5G:
            if(carimpl_ExecCmdHandler(SCRIPT_CMD_TYPE, "net_toggle.sh", "5G")){
                MenuSettingConfig()->uiWifi = WIFI_MODE_5G;
                carimpl_ExecCmdHandler(CGI_CMD_TYPE, "set WiFi", "5G");
            }
            break;
        #endif
        default:
            break;
    }
}

void carimpl_GeneralFunc_GetSoftwareVersion(void)
{
    carimpl_ExecCmdHandler(CGI_CMD_TYPE, "get FwVer", "");
}

/**********************************************************/
/********************** wifi ******************************/
/**********************************************************/
NET_STATUS_TYPE check_wifi_status(void)
{
    NET_STATUS_TYPE eRet = NET_STATUS_INIT_FAIL;
    int net_fd = -1;
    char status[10] = {0};
    net_fd = open("/sys/class/net/wlan0/operstate", O_RDONLY);
    if (net_fd < 0) {
        eRet = NET_STATUS_INIT_FAIL;
    } else {
        if (read(net_fd, status, sizeof(status)) > 0) {
            if (NULL != strstr(status,"up")) {
                eRet = NET_STATUS_UP;
            } else if (NULL != strstr(status,"down")) {
                eRet = NET_STATUS_DOWN;
            } else {
                eRet = NET_STATUS_UNKNOW;
            }
        }
        close (net_fd);
    }

    if (eRet == NET_STATUS_UP) {
        char cmdstr[256] = {0};
        int SignalLevel = 0;
        //AP mode find deviceinfo
        sprintf(cmdstr, "iw dev wlan0 station dump | grep signal | /usr/bin/awk '{print $2}'");
        FILE *fp = popen(cmdstr, "r");
        if (!fp) {
            return eRet;
        }
        memset(gCmdBuf, 0, sizeof(gCmdBuf));
        while (fgets(gCmdBuf, sizeof(gCmdBuf), fp) != NULL) {
            //printf("DeviceInfo: %s\n", gCmdBuf);
        }
        pclose(fp);

        if (strlen(gCmdBuf) != 0) {
            //printf("check signal level dBm\n");
            SignalLevel = atoi(gCmdBuf);
            if (SignalLevel < -60 && SignalLevel > -90)
                eRet = NET_STATUS_WEAK;
            else
                eRet = NET_STATUS_READY;
        }
    }
    return eRet;
}

/*===========================================================================
* Warning message related
*===========================================================================*/
extern void StatusBar_Update(void);
void carimpl_show_wmsg(bool bEnable, WMSG_INFO WMSGInfo)
{
    if (bEnable && (WMSGInfo == WMSG_NONE))
        return;
    if (bEnable) {
        m_WMSGInfo = WMSGInfo;
        StatusBar_Update();
        if (CusGetStatusbarFlags() == false) {
        CusSetStatusbarFlags(true);
        }
    } else {
        m_WMSGInfo = WMSG_NONE;
        CusSetStatusbarFlags(false);
    }
}

WMSG_INFO carimpl_get_wmsginfo(void)
{
    return m_WMSGInfo;
}

/**********************************************************/
/**********************others******************************/
/**********************************************************/
void carimpl_Poweroff_handler(void)
{
    carimpl_show_wmsg(true, WMSG_SYSTEM_POWER_OFF);
    if (IMPL_REC_STATUS || IMPL_EMERG_REC_STATUS || IMPL_MD_REC_STATUS) {
        carimpl_VideoFunc_StartRecording(0);
    }
}

void carimpl_MenuSetting_Init(void)
{
    carimpl_GeneralFunc_UpdateUILocalesCode();
}

void carimpl_set_colorkey(const char * dev, bool bEnable)
{

}

void carimpl_set_alpha_blending(const char * dev, bool bEnable, MI_U8 u8GlobalAlpha)
{

}

void carimpl_update_status(const char *status_name, const char *status, size_t size)
{
    int fd_wr = -1;
    fd_wr = open(status_name, O_RDWR | O_CREAT, 0666);
    if (fd_wr >= 0) {
        write(fd_wr, status, size);
        close(fd_wr);
    }
    else {
        printf("Error: failed to open %s\n", status_name);
    }
}

MI_S32 carimpl_process_msg(IPC_MSG_ID id, MI_S8 *param, MI_S32 paramLen)
{
    printf("[UI] msg [%d]\n", id);
    if (!(CusIsMainScreen() || CusIsVideoMenuScreen() || CusIsPlayBackScreen() || CusIsPowerOffScreen()))
    return 0;

    IPC_CarInfo_Read(&carimpl);

    switch (id) {
    case IPC_MSG_UI_SD_MOUNT:
        //carimpl_show_wmsg(true, WMSG_WAIT_INITIAL_DONE);
        if (CusIsPlayBackScreen() == TRUE &&
        (gu8LastUIMode == UI_BROWSER_MODE || gu8LastUIMode == UI_PLAYBACK_MODE)){
        Carimpl_DcfMount();
        }
        break;
    case IPC_MSG_UI_SD_PRE_MOUNT:
        carimpl_show_wmsg(true, WMSG_WAIT_INITIAL_DONE);
        break;
    case IPC_MSG_UI_SD_UNMOUNT:
        if (CusIsPlayBackScreen() == TRUE) {
        gu8LastUIMode = UI_PLAYBACK_MODE;
        }
        Carimpl_DcfUnmount();
        carimpl_show_wmsg(true, WMSG_PLUG_OUT_SD_CARD);
        break;
    case IPC_MSG_UI_SD_NEED_FORMAT:
        format_sd_card_flag = true;
        carimpl_show_wmsg(true, WMSG_FORMAT_SD_CARD);
        break;
    case IPC_MSG_UI_FORMAT_SD_OK:
        carimpl_show_wmsg(true, WMSG_FORMAT_SD_CARD_OK);
        break;
    case IPC_MSG_UI_FORMAT_SD_FAILED:
        carimpl_show_wmsg(true, WMSG_FORMAT_SD_CARD_FAIL);
        break;
    case IPC_MSG_UI_SPEECH_POWEROFF:
        carimpl_Poweroff_handler();
        break;
    case IPC_MSG_UI_SETTING_UPDATE:
        if(CusIsPlayBackScreen() == FALSE){
        MenuSettingInit();//Maybe config bin has been modify by android APP.
        }
        break;
    case IPC_MSG_UI_SD_NOCARD:
        carimpl_show_wmsg(true, WMSG_NO_CARD);
        break;
    case IPC_MSG_UI_STOP:
        break;
    default:
        printf("%s: a msg from CardvImpl,unsupported!\n",__FUNCTION__);
        break;
    }

    return 0;
}

void carimpl_msg_handler(struct IPCMsgBuf *pMsgBuf)
{
    carimpl_process_msg(pMsgBuf->eIPCMsgId, (MI_S8 *)pMsgBuf->s8ParamBuf, pMsgBuf->u32ParamLen);
}
