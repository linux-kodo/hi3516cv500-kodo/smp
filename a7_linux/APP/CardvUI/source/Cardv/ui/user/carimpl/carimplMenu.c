/*
* carimplMenu.h
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include <string.h>
#include <stdlib.h>
#include "carimpl.h"
#include "common.h"
#include "carimplMenu.h"

//==============================================================================
//                              GOBAL VARIATE
//==============================================================================


//==============================================================================
//                              EXTERN VARIATE
//==============================================================================
extern struct CarDV_Info carimpl;

//==============================================================================
//                              MACRO DEFINES
//==============================================================================

//==============================================================================
//                              EXTERN FUNCTION
//==============================================================================

//==============================================================================
//                              FUNCTIONS
//==============================================================================

/*
* function:mov size hander
*/
void carimpl_menu_hander_mov_size(char *pBuff)
{
	LOG_UI_DBG("pBuff:%s \r\n", pBuff );
#if MENU_MOVIE_SIZE_4K_25P_EN
	if(0==strcmp("4K",pBuff))
	{
		MenuSettingConfig()->uiMOVSize =MOVIE_SIZE_4K_25P;
	}
#endif
#if MENU_MOVIE_SIZE_1440_30P_EN
	if(0==strcmp("2K",pBuff))
	{
		MenuSettingConfig()->uiMOVSize =MOVIE_SIZE_1440_30P;
	}
#endif
#if MENU_MOVIE_SIZE_1080P_EN
	if(0==strcmp("1080P",pBuff))
	{
		MenuSettingConfig()->uiMOVSize =MOVIE_SIZE_1080P;
	}
#endif
}

/*
* function:loopvideo hander
*/
void carimpl_menu_hander_loopvideo(char *pBuff)
{
	LOG_UI_DBG("pBuff:%s \r\n", pBuff );
#if (MENU_MOVIE_CLIP_TIME_1MIN_EN)
	if(0==strcmp("1MIN",pBuff))
	{
		MenuSettingConfig()->uiMOVClipTime =MOVIE_CLIP_TIME_1MIN;
	}
#endif
#if (MENU_MOVIE_CLIP_TIME_2MIN_EN)
	else if(0==strcmp("2MIN",pBuff))
	{
		MenuSettingConfig()->uiMOVClipTime =MOVIE_CLIP_TIME_2MIN;
	}
#endif
#if (MENU_MOVIE_CLIP_TIME_3MIN_EN)
	else if(0==strcmp("3MIN",pBuff))
	{
		MenuSettingConfig()->uiMOVClipTime =MOVIE_CLIP_TIME_3MIN;
	}
#endif
#if (MENU_MOVIE_CLIP_TIME_5MIN_EN)
	else if(0==strcmp("5MIN",pBuff))
	{
		MenuSettingConfig()->uiMOVClipTime =MOVIE_CLIP_TIME_5MIN;
	}
#endif
}

/*
* function:sound record hander
*/
void carimpl_menu_hander_sound_record(char *pBuff)
{
	LOG_UI_DBG("pBuff:%s \r\n", pBuff );

	if(0==strcmp("ON",pBuff))
	{
		MenuSettingConfig()->uiMOVSoundRecord =MOVIE_SOUND_RECORD_ON;
	}
	else if(0==strcmp("OFF",pBuff))
	{
		MenuSettingConfig()->uiMOVSoundRecord =MOVIE_SOUND_RECORD_OFF;
	}
}

/*
* function:gensor sensitivity hander
*/
void carimpl_menu_hander_gensor_sensitivity(char *pBuff)
{
	LOG_UI_DBG("pBuff:%s \r\n", pBuff );

	if(0==strcmp("OFF",pBuff))
	{
		MenuSettingConfig()->uiGsensorSensitivity =GSENSOR_SENSITIVITY_OFF;
	}
	else if(0==strcmp("LOW",pBuff))
	{
		MenuSettingConfig()->uiGsensorSensitivity =GSENSOR_SENSITIVITY_L0;
	}
	else if(0==strcmp("MID",pBuff))
	{
		MenuSettingConfig()->uiGsensorSensitivity =GSENSOR_SENSITIVITY_L2;
	}
	else if(0==strcmp("HIGH",pBuff))
	{
		MenuSettingConfig()->uiGsensorSensitivity =GSENSOR_SENSITIVITY_L4;
	}
}

/*
* function:lcd power save hander
*/
void carimpl_menu_hander_lcd_power_save(char *pBuff)
{
	LOG_UI_DBG("pBuff:%s \r\n", pBuff );

	if(0==strcmp("1MIN",pBuff))
	{
		MenuSettingConfig()->uiLCDPowerSave =LCD_POWER_SAVE_1MIN;
	}
	else if(0==strcmp("3MIN",pBuff))
	{
		MenuSettingConfig()->uiLCDPowerSave =LCD_POWER_SAVE_3MIN;
	}
	else if(0==strcmp("5MIN",pBuff))
	{
		MenuSettingConfig()->uiLCDPowerSave =LCD_POWER_SAVE_5MIN;
	}
	else if(0==strcmp("OFF",pBuff))
	{
		MenuSettingConfig()->uiLCDPowerSave =LCD_POWER_SAVE_OFF;
	}
}

/*
* function:date time format hander
*/
void carimpl_menu_hander_date_time_format(char *pBuff)
{
	LOG_UI_DBG("pBuff:%s \r\n", pBuff );

	if(0==strcmp("YMD",pBuff))
	{
		MenuSettingConfig()->uiDateTimeFormat =DATETIME_SETUP_YMD;
	}
	else if(0==strcmp("DMY",pBuff))
	{
		MenuSettingConfig()->uiDateTimeFormat =DATETIME_SETUP_DMY;
	}
	else if(0==strcmp("MDY",pBuff))
	{
		MenuSettingConfig()->uiDateTimeFormat =DATETIME_SETUP_MDY;
	}
}

/*
* function:wifi hander
*/
void carimpl_menu_hander_wifi(char *pBuff)
{
	LOG_UI_DBG("pBuff:%s \r\n", pBuff );

	if(0==strcmp("OFF",pBuff))
	{
		MenuSettingConfig()->uiWifi = WIFI_MODE_OFF;
	}
	#if (MENU_GENERAL_WIFI_ON_EN)
	else if(0==strcmp("2.4G",pBuff))
	{
		carimpl_show_wmsg(true, WMSG_WIFI_STARTING);
		MenuSettingConfig()->uiWifi =WIFI_MODE_ON;
	}
	#endif
	#if (MENU_GENERAL_WIFI_2_4G_EN)
	else if(0==strcmp("2.4G",pBuff))
	{
		carimpl_show_wmsg(true, WMSG_WIFI_STARTING);
		MenuSettingConfig()->uiWifi =WIFI_MODE_2_4G;
	}
	#endif
	#if (MENU_GENERAL_WIFI_5G_EN)
	else if(0==strcmp("5G",pBuff))
	{
		carimpl_show_wmsg(true, WMSG_WIFI_STARTING);
		MenuSettingConfig()->uiWifi =WIFI_MODE_5G;
	}
	#endif
}

/*
* function:parse menu
*/
E_CARIMMPL_MENU_ID carimpl_menu_id_parse(char *pBuff, int iBuff_sz)
{
	int i =0;
	CARIMMPL_MENU_ST menu_st[]={
		{CARIMMPL_MENU_SOUND_RECORD,		"SoundRecord"},\
		{CARIMMPL_MENU_LCD_POWER_SAVE,		"LcdPowerSave"},\
		{CARIMMPL_MENU_LCD_GSENSOR_SEN,		"GSensorSensitivity"},\
		{CARIMMPL_MENU_LCD_LOOPVIDEO,		"LoopingVideo"},\
		{CARIMMPL_MENU_WIFI,				"WiFi"},\
		{CARIMMPL_MENU_DATE_TIME_FORMAT,	"DateTimeFormat"},\
		{CARIMMPL_MENU_MOV_SIZE,			"res"}};

	for( i =0;i<CARIMMPL_MENU_MAX;i++)
	{
		if(0 == memcmp(pBuff, menu_st[i].cmd, strlen(menu_st[i].cmd)))
		{
			LOG_UI_DBG("menu_st[%d].cmd:%s, len:%d\r\n", i, menu_st[i].cmd, strlen(menu_st[i].cmd));
			return menu_st[i].id;
		}
	}
	return CARIMMPL_MENU_MAX;
}

/*
* function:menu parse hander
*/
void carimpl_menu_hander(char *msgBuf, MI_S32 msgBufLen)
{
	char* str;
	char cmd[64];
	char value[64];
	E_CARIMMPL_MENU_ID cmd_id = CARIMMPL_MENU_MAX;

	if(msgBufLen<2)
	{
		LOG_UI_WARN("msgBufLen<2 & return; \r\n");
		return;
	}

	memset(cmd,0,64);
	str = strtok(msgBuf, "/");
	memcpy(cmd, str, strlen(str));
	cmd_id = carimpl_menu_id_parse(cmd,strlen(cmd));
	LOG_UI_DBG("cmd_id:%d \r\n", (int)cmd_id );

	if(CARIMMPL_MENU_MAX != cmd_id)
	{
		str = strtok(NULL, "/");
		memset(value,0,64);
		memcpy(value, str, strlen(str));
		LOG_UI_DBG("value:%s \r\n", value );
	} else {
		return;
	}

	switch(cmd_id)
	{
		case CARIMMPL_MENU_SOUND_RECORD:
			carimpl_menu_hander_sound_record(value);
			break;
		case CARIMMPL_MENU_LCD_POWER_SAVE:
			carimpl_menu_hander_lcd_power_save(value);
			break;
		case CARIMMPL_MENU_LCD_GSENSOR_SEN:
			carimpl_menu_hander_gensor_sensitivity(value);
			break;
		case CARIMMPL_MENU_WIFI:
			carimpl_menu_hander_wifi(value);
			break;
		case CARIMMPL_MENU_DATE_TIME_FORMAT:
			carimpl_menu_hander_date_time_format(value);
			break;
		case CARIMMPL_MENU_MOV_SIZE:
			carimpl_menu_hander_mov_size(value);
			break;
		default:
			break;
	}
}
