#include "carimpl.h"
#include "MenuCommon.h"

/*===========================================================================
* Local function
*===========================================================================*/
bool MenuItemClockSettings(PSMENUITEM pItem);
bool MenuItemDateTimeFormat(PSMENUITEM pItem);
bool MenuItemLanguage(PSMENUITEM pItem);
bool MenuItemFormatSDCard(PSMENUITEM pItem);
bool MenuItemResetSetup(PSMENUITEM pItem);
bool MenuItemFwVersion(PSMENUITEM pItem);
bool MenuItemQRCode(PSMENUITEM pItem);
bool MenuItemFlickerHz(PSMENUITEM pItem);
bool MenuItemReversingLine(PSMENUITEM pItem);
bool MenuItemLCDPowerSave(PSMENUITEM pItem);
bool MenuItemKeyTone(PSMENUITEM pItem);
bool MenuItemShutdownTone(PSMENUITEM pItem);
bool MenuItemGsensorSensitivity(PSMENUITEM pItem);
bool MenuItemWiFiMode(PSMENUITEM pItem);
bool MenuItemDateLogoDisplay(PSMENUITEM pItem);

int  MenuGetDefault_GeneralSetting(PSMENUSTRUCT pMenu );

/*===========================================================================
* Extern varible
*===========================================================================*/

/*===========================================================================
* Global varible : Menu
*===========================================================================*/
extern SMENUSTRUCT sMainMenuGeneral;
extern SMENUSTRUCT sMainMenuSystem;
extern SMENUSTRUCT sMainMenuSystemEdog;

extern SMENUSTRUCT sSubClockSettings;
extern SMENUSTRUCT sSubDateTimeFormat;
extern SMENUSTRUCT sSubLanguage;
extern SMENUSTRUCT sSubFormatSDCard;
extern SMENUSTRUCT sSubResetSetup;
extern SMENUSTRUCT sSubFwVersionInfo;
extern SMENUSTRUCT sSubQRCodeInfo;
extern SMENUSTRUCT sSubFlickerHz;
extern SMENUSTRUCT sSubReversingLine;
extern SMENUSTRUCT sSubLCDPowerSave;
extern SMENUSTRUCT sSubKeyTone;
extern SMENUSTRUCT sSubShutdownTone;
extern SMENUSTRUCT sSubGsensorSensitivity;
extern SMENUSTRUCT sSubWiFiMode;
extern SMENUSTRUCT sSubDateLogoDisplay;

/*===========================================================================
* Global variable : Item Structure
*===========================================================================*/
// Clock Settings
#if (MENU_GENERAL_CLOCK_SETTING_EN)
SMENUITEM sItemClockSettings        = { ITEMID_CLOCK_SETTINGS,   IDS_DS_CLOCK_SETTINGS,   &sSubClockSettings, 0 };
SMENUITEM sItemClockSettings_Year   = { ITEMID_DATETIME_YEAR,    IDS_DS_DATETIME_Y,       NULL, MenuItemClockSettings };
SMENUITEM sItemClockSettings_Month  = { ITEMID_DATETIME_MONTH,   IDS_DS_DATETIME_M,       NULL, MenuItemClockSettings };
SMENUITEM sItemClockSettings_Day    = { ITEMID_DATETIME_DAY,     IDS_DS_DATETIME_D,       NULL, MenuItemClockSettings };
SMENUITEM sItemClockSettings_Hour   = { ITEMID_DATETIME_HOUR,    IDS_DS_DATETIME_H,       NULL, MenuItemClockSettings };
SMENUITEM sItemClockSettings_Minute = { ITEMID_DATETIME_MINUTE,  IDS_DS_DATETIME_MM,      NULL, MenuItemClockSettings };
SMENUITEM sItemClockSettings_Sec    = { ITEMID_DATETIME_SEC,     IDS_DS_DATETIME_S,       NULL, MenuItemClockSettings };
SMENUITEM sItemClockSettings_OK     = { ITEMID_DATETIME_OK,      IDS_DS_OK,               NULL, MenuItemClockSettings };
SMENUITEM sItemClockSettings_Cancel = { ITEMID_DATETIME_CANCEL,  IDS_DS_CANCEL,           NULL, MenuItemClockSettings };
#endif

// Date/Time Format
#if (MENU_GENERAL_DATE_FORMAT_EN)
SMENUITEM sItemDateTimeFormat       = { ITEMID_DATETIME_FORMAT, IDS_DS_DATETIME_FORMAT, &sSubDateTimeFormat, 0 };
#if (MENU_GENERAL_DATE_FORMAT_NONE_EN)
SMENUITEM sItemDateTime_None        = { ITEMID_DATETIME_NONE,   IDS_DS_NONE,            NULL, MenuItemDateTimeFormat };
#endif
#if (MENU_GENERAL_DATE_FORMAT_YMD_EN)
SMENUITEM sItemDateTime_YMD         = { ITEMID_DATETIME_YMD,    IDS_DS_DATETIME_YMD,    NULL, MenuItemDateTimeFormat };
#endif
#if (MENU_GENERAL_DATE_FORMAT_MDY_EN)
SMENUITEM sItemDateTime_MDY         = { ITEMID_DATETIME_MDY,    IDS_DS_DATETIME_MDY,    NULL, MenuItemDateTimeFormat };
#endif
#if (MENU_GENERAL_DATE_FORMAT_DMY_EN)
SMENUITEM sItemDateTime_DMY         = { ITEMID_DATETIME_DMY,    IDS_DS_DATETIME_DMY,    NULL, MenuItemDateTimeFormat };
#endif
#endif

// Language
#if (MENU_GENERAL_LANGUAGE_EN)
SMENUITEM sItemLanguage             = { ITEMID_LANGUAGE,             IDS_DS_LANGUAGE,                &sSubLanguage, 0 };
#if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
SMENUITEM sItemLanguage_English     = { ITEMID_LANGUAGES_ENGLISH,    IDS_DS_LANGUAGES_ENGLISH,       NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_SPANISH_EN)
SMENUITEM sItemLanguage_Spanish     = { ITEMID_LANGUAGES_SPANISH,    IDS_DS_LANGUAGES_SPANISH,       NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_PORTUGUESE_EN)
SMENUITEM sItemLanguage_Portuguese  = { ITEMID_LANGUAGES_PORTUGUESE, IDS_DS_LANGUAGES_PORTUGUESE,    NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_RUSSIAN_EN)
SMENUITEM sItemLanguage_Russian     = { ITEMID_LANGUAGES_RUSSIAN,    IDS_DS_LANGUAGES_RUSSIAN,       NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
SMENUITEM sItemLanguage_SChinese    = { ITEMID_LANGUAGES_SCHINESE,   IDS_DS_LANGUAGES_SCHINESE,      NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
SMENUITEM sItemLanguage_TChinese    = { ITEMID_LANGUAGES_TCHINESE,   IDS_DS_LANGUAGES_TCHINESE,      NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_GERMAN_EN)
SMENUITEM sItemLanguage_German     = { ITEMID_LANGUAGES_GERMAN,      IDS_DS_LANGUAGES_GERMAN,        NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_ITALIAN_EN)
SMENUITEM sItemLanguage_Italian    = { ITEMID_LANGUAGES_ITALIAN,     IDS_DS_LANGUAGES_ITALIAN,       NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_LATVIAN_EN)
SMENUITEM sItemLanguage_Latvian     = { ITEMID_LANGUAGES_LATVIAN,    IDS_DS_LANGUAGES_LATVIAN,       NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_POLISH_EN)
SMENUITEM sItemLanguage_Polish      = { ITEMID_LANGUAGES_POLISH,     IDS_DS_LANGUAGES_POLISH,        NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_ROMANIAN_EN)
SMENUITEM sItemLanguage_Romanian    = { ITEMID_LANGUAGES_ROMANIAN,   IDS_DS_LANGUAGES_ROMANIAN,      NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_SLOVAK_EN)
SMENUITEM sItemLanguage_Slovak      = { ITEMID_LANGUAGES_SLOVAK,     IDS_DS_LANGUAGES_SLOVAK,        NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_UKRANINIAN_EN)
SMENUITEM sItemLanguage_Ukraninian  = { ITEMID_LANGUAGES_UKRANINIAN, IDS_DS_LANGUAGES_UKRANINIAN,    NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_FRENCH_EN)
SMENUITEM sItemLanguage_French      = { ITEMID_LANGUAGES_FRENCH,     IDS_DS_LANGUAGES_FRENCH,        NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_JAPANESE_EN)
SMENUITEM sItemLanguage_Japanese    = { ITEMID_LANGUAGES_JAPANESE,   IDS_DS_LANGUAGES_JAPANESE,      NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_KOREAN_EN)
SMENUITEM sItemLanguage_Korean      = { ITEMID_LANGUAGES_KOREAN,     IDS_DS_LANGUAGES_KOREAN,        NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_CZECH_EN)
SMENUITEM sItemLanguage_Czech       = { ITEMID_LANGUAGES_CZECH,      IDS_DS_LANGUAGES_CZECH,         NULL, MenuItemLanguage };
#endif
#if (MENU_GENERAL_LANGUAGE_VIETNAMESE_EN)
SMENUITEM sItemLanguage_Vietnamese  = { ITEMID_LANGUAGES_VIETNAMESE, IDS_DS_LANGUAGES_VIETNAMESE,   NULL, MenuItemLanguage };
#endif
#endif

// Format SD-Card
#if (MENU_MEDIA_FORMAT_SD_EN)
SMENUITEM sItemFormatCard        = { ITEMID_FORMAT_SD_CARD,  IDS_DS_FORMAT_SD_CARD, &sSubFormatSDCard, 0 };
SMENUITEM sItemFormatCard_Yes    = { ITEMID_FORMAT_CARD_YES, IDS_DS_YES, NULL, MenuItemFormatSDCard };
SMENUITEM sItemFormatCard_No     = { ITEMID_FORMAT_CARD_NO,  IDS_DS_NO,  NULL, MenuItemFormatSDCard };
#endif

// Reset Setup
#if (MENU_GENERAL_RESET_SETUP_EN)
SMENUITEM sItemResetSetup           = { ITEMID_RESET_SETUP, IDS_DS_RESET_SETUP, &sSubResetSetup, 0 };
#if (MENU_GENERAL_RESET_SETUP_YES_EN)
SMENUITEM sItemResetSetup_Yes       = { ITEMID_RESET_YES,   IDS_DS_YES,         NULL, MenuItemResetSetup };
#endif
#if (MENU_GENERAL_RESET_SETUP_NO_EN)
SMENUITEM sItemResetSetup_No        = { ITEMID_RESET_NO,    IDS_DS_NO,          NULL, MenuItemResetSetup };
#endif
#endif

// FW Version Info
#if (MENU_GENERAL_FW_VERSION_EN)
SMENUITEM sItemFWVersionInfo        = { ITEMID_FW_VERSION_INFO, IDS_DS_FW_VERSION_INFO, &sSubFwVersionInfo, 0 };
SMENUITEM sItemFWVersion            = { ITEMID_FW_VERSION,      IDS_DS_FW_VERSION,      NULL, MenuItemFwVersion };
#endif

// QR code
#if (MENU_GENERAL_QR_CODE_EN)
SMENUITEM sItemQRCodeInfo  = { ITEMID_QR_CODE_INFO, IDS_DS_QR_CODE_INFO, &sSubQRCodeInfo, 0 };
SMENUITEM sItemQRCode      = { ITEMID_QR_CODE,      IDS_DS_QR_CODE,      NULL, MenuItemQRCode };
#endif

// Flicker Hz
#if (MENU_GENERAL_FLICKER_EN)
SMENUITEM sItemFlickerHz            = { ITEMID_FLICKER,      IDS_DS_FLICKER,   &sSubFlickerHz, 0 };
#if (MENU_GENERAL_FLICKER_50HZ_EN)
SMENUITEM sItemFlicker50Hz          = { ITEMID_FLICKER_50HZ, IDS_FLICKER_50HZ, NULL, MenuItemFlickerHz };
#endif
#if (MENU_GENERAL_FLICKER_60HZ_EN)
SMENUITEM sItemFlicker60Hz          = { ITEMID_FLICKER_60HZ, IDS_FLICKER_60HZ, NULL, MenuItemFlickerHz };
#endif
#endif

#if (MENU_GENERAL_REVERSING_LINE_EN)
SMENUITEM sItemReversingLine    = { ITEMID_REVERSING_LINE, IDS_DS_REVERSING_LINE,   &sSubReversingLine, 0 };
#if (MENU_GENERAL_REVERSING_LINE_ON_EN)
SMENUITEM sItemReversingLineOn  = { ITEMID_REVERSING_LINE_ON, IDS_REVERSING_LINE_ON, NULL, MenuItemReversingLine };
#endif
#if (MENU_GENERAL_REVERSING_LINE_OFF_EN)
SMENUITEM sItemReversingLineOff = { ITEMID_REVERSING_LINE_OFF, IDS_REVERSING_LINE_OFF, NULL, MenuItemReversingLine };
#endif
#endif

// LCD Power Save
#if (MENU_GENERAL_LCD_POWER_SAVE_EN)
SMENUITEM sItemLCDPowerSave         = { ITEMID_LCD_POWER_SAVE,          IDS_DS_LCD_POWER_SAVE, &sSubLCDPowerSave,     0 };
#if (MENU_GENERAL_LCD_POWER_SAVE_OFF_EN)
SMENUITEM sItemLCDPowerSave_Off     = { ITEMID_LCD_POWER_SAVE_OFF,      IDS_DS_OFF,          NULL, MenuItemLCDPowerSave };
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_10SEC_EN)
SMENUITEM sItemLCDPowerSave_10sec     = { ITEMID_LCD_POWER_SAVE_10SEC,  IDS_TIME_10SEC,      NULL, MenuItemLCDPowerSave };
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_30SEC_EN)
SMENUITEM sItemLCDPowerSave_30sec     = { ITEMID_LCD_POWER_SAVE_30SEC,  IDS_TIME_30SEC,      NULL, MenuItemLCDPowerSave };
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_1MIN_EN)
SMENUITEM sItemLCDPowerSave_1min    = { ITEMID_LCD_POWER_SAVE_1MIN,     IDS_TIME_1MIN,       NULL, MenuItemLCDPowerSave };
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_3MIN_EN)
SMENUITEM sItemLCDPowerSave_3min    = { ITEMID_LCD_POWER_SAVE_3MIN,     IDS_TIME_3MIN,       NULL, MenuItemLCDPowerSave };
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_5MIN_EN)
SMENUITEM sItemLCDPowerSave_5min    = { ITEMID_LCD_POWER_SAVE_5MIN,     IDS_TIME_5MIN,       NULL, MenuItemLCDPowerSave };
#endif
#endif

// SHUTDOWN TONE
#if (MENU_GENERAL_SHUTDOWN_TONE_EN)
SMENUITEM sItemShutdownTone = { ITEMID_SHUTDOWN_TONE,       IDS_DS_SHUTDOWN_TONE,    &sSubShutdownTone, 0 };
#if (MENU_GENERAL_SHUTDOWN_TONE_ON_EN)
    SMENUITEM sItemShutdownToneOn     = { ITEMID_SHUTDOWN_TONE_ON,    IDS_DS_ON,    NULL, MenuItemShutdownTone };
#endif
#if (MENU_GENERAL_SHUTDOWN_TONE_OFF_EN)
    SMENUITEM sItemShutdownToneOff    = { ITEMID_SHUTDOWN_TONE_OFF,   IDS_DS_OFF,   NULL, MenuItemShutdownTone };
#endif
SMENUITEM sItemShutdownToneLow    = { ITEMID_SHUTDOWN_TONE_LOW,   IDS_DS_LOW,   NULL, MenuItemShutdownTone };
SMENUITEM sItemShutdownToneMid    = { ITEMID_SHUTDOWN_TONE_MID,   IDS_DS_MIDDLE,   NULL, MenuItemShutdownTone };
SMENUITEM sItemShutdownToneHigh   = { ITEMID_SHUTDOWN_TONE_HIGH,   IDS_DS_HIGH,   NULL, MenuItemShutdownTone };
#endif

// KEY TONE
#if (MENU_GENERAL_KEY_TONE_EN)
SMENUITEM sItemKeyTone       = { ITEMID_KEY_TONE,       IDS_DS_KEY_TONE,    &sSubKeyTone, 0 };
#if (MENU_GENERAL_KEY_TONE_ON_EN)
    SMENUITEM sItemKeyToneOn     = { ITEMID_KEY_TONE_ON,    IDS_DS_ON,    NULL, MenuItemKeyTone };
#endif
#if (MENU_GENERAL_KEY_TONE_OFF_EN)
    SMENUITEM sItemKeyToneOff    = { ITEMID_KEY_TONE_OFF,   IDS_DS_OFF,   NULL, MenuItemKeyTone };
#endif
SMENUITEM sItemKeyToneLow    = { ITEMID_KEY_TONE_LOW,   IDS_DS_LOW,   NULL, MenuItemKeyTone };
SMENUITEM sItemKeyToneMid    = { ITEMID_KEY_TONE_MID,   IDS_DS_MIDDLE,   NULL, MenuItemKeyTone };
SMENUITEM sItemKeyToneHigh    = { ITEMID_KEY_TONE_HIGH,   IDS_DS_HIGH,   NULL, MenuItemKeyTone };
#endif

// G-sensor Sensitivity, 5 - level
#if (MENU_GENERAL_GSENSOR_EN==GSENSOR_5_LEVEL)
SMENUITEM sItemGsensorSensitivity       = { ITEMID_GSENSOR_SENSITIVITY,     IDS_DS_GSENSOR_SENSITIVETY,          &sSubGsensorSensitivity,    0 };
#if (MENU_GENERAL_GSENSOR_OFF_EN)
SMENUITEM sItemGsensorSensitivity_Off   = { ITEMID_GSENSOR_SENSITIVITY_OFF, IDS_DS_OFF,                           NULL, MenuItemGsensorSensitivity };
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL0_EN)
SMENUITEM sItemGsensorSensitivity_L0    = { ITEMID_GSENSOR_SENSITIVITY_L0,  IDS_DS_GSENSOR_SENSITIVETY_LEVEL0,    NULL, MenuItemGsensorSensitivity };
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL1_EN)
SMENUITEM sItemGsensorSensitivity_L1    = { ITEMID_GSENSOR_SENSITIVITY_L1,  IDS_DS_GSENSOR_SENSITIVETY_LEVEL1,    NULL, MenuItemGsensorSensitivity };
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL2_EN)
SMENUITEM sItemGsensorSensitivity_L2    = { ITEMID_GSENSOR_SENSITIVITY_L2,  IDS_DS_GSENSOR_SENSITIVETY_LEVEL2,    NULL, MenuItemGsensorSensitivity };
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL3_EN)
SMENUITEM sItemGsensorSensitivity_L3    = { ITEMID_GSENSOR_SENSITIVITY_L3,  IDS_DS_GSENSOR_SENSITIVETY_LEVEL3,    NULL, MenuItemGsensorSensitivity };
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL4_EN)
SMENUITEM sItemGsensorSensitivity_L4    = { ITEMID_GSENSOR_SENSITIVITY_L4,  IDS_DS_GSENSOR_SENSITIVETY_LEVEL4,    NULL, MenuItemGsensorSensitivity };
#endif
#elif (MENU_GENERAL_GSENSOR_EN==GSENSOR_4_LEVEL)
// G-sensor Sensitivity, 4 - level
SMENUITEM sItemGsensorSensitivity       = { ITEMID_GSENSOR_SENSITIVITY,       IDS_DS_GSENSOR_SENSITIVETY, &sSubGsensorSensitivity,    0 };
SMENUITEM sItemGsensorSensitivity_Off   = { ITEMID_GSENSOR_SENSITIVITY_OFF,   IDS_DS_OFF,       NULL, MenuItemGsensorSensitivity };
SMENUITEM sItemGsensorSensitivity_L0    = { ITEMID_GSENSOR_SENSITIVITY_L0,    IDS_DS_HIGH,      NULL, MenuItemGsensorSensitivity };
SMENUITEM sItemGsensorSensitivity_L1    = { ITEMID_GSENSOR_SENSITIVITY_L1,    IDS_DS_MIDDLE,    NULL, MenuItemGsensorSensitivity };
SMENUITEM sItemGsensorSensitivity_L2    = { ITEMID_GSENSOR_SENSITIVITY_L2,    IDS_DS_LOW,       NULL, MenuItemGsensorSensitivity,   0, true, NULL };
SMENUITEM sItemGsensorSensitivity_L4    = { ITEMID_GSENSOR_SENSITIVITY_L4,    IDS_DS_STANDARD,  NULL, MenuItemGsensorSensitivity };
#elif (MENU_GENERAL_GSENSOR_EN==GSENSOR_3_LEVEL)
// G-sensor Sensitivity, 3 - level
SMENUITEM sItemGsensorSensitivity       = { ITEMID_GSENSOR_SENSITIVITY, IDS_DS_GSENSOR_SENSITIVETY,   &sSubGsensorSensitivity,    0 };
SMENUITEM sItemGsensorSensitivity_Off   = { ITEMID_GSENSOR_SENSITIVITY_OFF,   IDS_DS_OFF,       NULL, MenuItemGsensorSensitivity };
SMENUITEM sItemGsensorSensitivity_L0    = { ITEMID_GSENSOR_SENSITIVITY_L0,    IDS_DS_LOW,      NULL, MenuItemGsensorSensitivity };
SMENUITEM sItemGsensorSensitivity_L2    = { ITEMID_GSENSOR_SENSITIVITY_L2,    IDS_DS_MIDDLE,    NULL, MenuItemGsensorSensitivity };
SMENUITEM sItemGsensorSensitivity_L4    = { ITEMID_GSENSOR_SENSITIVITY_L4,    IDS_DS_HIGH,       NULL, MenuItemGsensorSensitivity };
#endif

#if (MENU_GENERAL_WIFI_EN)
SMENUITEM sItemWiFiMode             = { ITEMID_WIFI_MODE,       IDS_DS_WIFI, &sSubWiFiMode, 0 };
#if (MENU_GENERAL_WIFI_OFF_EN)
SMENUITEM sItemWiFiMode_NO          = { ITEMID_WIFI_MODE_OFF,   IDS_DS_OFF,  NULL, MenuItemWiFiMode };
#endif
#if (MENU_GENERAL_WIFI_ON_EN)
SMENUITEM sItemWiFiMode_YES         = { ITEMID_WIFI_MODE_ON,    IDS_DS_ON,   NULL, MenuItemWiFiMode };
#endif
#if (MENU_GENERAL_WIFI_2_4G_EN)
SMENUITEM sItemWiFiMode_2_4G        = { ITEMID_WIFI_MODE_2_4G,  IDS_DS_2_4G,   NULL, MenuItemWiFiMode };
#endif
#if (MENU_GENERAL_WIFI_5G_EN)
SMENUITEM sItemWiFiMode_5G          = { ITEMID_WIFI_MODE_5G,    IDS_DS_5G,   NULL, MenuItemWiFiMode };
#endif
#endif

#if (MENU_GENERAL_DATE_LOGO_DISPLAY_EN)
SMENUITEM sItemDateLogoDisplay   = { ITEMID_DATE_LOGO_DISPLAY, IDS_DS_DATE_LOGO_DISPLAY, &sSubDateLogoDisplay, 0 };
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_ON_EN)
SMENUITEM sItemDateLogoDisplay_NO  = { ITEMID_DATE_LOGO_DISPLAY_ON, IDS_DS_ON,  NULL, MenuItemDateLogoDisplay };
#endif
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_OFF_EN)
SMENUITEM sItemDateLogoDisplay_OFF = { ITEMID_DATE_LOGO_DISPLAY_OFF, IDS_DS_OFF,  NULL, MenuItemDateLogoDisplay };
#endif
#endif

/*===========================================================================
* Global varible : Item List
*===========================================================================*/
#if (MENU_GENERAL_CLOCK_SETTING_EN)
PSMENUITEM   sMenuListClockSettings[] =
{
    &sItemClockSettings_Year,
    &sItemClockSettings_Month,
    &sItemClockSettings_Day,
    &sItemClockSettings_Hour,
    &sItemClockSettings_Minute,
    &sItemClockSettings_Sec,
    &sItemClockSettings_OK
};
#endif

#if (MENU_GENERAL_DATE_FORMAT_EN)
PSMENUITEM   sMenuListDateTimeFormat[] =
{
#if (MENU_GENERAL_DATE_FORMAT_NONE_EN)
    &sItemDateTime_None,
#endif
#if (MENU_GENERAL_DATE_FORMAT_YMD_EN)
    &sItemDateTime_YMD,
#endif
#if (MENU_GENERAL_DATE_FORMAT_MDY_EN)
    &sItemDateTime_MDY,
#endif
#if (MENU_GENERAL_DATE_FORMAT_DMY_EN)
    &sItemDateTime_DMY
#endif
};
#endif

#if (MENU_GENERAL_LANGUAGE_EN)
PSMENUITEM   sMenuListLanguage[] =
{
#if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
    &sItemLanguage_English,
#endif
#if (MENU_GENERAL_LANGUAGE_SPANISH_EN)
    &sItemLanguage_Spanish,
#endif
#if (MENU_GENERAL_LANGUAGE_PORTUGUESE_EN)
    &sItemLanguage_Portuguese,
#endif
#if (MENU_GENERAL_LANGUAGE_RUSSIAN_EN)
    &sItemLanguage_Russian,
#endif
#if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
    &sItemLanguage_SChinese,
#endif
#if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
    &sItemLanguage_TChinese,
#endif
#if (MENU_GENERAL_LANGUAGE_GERMAN_EN)
    &sItemLanguage_German,
#endif
#if (MENU_GENERAL_LANGUAGE_ITALIAN_EN)
    &sItemLanguage_Italian,
#endif
#if (MENU_GENERAL_LANGUAGE_LATVIAN_EN)
    &sItemLanguage_Latvian,
#endif
#if (MENU_GENERAL_LANGUAGE_POLISH_EN)
    &sItemLanguage_Polish,
#endif
#if (MENU_GENERAL_LANGUAGE_ROMANIAN_EN)
    &sItemLanguage_Romanian,
#endif
#if (MENU_GENERAL_LANGUAGE_SLOVAK_EN)
    &sItemLanguage_Slovak,
#endif
#if (MENU_GENERAL_LANGUAGE_UKRANINIAN_EN)
    &sItemLanguage_Ukraninian,
#endif
#if (MENU_GENERAL_LANGUAGE_FRENCH_EN)
    &sItemLanguage_French,
#endif
#if (MENU_GENERAL_LANGUAGE_JAPANESE_EN)
    &sItemLanguage_Japanese,
#endif
#if (MENU_GENERAL_LANGUAGE_KOREAN_EN)
    &sItemLanguage_Korean,
#endif
#if (MENU_GENERAL_LANGUAGE_CZECH_EN)
    &sItemLanguage_Czech,
#endif
#if (MENU_GENERAL_LANGUAGE_VIETNAMESE_EN)
    &sItemLanguage_Vietnamese,
#endif
};
#endif

#if (MENU_MEDIA_FORMAT_SD_EN)
PSMENUITEM   sMenuListFormatSDCard[] =
{
    &sItemFormatCard_Yes,
    &sItemFormatCard_No
};
#endif

#if (MENU_GENERAL_RESET_SETUP_EN)
PSMENUITEM   sMenuListResetSetup[] =
{
#if (MENU_GENERAL_RESET_SETUP_YES_EN)
    &sItemResetSetup_Yes,
#endif
#if (MENU_GENERAL_RESET_SETUP_NO_EN)
    &sItemResetSetup_No
#endif
};
#endif

#if (MENU_GENERAL_FW_VERSION_EN)
PSMENUITEM   sMenuListFWVersionInfo[] =
{
    &sItemFWVersion
};
#endif

#if (MENU_GENERAL_QR_CODE_EN)
PSMENUITEM   sMenuListQRCodeInfo[] =
{
    &sItemQRCode
};
#endif

#if (MENU_GENERAL_FLICKER_EN)
PSMENUITEM   sMenuListFlickerHz[] =
{
#if (MENU_GENERAL_FLICKER_50HZ_EN)
    &sItemFlicker50Hz,
#endif
#if (MENU_GENERAL_FLICKER_60HZ_EN)
    &sItemFlicker60Hz
#endif
};
#endif

#if (MENU_GENERAL_REVERSING_LINE_EN)
PSMENUITEM   sMenuListReversingLine[] =
{
#if (MENU_GENERAL_REVERSING_LINE_ON_EN)
    &sItemReversingLineOn,
#endif
#if (MENU_GENERAL_REVERSING_LINE_OFF_EN)
    &sItemReversingLineOff
#endif
};
#endif

#if (MENU_GENERAL_LCD_POWER_SAVE_EN)
PSMENUITEM   sMenuListLCDPowerSave[] =
{
#if (MENU_GENERAL_LCD_POWER_SAVE_OFF_EN)
    &sItemLCDPowerSave_Off,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_10SEC_EN)
    &sItemLCDPowerSave_10sec,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_30SEC_EN)
    &sItemLCDPowerSave_30sec,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_1MIN_EN)
    &sItemLCDPowerSave_1min,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_3MIN_EN)
    &sItemLCDPowerSave_3min,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_5MIN_EN)
    &sItemLCDPowerSave_5min
#endif
};
#endif

#if (MENU_GENERAL_SHUTDOWN_TONE_EN)
PSMENUITEM   sMenuListShutdownTone[] =
{
#if (MENU_GENERAL_SHUTDOWN_TONE_ON_EN)
    &sItemShutdownToneOn,
#endif
#if (MENU_GENERAL_SHUTDOWN_TONE_OFF_EN)
    &sItemShutdownToneOff,
#endif
    &sItemShutdownToneLow,
    &sItemShutdownToneMid,
    &sItemShutdownToneHigh,
};
#endif

#if (MENU_GENERAL_KEY_TONE_EN)
PSMENUITEM   sMenuListKeyTone[] =
{
#if (MENU_GENERAL_KEY_TONE_ON_EN)
    &sItemKeyToneOn,
#endif
#if (MENU_GENERAL_KEY_TONE_OFF_EN)
    &sItemKeyToneOff,
#endif
    &sItemKeyToneLow,
    &sItemKeyToneMid,
    &sItemKeyToneHigh,
};
#endif

#if (MENU_GENERAL_GSENSOR_EN==GSENSOR_5_LEVEL)
PSMENUITEM   sMenuListGsensorSensitivity[] =
{
#if (MENU_GENERAL_GSENSOR_OFF_EN)
    &sItemGsensorSensitivity_Off,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL0_EN)
    &sItemGsensorSensitivity_L0,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL1_EN)
    &sItemGsensorSensitivity_L1,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL2_EN)
    &sItemGsensorSensitivity_L2,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL3_EN)
    &sItemGsensorSensitivity_L3,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL4_EN)
    &sItemGsensorSensitivity_L4
#endif
};

#elif (MENU_GENERAL_GSENSOR_EN==GSENSOR_4_LEVEL)
PSMENUITEM   sMenuListGsensorSensitivity[] =
{
    &sItemGsensorSensitivity_Off,
    &sItemGsensorSensitivity_L0,    // High
    &sItemGsensorSensitivity_L2,    // Middle
    &sItemGsensorSensitivity_L3,    // Low
    &sItemGsensorSensitivity_L4     // Standard
};

#elif (MENU_GENERAL_GSENSOR_EN==GSENSOR_3_LEVEL)
PSMENUITEM   sMenuListGsensorSensitivity[] =
{
    &sItemGsensorSensitivity_Off,
    &sItemGsensorSensitivity_L0,
    &sItemGsensorSensitivity_L2,
    &sItemGsensorSensitivity_L4
};
#endif

#if (MENU_GENERAL_WIFI_EN)
PSMENUITEM   sMenuListWiFiMode[] =
{
#if (MENU_GENERAL_WIFI_OFF_EN)
    &sItemWiFiMode_NO,
#endif
#if (MENU_GENERAL_WIFI_ON_EN)
    &sItemWiFiMode_YES,
#endif
#if (MENU_GENERAL_WIFI_2_4G_EN)
    &sItemWiFiMode_2_4G,
#endif
#if (MENU_GENERAL_WIFI_5G_EN)
    &sItemWiFiMode_5G
#endif
};
#endif

#if (MENU_GENERAL_DATE_LOGO_DISPLAY_EN)
PSMENUITEM   sMenuListDateLogoDisplay[] =
{
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_ON_EN)
    &sItemDateLogoDisplay_NO,
#endif
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_OFF_EN)
    &sItemDateLogoDisplay_OFF
#endif
};
#endif

PSMENUITEM   sMenuListMainGeneral[]=
{
#if (MENU_GENERAL_LCD_POWER_SAVE_EN)
    &sItemLCDPowerSave,
#endif
#if (MENU_GENERAL_SHUTDOWN_TONE_EN)
    &sItemShutdownTone,
#endif
#if (MENU_GENERAL_KEY_TONE_EN)
    &sItemKeyTone,
#endif
#if (MENU_GENERAL_CLOCK_SETTING_EN)
    &sItemClockSettings,
#endif
#if (MENU_GENERAL_DATE_FORMAT_EN)
    &sItemDateTimeFormat,
#endif
#if (MENU_GENERAL_WIFI_EN)
    &sItemWiFiMode,
#endif
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_EN)
    &sItemDateLogoDisplay,
#endif
#if (MENU_GENERAL_LANGUAGE_EN)
    &sItemLanguage,
#endif
#if (MENU_MEDIA_FORMAT_SD_EN)
    &sItemFormatCard,
#endif
#if (MENU_GENERAL_RESET_SETUP_EN)
    &sItemResetSetup,
#endif
};

PSMENUITEM   sMenuListMainSystem[]=
{
#if (MENU_GENERAL_LANGUAGE_EN)
    &sItemLanguage,
#endif
#if (MENU_GENERAL_RESET_SETUP_EN)
    &sItemResetSetup,
#endif
#if (MENU_GENERAL_FW_VERSION_EN)
    &sItemFWVersionInfo,
#endif
#if (MENU_GENERAL_QR_CODE_EN)
    &sItemQRCodeInfo,
#endif
};

PSMENUITEM   sMenuListMainGuide[]=
{
#if (MENU_GENERAL_LANGUAGE_EN)
    &sItemLanguage,
#endif
#if (MENU_GENERAL_CLOCK_SETTING_EN)
    &sItemClockSettings,
#endif
};

/*===========================================================================
* Global variable : Menu Structure
*===========================================================================*/
SMENUSTRUCT   sMainMenuGeneral =
{
    MENUID_MAIN_MENU_GENERAL,
    IDS_DS_GENERAL_SETTINGS,
    NULL,
    sizeof(sMenuListMainGeneral)/sizeof(PSMENUITEM),
    sMenuListMainGeneral,
    NULL,
};

SMENUSTRUCT   sMainMenuSystem =
{
    MENUID_MAIN_MENU_SYSTEM,
    IDS_DS_SYSTEM_SETTINGS,
    NULL,
    sizeof(sMenuListMainSystem)/sizeof(PSMENUITEM),
    sMenuListMainSystem,
    NULL,
};

SMENUSTRUCT sMainMenuGuide =
{
    MENUID_MAIN_MENU_GUIDE,
    IDS_DS_SYSTEM_SETTINGS,
    NULL,
    sizeof(sMenuListMainGuide)/sizeof(PSMENUITEM),
    sMenuListMainGuide,
    NULL,
};

//--------------SUB MENU-------------------
#if (MENU_GENERAL_CLOCK_SETTING_EN)
SMENUSTRUCT sSubClockSettings =
{
    MENUID_SUB_MENU_CLOCK_SETTINGS,
    IDS_DS_CLOCK_SETTINGS,
    &sMainMenuGeneral,
    sizeof(sMenuListClockSettings)/sizeof(PSMENUITEM),
    sMenuListClockSettings,
    NULL,
};
#endif

#if (MENU_GENERAL_DATE_FORMAT_EN)
SMENUSTRUCT sSubDateTimeFormat =
{
    MENUID_SUB_MENU_DATETIME_FORMAT,
    IDS_DS_DATETIME_FORMAT,
    &sMainMenuGeneral,
    sizeof(sMenuListDateTimeFormat)/sizeof(PSMENUITEM),
    sMenuListDateTimeFormat,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_LANGUAGE_EN)
SMENUSTRUCT sSubLanguage =
{
    MENUID_SUB_MENU_LANGUAGE,
    IDS_DS_LANGUAGE,
    &sMainMenuGeneral,
    sizeof(sMenuListLanguage)/sizeof(PSMENUITEM),
    sMenuListLanguage,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_MEDIA_FORMAT_SD_EN)
SMENUSTRUCT sSubFormatSDCard =
{
    MENUID_SUB_MENU_FORMAT_SD_CARD,
    IDS_DS_FORMAT_SD_CARD,
    &sMainMenuGeneral,
    sizeof(sMenuListFormatSDCard)/sizeof(PSMENUITEM),
    sMenuListFormatSDCard,
    NULL,
};
#endif

#if (MENU_GENERAL_RESET_SETUP_EN)
SMENUSTRUCT sSubResetSetup =
{
    MENUID_SUB_MENU_RESET_SETUP,
    IDS_DS_RESET_SETUP,
    &sMainMenuGeneral,
    sizeof(sMenuListResetSetup)/sizeof(PSMENUITEM),
    sMenuListResetSetup,
    NULL,
};
#endif

#if (MENU_GENERAL_FW_VERSION_EN)
SMENUSTRUCT sSubFwVersionInfo =
{
    MENUID_SUB_MENU_FW_VERSION_INFO,
    IDS_DS_FW_VERSION_INFO,
    &sMainMenuGeneral,
    sizeof(sMenuListFWVersionInfo)/sizeof(PSMENUITEM),
    sMenuListFWVersionInfo,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_QR_CODE_EN)
SMENUSTRUCT sSubQRCodeInfo =
{
    MENUID_SUB_MENU_QR_CODE_INFO,
    IDS_DS_QR_CODE_INFO,
    &sMainMenuGeneral,
    sizeof(sMenuListQRCodeInfo)/sizeof(PSMENUITEM),
    sMenuListQRCodeInfo,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_FLICKER_EN)
SMENUSTRUCT sSubFlickerHz =
{
    MENUID_SUB_MENU_FLICKER_FREQUENCY,
    IDS_DS_FLICKER,
    &sMainMenuGeneral,
    sizeof(sMenuListFlickerHz)/sizeof(PSMENUITEM),
    sMenuListFlickerHz,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_REVERSING_LINE_EN)
SMENUSTRUCT sSubReversingLine =
{
    MENUID_SUB_MENU_REVERSING_LINE,
    IDS_DS_REVERSING_LINE,
    &sMainMenuGeneral,
    sizeof(sMenuListReversingLine)/sizeof(PSMENUITEM),
    sMenuListReversingLine,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_LCD_POWER_SAVE_EN)
SMENUSTRUCT sSubLCDPowerSave =
{
    MENUID_SUB_MENU_LCD_POWER_SAVE,
    IDS_DS_LCD_POWER_SAVE,
    &sMainMenuGeneral,
    sizeof(sMenuListLCDPowerSave)/sizeof(PSMENUITEM),
    sMenuListLCDPowerSave,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_SHUTDOWN_TONE_EN)
SMENUSTRUCT sSubShutdownTone =
{
    MENUID_SUB_SHUTDOWN_TONE,
    IDS_DS_SHUTDOWN_TONE,
    &sMainMenuGeneral,
    sizeof(sMenuListShutdownTone)/sizeof(PSMENUITEM),
    sMenuListShutdownTone,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_KEY_TONE_EN)
SMENUSTRUCT sSubKeyTone =
{
    MENUID_SUB_KEY_TONE,
    IDS_DS_KEY_TONE,
    &sMainMenuGeneral,
    sizeof(sMenuListKeyTone)/sizeof(PSMENUITEM),
    sMenuListKeyTone,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_GSENSOR_EN)
SMENUSTRUCT sSubGsensorSensitivity =
{
    MENUID_SUB_MENU_GSENSOR_SENSITIVITY,
    IDS_DS_GSENSOR_SENSITIVETY,
    &sMainMenuGeneral,
    sizeof(sMenuListGsensorSensitivity)/sizeof(PSMENUITEM),
    sMenuListGsensorSensitivity,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_WIFI_EN)
SMENUSTRUCT sSubWiFiMode =
{
    MENUID_SUB_MENU_WIFI_MODE,
    IDS_DS_WIFI,
    &sMainMenuGeneral,
    sizeof(sMenuListWiFiMode)/sizeof(PSMENUITEM),
    sMenuListWiFiMode,
    MenuGetDefault_GeneralSetting,
};
#endif

#if (MENU_GENERAL_DATE_LOGO_DISPLAY_EN)
SMENUSTRUCT sSubDateLogoDisplay =
{
    MENUID_SUB_MENU_DATE_LOGO_DISPLAY,
    IDS_DS_DATE_LOGO_DISPLAY,
    &sMainMenuGeneral,
    sizeof(sMenuListDateLogoDisplay)/sizeof(PSMENUITEM),
    sMenuListDateLogoDisplay,
    MenuGetDefault_GeneralSetting,
};
#endif

/*===========================================================================
* Main body
*===========================================================================*/
#if (MENU_GENERAL_CLOCK_SETTING_EN)
bool MenuItemClockSettings(PSMENUITEM pItem)
{
    return true;
}
#endif

#if (MENU_GENERAL_DATE_FORMAT_EN)
bool MenuItemDateTimeFormat(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiDateTimeFormat = pItem->iItemId - ITEMID_DATETIME_FORMAT-1;
    carimpl_GeneralFunc_SetDatetimeFormat();
    return true;
}
#endif

#if (MENU_GENERAL_LANGUAGE_EN)
bool MenuItemLanguage(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiLanguage = pItem->iItemId - ITEMID_LANGUAGE-1;
    carimpl_GeneralFunc_SetLanguage();
    return true;
}
#endif

#if (MENU_MEDIA_FORMAT_SD_EN)
bool MenuItemFormatSDCard(PSMENUITEM pItem)
{
    if (pItem->iItemId == ITEMID_FORMAT_CARD_YES) {
        carimpl_MediaToolFunc_FormatSDCard();
        return true;
    } else {
        return false;
    }
}
#endif

#if (MENU_GENERAL_RESET_SETUP_EN)
bool MenuItemResetSetup(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiResetSetting = (pItem->iItemId==ITEMID_RESET_YES)? 1:0;
    if (pItem->iItemId != ITEMID_RESET_YES)
        return false;
    carimpl_GeneralFunc_SetResetSetting();
    return true;
}
#endif

#if (MENU_GENERAL_FW_VERSION_EN)
bool MenuItemFwVersion(PSMENUITEM pItem)
{
    carimpl_GeneralFunc_GetSoftwareVersion();
    return true;
}
#endif

#if (MENU_GENERAL_QR_CODE_EN)
bool MenuItemQRCode(PSMENUITEM pItem)
{
    return true;
}
#endif

#if (MENU_GENERAL_FLICKER_EN)
bool MenuItemFlickerHz(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiFlickerHz = pItem->iItemId - ITEMID_FLICKER -1;
    carimpl_IQFunc_SetFlicker();
    return true;
}
#endif

#if (MENU_GENERAL_REVERSING_LINE_EN)
bool MenuItemReversingLine(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiReversingLine = pItem->iItemId - ITEMID_REVERSING_LINE -1;
    carimpl_GeneralFunc_SetReversingLine();
    return true;
}
#endif

#if (MENU_GENERAL_LCD_POWER_SAVE_EN)
bool MenuItemLCDPowerSave(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiLCDPowerSave = pItem->iItemId - ITEMID_LCD_POWER_SAVE-1;
    carimpl_GeneralFunc_SetLcdPowerSave();
    return true;
}
#endif

#if (MENU_GENERAL_SHUTDOWN_TONE_EN)
bool MenuItemShutdownTone(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiShutdownTone = pItem->iItemId - ITEMID_SHUTDOWN_TONE-1;
    carimpl_GeneralFunc_SetShutdownTone();
    return true;
}
#endif

#if (MENU_GENERAL_KEY_TONE_EN)
bool MenuItemKeyTone(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiBeep = pItem->iItemId - ITEMID_KEY_TONE-1;
    carimpl_GeneralFunc_SetBeep();
    return true;
}
#endif

#if (MENU_GENERAL_GSENSOR_EN)
bool MenuItemGsensorSensitivity(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiGsensorSensitivity = pItem->iItemId - ITEMID_GSENSOR_SENSITIVITY-1;
    carimpl_GeneralFunc_setGSensorSensitivity();
    return true;
}
#endif

#if (MENU_GENERAL_WIFI_EN)
bool MenuItemWiFiMode(PSMENUITEM pItem)
{
    int iOnOff = pItem->iItemId - ITEMID_WIFI_MODE-1;
    if(iOnOff != MenuSettingConfig()->uiWifi){
        MenuSettingConfig()->uiWifi = iOnOff;
        carimpl_GeneralFunc_SetWifiOnOff();
    }
    return true;
}
#endif

#if (MENU_GENERAL_DATE_LOGO_DISPLAY_EN)
bool MenuItemDateLogoDisplay(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiDateLogoDisplay = pItem->iItemId - ITEMID_DATE_LOGO_DISPLAY-1;
    carimpl_GeneralFunc_SetDateLogoDisplay();
    return true;
}
#endif

int MenuGetDefault_GeneralSetting(PSMENUSTRUCT pMenu )
{
    int DefaultValue = 0;

    switch (pMenu->iMenuId)
    {
#if (MENU_GENERAL_DATE_FORMAT_EN)
    case MENUID_SUB_MENU_DATETIME_FORMAT:
        DefaultValue = MenuSettingConfig()->uiDateTimeFormat;
    break;
#endif
#if (MENU_GENERAL_LANGUAGE_EN)
    case MENUID_SUB_MENU_LANGUAGE:
        DefaultValue = MenuSettingConfig()->uiLanguage;
    break;
#endif
#if (MENU_GENERAL_FLICKER_EN)
    case MENUID_SUB_MENU_FLICKER_FREQUENCY:
        DefaultValue = MenuSettingConfig()->uiFlickerHz;
    break;
#endif
#if (MENU_GENERAL_REVERSING_LINE_EN)
    case MENUID_SUB_MENU_REVERSING_LINE:
        DefaultValue = MenuSettingConfig()->uiReversingLine;
    break;
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_EN)
    case MENUID_SUB_MENU_LCD_POWER_SAVE:
        DefaultValue = MenuSettingConfig()->uiLCDPowerSave;
    break;
#endif
#if (MENU_GENERAL_SHUTDOWN_TONE_EN)
    case MENUID_SUB_SHUTDOWN_TONE:
        DefaultValue = MenuSettingConfig()->uiShutdownTone;
    break;
#endif
#if (MENU_GENERAL_KEY_TONE_EN)
    case MENUID_SUB_KEY_TONE:
        DefaultValue = MenuSettingConfig()->uiBeep;
    break;
#endif
#if (MENU_GENERAL_GSENSOR_EN)
    case MENUID_SUB_MENU_GSENSOR_SENSITIVITY:
        DefaultValue = MenuSettingConfig()->uiGsensorSensitivity;
    break;
#endif
#if (MENU_GENERAL_WIFI_EN)
    case MENUID_SUB_MENU_WIFI_MODE:
        DefaultValue = MenuSettingConfig()->uiWifi;
    break;
#endif
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_EN)
    case MENUID_SUB_MENU_DATE_LOGO_DISPLAY:
        DefaultValue = MenuSettingConfig()->uiDateLogoDisplay;
    break;
#endif
#if MENU_GENERAL_FW_VERSION_EN
    case MENUID_SUB_MENU_FW_VERSION_INFO:
        DefaultValue = MenuSettingConfig()->uiVersion;
    break;
#endif
    }
    return DefaultValue;
}
