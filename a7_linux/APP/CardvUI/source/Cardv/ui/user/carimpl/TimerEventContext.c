/*
* TimerEventContext.c
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <time.h>
#include <signal.h>
#include <pthread.h>

#include "TimerEventContext.h"

#define TIMER_MS	10
#define TIMER_NAME	"cardv_timer"

bool callback_run_flag = false;

static void *m_cardv_timer = NULL;
static uint32_t timerNum = 0;
static S_ACTIVITY_TIMEER *s_timer = NULL;
static on_timer_event_callback s_callback = NULL;

extern pthread_mutex_t mutex;

static void timer_event_handler(int signo)
{
    for (int i = 0; i < timerNum; ++i) {
        if(s_timer != NULL){
            if ((s_timer[i].time != 0) && (++s_timer[i].time_count >= s_timer[i].time)) {
                callback_run_flag = true;
                if(s_callback){
                    pthread_mutex_lock(&mutex); //互斥锁上锁
                    s_callback(s_timer[i].id);
                    pthread_mutex_unlock(&mutex);//互斥锁解锁
                }
                s_timer[i].time_count = 0;
            }
        }
        callback_run_flag = false;
    }
}

static void *cardv_timer_create(const char *name, void (*func)(void *), void *user_data)
{
    timer_t *timer = NULL;

    struct sigevent ent = { 0 };

    /* check parameter */
    if (func == NULL)
    {
        printf("%d timer invaild!\n", __LINE__);
        return NULL;
    }

    timer = (timer_t *)malloc(sizeof(time_t));

    /* Init */
    memset(&ent, 0x00, sizeof(struct sigevent));

    /* create a timer */
    ent.sigev_notify          = SIGEV_THREAD;
    ent.sigev_notify_function = (void (*)(union sigval))func;
    ent.sigev_value.sival_ptr = user_data;

    printf("create %s\n", name);

    if (timer_create(CLOCK_MONOTONIC, &ent, timer) != 0)
    {
        printf("create faild!\n");
        free(timer);
        return NULL;
    }

    printf("create %p timer\n", timer);
    return (void *)timer;
}

static int cardv_timer_start(void *timer, int ms)
{
    struct itimerspec ts;

    /* check parameter */
    if (timer == NULL)
    {
        printf("timer invaild!\n");
        return -1;
    }

    printf("strart %p %d ms\n", timer, ms);

    /* it_interval=0: timer run only once */
    ts.it_interval.tv_sec  = ms / 1000;
    ts.it_interval.tv_nsec = (ms % 1000) * 1000000;

    /* it_value=0: stop timer */
    ts.it_value.tv_sec  = ms / 1000;
    ts.it_value.tv_nsec = (ms % 1000) * 1000000;

    return timer_settime(*(timer_t *)timer, 0, &ts, NULL);
}

static int cardv_timer_stop(void *timer)
{
    struct itimerspec ts;

    /* check parameter */
    if (timer == NULL)
    {
        printf("timer invaild!\n");
        return -1;
    }

    printf("stop %p\n", timer);
    /* it_interval=0: timer run only once */
    ts.it_interval.tv_sec  = 0;
    ts.it_interval.tv_nsec = 0;

    /* it_value=0: stop timer */
    ts.it_value.tv_sec  = 0;
    ts.it_value.tv_nsec = 0;

    return timer_settime(*(timer_t *)timer, 0, &ts, NULL);
}

static int cardv_timer_delete(void *timer)
{
    int ret = 0;

    /* check parameter */
    if (timer == NULL)
    {
        printf("timer invaild!\n");
        return -1;
    }

    printf("delete %p\n", timer);
    ret = timer_delete(*(timer_t *)timer);

    free(timer);

    return ret;
}

void cardv_timer_init(void)
{
    printf("run here\n");
    if (!m_cardv_timer)
    {
        printf("run here\n");
        m_cardv_timer = cardv_timer_create(TIMER_NAME, (void (*)(void *))timer_event_handler, NULL);
        cardv_timer_start(m_cardv_timer, TIMER_MS);
    }
}

void cardv_timer_uninit(void)
{
    if (m_cardv_timer)
    {
        cardv_timer_stop(m_cardv_timer);
        cardv_timer_delete(m_cardv_timer);
    }
}

void register_timer_callback(on_timer_event_callback cb, S_ACTIVITY_TIMEER *timer, uint32_t num) {
    timerNum = num;
    s_callback = cb;
    s_timer = timer;
}
