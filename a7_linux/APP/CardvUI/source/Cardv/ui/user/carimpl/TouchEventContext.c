/*
* TimerEventContext.c
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "TouchEventContext.h"

extern pthread_mutex_t mutex;
touch_event_fun touch_event_callback = NULL;

void reset(MOTION_EVENT *p) {
	p->mActionStatus = E_ACTION_NONE;
	p->mX = 0;
	p->mY = 0;
}

void register_touch_callback(touch_event_fun cb)
{
	touch_event_callback = cb;
}

bool run_touch_callback(MOTION_EVENT ev)
{
	bool ret = false;

	if(touch_event_callback){
		ret = touch_event_callback(ev);
	}
	return ret;
}
