/*
 * commom.h
 *
 *  Created on: Mar 1, 2024
 *      Author: XiongYingDan
 */
#ifndef _JM_COMMON_H_
#define _JM_COMMON_H_

//==============================================================================
//
//                              MACRO DEFINES
//
//==============================================================================
#define CARIMPL_DBGLV_NONE    0 // disable all the debug message
#define CARIMPL_DBGLV_INFO    1 // information
#define CARIMPL_DBGLV_NOTICE  2 // normal but significant condition
#define CARIMPL_DBGLV_DEBUG   3 // debug-level messages
#define CARIMPL_DBGLV_WARNING 4 // warning conditions
#define CARIMPL_DBGLV_ERR     5 // error conditions
#define CARIMPL_DBGLV_CRIT    6 // critical conditions
#define CARIMPL_DBGLV_ALERT   7 // action must be taken immediately
#define CARIMPL_DBGLV_EMERG   8 // system is unusable

#ifndef LOG_COLOR_NONE
#define LOG_COLOR_NONE   "\033[0m"
#endif
#ifndef LOG_COLOR_GREEN
#define LOG_COLOR_GREEN  "\033[0;32m"
#endif
#ifndef LOG_COLOR_RED
#define LOG_COLOR_RED    "\033[0;31m"
#endif
#ifndef LOG_COLOR_YELLOW
#define LOG_COLOR_YELLOW "\033[1;33m"
#endif

int g_ui_log_dbglevel = CARIMPL_DBGLV_WARNING;

#define LOG_UI_DBG(fmt, args...)                                                      \
    if (g_ui_log_dbglevel <= CARIMPL_DBGLV_DEBUG)                                             \
        do                                                                           \
        {                                                                            \
            printf(LOG_COLOR_GREEN "[##jimu##DBG]:%s[%d]: " LOG_COLOR_NONE, __FUNCTION__, __LINE__); \
            printf(fmt, ##args);                                                     \
    } while (0)

#define LOG_UI_WARN(fmt, args...)                                                       \
    if (g_ui_log_dbglevel <= CARIMPL_DBGLV_WARNING)                                             \
        do                                                                             \
        {                                                                              \
            printf(LOG_COLOR_YELLOW "[##jimu##WARN]:%s[%d]: " LOG_COLOR_NONE, __FUNCTION__, __LINE__); \
            printf(fmt, ##args);                                                       \
    } while (0)

#define LOG_UI_INFO(fmt, args...)                                 \
    if (g_ui_log_dbglevel <= CARIMPL_DBGLV_INFO)                          \
        do                                                       \
        {                                                        \
            printf("[##jimu##INFO]:%s[%d]: \n", __FUNCTION__, __LINE__); \
            printf(fmt, ##args);                                 \
    } while (0)

#define LOG_UI_ERR(fmt, args...)                                                    \
    if (g_ui_log_dbglevel <= CARIMPL_DBGLV_ERR)                                             \
        do                                                                         \
        {                                                                          \
            printf(LOG_COLOR_RED "[##jimu##ERR]:%s[%d]: " LOG_COLOR_NONE, __FUNCTION__, __LINE__); \
            printf(fmt, ##args);                                                   \
    } while (0)

#endif //#define _COMMON_H_
