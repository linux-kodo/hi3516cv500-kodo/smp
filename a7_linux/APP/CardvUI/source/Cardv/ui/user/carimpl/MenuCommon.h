#ifndef _MENU_COMMON_H_
#define _MENU_COMMON_H_

/*===========================================================================
* Include file 
*===========================================================================*/ 
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>

#include "MenuConfigSDK.h"
#include "OSDStringCommon.h"

/*===========================================================================
* Macro define 
*===========================================================================*/ 
#define MAGIC_NUM           (0x123456) //menu setting header
#define HDRTAIL_NUM         (0x0A524448)
#define NUMBER_SETTING      (100)
#define NUMBER_ITEM_VALS    (35)

struct _setting_atom;
typedef int (*MFUN)(struct _setting_atom*, int, char*, void*);
typedef int (*GET_FUN)(int icur_val, va_list *ap);

typedef struct sMENU*  PSMENUSTRUCT;
typedef struct sITEM*  PSMENUITEM;

typedef  bool	(*pfItemHandler)(PSMENUITEM pItem );
typedef  int 	(*pfMenuEventHandler)(PSMENUSTRUCT pMenu, int ulEvent, int ulParam );
typedef  int 	(*pfMenuGetDefault)(PSMENUSTRUCT pMenu );

/*===========================================================================
* Structure define 
*===========================================================================*/
typedef struct _settingtovals {
    const char*   szItemKey;
    int       nItemValue;
} SETTING_VALS;

typedef struct _setting_atom {
    const char*       szSKey;
    int         nSVal;
    int         bSAct;
    MFUN     fnSet;
    GET_FUN     fnGet;
    SETTING_VALS  szItemMaps[NUMBER_ITEM_VALS];
} SETTING_ATOM;

typedef struct {
    int             HDR_MAGIC;
    int             HDR_TAIL;
    SETTING_ATOM    sAtom[NUMBER_SETTING];
} MENU_ATOMS;

typedef struct sITEM {
    int iItemId;
    int iStringId;
    struct sMENU* pSubMenu;
    pfItemHandler pfItemSelectHandler;
} SMENUITEM, *PSMENUITEM;

typedef struct sMENU {
    int iMenuId;
    int iStringId;
    struct sMENU* pParentMenu;
    int iNumOfItems;
    PSMENUITEM* pItemsList;
    pfMenuGetDefault pfMenuGetDefaultVal;
    int iCurrentPos;
} SMENUSTRUCT, *PSMENUSTRUCT;

typedef struct _MenuInfo
{
    // Movie
    int uiMOVSize;              //001
    int uiMOVClipTime;
    int uiMOVSoundRecord;
    // General
    int uiDateTimeFormat;       //004
    int uiLanguage;
    int uiResetSetting;
    int uiFlickerHz;
    int uiLCDPowerSave;
    int uiBeep;
    int uiShutdownTone;
    int uiGsensorSensitivity;
    int uiDateLogoDisplay;
    int uiWifi;
    int uiSystemVolume;
    int uiLCDBacklight;
    //ReversingLine             //016
    int uiReversingLine;
    int uiLeftTopX;
    int uiLeftTopY;
    int uiLeftBottomX;
    int uiLeftBottomY;
    int uiLeftMid1Ratio;
    int uiLeftMid2Ratio;
    int uiRightTopX;
    int uiRightTopY;
    int uiRightBottomX;
    int uiRightBottomY;
    int uiRightMid1Ratio;
    int uiRightMid2Ratio;
    //version
    int uiVersion;      //56
    // Playback
    int uiPlaybackVideoType;
} MenuInfo, *pMenuInfo;

/*===========================================================================
* Enum define 
*===========================================================================*/ 
typedef enum{
    MENUID_TOP_MENULIST = 0      ,
    MENUID_MAIN_MENULIST         ,
    MENUID_SUB_MENULIST          ,
    MENUID_MAIN_PAGE_MAX
}MENUID_MAIN_PAGE;

//Menu Item
typedef enum{
    MENUID_MAIN_MENU_VIDEO          ,
    MENUID_MAIN_MENU_PLAYBACK       ,
    MENUID_MAIN_MENU_GENERAL        ,
    MENUID_MAIN_MENU_SYSTEM         ,
    MENUID_MAIN_MENU_GUIDE          ,
    MENUID_MAIN_MAX
}MENUID_MAIN;

typedef enum{
    //Movie
    MENUID_SUB_MENU_MOVIE_MODE = 0          , // 0
    MENUID_SUB_MENU_MOVIE_SOUND_RECORD      ,
    MENUID_SUB_MENU_MOVIE_CLIPTIME          ,
    //Setting
    MENUID_SUB_MENU_CLOCK_SETTINGS          ,
    MENUID_SUB_MENU_DATETIME_FORMAT         ,
    MENUID_SUB_MENU_LANGUAGE                ,
    MENUID_SUB_MENU_RESET_SETUP             ,
    MENUID_SUB_MENU_FORMAT_SD_CARD          ,
    MENUID_SUB_MENU_FW_VERSION_INFO         ,
    MENUID_SUB_MENU_QR_CODE_INFO            ,
    MENUID_SUB_MENU_FLICKER_FREQUENCY       ,
    MENUID_SUB_MENU_REVERSING_LINE          ,
    MENUID_SUB_MENU_LCD_POWER_SAVE          ,
    MENUID_SUB_SHUTDOWN_TONE                ,
    MENUID_SUB_KEY_TONE                     ,
    MENUID_SUB_MENU_GSENSOR_SENSITIVITY     ,
    MENUID_SUB_MENU_WIFI_MODE               ,
    MENUID_SUB_MENU_DATE_LOGO_DISPLAY       ,
    MENUID_SUB_MENU_MAX
}MENUID_SUB;

typedef enum {
#if MENU_MOVIE_SIZE_EN
    ITEMID_MOVIE_MODE= 0,
#if (MENU_MOVIE_SIZE_4K_25P_EN)
    ITEMID_MOVIE_MODE_4K_25P,
#endif
#if (MENU_MOVIE_SIZE_1440_30P_EN)
    ITEMID_MOVIE_MODE_1440_30P,
#endif
#if (MENU_MOVIE_SIZE_SHD_30P_EN)
    ITEMID_MOVIE_MODE_SHD,
#endif
#if (MENU_MOVIE_SIZE_SHD_25P_EN)
    ITEMID_MOVIE_MODE_SHD_25P,
#endif
#if (MENU_MOVIE_SIZE_1080_60P_EN)
    ITEMID_MOVIE_MODE_FHD_60P,
#endif
#if (MENU_MOVIE_SIZE_1080P_30_HDR_EN)
    ITEMID_MOVIE_MODE_FHD_HDR,
#endif
#if (MENU_MOVIE_SIZE_1080P_EN)
    ITEMID_MOVIE_MODE_FHD,
#endif
#if (MENU_MOVIE_SIZE_720P_EN)
    ITEMID_MOVIE_MODE_HD,
#endif
#if (MENU_MOVIE_SIZE_720_60P_EN)
    ITEMID_MOVIE_MODE_HD_60P,
#endif
#if (MENU_MOVIE_SIZE_VGA30P_EN)
    ITEMID_MOVIE_MODE_VGA_30P,
#endif
#endif

#if MENU_MOVIE_SOUND_RECORD_EN
    ITEMID_MOVIE_SOUND_RECORD,
    ITEMID_MOVIE_SOUND_RECORD_ON,
    ITEMID_MOVIE_SOUND_RECORD_OFF,
#endif

#if MENU_MOVIE_CLIP_TIME_EN
    ITEMID_MOVIE_CLIPTIME,
#if (MENU_MOVIE_CLIP_TIME_OFF_EN)
    ITEMID_MOVIE_CLIPTIME_OFF,
#endif
#if (MENU_MOVIE_CLIP_TIME_1MIN_EN)
    ITEMID_MOVIE_CLIPTIME_1MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_2MIN_EN)
    ITEMID_MOVIE_CLIPTIME_2MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_3MIN_EN)
    ITEMID_MOVIE_CLIPTIME_3MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_5MIN_EN)
    ITEMID_MOVIE_CLIPTIME_5MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_10MIN_EN)
    ITEMID_MOVIE_CLIPTIME_10MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_30MIN_EN)
    ITEMID_MOVIE_CLIPTIME_30MIN,
#endif
#endif
    ITEMID_MOVIE_ITEM_END
}MOVIEITEMID;

typedef enum {
#if (MENU_GENERAL_CLOCK_SETTING_EN)
    ITEMID_CLOCK_SETTINGS  = ITEMID_MOVIE_ITEM_END,
    ITEMID_DATETIME_YEAR,
    ITEMID_DATETIME_MONTH,
    ITEMID_DATETIME_DAY,
    ITEMID_DATETIME_HOUR,
    ITEMID_DATETIME_MINUTE,
    ITEMID_DATETIME_SEC,
    ITEMID_DATETIME_OK,
    ITEMID_DATETIME_CANCEL,
#endif

#if MENU_GENERAL_DATE_FORMAT_EN
    ITEMID_DATETIME_FORMAT,
#if (MENU_GENERAL_DATE_FORMAT_NONE_EN)
    ITEMID_DATETIME_NONE,
#endif
#if (MENU_GENERAL_DATE_FORMAT_YMD_EN)
    ITEMID_DATETIME_YMD,
#endif
#if (MENU_GENERAL_DATE_FORMAT_MDY_EN)
    ITEMID_DATETIME_MDY,
#endif
#if (MENU_GENERAL_DATE_FORMAT_DMY_EN)
    ITEMID_DATETIME_DMY,
#endif
#endif

#if MENU_GENERAL_LANGUAGE_EN
    ITEMID_LANGUAGE,
#if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
    ITEMID_LANGUAGES_ENGLISH,
#endif
#if (MENU_GENERAL_LANGUAGE_SPANISH_EN)
    ITEMID_LANGUAGES_SPANISH,
#endif
#if (MENU_GENERAL_LANGUAGE_PORTUGUESE_EN)
    ITEMID_LANGUAGES_PORTUGUESE,
#endif
#if (MENU_GENERAL_LANGUAGE_RUSSIAN_EN)
    ITEMID_LANGUAGES_RUSSIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
    ITEMID_LANGUAGES_SCHINESE,
#endif
#if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
    ITEMID_LANGUAGES_TCHINESE,
#endif
#if (MENU_GENERAL_LANGUAGE_GERMAN_EN)
    ITEMID_LANGUAGES_GERMAN,
#endif
#if (MENU_GENERAL_LANGUAGE_ITALIAN_EN)
    ITEMID_LANGUAGES_ITALIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_LATVIAN_EN)
    ITEMID_LANGUAGES_LATVIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_POLISH_EN)
    ITEMID_LANGUAGES_POLISH,
#endif
#if (MENU_GENERAL_LANGUAGE_ROMANIAN_EN)
    ITEMID_LANGUAGES_ROMANIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_SLOVAK_EN)
    ITEMID_LANGUAGES_SLOVAK,
#endif
#if (MENU_GENERAL_LANGUAGE_UKRANINIAN_EN)
    ITEMID_LANGUAGES_UKRANINIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_FRENCH_EN)
    ITEMID_LANGUAGES_FRENCH,
#endif
#if (MENU_GENERAL_LANGUAGE_JAPANESE_EN)
    ITEMID_LANGUAGES_JAPANESE,
#endif
#if (MENU_GENERAL_LANGUAGE_KOREAN_EN)
    ITEMID_LANGUAGES_KOREAN,
#endif
#if (MENU_GENERAL_LANGUAGE_CZECH_EN)
    ITEMID_LANGUAGES_CZECH,
#endif
#if (MENU_GENERAL_LANGUAGE_VIETNAMESE_EN)
    ITEMID_LANGUAGES_VIETNAMESE,
#endif
#endif

#if MENU_GENERAL_RESET_SETUP_EN
    ITEMID_RESET_SETUP,
    ITEMID_RESET_YES,
    ITEMID_RESET_NO,
#endif

#if (MENU_GENERAL_FW_VERSION_EN)
    ITEMID_FW_VERSION_INFO,
    ITEMID_FW_VERSION,
#endif

#if (MENU_GENERAL_QR_CODE_EN)
    ITEMID_QR_CODE_INFO,
    ITEMID_QR_CODE,
#endif

#if MENU_GENERAL_FLICKER_EN
    ITEMID_FLICKER,
    ITEMID_FLICKER_50HZ,
    ITEMID_FLICKER_60HZ,
#endif

#if (MENU_GENERAL_REVERSING_LINE_EN)
    ITEMID_REVERSING_LINE,
#if (MENU_GENERAL_REVERSING_LINE_ON_EN)
    ITEMID_REVERSING_LINE_ON,
#endif
#if (MENU_GENERAL_REVERSING_LINE_OFF_EN)
    ITEMID_REVERSING_LINE_OFF,
#endif
#endif

#if MENU_GENERAL_LCD_POWER_SAVE_EN
    ITEMID_LCD_POWER_SAVE,
#if (MENU_GENERAL_LCD_POWER_SAVE_OFF_EN)
    ITEMID_LCD_POWER_SAVE_OFF,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_10SEC_EN)
    ITEMID_LCD_POWER_SAVE_10SEC,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_30SEC_EN)
    ITEMID_LCD_POWER_SAVE_30SEC,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_1MIN_EN)
    ITEMID_LCD_POWER_SAVE_1MIN,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_3MIN_EN)
    ITEMID_LCD_POWER_SAVE_3MIN,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_5MIN_EN)
    ITEMID_LCD_POWER_SAVE_5MIN,
#endif
#endif

#if (MENU_GENERAL_SHUTDOWN_TONE_EN)
    ITEMID_SHUTDOWN_TONE,
#if (MENU_GENERAL_SHUTDOWN_TONE_ON_EN)
    ITEMID_SHUTDOWN_TONE_ON,
#endif
#if (MENU_GENERAL_SHUTDOWN_TONE_OFF_EN)
    ITEMID_SHUTDOWN_TONE_OFF,
#endif
    ITEMID_SHUTDOWN_TONE_LOW,
    ITEMID_SHUTDOWN_TONE_MID,
    ITEMID_SHUTDOWN_TONE_HIGH,
#endif

#if (MENU_GENERAL_KEY_TONE_EN)
    ITEMID_KEY_TONE,
#if (MENU_GENERAL_KEY_TONE_ON_EN)
    ITEMID_KEY_TONE_ON,
#endif
#if (MENU_GENERAL_KEY_TONE_OFF_EN)
    ITEMID_KEY_TONE_OFF,
#endif
    ITEMID_KEY_TONE_LOW,
    ITEMID_KEY_TONE_MID,
    ITEMID_KEY_TONE_HIGH,
#endif

#if (MENU_GENERAL_GSENSOR_OFF_EN)
    ITEMID_GSENSOR_SENSITIVITY,
    ITEMID_GSENSOR_SENSITIVITY_OFF,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL0_EN)
    ITEMID_GSENSOR_SENSITIVITY_L0,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL1_EN)
    ITEMID_GSENSOR_SENSITIVITY_L1,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL2_EN)
    ITEMID_GSENSOR_SENSITIVITY_L2,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL3_EN)
    ITEMID_GSENSOR_SENSITIVITY_L3,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL4_EN)
    ITEMID_GSENSOR_SENSITIVITY_L4,
#endif

#if (MENU_GENERAL_WIFI_EN)
    ITEMID_WIFI_MODE,
#if (MENU_GENERAL_WIFI_OFF_EN)
    ITEMID_WIFI_MODE_OFF,
#endif
#if (MENU_GENERAL_WIFI_ON_EN)
    ITEMID_WIFI_MODE_ON,
#endif
#if (MENU_GENERAL_WIFI_2_4G_EN)
    ITEMID_WIFI_MODE_2_4G,
#endif
#if (MENU_GENERAL_WIFI_5G_EN)
    ITEMID_WIFI_MODE_5G,
#endif
#endif

#if (MENU_GENERAL_DATE_LOGO_DISPLAY_EN)
    ITEMID_DATE_LOGO_DISPLAY,
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_ON_EN)
    ITEMID_DATE_LOGO_DISPLAY_ON,
#endif
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_OFF_EN)
    ITEMID_DATE_LOGO_DISPLAY_OFF,
#endif
#endif
    ITEMID_GENERAL_END
}GENERALSETTINGITEMID;

typedef enum {
    ITEMID_MEDIA_SELECT = ITEMID_GENERAL_END,
    ITEMID_MEDIA_SD_CARD,
    ITEMID_MEDIA_INTERNAL,
    ITEMID_FORMAT_SD_CARD,
    ITEMID_FORMAT_CARD_YES,
    ITEMID_FORMAT_CARD_NO,
    ITEMID_FORMAT_INTMEM,
    ITEMID_FORMAT_INTMEM_YES,
    ITEMID_FORMAT_INTMEM_NO,
    ITEMID_SD_CARD_INFO,
    ITEMID_FORMAT_REMINDER,
    ITEMID_FORMAT_REMINDER_NO,
    ITEMID_FORMAT_REMINDER_15DAY,
    ITEMID_FORMAT_REMINDER_30DAY,
    ITEMID_FORMAT_REMINDER_60DAY,

    ITEMID_MEDIA_END
} MEDIATOOLITEMID;

//Menu Settting
typedef enum{
    #if MENU_MOVIE_SIZE_4K_25P_EN
    MOVIE_SIZE_4K_25P,
    #endif
    #if(MENU_MOVIE_SIZE_1440_30P_EN)
    MOVIE_SIZE_1440_30P,
    #endif
    #if MENU_MOVIE_SIZE_SHD_30P_EN
    MOVIE_SIZE_SHD_30P,
    #endif
    #if MENU_MOVIE_SIZE_SHD_25P_EN
    MOVIE_SIZE_SHD_25P,
    #endif
    #if MENU_MOVIE_SIZE_1080P_30_HDR_EN
    MOVIE_SIZE_1080_30P_HDR,
    #endif
    #if MENU_MOVIE_SIZE_1080_60P_EN
    MOVIE_SIZE_1080_60P,
    #endif
    #if MENU_MOVIE_SIZE_1080_25P_EN
    MOVIE_SIZE_1080_25P,
    #endif
    #if MENU_MOVIE_SIZE_1080P_EN
    MOVIE_SIZE_1080P,
    MOVIE_SIZE_1080_30P = MOVIE_SIZE_1080P,
    #endif
    #if (MENU_MOVIE_SIZE_960P_30P_EN)
    MOVIE_SIZE_960P_30P,
    #endif
    #if MENU_MOVIE_SIZE_900P_30P_EN
    MOVIE_SIZE_900P_30P,
    #endif
    #if MENU_MOVIE_SIZE_720P_EN
    MOVIE_SIZE_720P,
    MOVIE_SIZE_720_30P = MOVIE_SIZE_720P,
    #endif
    #if MENU_MOVIE_SIZE_720_120P_EN
    MOVIE_SIZE_720_120P,
    #endif
    #if MENU_MOVIE_SIZE_720_60P_EN
    MOVIE_SIZE_720_60P,
    #endif
    #if MENU_MOVIE_SIZE_720_50P_EN
    MOVIE_SIZE_720_50P,
    #endif
    #if MENU_MOVIE_SIZE_VGA30P_EN
    MOVIE_SIZE_VGA30P,
    MOVIE_SIZE_VGA_30P = MOVIE_SIZE_VGA30P,
    #endif
    MOVIE_SIZE_NUM,
}MOVIESIZE_SETTING;

typedef enum{
#if (MENU_MOVIE_CLIP_TIME_OFF_EN)
    MOVIE_CLIP_TIME_OFF,
#endif
#if (MENU_MOVIE_CLIP_TIME_1MIN_EN)
    MOVIE_CLIP_TIME_1MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_2MIN_EN)
    MOVIE_CLIP_TIME_2MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_3MIN_EN)
    MOVIE_CLIP_TIME_3MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_5MIN_EN)
    MOVIE_CLIP_TIME_5MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_10MIN_EN)
    MOVIE_CLIP_TIME_10MIN,
#endif
#if (MENU_MOVIE_CLIP_TIME_30MIN_EN)
    MOVIE_CLIP_TIME_30MIN,
#endif
    MOVIE_CLIP_TIME_NUM,
}MOVIE_CLIPTIME;

typedef enum{
#if (MENU_MOVIE_SOUND_RECORD_ON_EN)
    MOVIE_SOUND_RECORD_ON,
#endif
#if (MENU_MOVIE_SOUND_RECORD_OFF_EN)
    MOVIE_SOUND_RECORD_OFF,
#endif
    MOVIE_SOUND_RECORD_NUM,
}MOVIE_SOUND_RECORD;

typedef enum{
#if (MENU_GENERAL_DATE_FORMAT_NONE_EN)
    DATETIME_SETUP_NONE = 0,
#endif
#if (MENU_GENERAL_DATE_FORMAT_YMD_EN)
    DATETIME_SETUP_YMD,
#endif
#if (MENU_GENERAL_DATE_FORMAT_MDY_EN)
    DATETIME_SETUP_MDY,
#endif
#if (MENU_GENERAL_DATE_FORMAT_DMY_EN)
    DATETIME_SETUP_DMY,
#endif
    DATETIME_SETUP_NUM,
    DATETIME_SETUP_DEFAULT = 0
}DATETIMESETUP_SETTING;

typedef enum{
#if (MENU_GENERAL_LANGUAGE_ENGLISH_EN)
    LANGUAGE_ENGLISH,
#endif
#if (MENU_GENERAL_LANGUAGE_SPANISH_EN)
    LANGUAGE_SPANISH,
#endif
#if (MENU_GENERAL_LANGUAGE_PORTUGUESE_EN)
    LANGUAGE_PORTUGUESE,
#endif
#if (MENU_GENERAL_LANGUAGE_RUSSIAN_EN)
    LANGUAGE_RUSSIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_SCHINESE_EN)
    LANGUAGE_SCHINESE,
#endif
#if (MENU_GENERAL_LANGUAGE_TCHINESE_EN)
    LANGUAGE_TCHINESE,
#endif
#if (MENU_GENERAL_LANGUAGE_GERMAN_EN)
    LANGUAGE_GERMAN,
#endif
#if (MENU_GENERAL_LANGUAGE_ITALIAN_EN)
    LANGUAGE_ITALIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_LATVIAN_EN)
    LANGUAGE_LATVIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_POLISH_EN)
    LANGUAGE_POLISH,
#endif
#if (MENU_GENERAL_LANGUAGE_ROMANIAN_EN)
    LANGUAGE_ROMANIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_SLOVAK_EN)
    LANGUAGE_SLOVAK,
#endif
#if (MENU_GENERAL_LANGUAGE_UKRANINIAN_EN)
    LANGUAGE_UKRANINIAN,
#endif
#if (MENU_GENERAL_LANGUAGE_FRENCH_EN)
    LANGUAGE_FRENCH,
#endif
#if (MENU_GENERAL_LANGUAGE_JAPANESE_EN)
    LANGUAGE_JAPANESE,
#endif
#if (MENU_GENERAL_LANGUAGE_KOREAN_EN)
    LANGUAGE_KOREAN,
#endif
#if (MENU_GENERAL_LANGUAGE_CZECH_EN)
    LANGUAGE_CZECH,
#endif
#if (MENU_GENERAL_LANGUAGE_VIETNAMESE_EN)
    LANGUAGE_VIETNAMESE,
#endif
    LANGUAGE_NUM,
}LANGUAGE_SETTING;

typedef enum{
    RESET_NO  ,
    RESET_YES  ,
    RESET_DEFAULT = RESET_NO
}RESET_SETTINGS;

typedef enum{
#if (MENU_GENERAL_FLICKER_50HZ_EN)
    FLICKER_50HZ,
#endif
#if (MENU_GENERAL_FLICKER_60HZ_EN)
    FLICKER_60HZ,
#endif

    FLICKER_NUM,
}FLICKER_HZ;

typedef enum{
#if (MENU_GENERAL_LCD_POWER_SAVE_OFF_EN)
    LCD_POWER_SAVE_OFF,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_10SEC_EN)
    LCD_POWER_SAVE_10SEC,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_30SEC_EN)
    LCD_POWER_SAVE_30SEC,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_1MIN_EN)
    LCD_POWER_SAVE_1MIN,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_3MIN_EN)
    LCD_POWER_SAVE_3MIN,
#endif
#if (MENU_GENERAL_LCD_POWER_SAVE_5MIN_EN)
    LCD_POWER_SAVE_5MIN,
#endif

    LCD_POWER_SAVE_NUM,
}LCD_PowerSave;

typedef enum{
#if (MENU_GENERAL_KEY_TONE_ON_EN)
    BEEP_ON,
#endif
#if (MENU_GENERAL_KEY_TONE_OFF_EN)
    BEEP_OFF,
#endif
    BEEP_LOW,
    BEEP_MID,
    BEEP_HIGH,

    BEEP_NUM,
    BEEP_DEFAULT = 0
}BEEP_SETTING;

typedef enum{
#if (MENU_GENERAL_SHUTDOWN_TONE_ON_EN)
    SHUTDOWN_TONE_ON,
#endif
#if (MENU_GENERAL_SHUTDOWN_TONE_OFF_EN)
    SHUTDOWN_TONE_OFF,
#endif
    SHUTDOWN_TONE_LOW,
    SHUTDOWN_TONE_MID,
    SHUTDOWN_TONE_HIGH,

    SHUTDOWN_TONE_NUM,
    SHUTDOWN_TONE_DEFAULT = 0
}SHUTDOWN_TONE_SETTING;

typedef enum{
#if (MENU_GENERAL_GSENSOR_OFF_EN)
    GSENSOR_SENSITIVITY_OFF,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL0_EN)
    GSENSOR_SENSITIVITY_L0,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL1_EN)
    GSENSOR_SENSITIVITY_L1,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL2_EN)
    GSENSOR_SENSITIVITY_L2,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL3_EN)
    GSENSOR_SENSITIVITY_L3,
#endif
#if (MENU_GENERAL_GSENSOR_LEVEL4_EN)
    GSENSOR_SENSITIVITY_L4,
#endif
    GSENSOR_SENSITIVITY_NUM,
}GSENSOR_SENSITIVITY;

typedef enum{
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_ON_EN)
    DATE_LOGO_DISPLAY_ON,
#endif
#if (MENU_GENERAL_DATE_LOGO_DISPLAY_OFF_EN)
    DATE_LOGO_DISPLAY_OFF,
#endif
    DATE_LOGO_DISPLAY_DEFAULT = 0,
}DATE_LOGO_DISPLAY;

typedef enum{
#if (MENU_GENERAL_WIFI_OFF_EN)
    WIFI_MODE_OFF,
#endif
#if (MENU_GENERAL_WIFI_ON_EN)
    WIFI_MODE_ON,
#endif
    WIFI_MODE_2_4G,
    WIFI_MODE_5G,

    WIFI_MODE_NUM,
}WIFI_MODE;

typedef enum{
#if (MENU_SYSTEM_VOLUME_LV0_EN)
    VOLUME_00,
#endif
#if (MENU_SYSTEM_VOLUME_LV1_EN)
    VOLUME_01 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV2_EN)
    VOLUME_02 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV3_EN)
    VOLUME_03 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV4_EN)
    VOLUME_04 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV5_EN)
    VOLUME_05 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV6_EN)
    VOLUME_06 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV7_EN)
    VOLUME_07 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV8_EN)
    VOLUME_08 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV9_EN)
    VOLUME_09 ,
#endif
#if (MENU_SYSTEM_VOLUME_LV10_EN)
    VOLUME_10 ,
#endif

    VOLUME_NUM,
    VOLUME_DEFAULT = 0
}VOLUME_SETTING;

typedef enum{
#if (MENU_SYSTEM_BACKLIGHT_LV0_EN)
    BACKLIGHT_00,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV1_EN)
    BACKLIGHT_01 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV2_EN)
    BACKLIGHT_02 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV3_EN)
    BACKLIGHT_03 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV4_EN)
    BACKLIGHT_04 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV5_EN)
    BACKLIGHT_05 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV6_EN)
    BACKLIGHT_06 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV7_EN)
    BACKLIGHT_07 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV8_EN)
    BACKLIGHT_08 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV9_EN)
    BACKLIGHT_09 ,
#endif
#if (MENU_SYSTEM_BACKLIGHT_LV10_EN)
    BACKLIGHT_10 ,
#endif
    BACKLIGHT_NUM,
    BACKLIGHT_DEFAULT = 0
}BACKLIGHT_SETTING;

typedef enum{
#if (MENU_GENERAL_REVERSING_LINE_EN)
#if (MENU_GENERAL_REVERSING_LINE_ON_EN)
    REVERSING_LINE_ON,
#endif
#if (MENU_GENERAL_REVERSING_LINE_OFF_EN)
    REVERSING_LINE_OFF,
#endif
    REVERSING_LINE_DEFAULT = 0,
#endif
}REVERSING_LINE;

typedef enum{
    REVERSING_LEFT_TOP_X,
    REVERSING_LEFT_TOP_Y,
    REVERSING_LEFT_BOTTOM_X,
    REVERSING_LEFT_BOTTOM_Y,
    REVERSING_LEFT_MIDDLE1_RATIO,
    REVERSING_LEFT_MIDDLE2_RATIO,
    REVERSING_RIGHT_TOP_X,
    REVERSING_RIGHT_TOP_Y,
    REVERSING_RIGHT_BOTTOM_X,
    REVERSING_RIGHT_BOTTOM_Y,
    REVERSING_RIGHT_MIDDLE1_RATIO,
    REVERSING_RIGHT_MIDDLE2_RATIO,
}REVERSING_POINT;

//menu setting key maybe in cgi_config.bin
#define COMMON_KEY_MOVIE_SIZE				"Camera.Menu.VideoRes"
#define COMMON_KEY_VR_CLIP_TIME             "Camera.Menu.LoopingVideo"     //LoopingVideo=VideoClipTime
#define COMMON_KEY_RECD_SOUND               "Camera.Menu.SoundRecord"

#define COMMON_DATE_TIME_FMT                "Camera.Menu.DateTimeFormat"
#define COMMON_KEY_LANGUAGE                 "Camera.Menu.Language"
#define COMMON_KEY_RESET                    "Camera.Menu.ResetSetting"
#define COMMON_KEY_FLICKER                  "Camera.Menu.Flicker"
#define COMMON_KEY_LCD_POWERS               "Camera.Menu.PowerSaving"
#define COMMON_KEY_BEEP                     "Camera.Menu.Beep"
#define COMMON_KEY_SHUTDOWN_TONE            "Camera.Menu.ShutdownTone"
#define COMMON_KEY_GSENSOR_SENS             "Camera.Menu.GSensorSensitivity"
#define COMMON_KEY_DATE_LOGO_DISPLAY        "Camera.Menu.DateLogoDisplay"
#define COMMON_KEY_WIFI_EN                  "Camera.Menu.WiFi"
#define COMMON_KEY_STSTEM_VOLUME            "Camera.Menu.SystemVolume"
#define COMMON_KEY_LCD_BACKLIGHT            "Camera.Menu.LCDBacklight"

#define COMMON_KEY_REVERSING_LINE           "Camera.Menu.ReversingLine"
#define COMMON_REVERSING_LEFT_TOP_X         "Camera.Menu.LeftTopX"
#define COMMON_REVERSING_LEFT_TOP_Y         "Camera.Menu.LeftTopY"
#define COMMON_REVERSING_LEFT_BOTTOM_X      "Camera.Menu.LeftBottomX"
#define COMMON_REVERSING_LEFT_BOTTOM_Y      "Camera.Menu.LeftBottomY"
#define COMMON_REVERSING_LEFT_MIDDLE1_RATIO "Camera.Menu.LeftMid1Ratio"
#define COMMON_REVERSING_LEFT_MIDDLE2_RATIO "Camera.Menu.LeftMid2Ratio"
#define COMMON_REVERSING_RIGHT_TOP_X        "Camera.Menu.RightTopX"
#define COMMON_REVERSING_RIGHT_TOP_Y        "Camera.Menu.RightTopY"
#define COMMON_REVERSING_RIGHT_BOTTOM_X     "Camera.Menu.RightBottomX"
#define COMMON_REVERSING_RIGHT_BOTTOM_Y     "Camera.Menu.RightBottomY"
#define COMMON_REVERSING_RIGHT_MIDDLE1_RATIO "Camera.Menu.RightMid1Ratio"
#define COMMON_REVERSING_RIGHT_MIDDLE2_RATIO "Camera.Menu.RightMid2Ratio"

//String Map ID
typedef enum _OSD_STRING_IDS {
IDS_DS_EMPTY = 0,
IDS_DS_NO,
IDS_DS_YES,
IDS_DS_ON,
IDS_DS_OFF,
IDS_DS_LOW,
IDS_DS_MIDDLE,
IDS_DS_HIGH,
IDS_DS_STANDARD,
IDS_DS_NONE,
IDS_DS_OK,
IDS_DS_CANCEL,
IDS_TIME_10SEC,
IDS_TIME_30SEC,
IDS_TIME_1MIN,
IDS_TIME_2MIN,
IDS_TIME_3MIN,
IDS_TIME_5MIN,
IDS_TIME_10MIN,
IDS_TIME_30MIN,

IDS_DS_MOVIE_MODE,
IDS_DS_MOVIE_MODE_4K_25,
IDS_DS_MOVIE_MODE_1440_30,
IDS_DS_MOVIE_MODE_SHD,
IDS_DS_MOVIE_MODE_SHD_25P,
IDS_DS_MOVIE_MODE_FHD_60P,
IDS_DS_MOVIE_MODE_FHD,
IDS_DS_MOVIE_MODE_FHD_HDR,
IDS_DS_MOVIE_MODE_HD,
IDS_DS_MOVIE_MODE_HD_60P,
IDS_DS_MOVIE_MODE_VGA_30P,

IDS_MOVIE_CLIPTIME,

IDS_MOVIE_SOUND_RECORD,

IDS_DS_CLOCK_SETTINGS,
IDS_DS_DATETIME_Y,
IDS_DS_DATETIME_M,
IDS_DS_DATETIME_D,
IDS_DS_DATETIME_H,
IDS_DS_DATETIME_MM,
IDS_DS_DATETIME_S,

IDS_DS_DATETIME_FORMAT,
IDS_DS_DATETIME_YMD,
IDS_DS_DATETIME_MDY,
IDS_DS_DATETIME_DMY,

IDS_DS_LANGUAGE,
IDS_DS_LANGUAGES_ENGLISH,
IDS_DS_LANGUAGES_SPANISH,
IDS_DS_LANGUAGES_PORTUGUESE,
IDS_DS_LANGUAGES_RUSSIAN,
IDS_DS_LANGUAGES_SCHINESE,
IDS_DS_LANGUAGES_TCHINESE,
IDS_DS_LANGUAGES_GERMAN,
IDS_DS_LANGUAGES_ITALIAN,
IDS_DS_LANGUAGES_LATVIAN,
IDS_DS_LANGUAGES_POLISH,
IDS_DS_LANGUAGES_ROMANIAN,
IDS_DS_LANGUAGES_SLOVAK,
IDS_DS_LANGUAGES_UKRANINIAN,
IDS_DS_LANGUAGES_FRENCH,
IDS_DS_LANGUAGES_KOREAN,
IDS_DS_LANGUAGES_JAPANESE,
IDS_DS_LANGUAGES_CZECH,
IDS_DS_LANGUAGES_VIETNAMESE,
IDS_DS_LANGUAGE_DESCRIPTION,

IDS_DS_RESET_SETUP,
IDS_DS_RESET_SETUP_CONFIRM,
IDS_DS_RESET_INFO,

IDS_DS_FW_VERSION_INFO,
IDS_DS_FW_VERSION,

IDS_DS_QR_CODE_INFO,
IDS_DS_QR_CODE,

IDS_DS_FLICKER,
IDS_FLICKER_50HZ,
IDS_FLICKER_60HZ,

IDS_DS_REVERSING_LINE,
IDS_REVERSING_LINE_ON,
IDS_REVERSING_LINE_OFF,

IDS_DS_LCD_POWER_SAVE,

IDS_DS_KEY_TONE,
IDS_DS_SHUTDOWN_TONE,

IDS_DS_GSENSOR_SENSITIVETY,
IDS_DS_GSENSOR_SENSITIVETY_LEVEL0,
IDS_DS_GSENSOR_SENSITIVETY_LEVEL1,
IDS_DS_GSENSOR_SENSITIVETY_LEVEL2,
IDS_DS_GSENSOR_SENSITIVETY_LEVEL3,
IDS_DS_GSENSOR_SENSITIVETY_LEVEL4,

IDS_DS_DATE_LOGO_DISPLAY,

IDS_DS_FORMAT_SD_CARD,
IDS_DS_FORMAT_CARD_CONFIRM,
IDS_DS_DATA_DELETED,
IDS_DS_MSG_LOCK_CURRENT_FILE,
IDS_DS_MSG_UNLOCK_FILE,
IDS_DS_MSG_DELETE_FILE_OK,
IDS_DS_MSG_PROTECT_FILE_OK,
IDS_DS_MSG_NO_FILE,
IDS_DS_MSG_SURE_TO_DELETE_SELETED,
IDS_DS_MSG_DELETE_SELETED_ERROR,
IDS_DS_MSG_CANNOT_DELETE,
IDS_DS_MSG_SURE_TO_PROTECT_SELETED,
IDS_DS_MSG_SURE_TO_UNPROTECT_SELETED,
IDS_DS_MSG_CARD_ERROR,
IDS_DS_MSG_STORAGE_FULL,
IDS_DS_MSG_NO_CARD,
IDS_DS_MSG_PLUG_OUT_SD,
IDS_DS_MSG_WAIT_INITIAL_DONE,
IDS_DS_MSG_INSERT_SD_AGAIN,
IDS_DS_MSG_FORMAT_SD_PROCESSING,
IDS_DS_MSG_FORMAT_SD_CARD_OK,
IDS_DS_MSG_FORMAT_SD_CARD_FAIL,
IDS_DS_MSG_FORMAT_SD_CARD,
IDS_DS_MSG_GOTO_POWER_OFF,
IDS_DS_MSG_OPENFILE_WAIT,
IDS_DS_MSG_ACC_OFF_TIMELAPSE_REC,
IDS_DS_MSG_ACC_ON_NORMAL_REC,
IDS_DS_15DAY,
IDS_DS_30DAY,
IDS_DS_60DAY,
IDS_DS_GENERAL_SETTINGS,
IDS_DS_SYSTEM_SETTINGS,
IDS_DS_VIDEO_SETTINGS,
IDS_DS_CAMID_AND_VIDEO_TYPE,
IDS_DS_VIDEO_TYPE_NORMAL_1ST,
IDS_DS_VIDEO_TYPE_NORMAL_2ST,
IDS_DS_VIDEO_TYPE_PARKING_1ST,
IDS_DS_VIDEO_TYPE_PARKING_2ST,
IDS_DS_VIDEO_TYPE_EMERGENCY_1ST,
IDS_DS_VIDEO_TYPE_EMERGENCY_2ST,
IDS_DS_VIDEO_TYPE_PHOTO_1ST,
IDS_DS_VIDEO_TYPE_PHOTO_2ST,
IDS_DS_2_4G,
IDS_DS_5G,
IDS_DS_WIFI,
IDS_DS_PASSWORD_INFO,
IDS_DS_WIFI_SSID_INFO,
IDS_DS_NOTE,
IDS_DS_WEEK_SUNDAY,
IDS_DS_WEEK_MONDAY,
IDS_DS_WEEK_TUESDAY,
IDS_DS_WEEK_WEDNESDAY,
IDS_DS_WEEK_THURSDAY,
IDS_DS_WEEK_FRIDAY,
IDS_DS_WEEK_STATURDAY,
IDS_DS_UP_PAGE,

IDS_DS_END
} OSD_STRING_IDS;

extern OSD_STRING *osdstringpool;
#define MAP_STRINGID(x) osdstringpool[x].osd_strings

typedef enum _WMSG_INFO {
    WMSG_NONE = 0                   ,
    WMSG_CARD_ERROR                 ,
    WMSG_STORAGE_FULL               ,
    WMSG_NO_CARD                    ,
    WMSG_LOW_BATTERY                ,
    WMSG_FILE_ERROR                 ,
    WMSG_CARD_LOCK                  ,
    WMSG_CARD_SLOW                  ,
    WMSG_ADAPTER_ERROR              ,
    WMSG_INVALID_OPERATION          ,
    WMSG_CANNOT_DELETE              ,
    WMSG_BATTERY_FULL               ,
    WMSG_LENS_ERROR                 ,
    WMSG_HDMI_TV                    ,
    WMSG_FHD_VR                     ,
    WMSG_PLUG_OUT_SD_CARD           ,
    WMSG_WAIT_INITIAL_DONE          ,
    WMSG_INSERT_SD_AGAIN            ,
    WMSG_FORMAT_SD_PROCESSING       ,
    WMSG_FORMAT_SD_CARD_OK          ,
    WMSG_FORMAT_SD_CARD_FAIL        ,
    WMSG_COME2EMER                  ,
    WMSG_TIME_ERROR                 ,
    WMSG_SHOW_FW_VERSION            ,
    WMSG_START_RECORD               ,
    WMSG_START_NORMAL_RECORD        ,
    WMSG_SPEEDCAM_ADDED             ,
    WMSG_ADD_SPEEDCAM               ,
    WMSG_DEL_SPEEDCAM               ,
    WMSG_NO_POI_SPACE               ,
    WMSG_WAIT_GPS_FIX               ,
    WMSG_PARKING_MODE_DISABLE,
    WMSG_LDWS_RightShift            ,
    WMSG_LDWS_LeftShift             ,
    WMSG_SEAMLESS_ERROR             ,
    WMSG_FORMAT_SD_CARD             ,
    WMSG_LOCK_CURRENT_FILE          ,
    WMSG_UNLOCK_CUR_FILE            ,
    WMSG_DELETE_FILE_OK             ,
    WMSG_PROTECT_FILE_OK            ,
    WMSG_GOTO_POWER_OFF             ,
    WMSG_NO_FILE_IN_BROWSER         ,
    WMSG_CAPTURE_CUR_FRAME          ,
    WMSG_FCWS                       ,
    WMSG_FATIGUEALERT               ,
    WMSG_OPENFILE_WAIT              ,
    WMSG_NO_CARD1                   ,
    WMSG_NO_CARD2                   ,
    WMSG_CARD2_FULL                 ,
    WMSG_NO_CARD2_2                 ,
    WMSG_BACKUP_FINISH              ,
    WMSG_WIFI_STARTING              ,
    WMSG_SYSTEM_POWER_OFF           ,

    WMSG_MOTION_OPERATING_DESCRIPTION,
    WMSG_MSG_EMERGENCY_FILE_FULL,
    WMSG_REMIND_HEADLIGHT,
    WMSG_PARKING_OPERATING_DESCRIPTION,
    WMSG_SYSTEM_ACC_OFF_POWEROFF,
    WMSG_SYSTEM_DELAY_ACC_OFF_POWEROFF,
    WMSG_SYSTEM_ACC_OFF_TIMELAPSE_REC,
    WMSG_SYSTEM_ACC_ON_NORMAL_REC,
    WMSG_LOW_POWER,
    WMSG_SD_FULL,
    WMSG_TIMELAPSE_REC_5S,
    WMSG_TIMELAPSE_RECING,
    WMSG_STOP_RECING,
    WMSG_FILE_LOCK,
    WMSG_FILE_UNLOCK,
    WMSG_PHOTO_OK,
    WMSG_WIFI_CONNECT,
    WMSG_WIFI_DISCONNECT,
    WMSG_NORMAL_FILE_FULL,
    WMSG_LOCK_FILE_FULL,
    WMSG_PHOTO_FILE_FULL,
    WMSG_RES_NO_SUPPROT,
    WMSG_NETWORK_MODE,
    WMSG_RES_CHG_2K,
    WMSG_RES_CHG_4K,
    WMSG_WIFI_MODE_CHG_STARTING,
    WMSG_PARKING_FILE_NOTICE,
    WMSG_PARKING_ACC_CONNECT,
    WMSG_STOP_RECING_REMIND,
    WMSG_FATIGUE_DRIVING_WARN,
    WMSG_PARKING_REC_EXIST,

    END_OF_WMSG
} WMSG_INFO;

/*===========================================================================
* Extern variable
*===========================================================================*/
extern SMENUSTRUCT  sMainMenuVideo;
extern SMENUSTRUCT  sMainMenuGeneral;
extern SMENUSTRUCT  sMainMenuSystem;
extern SMENUSTRUCT  sMainMenuGuide;

/*===========================================================================
* Macro define
*===========================================================================*/
#define MENU_PAGE_NUM   sizeof(MenuPageList)/sizeof(PSMENUSTRUCT)
/* For RTC Default Time */
#ifndef RTC_DEFAULT_YEAR
#define RTC_DEFAULT_YEAR                (2020)
#endif
#ifndef RTC_DEFAULT_MONTH
#define RTC_DEFAULT_MONTH               (1)
#endif
#ifndef RTC_DEFAULT_DAY
#define RTC_DEFAULT_DAY                 (1)
#endif

#ifndef RTC_DEFAULT_HOUR 
#define RTC_DEFAULT_HOUR                (8)
#endif
#ifndef RTC_DEFAULT_MIN
#define RTC_DEFAULT_MIN                 (0)
#endif
#ifndef RTC_DEFAULT_SEC
#define RTC_DEFAULT_SEC                 (0)
#endif

#ifndef RTC_MAX_YEAR
#define RTC_MAX_YEAR                    (2037)
#endif
#ifndef RTC_MIN_YEAR
#define RTC_MIN_YEAR                    (RTC_DEFAULT_YEAR)
#endif
#define RTC_MAX_MONTH                   (12)
#define RTC_MIN_MONTH                   (1)
#define RTC_MAX_DAY_31                  (31)
#define RTC_MAX_DAY_30                  (30)
#define RTC_MAX_DAY_FEB_LEAP_YEAR       (29)
#define RTC_MAX_DAY_FEB_NONLEAP_YEAR    (28)
#define RTC_MIN_DAY                     (1)
#define RTC_MAX_HOUR                    (23)
#define RTC_MIN_HOUR                    (0)
#define RTC_MAX_MIN                     (59)
#define RTC_MIN_MIN                     (0)
#define RTC_MAX_SEC                     (59)
#define RTC_MIN_SEC                     (0)

/* For Clock Setting : YYYY/MM/DD HH/MM/SS */
#define IDX_YEAR                        (0)
#define IDX_MONTH                       (1)
#define IDX_DAY                         (2)
#define IDX_HOUR                        (3)
#define IDX_MIN                         (4)
#define IDX_SEC                         (5)
#define CHECK_PASS                      (0xFF)

#define CHECK_YEAR                      (0x01)
#define CHECK_MONTH                     (0x02)
#define CHECK_DAY                       (0x04)
#define CHECK_HOUR                      (0x08)
#define CHECK_MIN                       (0x10)
#define CHECK_SEC                       (0x20)
#define CHECK_ALL                       (CHECK_YEAR | CHECK_MONTH | CHECK_DAY | CHECK_HOUR | CHECK_MIN | CHECK_SEC)

#define GET_CONFIG_SETTING   "nvconf get 0 "
#define SET_CONFIG_SETTING   "nvconf set 0 "
static const char MenuCfgFile[] = "/customer/wifi/cgi_config.bin";

/*===========================================================================
* Function prototype 
*===========================================================================*/
bool MenuSetting_GetCB(char *cmenu_string, ...);
int ParseMenuSet(const char *file, MenuInfo* pmi /*out*/);
void Menu_WriteSetting(void);
pMenuInfo MenuSettingConfig(void);
bool Menu_General_EnGet(char *key, char *value, int len);
bool Menu_General_EnSet(const char *key, char *value);
void MenuSettingInit(void);
void ListAllMenuSetting(MenuInfo *Info);

#endif //_MENU_COMMON_H_
