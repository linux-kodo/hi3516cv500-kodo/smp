#include "carimpl.h"
#include "MenuCommon.h"

/*===========================================================================
* Local function
*===========================================================================*/
bool MenuItemMovieMode(PSMENUITEM pItem);
bool MenuItemMovieClipTime(PSMENUITEM pItem);
bool MenuItemMovieSoundRecord(PSMENUITEM pItem);

int  MenuGetDefault_Movie(PSMENUSTRUCT pMenu);

/*===========================================================================
* Extern varible
*===========================================================================*/

/*===========================================================================
* Global varible : Menu
*===========================================================================*/
extern SMENUSTRUCT sMainMenuVideo;
extern SMENUSTRUCT sSubMovieMode;
extern SMENUSTRUCT sSubMovieClipTime;
extern SMENUSTRUCT sSubMovieSoundRecord;

/*===========================================================================
* Global varible : Item Structure
*===========================================================================*/
// Movie Mode
#if (MENU_MOVIE_SIZE_EN)
SMENUITEM sItemMovieMode                = { ITEMID_MOVIE_MODE,           IDS_DS_MOVIE_MODE,          &sSubMovieMode, 0 };
#if (MENU_MOVIE_SIZE_4K_25P_EN)
SMENUITEM sItemMovieMode_4K_25P         = { ITEMID_MOVIE_MODE_4K_25P,    IDS_DS_MOVIE_MODE_4K_25,    NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_1440_30P_EN)
SMENUITEM sItemMovieMode_1440_30P       = { ITEMID_MOVIE_MODE_1440_30P,  IDS_DS_MOVIE_MODE_1440_30,  NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_SHD_30P_EN)
SMENUITEM sItemMovieMode_SHD            = { ITEMID_MOVIE_MODE_SHD,       IDS_DS_MOVIE_MODE_SHD,      NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_SHD_25P_EN)
SMENUITEM sItemMovieMode_SHD_25P        = { ITEMID_MOVIE_MODE_SHD_25P,   IDS_DS_MOVIE_MODE_SHD_25P,  NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_1080_60P_EN)
SMENUITEM sItemMovieMode_FHD_60P        = { ITEMID_MOVIE_MODE_FHD_60P,   IDS_DS_MOVIE_MODE_FHD_60P,  NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_1080P_EN)
SMENUITEM sItemMovieMode_FHD            = { ITEMID_MOVIE_MODE_FHD,       IDS_DS_MOVIE_MODE_FHD,      NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_1080P_30_HDR_EN)
SMENUITEM sItemMovieMode_FHD_HDR        = { ITEMID_MOVIE_MODE_FHD_HDR,   IDS_DS_MOVIE_MODE_FHD_HDR,  NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_720P_EN)
SMENUITEM sItemMovieMode_HD             = { ITEMID_MOVIE_MODE_HD,        IDS_DS_MOVIE_MODE_HD,       NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_720_60P_EN)
SMENUITEM sItemMovieMode_HD_60P         = { ITEMID_MOVIE_MODE_HD_60P,    IDS_DS_MOVIE_MODE_HD_60P,   NULL, MenuItemMovieMode };
#endif
#if (MENU_MOVIE_SIZE_VGA30P_EN)
SMENUITEM sItemMovieMode_VGA_30P        = { ITEMID_MOVIE_MODE_VGA_30P,   IDS_DS_MOVIE_MODE_VGA_30P,  NULL, MenuItemMovieMode };
#endif
#endif

// Movie Clip Time
#if (MENU_MOVIE_CLIP_TIME_EN)
SMENUITEM sItemMovieClipTime            = { ITEMID_MOVIE_CLIPTIME,       IDS_MOVIE_CLIPTIME,     &sSubMovieClipTime, 0 };
#if (MENU_MOVIE_CLIP_TIME_OFF_EN)
SMENUITEM sItemMovieClipTime_off        = { ITEMID_MOVIE_CLIPTIME_OFF,   IDS_DS_OFF,             NULL, MenuItemMovieClipTime };
#endif
#if (MENU_MOVIE_CLIP_TIME_1MIN_EN)
SMENUITEM sItemMovieClipTime_1min       = { ITEMID_MOVIE_CLIPTIME_1MIN,  IDS_TIME_1MIN,          NULL, MenuItemMovieClipTime };
#endif
#if (MENU_MOVIE_CLIP_TIME_2MIN_EN)
SMENUITEM sItemMovieClipTime_2min       = { ITEMID_MOVIE_CLIPTIME_2MIN,  IDS_TIME_2MIN,          NULL, MenuItemMovieClipTime };
#endif
#if (MENU_MOVIE_CLIP_TIME_3MIN_EN)
SMENUITEM sItemMovieClipTime_3min       = { ITEMID_MOVIE_CLIPTIME_3MIN,  IDS_TIME_3MIN,          NULL, MenuItemMovieClipTime };
#endif
#if (MENU_MOVIE_CLIP_TIME_5MIN_EN)
SMENUITEM sItemMovieClipTime_5min       = { ITEMID_MOVIE_CLIPTIME_5MIN,  IDS_TIME_5MIN,          NULL, MenuItemMovieClipTime };
#endif
#if (MENU_MOVIE_CLIP_TIME_10MIN_EN)
SMENUITEM sItemMovieClipTime_10min      = { ITEMID_MOVIE_CLIPTIME_10MIN, IDS_TIME_10MIN,         NULL, MenuItemMovieClipTime };
#endif
#if (MENU_MOVIE_CLIP_TIME_30MIN_EN)
SMENUITEM sItemMovieClipTime_30min      = { ITEMID_MOVIE_CLIPTIME_30MIN, IDS_TIME_30MIN,          NULL, MenuItemMovieClipTime };
#endif
#endif

// Movie Sound Record
#if (MENU_MOVIE_SOUND_RECORD_EN)
SMENUITEM sItemMovieSoundRecord         = { ITEMID_MOVIE_SOUND_RECORD,           IDS_MOVIE_SOUND_RECORD, &sSubMovieSoundRecord,  0 };
SMENUITEM sItemMovieSoundRecord_On      = { ITEMID_MOVIE_SOUND_RECORD_ON,        IDS_DS_ON,      NULL, MenuItemMovieSoundRecord };
SMENUITEM sItemMovieSoundRecord_Off     = { ITEMID_MOVIE_SOUND_RECORD_OFF,       IDS_DS_OFF,     NULL, MenuItemMovieSoundRecord };
#endif

/*===========================================================================
* Global varible : Item List
*===========================================================================*/
#if (MENU_MOVIE_SIZE_EN)
PSMENUITEM sMenuListMovieMode[] =
{
#if (MENU_MOVIE_SIZE_4K_25P_EN)
    &sItemMovieMode_4K_25P,
#endif
#if (MENU_MOVIE_SIZE_1440_30P_EN)
    &sItemMovieMode_1440_30P,
#endif
#if (MENU_MOVIE_SIZE_SHD_30P_EN)
    &sItemMovieMode_SHD,
#endif
#if (MENU_MOVIE_SIZE_SHD_25P_EN)
    &sItemMovieMode_SHD_25P,
#endif
#if (MENU_MOVIE_SIZE_1080_60P_EN)
    &sItemMovieMode_FHD_60P,
#endif
#if (MENU_MOVIE_SIZE_1080P_30_HDR_EN)
    &sItemMovieMode_FHD_HDR,
#endif
#if (MENU_MOVIE_SIZE_1080P_EN)
    &sItemMovieMode_FHD,
#endif
#if (MENU_MOVIE_SIZE_720P_EN)
    &sItemMovieMode_HD,
#endif
#if (MENU_MOVIE_SIZE_720_60P_EN)
    &sItemMovieMode_HD_60P,
#endif
#if (MENU_MOVIE_SIZE_VGA30P_EN)
    &sItemMovieMode_VGA_30P,
#endif
};
#endif

#if (MENU_MOVIE_CLIP_TIME_EN)
PSMENUITEM sMenuListMovieClipTime[] =
{
#if (MENU_MOVIE_CLIP_TIME_OFF_EN)
    &sItemMovieClipTime_off,
#endif
#if (MENU_MOVIE_CLIP_TIME_1MIN_EN)
    &sItemMovieClipTime_1min,
#endif
#if (MENU_MOVIE_CLIP_TIME_2MIN_EN)
    &sItemMovieClipTime_2min,
#endif
#if (MENU_MOVIE_CLIP_TIME_3MIN_EN)
    &sItemMovieClipTime_3min,
#endif
#if (MENU_MOVIE_CLIP_TIME_5MIN_EN)
    &sItemMovieClipTime_5min,
#endif
#if (MENU_MOVIE_CLIP_TIME_10MIN_EN)
    &sItemMovieClipTime_10min,
#endif
#if (MENU_MOVIE_CLIP_TIME_30MIN_EN)
    &sItemMovieClipTime_30min,
#endif
};
#endif

#if (MENU_MOVIE_SOUND_RECORD_EN)
PSMENUITEM sMenuListMovieSoundRecord[] =
{
#if (MENU_MOVIE_SOUND_RECORD_ON_EN)
    &sItemMovieSoundRecord_On,
#endif
#if (MENU_MOVIE_SOUND_RECORD_OFF_EN)
    &sItemMovieSoundRecord_Off,
#endif
};
#endif

PSMENUITEM sMenuListMainVideo[] =
{
#if (MENU_MOVIE_SIZE_EN)
    &sItemMovieMode,
#endif
#if (MENU_MOVIE_CLIP_TIME_EN)
    &sItemMovieClipTime,
#endif
#if (MENU_MOVIE_SOUND_RECORD_EN)
    &sItemMovieSoundRecord,
#endif
};

/*===========================================================================
* Global variable : Menu Structure
*===========================================================================*/
SMENUSTRUCT   sMainMenuVideo =
{
    MENUID_MAIN_MENU_VIDEO,
    IDS_DS_MOVIE_MODE,
    NULL,
    sizeof(sMenuListMainVideo)/sizeof(PSMENUITEM),
    sMenuListMainVideo,
    NULL,
};

//--------------SUB MENU-------------------
#if (MENU_MOVIE_SIZE_EN)
SMENUSTRUCT sSubMovieMode =
{
    MENUID_SUB_MENU_MOVIE_MODE,
    IDS_DS_MOVIE_MODE,
    &sMainMenuVideo,
    sizeof(sMenuListMovieMode)/sizeof(PSMENUITEM),
    sMenuListMovieMode,
    MenuGetDefault_Movie,
};
#endif

#if (MENU_MOVIE_CLIP_TIME_EN)
SMENUSTRUCT sSubMovieClipTime =
{
    MENUID_SUB_MENU_MOVIE_CLIPTIME,
    IDS_MOVIE_CLIPTIME,
    &sMainMenuVideo,
    sizeof(sMenuListMovieClipTime)/sizeof(PSMENUITEM),
    sMenuListMovieClipTime,
    MenuGetDefault_Movie,
};
#endif

#if (MENU_MOVIE_SOUND_RECORD_EN)
SMENUSTRUCT sSubMovieSoundRecord =
{
    MENUID_SUB_MENU_MOVIE_SOUND_RECORD,
    IDS_MOVIE_SOUND_RECORD,
    &sMainMenuVideo,
    sizeof(sMenuListMovieSoundRecord)/sizeof(PSMENUITEM),
    sMenuListMovieSoundRecord,
    MenuGetDefault_Movie,
};
#endif

/*===========================================================================
* Main body
*===========================================================================*/
#if (MENU_MOVIE_SIZE_EN)
bool MenuItemMovieMode(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiMOVSize= pItem->iItemId - ITEMID_MOVIE_MODE -1;
    carimpl_VideoFunc_SetResolution();
    return true;
}
#endif

#if (MENU_MOVIE_CLIP_TIME_EN)
bool MenuItemMovieClipTime(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiMOVClipTime = pItem->iItemId - ITEMID_MOVIE_CLIPTIME -1;
    carimpl_VideoFunc_SetClipTime();
    return true;
}
#endif

#if (MENU_MOVIE_SOUND_RECORD_EN)
bool MenuItemMovieSoundRecord(PSMENUITEM pItem)
{
    MenuSettingConfig()->uiMOVSoundRecord = pItem->iItemId - ITEMID_MOVIE_SOUND_RECORD -1;
    carimpl_VideoFunc_SetSoundRecord();
    return true;
}
#endif

int MenuGetDefault_Movie(PSMENUSTRUCT pMenu)
{
    int  DefaultValue = 0;

    switch (pMenu->iMenuId)
    {
#if (MENU_MOVIE_SIZE_EN)
    case MENUID_SUB_MENU_MOVIE_MODE:
        DefaultValue = MenuSettingConfig()->uiMOVSize ;
        break;
#endif
#if (MENU_MOVIE_CLIP_TIME_EN)
    case MENUID_SUB_MENU_MOVIE_CLIPTIME:
        DefaultValue = MenuSettingConfig()->uiMOVClipTime;
        break;
#endif
#if (MENU_MOVIE_SOUND_RECORD_EN)
    case MENUID_SUB_MENU_MOVIE_SOUND_RECORD:
        DefaultValue = MenuSettingConfig()->uiMOVSoundRecord;
        break;
#endif
    }
    return DefaultValue;
}
