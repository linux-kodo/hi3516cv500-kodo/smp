/*
* statusbar.c
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include "carimpl.h"
#include "MenuCommon.h"

#include "lvgl/lvgl.h"
#include "ui_helpers.h"
#include "ui_events.h"
#include "statusbar.h"

#include "ui_common.h"

#define WMSGTIMEOUT       3 //s
#define WPOWEROFFMSGTIMEOUT     2//s
#define WDELAYPOWEROFFMSGTIMEOUT     6//s
#define WDELAYWIFIMSGTIMEOUT     10//s
#define WDELAYFORMATREMINDMSGTIMEOUT     4//s
static int m_WMSGShowTime = 0;

void carimpl_handler_status_bar(void)
{
	WMSG_INFO wmsg_info = carimpl_get_wmsginfo();

	m_WMSGShowTime++;
	//LOG_UI_DBG("wmsg_info:%d, m_WMSGShowTime:%d \r\n", wmsg_info, m_WMSGShowTime);
	if((WMSG_SYSTEM_ACC_OFF_POWEROFF == wmsg_info || WMSG_LOW_POWER== wmsg_info || WMSG_SYSTEM_POWER_OFF== wmsg_info) )
	{
		if(m_WMSGShowTime >= WPOWEROFFMSGTIMEOUT)
		{
			CusSetStatusbarFlags(_UI_MODIFY_FLAG_ADD);
			m_WMSGShowTime = 0;
		}
	}
	else if(WMSG_SYSTEM_DELAY_ACC_OFF_POWEROFF == wmsg_info )
	{
		if(m_WMSGShowTime >= WDELAYPOWEROFFMSGTIMEOUT)
		{
			CusSetStatusbarFlags(_UI_MODIFY_FLAG_ADD);
			m_WMSGShowTime = 0;
		}
	}
	else if(WMSG_WIFI_MODE_CHG_STARTING == wmsg_info || WMSG_WIFI_STARTING == wmsg_info )
	{
		if(m_WMSGShowTime >= WDELAYWIFIMSGTIMEOUT)
		{
			CusSetStatusbarFlags(_UI_MODIFY_FLAG_ADD);
			m_WMSGShowTime = 0;
		}
	}
	else if(WMSG_FORMAT_SD_CARD == wmsg_info)
	{
		if (m_WMSGShowTime >= WDELAYFORMATREMINDMSGTIMEOUT)
		{
			CusSetStatusbarFlags(_UI_MODIFY_FLAG_ADD);
			m_WMSGShowTime = 0;
		}
	}
	else
	{
		if (m_WMSGShowTime >= WMSGTIMEOUT /*&& wmsg_info != WMSG_FORMAT_SD_PROCESSING*/)
		{
			CusSetStatusbarFlags(_UI_MODIFY_FLAG_ADD);
			m_WMSGShowTime = 0;
		}
	}
}

void StatusBar_Update(void)
{
	int iStrID = IDS_DS_EMPTY;
	WMSG_INFO WMSGInfo = carimpl_get_wmsginfo();

	switch (WMSGInfo)
	{
	case WMSG_CARD_ERROR:
		iStrID = IDS_DS_MSG_CARD_ERROR;
		break;
	case WMSG_STORAGE_FULL:
		iStrID = IDS_DS_MSG_STORAGE_FULL;
		break;
	case WMSG_NO_CARD:
		iStrID = IDS_DS_MSG_NO_CARD;
		break;
	case WMSG_LOW_BATTERY:
		break;
	case WMSG_FILE_ERROR:
		break;
	case WMSG_CARD_LOCK:
		break;
	case WMSG_CARD_SLOW:
		break;
	case WMSG_PLUG_OUT_SD_CARD:
		iStrID = IDS_DS_MSG_PLUG_OUT_SD;
		break;
	case WMSG_WAIT_INITIAL_DONE:
		iStrID = IDS_DS_MSG_WAIT_INITIAL_DONE;
		break;
	case WMSG_INSERT_SD_AGAIN:
		iStrID = IDS_DS_MSG_INSERT_SD_AGAIN;
		break;
	case WMSG_FORMAT_SD_PROCESSING:
		iStrID = IDS_DS_MSG_FORMAT_SD_PROCESSING;
		break;
	case WMSG_FORMAT_SD_CARD_OK:
		iStrID = IDS_DS_MSG_FORMAT_SD_CARD_OK;
		break;
	case WMSG_FORMAT_SD_CARD_FAIL:
		iStrID = IDS_DS_MSG_FORMAT_SD_CARD_FAIL;
		break;
	case WMSG_COME2EMER:
		break;
	case WMSG_TIME_ERROR:
		break;
	case WMSG_SHOW_FW_VERSION:
		iStrID = IDS_DS_FW_VERSION_INFO;
		break;
	case WMSG_START_RECORD:
		break;
	case WMSG_START_NORMAL_RECORD:
		break;
	case WMSG_PARKING_MODE_DISABLE:
		break;
	case WMSG_FORMAT_SD_CARD:
		iStrID = IDS_DS_MSG_FORMAT_SD_CARD;
		break;
	case WMSG_LOCK_CURRENT_FILE:
		iStrID = IDS_DS_MSG_LOCK_CURRENT_FILE;
		break;
	case WMSG_UNLOCK_CUR_FILE:
		iStrID = IDS_DS_MSG_UNLOCK_FILE;
		break;
	case WMSG_DELETE_FILE_OK:
		iStrID = IDS_DS_MSG_DELETE_FILE_OK;
		break;
	case WMSG_PROTECT_FILE_OK:
		iStrID = IDS_DS_MSG_PROTECT_FILE_OK;
		break;
	case WMSG_GOTO_POWER_OFF:
		iStrID = IDS_DS_MSG_GOTO_POWER_OFF;
		break;
	case WMSG_NO_FILE_IN_BROWSER:
		iStrID = IDS_DS_MSG_NO_FILE;
		break;
	case WMSG_FATIGUEALERT:
		iStrID = IDS_DS_NO;
		break;
	case WMSG_OPENFILE_WAIT:
		iStrID = IDS_DS_MSG_OPENFILE_WAIT;
		break;
	case WMSG_NO_CARD1:
		break;
	case WMSG_NO_CARD2:
		break;
	case WMSG_CARD2_FULL:
		break;
	case WMSG_NO_CARD2_2:
		break;
	case WMSG_BACKUP_FINISH:
		break;
	case WMSG_MOTION_OPERATING_DESCRIPTION:
		break;
	case WMSG_MSG_EMERGENCY_FILE_FULL:
		break;
	case WMSG_REMIND_HEADLIGHT:
		break;
	case WMSG_PARKING_OPERATING_DESCRIPTION:
		break;
	case WMSG_SYSTEM_POWER_OFF:
		break;
	case WMSG_SYSTEM_ACC_OFF_POWEROFF:
		break;
	case WMSG_SYSTEM_DELAY_ACC_OFF_POWEROFF:
		break;
	case WMSG_SYSTEM_ACC_OFF_TIMELAPSE_REC:
		break;
	case WMSG_SYSTEM_ACC_ON_NORMAL_REC:
		break;
	case WMSG_LOW_POWER:
		break;
	case WMSG_PARKING_FILE_NOTICE:
		break;
	case WMSG_NORMAL_FILE_FULL:
		break;
	case WMSG_LOCK_FILE_FULL:
		break;
	case WMSG_PHOTO_FILE_FULL:
		break;
	case WMSG_WIFI_STARTING:
		break;
	case WMSG_NETWORK_MODE:
		break;
	default:
		break;
	}
	CusStatusbarWarnText(MAP_STRINGID(iStrID));
	m_WMSGShowTime = 0;
}

/**
 * 定时器触发函数
 * 不建议在此函数中写耗时操作，否则将影响UI刷新
 */
void onUI_Timer_Statusbar(void) {
	if (CusGetStatusbarFlags()) {
		carimpl_handler_status_bar();
	}
}
