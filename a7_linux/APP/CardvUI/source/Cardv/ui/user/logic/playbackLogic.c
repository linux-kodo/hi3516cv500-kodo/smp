/*
* playbackLogic.C
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include "carimpl.h"
#include "KeyEventContext.h"
#include "TimerEventContext.h"
#include "TouchEventContext.h"

#include "mainLogic.h"
#include "playbackLogic.h"
#include "ui_common.h"

static PSMENUSTRUCT pTopMenu = NULL;
static PSMENUSTRUCT pSubMenu = NULL;
static PSMENUITEM   pCurItem = NULL;

/*********************************************
 *                viedo play
 *********************************************/
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdio.h>

//screen save
extern int screen_save_count;
extern bool screen_save_reset;

static void onUI_Timer(int id);
static bool onplaybackActivityTouchEvent(MOTION_EVENT ev);
static void CusPlayBackListRefresh(void);
static void CusSubPlayBackListRefresh(void);

/*
* 文件列表、文件信息、缩略图显示
*/
// file info
static bool bSdStatus = FALSE;
static int u32TotalFile = 0;
static int u32CurFileIdx = 0;
static int u32OldFileIdx = 0xffff;
static char u8DB = 0;
static bool first_refresh_list = true;

static void sd_insert_init(void);

static void sd_statue_check(void)
{
    bool bSdLastStatus = bSdStatus;
    if(CusGetWidgetFlags(ui_PlayBackFileListPanel)){
        IPC_CarInfo_Read_SdInfo(&carimpl.stSdInfo);
        if (!bSdStatus && IMPL_SD_ISINSERT) {
            bSdStatus = TRUE;
        } else if (bSdStatus && !IMPL_SD_ISINSERT) {
            bSdStatus = FALSE;
        }
        if (bSdLastStatus != bSdStatus) {
            if (bSdStatus) {
                Carimpl_DcfMount();
                sd_insert_init();
            }
        }
    }
}

static void playback_font_update(void)
{
    /*
    lv_obj_set_style_text_font(ui_PlayBackFileLabel, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);
    */
}

/**
 * 注册定时器
 * 填充数组用于注册定时器
 * 注意：id不能重复
 */
static S_ACTIVITY_TIMEER REGISTER_ACTIVITY_TIMER_TAB[] = {
    //{0,  6000}, //定时器id=0, 时间间隔6秒
    {0,  30,  0},
    {1,  80,  0},
    {2,  5,   0},
    {3,  20,  0},
    {4,  50,  0},
    {5,  20,  0}, // For Screen
    {6,  300, 0},
};

/**
 * 当界面构造时触发
 */
void onUI_init_playback(){
    //Tips :添加 UI初始化的显示代码到这里,如:mText1Ptr->setText("123");
    Carimpl_DcfUnmount();
    usleep(200000);
    Carimpl_SetCurFolder(DB_NORMAL, 0);
    Carimpl_SetCurFileIdx(0);

    printf(" playback onUI init!\n");
}

/*
* 当界面显示时触发
*/
void onUI_show_playback() {
    //防止菜单界面机器在录像
    IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
    if (IMPL_REC_STATUS || IMPL_MD_REC_STATUS || IMPL_EMERG_REC_STATUS){
        //carimpl_VideoFunc_StartRecording(0);
    }

    //register timer CB FUNC
    uint32_t tablen = sizeof(REGISTER_ACTIVITY_TIMER_TAB) / sizeof(S_ACTIVITY_TIMEER);
    register_timer_callback(onUI_Timer, REGISTER_ACTIVITY_TIMER_TAB, tablen);
    //register touch CB FUNC
    register_touch_callback(onplaybackActivityTouchEvent);

    playback_font_update();
    usleep(200000);

    carimpl.stUIModeInfo.u8Mode = UI_PLAYBACK_MODE;
    IPC_CarInfo_Write_UIModeInfo(&carimpl.stUIModeInfo);

    /* set DB type */
    Carimpl_DcfMount();
    if (Carimpl_GetCurDB() != DB_PHOTO) {
        printf("CurDB:%d Video\r\n", Carimpl_GetCurDB());
        Carimpl_SetCurFolder(DB_NORMAL, 0);
    } else {
        printf("CurDB:%d Photo\r\n", DB_NORMAL);
        Carimpl_SetCurFolder(DB_PHOTO, 0);
    }
    Carimpl_SetCurFileIdx(0);
    /* sd status */
    bSdStatus = FALSE;
    IPC_CarInfo_Read_SdInfo(&carimpl.stSdInfo);
    if (!bSdStatus && IMPL_SD_ISINSERT) {
        bSdStatus = TRUE;
    } else if (bSdStatus && !IMPL_SD_ISINSERT) {
        bSdStatus = FALSE;
    }
    /* show ui subplayback*/
    first_refresh_list = true;
    u8DB = Carimpl_GetCurDB();
    u32TotalFile = Carimpl_GetTotalFiles();
    u32CurFileIdx = Carimpl_GetCurFileIdx();

    _ui_flag_modify(ui_PlayBackFileListPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
    CusSubPlayBackListRefresh();

    printf(" playback onUI show!\n");
}

/*
* 当界面隐藏时触发
*/
void onUI_hide_playback() {
    register_touch_callback(NULL);
    register_timer_callback(NULL, NULL, 1);
    while(callback_run_flag) {
        usleep(100000);
    }
    printf(" playback onUI hide!\n");
}

/**
 * 定时器触发函数
 * 不建议在此函数中写耗时操作，否则将影响UI刷新
 * 参数： id
 *         当前所触发定时器的id，与注册时的id相同
 * 返回值: true
 *             继续运行当前定时器
 *         false
 *             停止运行当前定时器
 */
static void onUI_Timer(int id){
    switch (id) {
        case 0:
            break;
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            break;
        case 5:
            break;
        case 6:
            break;
        default:
            break;
    }
}

/**
 * 有新的触摸事件时触发
 * 参数：ev
 *         新的触摸事件
 * 返回值：true
 *            表示该触摸事件在此被拦截，系统不再将此触摸事件传递到控件上
 *         false
 *            触摸事件将继续传递到控件上
 */
static bool onplaybackActivityTouchEvent(MOTION_EVENT ev) {
    switch (ev.mActionStatus) {
        case E_ACTION_DOWN://触摸按下
            //printf("playback E_ACTION_DOWN %d,%d\r\n",ev.mX,ev.mY);
            break;
        case E_ACTION_MOVE://触摸滑动
            //printf("playback E_ACTION_MOVE %d,%d\r\n",ev.mX,ev.mY);
            break;
        case E_ACTION_UP:  //触摸抬起
            //printf("playback E_ACTION_UP %d,%d\r\n",ev.mX,ev.mY);
            break;
        default:
            break;
    }
    return false;
}

/*
* 视频播放页和文件浏览页
*/
static void CusSubPlayBackListBtnHandler(lv_event_t * e)
{
    /* Get Obj */
    lv_obj_t * obj = lv_event_get_target(e);
    /* Get Obj Index*/
    uint32_t index = lv_obj_get_index(obj);

    lv_obj_t * ListPanelchild = lv_obj_get_child(ui_FileListPanel, 0);
    lv_obj_t * Listchild = lv_obj_get_child(ListPanelchild, u32CurFileIdx);

    char szStr[24] = {0};

    /* select file*/
    lv_obj_set_style_text_color(Listchild, lv_color_hex(0x1F1F1F), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_color(obj, lv_color_hex(0x0B8AF2), LV_PART_MAIN | LV_STATE_DEFAULT);
    /* Update PageInfo*/
    /*
    if(u32TotalFile > 0)
        sprintf(szStr, "%d/%d", index + 1,u32TotalFile);
    else
        sprintf(szStr, "0/0");
    lv_label_set_text(ui_PlayBackFileLabel, szStr);
    */

    Carimpl_SetCurFileIdx(index);
    u8DB = Carimpl_GetCurDB();
    u32CurFileIdx = Carimpl_GetCurFileIdx();
    u32OldFileIdx = 0xffff;
}

static void CusSubPlayBackListRefresh(void)
{
    uint32_t i;
    uint32_t btn_position_x = 0;
    uint32_t btn_position_y = 0;
    static lv_obj_t * list = NULL;

    uint32_t list_num;
    PSMENUITEM pItem = NULL;

    char szStr[24] = {0};

    //Clean List
    lv_obj_t * child = lv_obj_get_child(ui_FileListPanel, 0);
    while(child) {
        lv_obj_del(child);
        child = lv_obj_get_child(ui_FileListPanel, 0);
    }
    //Refresh List
    list_num = u32TotalFile;
    list = lv_list_create(ui_FileListPanel);
    lv_obj_set_size(list, 840, 340);
    lv_obj_center(list);

    char *pFileName[4096] = {NULL};
    for(i = 0; i < list_num; i++) {
        /*get file name*/
        char *tmpFileName = Carimpl_GetCurFileName(i);
        if (tmpFileName != NULL) {
            if(u8DB == DB_PHOTO){
                if(Carimpl_GetCurCamId() == 0){
                    pFileName[i] = (tmpFileName + strlen(SD_ROOT) + strlen("/Photo/front/"));
                } else {
                    pFileName[i] = (tmpFileName + strlen(SD_ROOT) + strlen("/Photo/rear/"));
                }
            } else if (u8DB == DB_NORMAL){
                if(Carimpl_GetCurCamId() == 0){
                    pFileName[i] = (tmpFileName + strlen(SD_ROOT) + strlen("/Normal/front/"));
                } else {
                    pFileName[i] = (tmpFileName + strlen(SD_ROOT) + strlen("/Normal/rear/"));
                }
            } else if (u8DB == DB_EVENT){
                if(Carimpl_GetCurCamId() == 0){
                    pFileName[i] = (tmpFileName + strlen(SD_ROOT) + strlen("/Event/front/"));
                } else {
                    pFileName[i] = (tmpFileName + strlen(SD_ROOT) + strlen("/Event/rear/"));
                }
            }
        } else {
            pFileName[i] = NULL;
        }
        /*show file list*/
        btn_position_y = ((i / 2) * 78 - 20);
        lv_obj_t * VideoListItemBtn = lv_list_add_btn(list, NULL, pFileName[i]);
        lv_obj_set_height(VideoListItemBtn, 80);
        lv_obj_set_y(VideoListItemBtn, btn_position_y);
        lv_obj_set_x(VideoListItemBtn, lv_pct(0));
        lv_obj_set_align(VideoListItemBtn, LV_ALIGN_CENTER);
        lv_obj_add_flag(VideoListItemBtn, LV_OBJ_FLAG_SCROLL_ON_FOCUS);     /// Flags
        lv_obj_clear_flag(VideoListItemBtn, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
        lv_obj_set_style_radius(VideoListItemBtn, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_font(VideoListItemBtn, font_montserrat_30, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_bg_color(VideoListItemBtn, lv_color_hex(0x3F3F3F), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_bg_opa(VideoListItemBtn, 0, LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_t * LockIconLabel = lv_label_create(VideoListItemBtn);
        lv_obj_set_width(LockIconLabel, 30);
        lv_obj_set_height(LockIconLabel, 30);
        lv_obj_set_align(LockIconLabel, LV_ALIGN_CENTER);
        lv_label_set_text(LockIconLabel, "");
        if(Carimpl_IsProtectFile(i)){
            lv_obj_set_style_bg_img_src(LockIconLabel, NULL, LV_PART_MAIN | LV_STATE_DEFAULT);
        } else {
            lv_obj_set_style_bg_img_src(LockIconLabel, NULL, LV_PART_MAIN | LV_STATE_DEFAULT);
        }

        lv_obj_add_event_cb(VideoListItemBtn, CusSubPlayBackListBtnHandler, LV_EVENT_CLICKED, NULL);
    }
    /* Update PageInfo*/
    /*
    if(u32TotalFile > 0)
        sprintf(szStr, "%d/%d", u32CurFileIdx + 1,u32TotalFile);
    else
        sprintf(szStr, "0/0");
    lv_label_set_text(ui_PlayBackFileLabel, szStr);
    */

}

/*
* SD INSERT
*/
static void sd_insert_init(void)
{
    //更新文件列表
    Carimpl_SetCurFileIdx(0);
    first_refresh_list = true;
    u8DB = Carimpl_GetCurDB();
    u32TotalFile = Carimpl_GetTotalFiles();
    u32CurFileIdx = Carimpl_GetCurFileIdx();
    if(u32TotalFile <= 0){
        carimpl_show_wmsg(true, WMSG_NO_FILE_IN_BROWSER);
        printf("no file\n");
    } else {
        //显示缩略图
    }
}

/*
* Video File Operate Button
*/
bool onButtonClick_PlaybackQuitButton(void) {
    printf(" ButtonClick PlaybackQuitButton !!!\n");

    usleep(500000);
    gu8LastUIMode = UI_BROWSER_MODE;
    CusChangeMainScreen();
}

bool onButtonClick_PlaybackSubDelectButton(void) {
    printf(" ButtonClick DeleteSelectionYES !!!\n");
    Carimpl_DeleteFile(u32CurFileIdx);
    u32TotalFile = Carimpl_GetTotalFiles();
    if(u32CurFileIdx > (u32TotalFile - 1) && u32CurFileIdx > 0){
        Carimpl_SetCurFileIdx(u32CurFileIdx - 1);
        u32CurFileIdx = Carimpl_GetCurFileIdx();
    } else if (u32TotalFile == 0){
        
    } else {

    }
    u32OldFileIdx = 0xffff;
    CusSubPlayBackListRefresh();

    return false;
}
