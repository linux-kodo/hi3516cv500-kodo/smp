/*
* mainLogic.c
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "carimpl.h"
#include "KeyEventContext.h"
#include "TimerEventContext.h"
#include <signal.h>

#include "mainLogic.h"
#include "ui_common.h"

#include "module_common.h"
#include "module_storage.h"

#define SCREEN_SAVE_COLOCK_EN (0)

#define FB_WIDTH    896
#define FB_HEIGHT   512

#define LINE_WIDTH  2
#define LINE_COLOR_CLEAR 0x000000
#define LINE_COLOR_WHITE 0xFFFFFF
#define LINE_COLOR_RED   0xFF0000
#define LINE_COLOR_GREEN 0x00FF00
#define LINE_COLOR_BLUE  0x0000FF

#define SYNC_DATA_DUR	10	//Unit:SEC
#define VIDEO_TIMER_UNIT  1     //Uint:SEC
#define DURATION_OFFSET   10   //Unit:US

/* Icon show */
static bool bIconShowInit = FALSE;

/* External variable */
extern bool format_sd_card_flag;

/*
* Function
*/
static void onUI_Timer(int id);
static bool onmainActivityTouchEvent(MOTION_EVENT ev);

/**
 * 注册定时器
 * 填充数组用于注册定时器
 * 注意：id不能重复
 */
static S_ACTIVITY_TIMEER REGISTER_ACTIVITY_TIMER_TAB[] = {
	{0,  50,  0}, //定时器id=0, 时间间隔500ms
	{1,  3,   0}, // For ADAS Drawing
	{2,  20,  0}, // For Video Timer
	{3,  20,  0}, // For Screen
	{4,  20,  0}, // For Sound and Brightness set And Icon Hide
	{5,  100, 0}, // For EDOG
	{6,  300, 0},
};

/* Volume and Brightness SeekBar Auto Hide */
static char Volume = 0;
static char Brightness = 0;
static char old_Volume = 0;
static char old_Brightness = 0;
static bool VolumeSeekBarHide = TRUE;
static int VolumeSeekBarHide_Count = 0;

/* ICON Auto Hide And Show */
static int icon_hide_count = 0;
static bool icon_hide_flag = true;
static void IconShow(void);

/* Reversing Line */
static bool all_icon_hide_Flag = FALSE;
static bool reversing_line_Flag = FALSE;
static bool reversing_line_move[8] = {FALSE};
static lv_point_t Leftpoints1[3] = {0};
static lv_point_t Leftpoints2[3] = {0};
static lv_point_t Leftpoints3[3] = {0};
static lv_point_t Leftpoints4[1] = {0};
static lv_point_t Rightpoints1[3] = {0};
static lv_point_t Rightpoints2[3] = {0};
static lv_point_t Rightpoints3[3] = {0};
static lv_point_t Rightpoints4[1] = {0};

static bool bSdStatus = FALSE;

/*
* Key Callback
*/
static void on_key_callback(int keyCode, int keyStatus) {
	printf("[Video] key [%s] %s\n", KEYCODE_TO_STRING(keyCode), KEYSTATUS_TO_STRING(keyStatus));
	IconShow();
	switch (keyCode) {
	case KEY_ENTER:
		if (keyStatus == UI_KEY_RELEASE);
		break;
	case KEY_RIGHT:
		if (keyStatus == UI_KEY_RELEASE);
		break;
	case KEY_LEFT:
		if (keyStatus == UI_KEY_RELEASE);
		break;
	case KEY_DOWN:
		if (keyStatus == UI_KEY_RELEASE);
		break;
	case KEY_UP:
		if (keyStatus == UI_KEY_RELEASE);
		break;
	case KEY_MENU:
		if (keyStatus == UI_KEY_RELEASE &&
			IMPL_REC_STATUS == FALSE &&
			IMPL_EMERG_REC_STATUS == FALSE &&
			IMPL_MD_REC_STATUS == FALSE);
		break;
	case KEY_POWER:
		if (keyStatus == UI_KEY_RELEASE){
			/*
			if(carimpl.stRecInfo.b_lcd_state == false){
				carimpl_send_cmd_lcd_off();
			}
			*/
		}
		if (keyStatus == UI_KEY_LPRESS){
			/*
			carimpl_Poweroff_handler();
			*/
		}
		break;
	default:
		break;
	}
}

/*
* Icon Refresh Function
*/
static void VideoShowRecStatus(void)
{
	static int cnt = 0;

	if (IMPL_REC_STATUS || IMPL_EMERG_REC_STATUS || IMPL_MD_REC_STATUS) {
		if (cnt++ % 2 == 0)
		{
			CusSetWidgetFlags(ui_RecPointLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			CusSetWidgetFlags(ui_RecTxtLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		} else {
			CusSetWidgetFlags(ui_RecPointLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			CusSetWidgetFlags(ui_RecTxtLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		}
	} else {
		CusSetWidgetFlags(ui_RecPointLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_RecTxtLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
	}
}

static void VideoShowRtc(void)
{
	if (icon_hide_flag == true){
		char dateStr[32] = {0};
		char timeStr[16] = {0};
		time_t now = {0};
		struct tm *t = NULL;
		time(&now);
		t = localtime(&now);
		switch (MenuSettingConfig()->uiDateTimeFormat)
		{
		case DATETIME_SETUP_YMD:
			//sprintf(timeStr, "%04d-%02d-%02d %02d:%02d:%02d", 1900 + t->tm_year, t->tm_mon + 1, t->tm_mday, t->tm_hour,t->tm_min,t->tm_sec);
			sprintf(dateStr, "%04d/%02d/%02d", 1900 + t->tm_year, t->tm_mon + 1, t->tm_mday);
			sprintf(timeStr, "%02d:%02d:%02d", t->tm_hour,t->tm_min,t->tm_sec);
			break;
		case DATETIME_SETUP_MDY:
			//sprintf(timeStr, "%02d-%02d-%04d %02d:%02d:%02d", t->tm_mon + 1, t->tm_mday, 1900 + t->tm_year, t->tm_hour,t->tm_min,t->tm_sec);
			sprintf(dateStr, "%02d/%02d/%04d", t->tm_mon + 1, t->tm_mday, 1900 + t->tm_year);
			sprintf(timeStr, "%02d:%02d:%02d", t->tm_hour,t->tm_min,t->tm_sec);
			break;
		case DATETIME_SETUP_DMY:
			//sprintf(timeStr, "%02d-%02d-%04d %02d:%02d:%02d", t->tm_mday, t->tm_mon + 1, 1900 + t->tm_year,  t->tm_hour,t->tm_min,t->tm_sec);
			sprintf(dateStr, "%02d/%02d/%04d", t->tm_mday, t->tm_mon + 1, 1900 + t->tm_year);
			sprintf(timeStr, "%02d:%02d:%02d", t->tm_hour,t->tm_min,t->tm_sec);
			break;
		default:
			//sprintf(timeStr, "%04d-%02d-%02d %02d:%02d:%02d", 1900 + t->tm_year, t->tm_mon + 1, t->tm_mday, t->tm_hour,t->tm_min,t->tm_sec);
			sprintf(dateStr, "%04d/%02d/%02d", 1900 + t->tm_year, t->tm_mon + 1, t->tm_mday);
			sprintf(timeStr, "%02d:%02d:%02d", t->tm_hour,t->tm_min,t->tm_sec);
			break;
		}

		if (MenuSettingConfig()->uiDateLogoDisplay == DATE_LOGO_DISPLAY_OFF){
			CusSetWidgetFlags(ui_DataTimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			CusSetWidgetFlags(ui_TimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			CusSetWidgetFlags(ui_WeekLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		} else {
			CusSetWidgetFlags(ui_DataTimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			CusSetWidgetFlags(ui_TimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			CusSetWidgetFlags(ui_WeekLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			lv_label_set_text(ui_DataTimeLabel, dateStr);
			lv_label_set_text(ui_TimeLabel, timeStr);
			switch (t->tm_wday){
				case 0: lv_label_set_text(ui_WeekLabel, MAP_STRINGID(IDS_DS_WEEK_SUNDAY)); break;
				case 1: lv_label_set_text(ui_WeekLabel, MAP_STRINGID(IDS_DS_WEEK_MONDAY)); break;
				case 2: lv_label_set_text(ui_WeekLabel, MAP_STRINGID(IDS_DS_WEEK_TUESDAY)); break;
				case 3: lv_label_set_text(ui_WeekLabel, MAP_STRINGID(IDS_DS_WEEK_WEDNESDAY)); break;
				case 4: lv_label_set_text(ui_WeekLabel, MAP_STRINGID(IDS_DS_WEEK_THURSDAY)); break;
				case 5: lv_label_set_text(ui_WeekLabel, MAP_STRINGID(IDS_DS_WEEK_FRIDAY)); break;
				case 6: lv_label_set_text(ui_WeekLabel, MAP_STRINGID(IDS_DS_WEEK_STATURDAY)); break;
				default: break;
			}
		}
	} else {
		CusSetWidgetFlags(ui_DataTimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_TimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_WeekLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
	}
}

static void VideoShowWifiStatus(NET_STATUS_TYPE eNetStatus)
{
	if (icon_hide_flag == true){
		char strSize[32] = {0};
		static NET_STATUS_TYPE eNetStatus_old = NET_STATUS_UNKNOW;
		static char wifi_menu_old = 0xff;

		if(!CusGetWidgetFlags(ui_WifiLabel)) CusSetWidgetFlags(ui_WifiLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);

		if (eNetStatus == NET_STATUS_READY && (eNetStatus_old != NET_STATUS_READY || bIconShowInit)) {
			CusSetWidgetBgImgSrc(ui_WifiLabel, &ui_img_wifi_wifi_signal_5_png, LV_PART_MAIN | LV_STATE_DEFAULT);
		} else if (eNetStatus == NET_STATUS_WEAK && (eNetStatus_old != NET_STATUS_WEAK || bIconShowInit)) {
			CusSetWidgetBgImgSrc(ui_WifiLabel, &ui_img_wifi_wifi_signal_5_png, LV_PART_MAIN | LV_STATE_DEFAULT);
		} else if (eNetStatus == NET_STATUS_UP && (eNetStatus_old != NET_STATUS_UP || bIconShowInit)) {
			CusSetWidgetBgImgSrc(ui_WifiLabel, &ui_img_wifi_wifi_signal_5_png, LV_PART_MAIN | LV_STATE_DEFAULT);
		} else if (eNetStatus_old != eNetStatus || bIconShowInit){
			CusSetWidgetBgImgSrc(ui_WifiLabel, &ui_img_wifi_wifi_signal_0_png, LV_PART_MAIN | LV_STATE_DEFAULT);
		}

		eNetStatus_old = eNetStatus;
		wifi_menu_old = MenuSettingConfig()->uiWifi;
	} else {
		CusSetWidgetFlags(ui_WifiLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
	}
}

static void VideoStateUpdate(void)
{
	VideoShowRtc();
	VideoShowRecStatus();
	VideoShowWifiStatus(check_wifi_status());
	SHOW_SD_ICON(CusSetWidgetBgImgSrc, ui_SDCaedLabel, bSdStatus);
}

static void VideoTimer_Handle(int s)
{
	IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
}

/*
* Volume And Brightness
*/
static bool move_check_start = false;
static bool x_move_check_flag = true;
static bool y_move_check_flag = true;
static bool volume_save_flag = false;
static bool brightness_save_flag = false;

static void VolumeAndBrightnessInit(void){
	Volume = MenuSettingConfig()->uiSystemVolume;
	Brightness = MenuSettingConfig()->uiLCDBacklight;
	/*
	lv_bar_set_value(ui_VolumeSeekBar, Volume*10, LV_ANIM_ON);
	lv_bar_set_value(ui_BrightnessSeekBar, Brightness*10, LV_ANIM_ON);
	*/
	old_Volume = Volume;
	old_Brightness = Brightness;
}

static void VolumeAndBrightnessSetStart(void)
{
	if(MenuSettingConfig()->uiResetSetting == RESET_YES){
		move_check_start = false;
		x_move_check_flag = false;
		y_move_check_flag = false;
		volume_save_flag = false;
		brightness_save_flag = false;
	} else {
		move_check_start = false;
		x_move_check_flag = true;
		y_move_check_flag = true;
		volume_save_flag = false;
		brightness_save_flag = false;
	}
}

static void VolumeAndBrightnessRefresh(MOTION_EVENT ev, MOTION_EVENT s_ev, MOTION_EVENT *s_ev_tmp)
{
	/*
	if((((ev.mX - s_ev.mX) > 50) || ((ev.mX - s_ev.mX) < -50) || move_check_start == true) && x_move_check_flag)	//Brightness
	{
		int progress = lv_bar_get_value(ui_BrightnessSeekBar);
		//printf("main Brightness progress:%d\r\n", progress);
		if(move_check_start == false){
			move_check_start = true;
			y_move_check_flag = false;
			s_ev_tmp->mX = ev.mX;
			s_ev_tmp->mY = ev.mY;
		} else {
			if(ev.mX > s_ev_tmp->mX) progress += (ev.mX - s_ev_tmp->mX)/2;
			else progress += (ev.mX - s_ev_tmp->mX)/2;

			if(progress >= 100) progress = 100;
			lv_bar_set_value(ui_BrightnessSeekBar, progress, LV_ANIM_ON);
			onProgressChanged_BrightnessSeekBar(progress);
			brightness_save_flag = true;
		}
	}

	if((((ev.mY - s_ev.mY) > 50) || ((ev.mY - s_ev.mY) < -50) || move_check_start == true) && y_move_check_flag)	//Volume
	{
		int progress = lv_bar_get_value(ui_VolumeSeekBar);
		//printf("main Volume progress:%d\r\n", progress);
		if(move_check_start == false){
			VolumeSeekBarHide = FALSE;
			CusSetWidgetFlags(ui_VolumeSeekBar, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			CusSetWidgetFlags(ui_TextViewVolume, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			move_check_start = true;
			x_move_check_flag = false;
			s_ev_tmp->mX = ev.mX;
			s_ev_tmp->mY = ev.mY;
		} else {
			if(ev.mY > s_ev_tmp->mY) progress -= (ev.mY - s_ev_tmp->mY)/2;
			else progress -= (ev.mY - s_ev_tmp->mY)/2;

			if(progress >= 100) progress = 100;
			lv_bar_set_value(ui_VolumeSeekBar, progress, LV_ANIM_ON);
			onProgressChanged_VolumeSeekBar(progress);
			if(progress <= 0) {
				CusSetWidgetBgImgSrc(ui_TextViewVolume, &ui_img_home_volume_close_png, LV_PART_MAIN | LV_STATE_DEFAULT);
			} else {
				CusSetWidgetBgImgSrc(ui_TextViewVolume, &ui_img_home_volume_uping_png, LV_PART_MAIN | LV_STATE_DEFAULT);
			}
			volume_save_flag = true;
		}
	}
	s_ev_tmp->mX = ev.mX;
	s_ev_tmp->mY = ev.mY;
	*/
}

static void VolumeAndBrightnessSet(void)
{
	VolumeSeekBarHide_Count++;
	/* Volume SeekBar Hide */
	if(VolumeSeekBarHide_Count == 10){
		VolumeSeekBarHide_Count = 0;
		if(VolumeSeekBarHide && (old_Volume == Volume)){
			/*
			CusSetWidgetFlags(ui_VolumeSeekBar, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			CusSetWidgetFlags(ui_TextViewVolume, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			*/
		}
	}
	/* Volume Set */
	if(old_Volume != Volume){
		old_Volume = Volume;
		/*
		carimpl_GeneralFunc_setSystemVolume(Volume);
		*/
	}
	/* Brightness Set */
	if(old_Brightness != Brightness){
		old_Brightness = Brightness;
		/*
		carimpl_GeneralFunc_SetLCDBacklight(Brightness);
		*/
	}
}

static void VolumeAndBrightnessSave(void)
{
	if(volume_save_flag == true){
		volume_save_flag = false;
		/*
		carimpl_VideoFunc_SetSaveVolume(Volume);
		*/
	} else if (brightness_save_flag == true) {
		brightness_save_flag = false;
		/*
		carimpl_VideoFunc_SetSaveLCDBacklight(Brightness);
		*/
	}
}

/*
* ICON Hide And Show
*/
static void Icon_Auto_Hide_Init(void)
{
	icon_hide_count = 0;
	icon_hide_flag = false;
	IconShow();
}

static void IconHide(void)
{
	icon_hide_count++;
	if(icon_hide_count >= 50 && icon_hide_flag == true){
		icon_hide_flag = false;
		VideoShowRtc();
		VideoShowWifiStatus(check_wifi_status());
		CusSetWidgetFlags(ui_SDCaedLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_HomeButtonPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_BatteryLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_DataTimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_TimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_WeekLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		CusSetWidgetFlags(ui_AudioLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		/*
		CusSetWidgetFlags(ui_BrightnessSeekBar, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
		*/
	}
}

static void IconShow(void)
{
	char strSize[32] = {0};
	NET_STATUS_TYPE eNetStatus;

	icon_hide_count = 0;
	if(icon_hide_flag == false && reversing_line_Flag == false){
		icon_hide_flag = true;
		VideoShowRtc();
		VideoShowWifiStatus(check_wifi_status());
		CusSetWidgetFlags(ui_SDCaedLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		CusSetWidgetFlags(ui_HomeButtonPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		CusSetWidgetFlags(ui_BatteryLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		CusSetWidgetFlags(ui_DataTimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		CusSetWidgetFlags(ui_TimeLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		CusSetWidgetFlags(ui_WeekLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		CusSetWidgetFlags(ui_AudioLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		/*
		CusSetWidgetFlags(ui_BrightnessSeekBar, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
		*/
	}
}

/*
* Screens Saver
*/
int screen_save_count = 0;
bool screen_save_reset = 0;
static bool screen_save_state_cur = false;
static bool screen_save_state_old = false;
#if (SCREEN_SAVE_COLOCK_EN)
static bool screen_save_clock_flag = false;
#endif

/* 屏保时钟界面按power键黑屏 */
bool screen_save_clock_to_blank_screen(int keyCode, int keyStatus)
{
	#if (SCREEN_SAVE_COLOCK_EN)
	if((screen_save_clock_flag && !carimpl_close_backlight_get()) && isScreensaverOn() && !carimpl_timelapse_rec_state_get()){
		if(keyCode == KEY_POWER && keyStatus == UI_KEY_RELEASE){
			screen_save_clock_flag = false;
			carimpl_GeneralFunc_SetCloseBacklight();
		}
		return false;
	}
	#endif
	return true;
}

static void Screen_Saver_Init(void)
{
	screen_save_state_cur = carimpl.stRecInfo.b_lcd_state;
	screen_save_state_old = screen_save_state_cur;
}

void Screen_Saver_Handle(void)
{
	screen_save_count++;
	screen_save_state_cur = carimpl.stRecInfo.b_lcd_state;
	/* Quit Screens Saver */
	if(screen_save_state_cur == false && screen_save_state_old != screen_save_state_cur) {
		screen_save_state_old = screen_save_state_cur;
		/*
		#if (SCREEN_SAVE_COLOCK_EN)
		carimpl_close_backlight_set(false);
		screensaverOff();
		#else
		carimpl_GeneralFunc_SetLCDBacklight(Brightness);
		#endif
		*/
	}
	/* Enter Screens Saver */
	if(screen_save_state_cur == true && screen_save_state_old != screen_save_state_cur){
		screen_save_state_old = screen_save_state_cur;
		/*
		#if (SCREEN_SAVE_COLOCK_EN)
		screen_save_clock_flag = true;
		screensaverOn();
		#else
		carimpl_GeneralFunc_SetCloseBacklight();
		#endif
		*/
	}
	/* Screens Saver timer reset*/
	if(screen_save_reset){
		/*
		if(screen_save_count >= 2){
			carimpl_handler_lcd_power_save(KEY_MENU, UI_KEY_RELEASE);
		}
		if(screen_save_count >= 3){
			screen_save_reset = false;
			carimpl_handler_lcd_power_save_reset(KEY_MENU, UI_KEY_RELEASE);
		}
		*/
	}
}

static void quit_screen_save_and_open_backlight(void)
{
	if(carimpl.stRecInfo.b_lcd_state == true){
		#if (SCREEN_SAVE_COLOCK_EN)
		screen_save_clock_flag = false;
		#endif
		screen_save_reset = true;		//screen save reset
	}
}

/*
* Format SD-Card
*/
bool onButtonClick_FormatConfirmNO(void) {
	//printf(" ButtonClick FormatConfirmNO !!!\n");
	CusSetWidgetFlags(ui_FormatConfirm, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
	return false;
}

bool onButtonClick_FormatConfirmYes(void) {
	//printf(" ButtonClick FormatConfirmYes !!!\n");
	//carimpl_MediaToolFunc_FormatSDCard();
	StorageFormat(FS_TYPE_FAT32);
	CusSetWidgetFlags(ui_FormatConfirm, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
	return false;
}

static void format_sd_card_prompt(void)
{
	if(format_sd_card_flag == true){
		format_sd_card_flag = false;
		lv_label_set_text(ui_FormatTitleView, MAP_STRINGID(IDS_DS_NOTE));
		lv_label_set_text(ui_FormatConfirmText1, MAP_STRINGID(IDS_DS_FORMAT_CARD_CONFIRM));
		lv_label_set_text(ui_FormatConfirmText2, MAP_STRINGID(IDS_DS_DATA_DELETED));
		lv_label_set_text(ui_FormatConfirmYesLabel, MAP_STRINGID(IDS_DS_OK));
		lv_label_set_text(ui_FormatConfirmNOLabel, MAP_STRINGID(IDS_DS_CANCEL));
		CusSetWidgetFlags(ui_FormatConfirm, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
	}
}

/*
* Reversing Line
*/
#define CANVAS_WIDTH  1280
#define CANVAS_HEIGHT  320

static void reversing_line_move_start(MOTION_EVENT ev)
{
	if(ev.mX < Leftpoints1[1].x + 30 && ev.mX > Leftpoints1[1].x - 30 && ev.mY < Leftpoints1[1].y + 15 && ev.mY > Leftpoints1[1].y - 15){
		reversing_line_move[0] = true;
	} else if(ev.mX < Leftpoints2[1].x + 30 && ev.mX > Leftpoints2[1].x - 30 && ev.mY < Leftpoints2[1].y + 15 && ev.mY > Leftpoints2[1].y - 15){
		reversing_line_move[1] = true;
	} else if(ev.mX < Leftpoints3[1].x + 30 && ev.mX > Leftpoints3[1].x - 30 && ev.mY < Leftpoints3[1].y + 15 && ev.mY > Leftpoints3[1].y - 15){
		reversing_line_move[2] = true;
	} else if(ev.mX < Leftpoints4[0].x + 30 && ev.mX > Leftpoints4[0].x - 30 && ev.mY < Leftpoints4[0].y + 15 && ev.mY > Leftpoints4[0].y - 15){
		reversing_line_move[3] = true;
	} else if(ev.mX <  Rightpoints1[1].x + 30 && ev.mX >  Rightpoints1[1].x - 30 && ev.mY <  Rightpoints1[1].y + 15 && ev.mY >  Rightpoints1[1].y - 15){
		reversing_line_move[4] = true;
	} else if(ev.mX <  Rightpoints2[1].x + 30 && ev.mX >  Rightpoints2[1].x - 30 && ev.mY <  Rightpoints2[1].y + 15 && ev.mY >  Rightpoints2[1].y - 15){
		reversing_line_move[5] = true;
	} else if(ev.mX <  Rightpoints3[1].x + 30 && ev.mX >  Rightpoints3[1].x - 30 && ev.mY <  Rightpoints3[1].y + 15 && ev.mY >  Rightpoints3[1].y - 15){
		reversing_line_move[6] = true;
	} else if(ev.mX <  Rightpoints4[0].x + 30 && ev.mX >  Rightpoints4[0].x - 30 && ev.mY <  Rightpoints4[0].y + 15 && ev.mY >  Rightpoints4[0].y - 15){
		reversing_line_move[7] = true;
	} else {
		reversing_line_move[0] = false; reversing_line_move[1] = false;reversing_line_move[2] = false; reversing_line_move[3] = false;
		reversing_line_move[4] = false; reversing_line_move[5] = false;reversing_line_move[6] = false; reversing_line_move[7] = false;
	}
}

static void VideoShowreversingLine(bool show)
{
	#if 0
	if(!show){
		//Clean
		lv_obj_t * child = lv_obj_get_child(ui_ReversingLinePanel, 0);
		while(child) {
			lv_obj_del(child);
			child = lv_obj_get_child(ui_ReversingLinePanel, 0);
		}
		return;
	}

	/*Create style*/
	static lv_style_t style_line_red;
	lv_style_init(&style_line_red);
	lv_style_set_line_width(&style_line_red, 8);
	lv_style_set_line_color(&style_line_red, lv_palette_main(LV_PALETTE_RED));
	lv_style_set_line_rounded(&style_line_red, true);

	static lv_style_t style_line_yellow;
	lv_style_init(&style_line_yellow);
	lv_style_set_line_width(&style_line_yellow, 9);
	lv_style_set_line_color(&style_line_yellow, lv_palette_main(LV_PALETTE_YELLOW));
	lv_style_set_line_rounded(&style_line_yellow, true);

	static lv_style_t style_line_green;
	lv_style_init(&style_line_green);
	lv_style_set_line_width(&style_line_green, 10);
	lv_style_set_line_color(&style_line_green, lv_palette_main(LV_PALETTE_GREEN));
	lv_style_set_line_rounded(&style_line_green, true);

	/* Left */
	Leftpoints4[0].x = MenuSettingConfig()->uiLeftBottomX;
	Leftpoints4[0].y = MenuSettingConfig()->uiLeftBottomY;

	Leftpoints1[1].x = MenuSettingConfig()->uiLeftTopX;
	Leftpoints1[1].y = MenuSettingConfig()->uiLeftTopY;

	Leftpoints2[1].y = MenuSettingConfig()->uiLeftMid1Ratio;
	Leftpoints2[1].x = Leftpoints1[1].x - (Leftpoints1[1].x - Leftpoints4[0].x) * (Leftpoints2[1].y - Leftpoints1[1].y)/(Leftpoints4[0].y - Leftpoints1[1].y);//用Y计算X
	Leftpoints3[1].y = MenuSettingConfig()->uiLeftMid2Ratio;
	Leftpoints3[1].x = Leftpoints1[1].x - (Leftpoints1[1].x - Leftpoints4[0].x) * (Leftpoints3[1].y - Leftpoints1[1].y)/(Leftpoints4[0].y - Leftpoints1[1].y);//用Y计算X

	Leftpoints1[0].x = Leftpoints1[1].x + 40;
	Leftpoints1[0].y = Leftpoints1[1].y;
	Leftpoints2[0].x = Leftpoints2[1].x + 40;
	Leftpoints2[0].y = Leftpoints2[1].y;
	Leftpoints3[0].x = Leftpoints3[1].x + 40;
	Leftpoints3[0].y = Leftpoints3[1].y;

	Leftpoints1[2].x = Leftpoints2[1].x;
	Leftpoints1[2].y = Leftpoints2[1].y;
	Leftpoints2[2].x = Leftpoints3[1].x;
	Leftpoints2[2].y = Leftpoints3[1].y;
	Leftpoints3[2].x = Leftpoints4[0].x;
	Leftpoints3[2].y = Leftpoints4[0].y;

	/*Create a line and apply the new style*/
	lv_obj_t * left_line_red;
	left_line_red = lv_line_create(ui_ReversingLinePanel);
	lv_line_set_points(left_line_red, Leftpoints1, 3);     /*Set the points*/
	lv_obj_add_style(left_line_red, &style_line_red, 0);

	lv_obj_t * left_line_yellow;
	left_line_yellow = lv_line_create(ui_ReversingLinePanel);
	lv_line_set_points(left_line_yellow, Leftpoints2, 3);     /*Set the points*/
	lv_obj_add_style(left_line_yellow, &style_line_yellow, 0);

	lv_obj_t * left_line_green;
	left_line_green = lv_line_create(ui_ReversingLinePanel);
	lv_line_set_points(left_line_green, Leftpoints3, 3);     /*Set the points*/
	lv_obj_add_style(left_line_green, &style_line_green, 0);

	/* Right */
	Rightpoints4[0].x = MenuSettingConfig()->uiRightBottomX;
	Rightpoints4[0].y = MenuSettingConfig()->uiRightBottomY;

	Rightpoints1[1].x = MenuSettingConfig()->uiRightTopX;
	Rightpoints1[1].y = MenuSettingConfig()->uiRightTopY;

	Rightpoints2[1].y = MenuSettingConfig()->uiRightMid1Ratio;
	Rightpoints2[1].x = Rightpoints1[1].x + (Rightpoints4[0].x - Rightpoints1[1].x)*(Rightpoints2[1].y-Rightpoints1[1].y)/(Rightpoints4[0].y-Rightpoints1[1].y);//用Y计算X
	Rightpoints3[1].y = MenuSettingConfig()->uiRightMid2Ratio;
	Rightpoints3[1].x = Rightpoints1[1].x + (Rightpoints4[0].x - Rightpoints1[1].x)*(Rightpoints3[1].y-Rightpoints1[1].y)/(Rightpoints4[0].y-Rightpoints1[1].y);//用Y计算X

	Rightpoints1[0].x = Rightpoints1[1].x - 40;
	Rightpoints1[0].y = Rightpoints1[1].y;
	Rightpoints2[0].x = Rightpoints2[1].x - 40;
	Rightpoints2[0].y = Rightpoints2[1].y;
	Rightpoints3[0].x = Rightpoints3[1].x - 40;
	Rightpoints3[0].y = Rightpoints3[1].y;

	Rightpoints1[2].x = Rightpoints2[1].x;
	Rightpoints1[2].y = Rightpoints2[1].y;
	Rightpoints2[2].x = Rightpoints3[1].x;
	Rightpoints2[2].y = Rightpoints3[1].y;
	Rightpoints3[2].x = Rightpoints4[0].x;
	Rightpoints3[2].y = Rightpoints4[0].y;

	/*Create a line and apply the new style*/
	lv_obj_t * righr_line_red;
	righr_line_red = lv_line_create(ui_ReversingLinePanel);
	lv_line_set_points(righr_line_red, Rightpoints1, 3);     /*Set the points*/
	lv_obj_add_style(righr_line_red, &style_line_red, 0);

	lv_obj_t * righr_line_yellow;
	righr_line_yellow = lv_line_create(ui_ReversingLinePanel);
	lv_line_set_points(righr_line_yellow, Rightpoints2, 3);     /*Set the points*/
	lv_obj_add_style(righr_line_yellow, &style_line_yellow, 0);

	lv_obj_t * righr_line_green;
	righr_line_green = lv_line_create(ui_ReversingLinePanel);
	lv_line_set_points(righr_line_green, Rightpoints3, 3);     /*Set the points*/
	lv_obj_add_style(righr_line_green, &style_line_green, 0);
	#endif
}

static void reversing_line_move_refresh(MOTION_EVENT ev, MOTION_EVENT s_ev)
{
	int Ratio = 0;

	VideoShowreversingLine(false);
	if(reversing_line_move[0] == true){					//Left
		if((ev.mX > 150)){
			MenuSettingConfig()->uiLeftTopX = ev.mX;
		}
		if((ev.mY < (MenuSettingConfig()->uiLeftMid1Ratio - 15)) && (ev.mY > 20)){
			MenuSettingConfig()->uiLeftTopY = ev.mY;
		}
	} else if (reversing_line_move[1] == true) {
		Ratio = MenuSettingConfig()->uiLeftMid1Ratio;
		Ratio += (ev.mY - s_ev.mY);
		if((Ratio < (MenuSettingConfig()->uiLeftMid2Ratio - 15) && Ratio > (MenuSettingConfig()->uiLeftTopY + 15))){
			MenuSettingConfig()->uiLeftMid1Ratio = Ratio;
		}
	} else if (reversing_line_move[2] == true) {
		Ratio = MenuSettingConfig()->uiLeftMid2Ratio;
		Ratio += (ev.mY - s_ev.mY);
		if((Ratio < (MenuSettingConfig()->uiLeftBottomY - 15) && Ratio > (MenuSettingConfig()->uiLeftMid1Ratio + 15))){
			MenuSettingConfig()->uiLeftMid2Ratio = Ratio;
		}
	} else if (reversing_line_move[3] == true) {
		if((ev.mX > 150)){
			MenuSettingConfig()->uiLeftBottomX = ev.mX;
		}
		if((ev.mY > (MenuSettingConfig()->uiLeftMid2Ratio + 15)) && (ev.mY < 315)){
			MenuSettingConfig()->uiLeftBottomY = ev.mY;
		}
	} else if (reversing_line_move[4] == true){			//Righr
		if((ev.mX < 1130)){
			MenuSettingConfig()->uiRightTopX = ev.mX;
		}
		if((ev.mY < (MenuSettingConfig()->uiRightMid1Ratio - 15)) && (ev.mY > 20)){
			MenuSettingConfig()->uiRightTopY = ev.mY;
		}
	} else if (reversing_line_move[5] == true) {
		Ratio = MenuSettingConfig()->uiRightMid1Ratio;
		Ratio += (ev.mY - s_ev.mY);
		if((Ratio < (MenuSettingConfig()->uiRightMid2Ratio - 15) && Ratio > (MenuSettingConfig()->uiRightTopY + 15))){
			MenuSettingConfig()->uiRightMid1Ratio = Ratio;
		}
	} else if (reversing_line_move[6] == true) {
		Ratio = MenuSettingConfig()->uiRightMid2Ratio;
		Ratio += (ev.mY - s_ev.mY);
		if((Ratio < (MenuSettingConfig()->uiRightBottomY - 15) && Ratio > (MenuSettingConfig()->uiRightMid1Ratio + 15))){
			MenuSettingConfig()->uiRightMid2Ratio = Ratio;
		}
	} else if (reversing_line_move[7] == true) {
		if((ev.mX < 1130)){
			MenuSettingConfig()->uiRightBottomX = ev.mX;
		}
		if((ev.mY > (MenuSettingConfig()->uiRightMid2Ratio + 15)) && (ev.mY < 315)){
			MenuSettingConfig()->uiRightBottomY = ev.mY;
		}
	}
	VideoShowreversingLine(true);
}

static void reversing_line_move_set(void)
{
	if(reversing_line_Flag){
		//Left
		if(reversing_line_move[0] == true){
			reversing_line_move[0] = false;
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_LEFT_TOP_X);
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_LEFT_TOP_Y);
		} else if (reversing_line_move[1] == true) {
			reversing_line_move[1] = false;
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_LEFT_MIDDLE1_RATIO);
		} else if (reversing_line_move[2] == true) {
			reversing_line_move[2] = false;
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_LEFT_MIDDLE2_RATIO);
		} else if (reversing_line_move[3] == true) {
			reversing_line_move[3] = false;
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_LEFT_BOTTOM_X);
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_LEFT_BOTTOM_Y);
		}
		//Righe
		if(reversing_line_move[4] == true){
			reversing_line_move[4] = false;
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_RIGHT_TOP_X);
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_RIGHT_TOP_Y);
		} else if (reversing_line_move[5] == true) {
			reversing_line_move[5] = false;
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_RIGHT_MIDDLE1_RATIO);
		} else if (reversing_line_move[6] == true) {
			reversing_line_move[6] = false;
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_RIGHT_MIDDLE2_RATIO);
		} else if (reversing_line_move[7] == true) {
			reversing_line_move[7] = false;
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_RIGHT_BOTTOM_X);
			carimpl_GeneralFunc_SetReversingLinePoint(REVERSING_RIGHT_BOTTOM_Y);
		}
	}
}

void display_back_init(void)
{
	/*
	if(!carimpl_back_car_state_get()) {
		all_icon_hide_Flag = false;
		reversing_line_Flag = false;
	}
	*/
}

void display_back_to_hide_icon(char state)
{
	char strSize[32] = {0};
	NET_STATUS_TYPE eNetStatus;

	if(all_icon_hide_Flag){
		if(state == true){
			all_icon_hide_Flag = false;
			/*
			CusSetWidgetFlags(ui_SDCaedLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			CusSetWidgetFlags(ui_HomeButtonPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			CusSetWidgetFlags(ui_BrightnessSeekBar, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			CusSetWidgetFlags(ui_WifiLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
			*/
		} else {
			all_icon_hide_Flag = false;
			/*
			CusSetWidgetFlags(ui_SDCaedLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			CusSetWidgetFlags(ui_HomeButtonPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			CusSetWidgetFlags(ui_BrightnessSeekBar, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			CusSetWidgetFlags(ui_TextParking, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			CusSetWidgetFlags(ui_WifiLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
			eNetStatus = check_wifi_status();
			if(MenuSettingConfig()->uiWifi != WIFI_MODE_OFF &&
			(eNetStatus == NET_STATUS_READY || eNetStatus == NET_STATUS_WEAK || eNetStatus == NET_STATUS_UP)){
				CusSetWidgetFlags(ui_WifiSSID, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
				CusSetWidgetFlags(ui_Password, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
				carimpl_wifi_ssid_handler();
				sprintf(strSize, "%s",carimpl_wifi_ssid_get());
				lv_label_set_text(ui_WifiSSID, strSize);
				carimpl_wifi_password_handler();
				sprintf(strSize, "%s",carimpl_wifi_password_get());
				lv_label_set_text(ui_Password, strSize);
			}
			*/
			IconShow();
		}
		/*
		if(MenuSettingConfig()->uiReversingLine == REVERSING_LINE_ON) {
			if(state){
				lv_obj_set_x(ui_ReversingLinePanel, -20);
				lv_obj_set_y(ui_ReversingLinePanel, -10);
				CusSetWidgetFlags(ui_ReversingLinePanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
				VideoShowreversingLine(true);
			} else {
				CusSetWidgetFlags(ui_ReversingLinePanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
				VideoShowreversingLine(false);
			}
		}
		*/
	}
}

void display_back(bool state)
{
	printf("reversing line!!\r\n");
	all_icon_hide_Flag = true;
	reversing_line_Flag = state;
	//quit screen save
	screen_save_reset = true;
	#if (SCREEN_SAVE_COLOCK_EN)
	screensaverOff();
	#endif
	carimpl_GeneralFunc_SetLCDBacklight(Brightness);
}

/*
* Take Picture Icon
*/
static int CameraIconHide_Count = 0;

static void VideoShowCameraStatus(bool state)
{
	CusSetWidgetFlags(ui_CapLabel, LV_OBJ_FLAG_HIDDEN, state);
}

static void VideoShowCameraIconHide(void)
{
	CameraIconHide_Count++;
	/* Camera Icon Hide */
	if(CameraIconHide_Count == 5){
		if(CusGetWidgetFlags(ui_CapLabel)){
			VideoShowCameraStatus(false);
		}
	}
}

/* Font Set*/
static void main_font_update(void)
{
	lv_obj_set_style_text_font(ui_TimeLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui_WeekLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui_DataTimeLabel, font_montserrat_24, LV_PART_MAIN | LV_STATE_DEFAULT);
}

/**
 * 当界面构造时触发
 */
void onUI_init_main(void) {
	//Tips :添加 UI初始化的显示代码到这里,如:mText1Ptr->setText("123");
	//设置音量和背光
	/*
	carimpl_GeneralFunc_SetLCDBacklight(MenuSettingConfig()->uiLCDBacklight);
	carimpl_GeneralFunc_setSystemVolume(MenuSettingConfig()->uiSystemVolume);
	VolumeAndBrightnessInit();
	usleep(100000);
	*/

	//关闭倒车线
	/*
	display_back_init();
	*/

	printf(" main onUI_init!\n");
}

/*
* 当界面显示时触发
*/
void onUI_show_main() {
	printf(" main onUI_show!\n");
	carimpl.stUIModeInfo.u8Mode = UI_VIDEO_MODE;
	IPC_CarInfo_Write_UIModeInfo(&carimpl.stUIModeInfo);
	usleep(100000);

	CusSetStatusbarFlags(false);

	bIconShowInit = true;
	IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
	VideoShowRtc();
	VideoShowRecStatus();
	VideoShowCameraStatus(false);
	VideoShowWifiStatus(check_wifi_status());
	bIconShowInit = false;

	Icon_Auto_Hide_Init();
	Screen_Saver_Init();

	SHOW_SD_ICON(CusSetWidgetBgImgSrc, ui_SDCaedLabel, bSdStatus);

	//register key CB FUNC
	set_key_event_callback(on_key_callback);
	//register timer CB FUNC
	uint32_t tablen = sizeof(REGISTER_ACTIVITY_TIMER_TAB) / sizeof(S_ACTIVITY_TIMEER);
	register_timer_callback(onUI_Timer, REGISTER_ACTIVITY_TIMER_TAB, tablen);
	//register touch CB FUNC
	register_touch_callback(onmainActivityTouchEvent);

	main_font_update();
	/* 开机向导判断 */
	if(MenuSettingConfig()->uiResetSetting == RESET_YES){
		MenuSettingConfig()->uiResetSetting = RESET_NO;
	} else {

	}
}

/*
* 当界面完全退出时触发
*/
void onUI_hide_main() {
	register_touch_callback(NULL);
	register_timer_callback(NULL, NULL, 1);
	while(callback_run_flag) {
		usleep(100000);
	}
	printf(" main onUI hide!\n");
}

/**
 * 定时器触发函数
 * 不建议在此函数中写耗时操作，否则将影响UI刷新
 * 参数： id
 *         当前所触发定时器的id，与注册时的id相同
 * 返回值: true
 *             继续运行当前定时器
 *         false
 *             停止运行当前定时器
 */
static void onUI_Timer(int id) {

	if(MenuSettingConfig()->uiResetSetting == RESET_YES){
		if(id == 3) {
			IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
			Screen_Saver_Handle();
		}
		return;
	}
	switch (id) {
		case 0:
			/*
			carimpl_handler_auto_rec(false);
			*/
			VideoStateUpdate();
			break;
		case 1:
			break;
		case 2:
			VideoTimer_Handle(1);
			format_sd_card_prompt();
			break;
		case 3:
			Screen_Saver_Handle();
			break;
		case 4:
			IconHide();
			VolumeAndBrightnessSet();
			VideoShowCameraIconHide();
			display_back_to_hide_icon(reversing_line_Flag);
			break;
		case 5:
			break;
		case 6:
			break;
		default:
			break;
	}
}

/**
 * 有新的触摸事件时触发
 * 参数：ev
 *         新的触摸事件
 * 返回值：true
 *            表示该触摸事件在此被拦截，系统不再将此触摸事件传递到控件上
 *         false
 *            触摸事件将继续传递到控件上
 */
static bool onmainActivityTouchEvent(MOTION_EVENT ev) {
	static MOTION_EVENT s_ev;
	static MOTION_EVENT s_ev_tmp;
	bool ret = false;

	IconShow();
	switch (ev.mActionStatus) {
		case E_ACTION_DOWN://触摸按下
			//printf("main E_ACTION_DOWN %d,%d\r\n",ev.mX,ev.mY);
			/* key tone */
			/*
			carimpl_key_release_handler();
			*/
			/* Volume And Brightness Start */
			if(!reversing_line_Flag) VolumeAndBrightnessSetStart();
			/* ReversingLine Start */
			s_ev = ev;
			if(MenuSettingConfig()->uiReversingLine == REVERSING_LINE_ON && reversing_line_Flag){
				reversing_line_move_start(ev);
				s_ev_tmp = ev;
			}
			break;
		case E_ACTION_MOVE://触摸滑动
			//printf("1、main E_ACTION_MOVE (%d,%d)\r\n",ev.mX,ev.mY);
			/* reversing line refresh */
			if(MenuSettingConfig()->uiReversingLine == REVERSING_LINE_ON && reversing_line_Flag){
				reversing_line_move_refresh(ev, s_ev_tmp);
				s_ev_tmp = ev;
				return true;
			}
			/* Volume And Brightness Refresh */
			VolumeAndBrightnessRefresh(ev, s_ev, &s_ev_tmp);
			break;
		case E_ACTION_UP:  //触摸抬起
			//printf("main E_ACTION_UP %d,%d\r\n",ev.mX,ev.mY);
			/* Quit Screen Save */
			quit_screen_save_and_open_backlight();
			/* Reversing Line Save */
			reversing_line_move_set();
			/* Volume And Brightness Save */
			VolumeAndBrightnessSave();
			/* Volume SeekBar Hide */
			VolumeSeekBarHide = TRUE;
			reset(&s_ev);
			break;
		default:
			break;
	}
	return ret;
}

/*
* Touch Button
*/
bool onButtonClick_CameraButton(void) {
	printf(" ButtonClick CameraButton !!!\n");

	if (IMPL_SD_ISMOUNT && (bSdStatus == TRUE)) {
		if (IMPL_REC_STATUS || IMPL_EMERG_REC_STATUS || IMPL_MD_REC_STATUS) {
			CameraIconHide_Count = 0;
			VideoShowCameraStatus(true);
			cardv_send_cmd(CMD_VENC_SNAP, NULL, 0);
		} else {
			carimpl_show_wmsg(true, WMSG_START_RECORD);
		}
	} else if (bSdStatus == FALSE){
		carimpl_show_wmsg(true, WMSG_INSERT_SD_AGAIN);
	} else if ((!IMPL_SD_ISMOUNT && (bSdStatus == TRUE))){
		carimpl_show_wmsg(true, WMSG_FORMAT_SD_CARD);
	} else {
		carimpl_show_wmsg(true, WMSG_NO_CARD);
	}
	return true;
}

bool onButtonClick_VideoButton(void) {
	printf(" ButtonClick VideoButton !!!\n");

	IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
	if (IMPL_REC_STATUS || IMPL_EMERG_REC_STATUS || IMPL_MD_REC_STATUS) {
		cardv_send_cmd(CMD_VENC_STOP, NULL, 0);
	} else {
		if (IMPL_SD_ISMOUNT && (bSdStatus == TRUE)) {
			IMPL_REC_DURATION = 0;
			IMPL_EMERG_REC_DURATION = 0;
		} else if (bSdStatus == FALSE){
			carimpl_show_wmsg(true, WMSG_INSERT_SD_AGAIN);
		} else if ((!IMPL_SD_ISMOUNT && (bSdStatus == TRUE))){
			carimpl_show_wmsg(true, WMSG_FORMAT_SD_CARD);
		} else {
			carimpl_show_wmsg(true, WMSG_NO_CARD);
		}
		cardv_send_cmd(CMD_VENC_START, NULL, 0);
	}

	return true;
}

bool onButtonClick_AudioButton(void) {
	printf(" ButtonClick AudioButton !!!\n");
	return true;
}

bool onButtonClick_PlaybackButton(void) {
	int i = 0;
	int duration = 0;

	printf(" ButtonClick PlaybackButton !!!\n");
	/*
	IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
	while (!(IMPL_REC_STATUS == FALSE && IMPL_EMERG_REC_STATUS == FALSE && IMPL_MD_REC_STATUS == FALSE) && i < 10){
		i++;
		IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
		if (IMPL_REC_STATUS || IMPL_EMERG_REC_STATUS || IMPL_MD_REC_STATUS) carimpl_VideoFunc_StartRecording(0);
		usleep(200000);
	}
	*/

	if (IMPL_SD_ISMOUNT && (bSdStatus == TRUE)) {
		gu8LastUIMode = UI_VIDEO_MODE;
		onUI_hide_main();	//主动调用一次(界面切换时LVGL会自动调用该函数，但有概率不调用，导致UI死机)
		_ui_screen_change(&ui_PlaybackScreen, LV_SCR_LOAD_ANIM_NONE, 0, 0, &ui_PlaybackScreen_screen_init);
	} else if (bSdStatus == FALSE){
		carimpl_show_wmsg(true, WMSG_INSERT_SD_AGAIN);
	} else if ((!IMPL_SD_ISMOUNT && (bSdStatus == TRUE))){
		carimpl_show_wmsg(true, WMSG_FORMAT_SD_CARD);
	} else {
		carimpl_show_wmsg(true, WMSG_NO_CARD);
	}
	return true;
}

bool onButtonClick_SettingsButton(void) {
	int i = 0;
	int duration = 0;

	printf(" ButtonClick SettingsButton !!!\n");
	/*
	IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
	while (!(IMPL_REC_STATUS == FALSE && IMPL_EMERG_REC_STATUS == FALSE && IMPL_MD_REC_STATUS == FALSE) && i < 10){
		i++;
		IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
		if (IMPL_REC_STATUS || IMPL_EMERG_REC_STATUS || IMPL_MD_REC_STATUS) carimpl_VideoFunc_StartRecording(0);
		usleep(200000);
	}
	*/

	gu8LastUIMode = UI_VIDEO_MODE;
	onUI_hide_main();	//主动调用一次(界面切换时LVGL会自动调用该函数，但有概率不调用，导致UI死机)
	_ui_screen_change(&ui_MenuScreen, LV_SCR_LOAD_ANIM_NONE, 0, 0, &ui_MenuScreen_screen_init);
	return true;
}

void onProgressChanged_BrightnessSeekBar(int progress) {
	//printf(" ProgressChanged BrightnessSeekBar, progress:%d !!!\n", progress);
	if(progress >= 0 && progress <= 5){
		Brightness = 0;
	} else if (progress > 5 && progress <= 15){
		Brightness = 1;
	} else if (progress > 15 && progress <= 25){
		Brightness = 2;
	} else if (progress > 25 && progress <= 35){
		Brightness = 3;
	} else if (progress > 35 && progress <= 45){
		Brightness = 4;
	} else if (progress > 45 && progress <= 55){
		Brightness = 5;
	} else if (progress > 55 && progress <= 65){
		Brightness = 6;
	} else if (progress > 65 && progress <= 75){
		Brightness = 7;
	} else if (progress > 75 && progress <= 85){
		Brightness = 8;
	} else if (progress > 85 && progress <= 95){
		Brightness = 9;
	} else if (progress > 95 && progress <= 100){
		Brightness = 10;
	}
}

void onProgressChanged_VolumeSeekBar(int progress) {
	//printf(" ProgressChanged VolumeSeekBar, progress:%d !!!\n", progress);
	if(progress >= 0 && progress <= 5){
		Volume = 0;
	} else if (progress > 5 && progress <= 15){
		Volume = 1;
	} else if (progress > 15 && progress <= 25){
		Volume = 2;
	} else if (progress > 25 && progress <= 35){
		Volume = 3;
	} else if (progress > 35 && progress <= 45){
		Volume = 4;
	} else if (progress > 45 && progress <= 55){
		Volume = 5;
	} else if (progress > 55 && progress <= 65){
		Volume = 6;
	} else if (progress > 65 && progress <= 75){
		Volume = 7;
	} else if (progress > 75 && progress <= 85){
		Volume = 8;
	} else if (progress > 85 && progress <= 95){
		Volume = 9;
	} else if (progress > 95 && progress <= 100){
		Volume = 10;
	}
}
