/*
* VideoMenuLogic.h
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#ifndef _VIDEO_MENU_LOGIC_H_
#define _VIDEO_MENU_LOGIC_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void onUI_init_menu(void);
void onUI_show_menu(void);
void onUI_hide_menu(void);

bool onButtonClick_GeneralSettingsButton(void);
bool onButtonClick_MovieSettingsButton(void);
bool onButtonClick_SystemSettingsButton(void);

bool onButtonClick_SubMenuConfirm_yes(void);
bool onButtonClick_SubMenuConfirm_no(void);

bool onButtonClick_CancelButton(void);
bool onButtonClick_OKButton(void);

#endif /* _VIDEO_MENU_LOGIC_H_ */
