/*
* VideoMenuLogic.c
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#include <sys/time.h>
#include "carimpl.h"
#include "KeyEventContext.h"
#include "TimerEventContext.h"
#include "TouchEventContext.h"

#include "ui_common.h"
#include "VideoMenuLogic.h"

#define CLOCK_SETTING               (0)

static int cur_rtc_max_day = RTC_MAX_DAY_31;
static int Datetime[6] = {2019,01,01,12,0,0};//year,month,day,hour,minute
static unsigned char list_refresh_first[6] = {0,0,0,0,0,0};

static bool first_refresh_sub_list = false;
static unsigned char gcurrent_page_index = MENUID_MAIN_MENULIST;

static bool reset_setup_flag = false;

static PSMENUSTRUCT pTopMenu = NULL;
static PSMENUSTRUCT pSubMenu = NULL;
static PSMENUSTRUCT pSubMenuSub = NULL;
static PSMENUITEM   pCurItem = NULL;

static void data_getings(void);
static void CusClockSetListRefresh(void);

static void format_sd_card_prompt(void);
static bool sub_menu_page_Back(MOTION_EVENT s_ev, MOTION_EVENT ev);

static void CusMenuListRefresh(void);
static void CusSubMenuListRefresh();

static void onUI_Timer(int id);
static bool onVideoMenuActivityTouchEvent(MOTION_EVENT ev);

//screen save
extern int screen_save_count;
extern bool screen_save_reset;
extern void Screen_Saver_Handle(void);

extern bool format_sd_card_flag;

/**
 * 注册定时器
 * 填充数组用于注册定时器
 * 注意：id不能重复
 */
static S_ACTIVITY_TIMEER REGISTER_ACTIVITY_TIMER_TAB[] = {
    {0,  20,  0}, //定时器id=0, 时间间隔200毫秒
    {1,  50,  0},
    {2,  20,  0}, // For Screen
    {3,  300, 0},
};

static void menu_font_update(void)
{
    /*
    lv_obj_set_style_text_font(ui_MainListBtnLabel, font_montserrat_24, LV_PART_MAIN | LV_STATE_DEFAULT);
    */
}

/**
 * 当界面构造时触发
 */
void onUI_init_menu() {
    //Tips :添加 UI初始化的显示代码到这里;
    pTopMenu = &sMainMenuGeneral;
    printf("set pTopMenu [%s] Line [%d]\n", MAP_STRINGID(pTopMenu->iStringId), __LINE__);
}

/*
* 当界面显示时触发
*/
void onUI_show_menu() {
    carimpl.stUIModeInfo.u8Mode = UI_MENU_MODE;
    IPC_CarInfo_Write_UIModeInfo(&carimpl.stUIModeInfo);

    //防止菜单界面机器在录像
    IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
    if (IMPL_REC_STATUS || IMPL_MD_REC_STATUS || IMPL_EMERG_REC_STATUS){
        /*
        carimpl_VideoFunc_StartRecording(0);
        */
    }

    if (CusGetStatusbarFlags()) CusSetStatusbarFlags(false);

    //register timer CB FUNC
    printf(" register timer CB FUNC !!!\n");
    uint32_t tablen = sizeof(REGISTER_ACTIVITY_TIMER_TAB) / sizeof(S_ACTIVITY_TIMEER);
    register_timer_callback(onUI_Timer, REGISTER_ACTIVITY_TIMER_TAB, tablen);
    //register touch CB FUNC
    printf(" register touch CB FUNC !!!\n");
    register_touch_callback(onVideoMenuActivityTouchEvent);

    menu_font_update();
    /*
    lv_label_set_text(ui_GeneralSettingsButtonTxt, MAP_STRINGID(IDS_DS_GENERAL_SETTINGS));
    lv_label_set_text(ui_MovieSettingsButtonTxt, MAP_STRINGID(IDS_DS_VIDEO_SETTINGS));
    lv_label_set_text(ui_SystemSettingsButtonTxt, MAP_STRINGID(IDS_DS_SYSTEM_SETTINGS));
    gcurrent_page_index = MENUID_MAIN_MENULIST;
    lv_obj_set_style_text_color(ui_GeneralSettingsButtonTxt, lv_color_hex(0x0B8AF2), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_color(ui_MovieSettingsButtonTxt, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_color(ui_SystemSettingsButtonTxt, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    */
    usleep(200000);
    CusMenuListRefresh();
    printf(" VideoMenu onUI show!\n");
}

/*
* 当界面隐藏时触发
*/
void onUI_hide_menu() {
    register_touch_callback(NULL);
    register_timer_callback(NULL, NULL, 1);
    while(callback_run_flag) {
        usleep(100000);
    }
    printf(" VideoMenu onUI hide!\n");
}

/**
 * 定时器触发函数
 * 不建议在此函数中写耗时操作，否则将影响UI刷新
 * 参数： id
 *         当前所触发定时器的id，与注册时的id相同
 * 返回值: true
 *             继续运行当前定时器
 *         false
 *             停止运行当前定时器
 */
static void onUI_Timer(int id) {
    switch (id) {
    case 0:
        break;
    case 1:
        //App 开启录制功能自动返回预览界面
        IPC_CarInfo_Read_RecInfo(&carimpl.stRecInfo);
        /*
        if(CARIMPL_APP_STATE){
            if(carimpl_pwr_supply_state_get()){
                pTopMenu = &sMainMenuGeneralACC;
            } else {
                pTopMenu = &sMainMenuGeneral;
            }
            gcurrent_page_index = MENUID_MAIN_MENULIST;
            CusChangeMainScreen();
        }
        */
        //倒车自动返回预览界面
        /*
        if(carimpl_back_car_state_get()){
            if(carimpl_pwr_supply_state_get()){
                pTopMenu = &sMainMenuGeneralACC;
            } else {
                pTopMenu = &sMainMenuGeneral;
            }
            gcurrent_page_index = MENUID_MAIN_MENULIST;
            CusChangeMainScreen();
        }
        */
        //sd-card format
        format_sd_card_prompt();
        break;
    case 2:
        Screen_Saver_Handle();
        break;
    case 3:
        break;
    default:
        break;
    }
}

/**
 * 有新的触摸事件时触发
 * 参数：ev
 *         新的触摸事件
 * 返回值：true
 *            表示该触摸事件在此被拦截，系统不再将此触摸事件传递到控件上
 *         false
 *            触摸事件将继续传递到控件上
 */
static bool onVideoMenuActivityTouchEvent(MOTION_EVENT ev) {
    static MOTION_EVENT s_ev;
    bool ret = false;
    switch (ev.mActionStatus) {
        case E_ACTION_DOWN://触摸按下
            //printf("1、ACTION_DOWN -- Position: x = %d, y = %d \r\n", ev.mX, ev.mY);
            /*
            carimpl_key_release_handler();
            */
            screen_save_count = 0;
            screen_save_reset = true;
            s_ev = ev;
            break;
        case E_ACTION_MOVE://触摸滑动
            break;
        case E_ACTION_UP:  //触摸抬起
            //printf("2、ACTION_UP -- Position: x = %d, y = %d \r\n", ev.mX, ev.mY);
            if(carimpl.stRecInfo.b_lcd_state == true){
                screen_save_reset = true;		//screen save reset
                return true;
            }
            if(reset_setup_flag == false){
                ret = sub_menu_page_Back(s_ev, ev);
            }
            reset(&s_ev);
            break;
        default:
            break;
    }
    return ret;
}

/* Page Back */
static bool sub_menu_page_Back(MOTION_EVENT s_ev, MOTION_EVENT ev)
{
    if(gcurrent_page_index == MENUID_SUB_MENULIST){
        if (CusGetWidgetFlags(ui_SubListPanel)) {
            if ((s_ev.mX < 175 || s_ev.mY < 5) || (s_ev.mX > 680 || s_ev.mY > 475)){
                _ui_flag_modify(ui_SubListPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                gcurrent_page_index = MENUID_MAIN_MENULIST;
                return true;
            }
        } else if(CusGetWidgetFlags(ui_SubMenuConfirmPanel)){
            if ((s_ev.mX < 175 || s_ev.mY < 120) || (s_ev.mX > 680 || s_ev.mY > 360)){
                gcurrent_page_index = MENUID_MAIN_MENULIST;
                _ui_flag_modify(ui_SubMenuConfirmPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                return true;
            }
        } /*else if (CusGetWidgetFlags(ui_ClockSettingPanel)) {
            if ((s_ev.mX < 140 || s_ev.mY < 5) || (s_ev.mX > 1140 || s_ev.mY > 315)){
                gcurrent_page_index = MENUID_MAIN_MENULIST;
                _ui_flag_modify(ui_ClockSettingPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                _ui_flag_modify(ui_MenuPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
            }
        } else if (CusGetWidgetFlags(ui_QRCodePanel)) {
            gcurrent_page_index = MENUID_MAIN_MENULIST;
            _ui_flag_modify(ui_QRCodePanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
            _ui_flag_modify(ui_MenuPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        }
        */
    } else if(gcurrent_page_index == MENUID_MAIN_MENULIST){
        if ((s_ev.mX < 5 || s_ev.mY < 5) || (s_ev.mX > 850 || s_ev.mY > 475)){
            gu8LastUIMode = UI_MENU_MODE;
            CusChangeMainScreen();
            return true;
        } else if (((ev.mX - s_ev.mX) > 200) || ((s_ev.mX - ev.mX) > 200)) {
            gu8LastUIMode = UI_MENU_MODE;
            CusChangeMainScreen();
            return true;
        }
    }
}

/* Format SD Card */
static void format_sd_card_prompt(void)
{
    if(format_sd_card_flag == true){
        format_sd_card_flag = false;
        _ui_flag_modify(ui_SubListPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
        /*
        _ui_flag_modify(ui_QRCodePanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
        _ui_flag_modify(ui_ClockSettingPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
        */

        pCurItem = sMainMenuSystem.pItemsList[3];
        gcurrent_page_index = MENUID_SUB_MENULIST;
        lv_label_set_text(ui_TitleTextLabel, MAP_STRINGID(IDS_DS_NOTE));
        lv_label_set_text(ui_SubMenuConfirmTextLabel, MAP_STRINGID(IDS_DS_FORMAT_CARD_CONFIRM));
        lv_label_set_text(ui_SubMenuConfirmTextLabel2, MAP_STRINGID(IDS_DS_DATA_DELETED));
        lv_label_set_text(ui_MenuConfirmYesLabel, MAP_STRINGID(IDS_DS_OK));
        lv_label_set_text(ui_MenuConfirmNoLabel, MAP_STRINGID(IDS_DS_CANCEL));
        _ui_flag_modify(ui_SubMenuConfirmPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        _ui_flag_modify(ui_MenuConfirmNoButton, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
        _ui_flag_modify(ui_MenuConfirmYesButton, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
    }
}

/*
* Confirm And Cancel
*/
bool onButtonClick_SubMenuConfirm_yes() {
    printf(" ButtonClick SubMenuConfirm_yes !!!\n");
    if (pSubMenu->iMenuId == MENUID_SUB_MENU_RESET_SETUP) {
        reset_setup_flag = true;
    }
    if (pCurItem->pSubMenu->pItemsList[0]->pfItemSelectHandler != NULL) {
        /*
        pCurItem->pSubMenu->pItemsList[0]->pfItemSelectHandler(pCurItem->pSubMenu->pItemsList[0]);
        */
    }
    _ui_flag_modify(ui_SubMenuConfirmPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    gcurrent_page_index = MENUID_MAIN_MENULIST;
    pCurItem = pTopMenu->pItemsList[pTopMenu->iCurrentPos];
    return false;
}

bool onButtonClick_SubMenuConfirm_no() {
    printf(" ButtonClick SubMenuConfirm_no !!!\n");
    if (pSubMenu->iMenuId == MENUID_SUB_MENU_RESET_SETUP) {
        reset_setup_flag = true;
    }
    _ui_flag_modify(ui_SubMenuConfirmPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    gcurrent_page_index = MENUID_MAIN_MENULIST;
    pCurItem = pTopMenu->pItemsList[pTopMenu->iCurrentPos];
    return false;
}

/*
* Main Menu Button
*/
bool onButtonClick_GeneralSettingsButton(void) {
    printf(" ButtonClick GeneralSettingsButton !!!\n");
    /*
    lv_label_set_text(ui_GeneralSettingsButtonTxt, MAP_STRINGID(IDS_DS_GENERAL_SETTINGS));
    */
    if(gcurrent_page_index == MENUID_MAIN_MENULIST){
        /*
        lv_obj_set_style_text_color(ui_GeneralSettingsButtonTxt, lv_color_hex(0x0B8AF2), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_color(ui_MovieSettingsButtonTxt, lv_color_hex(0xffffff), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_color(ui_SystemSettingsButtonTxt, lv_color_hex(0xffffff), LV_PART_MAIN | LV_STATE_DEFAULT);
        */
        pTopMenu = &sMainMenuGeneral;

        pTopMenu->iCurrentPos = 0;
        pCurItem = pTopMenu->pItemsList[pTopMenu->iCurrentPos];
        pSubMenu = pCurItem->pSubMenu;
        pSubMenu->iCurrentPos = 0;

        CusMenuListRefresh();
    }
    return false;
}

bool onButtonClick_MovieSettingsButton(void) {
    printf(" ButtonClick MovieSettingsButton !!!\n");
    /*
    lv_label_set_text(ui_MovieSettingsButtonTxt, MAP_STRINGID(IDS_DS_VIDEO_SETTINGS));
    */
    if(gcurrent_page_index == MENUID_MAIN_MENULIST){
        /*
        lv_label_set_text(ui_MovieSettingsButtonTxt, MAP_STRINGID(IDS_DS_VIDEO_SETTINGS));
        lv_obj_set_style_text_color(ui_GeneralSettingsButtonTxt, lv_color_hex(0xffffff), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_color(ui_MovieSettingsButtonTxt, lv_color_hex(0x0B8AF2), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_color(ui_SystemSettingsButtonTxt, lv_color_hex(0xffffff), LV_PART_MAIN | LV_STATE_DEFAULT);
        */
        pTopMenu = &sMainMenuVideo;
        pTopMenu->iCurrentPos = 0;
        pCurItem = pTopMenu->pItemsList[pTopMenu->iCurrentPos];

        CusMenuListRefresh();
    }
    return false;
}

bool onButtonClick_SystemSettingsButton(void) {
    printf(" ButtonClick SystemSettingsButton !!!\n");
    /*
    lv_label_set_text(ui_SystemSettingsButtonTxt, MAP_STRINGID(IDS_DS_SYSTEM_SETTINGS));
    */
    if(gcurrent_page_index == MENUID_MAIN_MENULIST){
        /*
        lv_label_set_text(ui_SystemSettingsButtonTxt, MAP_STRINGID(IDS_DS_SYSTEM_SETTINGS));
        lv_obj_set_style_text_color(ui_GeneralSettingsButtonTxt, lv_color_hex(0xffffff), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_color(ui_MovieSettingsButtonTxt, lv_color_hex(0xffffff), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_color(ui_SystemSettingsButtonTxt, lv_color_hex(0x0B8AF2), LV_PART_MAIN | LV_STATE_DEFAULT);
        */
        pTopMenu = &sMainMenuSystem;

        pTopMenu->iCurrentPos = 0;
        pCurItem = pTopMenu->pItemsList[pTopMenu->iCurrentPos];

        CusMenuListRefresh();
    }
    return false;
}

/*
* Main Menu
*/
static void CusMenuListBtnEventHandler(lv_event_t * e)
{
    /* Get Obj */
    lv_obj_t * obj = lv_event_get_target(e);
    /* Get Obj Index*/
    uint32_t index = lv_obj_get_index(obj);
    lv_event_code_t event_code = lv_event_get_code(e);

    char strSize[32] = {0};
    if(event_code == LV_EVENT_CLICKED) {
        if(gcurrent_page_index == MENUID_MAIN_MENULIST){
            pCurItem = pTopMenu->pItemsList[index];
            printf("set pCurItem [%s] Line [%d]\n", MAP_STRINGID(pCurItem->iStringId), __LINE__);
            pSubMenu = pCurItem->pSubMenu;
            printf("set pSubMenu [%s] Line [%d]\n", pSubMenu ? MAP_STRINGID(pSubMenu->iStringId) : "No SubMenu", __LINE__);

            /* Selected List */
            lv_obj_t * obj_old = lv_obj_get_child(ui_MainListPanel, pTopMenu->iCurrentPos);
            lv_obj_set_style_bg_color(obj_old, lv_color_hex(0x2f2f2f), LV_PART_MAIN | LV_STATE_DEFAULT);
            lv_obj_set_style_bg_color(obj, lv_color_hex(0x2d3495), LV_PART_MAIN | LV_STATE_DEFAULT);
            pTopMenu->iCurrentPos = index;  // 更新选中项

            if (pSubMenu != NULL) {
                pSubMenu->pParentMenu = pTopMenu;
                /*
                if (pCurItem->iItemId == ITEMID_FW_VERSION_INFO) {
                    gcurrent_page_index = MENUID_SUB_MENULIST;
                    pCurItem->pSubMenu->iCurrentPos = 0;
                    lv_obj_set_style_text_font(ui_TitleTextLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_SubMenuConfirmTextLabel, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_SubMenuConfirmTextLabel2, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);

                    lv_label_set_text(ui_TitleTextLabel, MAP_STRINGID(IDS_DS_FW_VERSION_INFO));
                    sprintf(strSize, "%s",carimpl_fw_version_get());
                    lv_label_set_text(ui_SubMenuConfirmTextLabel, strSize);
                    lv_label_set_text(ui_SubMenuConfirmTextLabel2, MAP_STRINGID(IDS_DS_EMPTY));
                    _ui_flag_modify(ui_SubMenuConfirmPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    _ui_flag_modify(ui_MenuConfirmNoButton, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                    _ui_flag_modify(ui_MenuConfirmYesButton, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                } else if (pCurItem->iItemId == ITEMID_QR_CODE_INFO) {
                    gcurrent_page_index = MENUID_SUB_MENULIST;
                    lv_obj_set_style_text_font(ui_SSIDKeyLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_SSIDValueLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_PWDKeyLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_PWDValueLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_QRCodeTitleLabel, font_montserrat_24, LV_PART_MAIN | LV_STATE_DEFAULT);

                    _ui_flag_modify(ui_QRCodePanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    lv_label_set_text(ui_QRCodeTitleLabel, MAP_STRINGID(IDS_DS_QR_CODE));

                    _ui_flag_modify(ui_SSIDKeyLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    lv_label_set_text(ui_SSIDKeyLabel, MAP_STRINGID(IDS_DS_WIFI_SSID_INFO));
                    _ui_flag_modify(ui_PWDKeyLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    lv_label_set_text(ui_PWDKeyLabel, MAP_STRINGID(IDS_DS_PASSWORD_INFO));
                    if(MenuSettingConfig()->uiWifi != WIFI_MODE_OFF){
                        _ui_flag_modify(ui_PWDValueLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                        carimpl_wifi_password_handler();
                        sprintf(strSize, "%s",carimpl_wifi_password_get());
                        lv_label_set_text(ui_PWDValueLabel, strSize);

                        _ui_flag_modify(ui_SSIDValueLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                        carimpl_wifi_ssid_handler();
                        sprintf(strSize, "%s",carimpl_wifi_ssid_get());
                        lv_label_set_text(ui_SSIDValueLabel, strSize);
                    } else {
                        _ui_flag_modify(ui_SSIDValueLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                        _ui_flag_modify(ui_PWDValueLabel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                    }
                    CusSetWidgetBgImgSrc(ui_QRCodeLabel, &ui_img_qrcode_png, LV_PART_MAIN | LV_STATE_DEFAULT);
                    _ui_flag_modify(ui_MenuPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                } else if (pCurItem->iItemId == ITEMID_CLOCK_SETTINGS) {
                    gcurrent_page_index = MENUID_SUB_MENULIST;
                    lv_obj_set_style_text_font(ui_ClockSetTitle, font_montserrat_24, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetOKButtonLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetCancelButtonLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetYear, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetMon, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetMon, font_montserrat_22, LV_PART_SELECTED | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetDay, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetHour, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetMin, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_ClockSetSec, font_montserrat_18, LV_PART_MAIN | LV_STATE_DEFAULT);

                    lv_label_set_text(ui_ClockSetTitle, MAP_STRINGID(IDS_DS_CLOCK_SETTINGS));
                    lv_label_set_text(ui_ClockSetOKButtonLabel, MAP_STRINGID(IDS_DS_OK));
                    lv_label_set_text(ui_ClockSetCancelButtonLabel, MAP_STRINGID(IDS_DS_CANCEL));
                    for(int i = 6; i > 0; i--){
                        list_refresh_first[i-1] = 1;
                    }
                    data_getings();
                    CusClockSetListRefresh();
                    _ui_flag_modify(ui_ClockSettingPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    _ui_flag_modify(ui_MenuPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                } else*/
                if (pCurItem->iItemId == ITEMID_RESET_SETUP) {
                    gcurrent_page_index = MENUID_SUB_MENULIST;
                    pCurItem->pSubMenu->iCurrentPos = 0;
                    lv_obj_set_style_text_font(ui_TitleTextLabel, font_montserrat_22, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_SubMenuConfirmTextLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_SubMenuConfirmTextLabel2, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_MenuConfirmNoLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_MenuConfirmYesLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);

                    lv_label_set_text(ui_TitleTextLabel, MAP_STRINGID(IDS_DS_NOTE));
                    lv_label_set_text(ui_SubMenuConfirmTextLabel, MAP_STRINGID(IDS_DS_RESET_SETUP_CONFIRM));
                    lv_label_set_text(ui_SubMenuConfirmTextLabel2, MAP_STRINGID(IDS_DS_RESET_INFO));
                    lv_label_set_text(ui_MenuConfirmYesLabel, MAP_STRINGID(IDS_DS_OK));
                    lv_label_set_text(ui_MenuConfirmNoLabel, MAP_STRINGID(IDS_DS_CANCEL));
                    _ui_flag_modify(ui_SubMenuConfirmPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    _ui_flag_modify(ui_MenuConfirmNoButton, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    _ui_flag_modify(ui_MenuConfirmYesButton, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                } else if (pCurItem->iItemId == ITEMID_FORMAT_SD_CARD) {
                    gcurrent_page_index = MENUID_SUB_MENULIST;
                    pCurItem->pSubMenu->iCurrentPos = 0;
                    lv_obj_set_style_text_font(ui_TitleTextLabel, font_montserrat_22, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_SubMenuConfirmTextLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_SubMenuConfirmTextLabel2, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_MenuConfirmNoLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_MenuConfirmYesLabel, font_montserrat_20, LV_PART_MAIN | LV_STATE_DEFAULT);

                    lv_label_set_text(ui_TitleTextLabel, MAP_STRINGID(IDS_DS_NOTE));
                    lv_label_set_text(ui_SubMenuConfirmTextLabel, MAP_STRINGID(IDS_DS_FORMAT_CARD_CONFIRM));
                    lv_label_set_text(ui_SubMenuConfirmTextLabel2, MAP_STRINGID(IDS_DS_DATA_DELETED));
                    lv_label_set_text(ui_MenuConfirmYesLabel, MAP_STRINGID(IDS_DS_OK));
                    lv_label_set_text(ui_MenuConfirmNoLabel, MAP_STRINGID(IDS_DS_CANCEL));
                    _ui_flag_modify(ui_SubMenuConfirmPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    _ui_flag_modify(ui_MenuConfirmNoButton, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    _ui_flag_modify(ui_MenuConfirmYesButton, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                } else {
                    if (pSubMenu->pfMenuGetDefaultVal != NULL){
                        pSubMenu->iCurrentPos = pSubMenu->pfMenuGetDefaultVal(pSubMenu);
                        first_refresh_sub_list = true;
                    } else {
                        pSubMenu->iCurrentPos = 0;
                    }
                    gcurrent_page_index = MENUID_SUB_MENULIST;
                    _ui_flag_modify(ui_SubListPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
                    CusSubMenuListRefresh();
                }
            }
        }
    }
}

static void CusMenuListRefresh(void)
{
    uint32_t i;
    uint32_t btn_position_x = 0;
    uint32_t btn_position_y = 0;

    uint32_t list_num;
    PSMENUITEM pItem = NULL;

    //Clean List
    printf(" Clean List !!!\n");
    lv_obj_t * child = lv_obj_get_child(ui_MainListPanel, 0);
    while(child) {
        lv_obj_del(child);
        child = lv_obj_get_child(ui_MainListPanel, 0);
    }
    //Show List
    printf(" Show List !!!\n");
    list_num = pTopMenu->iNumOfItems;
    for(i = 0; i < list_num; i++) {
        btn_position_y = ((i * 120) - 180);

        lv_obj_t * MenuListItemBtn = lv_btn_create(ui_MainListPanel);
        lv_obj_set_width(MenuListItemBtn, 810);
        lv_obj_set_height(MenuListItemBtn, 110);
        lv_obj_set_y(MenuListItemBtn, btn_position_y);
        lv_obj_set_x(MenuListItemBtn, lv_pct(0));
        lv_obj_set_align(MenuListItemBtn, LV_ALIGN_CENTER);
        lv_obj_add_flag(MenuListItemBtn, LV_OBJ_FLAG_SCROLL_ON_FOCUS);     /// Flags
        lv_obj_clear_flag(MenuListItemBtn, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
        lv_obj_set_style_bg_opa(MenuListItemBtn, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_shadow_width(MenuListItemBtn, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_shadow_spread(MenuListItemBtn, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
        if (pTopMenu->iCurrentPos == i)
            lv_obj_set_style_bg_color(MenuListItemBtn, lv_color_hex(0x2d3495), LV_PART_MAIN | LV_STATE_DEFAULT);
        else
            lv_obj_set_style_bg_color(MenuListItemBtn, lv_color_hex(0x2f2f2f), LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_t * MainListBtnIcon = lv_label_create(MenuListItemBtn);
        lv_obj_set_width(MainListBtnIcon, 48);
        lv_obj_set_height(MainListBtnIcon, 48);
        lv_obj_set_x(MainListBtnIcon, lv_pct(-45));
        lv_obj_set_y(MainListBtnIcon, lv_pct(0));
        lv_obj_set_align(MainListBtnIcon, LV_ALIGN_CENTER);
        lv_label_set_text(MainListBtnIcon, "");
        lv_obj_set_style_bg_img_src(MainListBtnIcon, &ui_img_icon_sub_video_resolution_png, LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_t * MainListBtnIconSub = lv_label_create(MenuListItemBtn);
        lv_obj_set_width(MainListBtnIconSub, 18);
        lv_obj_set_height(MainListBtnIconSub, 33);
        lv_obj_set_x(MainListBtnIconSub, lv_pct(45));
        lv_obj_set_y(MainListBtnIconSub, lv_pct(0));
        lv_obj_set_align(MainListBtnIconSub, LV_ALIGN_CENTER);
        lv_label_set_text(MainListBtnIconSub, "");
        lv_obj_set_style_bg_img_src(MainListBtnIconSub, &ui_img_icon_sub_universal_right_png,
                                LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_t * MainListBtnLabel = lv_label_create(MenuListItemBtn);
        lv_obj_set_width(MainListBtnLabel, 350);
        lv_obj_set_height(MainListBtnLabel, 32);
        lv_obj_set_x(MainListBtnLabel, lv_pct(-15));
        lv_obj_set_y(MainListBtnLabel, lv_pct(0));
        lv_obj_set_align(MainListBtnLabel, LV_ALIGN_CENTER);
        lv_label_set_text(MainListBtnLabel, MAP_STRINGID(pTopMenu->pItemsList[i]->pSubMenu->iStringId));
        lv_obj_set_style_text_align(MainListBtnLabel, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_font(MainListBtnLabel, font_montserrat_26, LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_t * MainListBtnLabelSub = lv_label_create(MenuListItemBtn);
        lv_obj_set_width(MainListBtnLabelSub, 240);
        lv_obj_set_height(MainListBtnLabelSub, 28);
        lv_obj_set_x(MainListBtnLabelSub, 200);
        lv_obj_set_y(MainListBtnLabelSub, 0);
        lv_obj_set_align(MainListBtnLabelSub, LV_ALIGN_CENTER);
        lv_obj_set_style_text_color(MainListBtnLabelSub, lv_color_hex(0xFEFEFE), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_opa(MainListBtnLabelSub, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_align(MainListBtnLabelSub, LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_font(MainListBtnLabelSub, font_montserrat_24, LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_add_event_cb(MenuListItemBtn, CusMenuListBtnEventHandler, LV_EVENT_CLICKED, NULL);
        //Update Text
        int CurValue = -1;
        pItem = pTopMenu->pItemsList[i];
        if (pItem->pSubMenu->pfMenuGetDefaultVal != NULL)
            CurValue = pItem->pSubMenu->pfMenuGetDefaultVal(pItem->pSubMenu);
        else {
            lv_label_set_text(MainListBtnLabelSub, MAP_STRINGID(IDS_DS_EMPTY));
        }

        if (pItem->iItemId == ITEMID_FW_VERSION_INFO) {
            lv_label_set_text(MainListBtnLabelSub, MAP_STRINGID(IDS_DS_EMPTY));
        } else if (pItem->iItemId == ITEMID_FORMAT_SD_CARD) {
            lv_label_set_text(MainListBtnLabelSub, MAP_STRINGID(IDS_DS_EMPTY));
        } else if (pItem->iItemId == ITEMID_RESET_SETUP) {
            lv_label_set_text(MainListBtnLabelSub, MAP_STRINGID(IDS_DS_EMPTY));
        } else if (pItem->iItemId == ITEMID_QR_CODE_INFO) {
            lv_label_set_text(MainListBtnLabelSub, MAP_STRINGID(IDS_DS_EMPTY));
        } else if (CurValue != -1) {
            lv_label_set_text(MainListBtnLabelSub, MAP_STRINGID(pItem->pSubMenu->pItemsList[CurValue]->iStringId));
        }
    }
}

/*
* Sub Menu For Main Menu
*/
static void CustSubMenuListBtnEventHandler(lv_event_t * e)
{
    bool language_update_flag = false;
    /* Get Obj */
    lv_obj_t * obj = lv_event_get_target(e);
    /* Get Obj Index*/
    uint32_t index = lv_obj_get_index(obj);
    lv_event_code_t event_code = lv_event_get_code(e);

    /* Selected List */
    lv_obj_t * obj_old = lv_obj_get_child(ui_SubListPanel, pSubMenu->iCurrentPos);
    lv_obj_set_style_bg_color(obj_old, lv_color_hex(0x2f2f2f), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(obj, lv_color_hex(0x2d3495), LV_PART_MAIN | LV_STATE_DEFAULT);

    if(pSubMenu->iMenuId == MENUID_SUB_MENU_LANGUAGE){
        language_update_flag = true;
    }

    if(event_code == LV_EVENT_CLICKED) {
        pCurItem = pSubMenu->pItemsList[index];
        printf("set pCurItem [%s] Line [%d]\n", MAP_STRINGID(pCurItem->iStringId), __LINE__);
        pSubMenuSub = pCurItem->pSubMenu;
        printf("set pSubMenuSub [%s] Line [%d]\n", pSubMenuSub ? MAP_STRINGID(pSubMenuSub->iStringId) : "No SubMenu", __LINE__);
        pSubMenu->iCurrentPos = index;

        if (pCurItem->pfItemSelectHandler != NULL) {
            bool bRet = true;
            /*
            bRet = pCurItem->pfItemSelectHandler(pCurItem);
            */

            if (bRet == true) {
                gcurrent_page_index = MENUID_MAIN_MENULIST;
                _ui_flag_modify(ui_SubListPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
                //Update Main List Text
                int CurValue = -1;
                if (pTopMenu->pItemsList[pTopMenu->iCurrentPos]->pSubMenu->pfMenuGetDefaultVal != NULL) {
                    printf("Update Main List Text!!\r\n");
                    lv_obj_t * obj_main = lv_obj_get_child(ui_MainListPanel, pTopMenu->iCurrentPos);
                    lv_obj_t * obj_sub = lv_obj_get_child(obj_main, 2);
                    /*
                    CurValue = pTopMenu->pItemsList[pTopMenu->iCurrentPos]->pSubMenu->pfMenuGetDefaultVal(pTopMenu->pItemsList[pTopMenu->iCurrentPos]->pSubMenu);
                    lv_label_set_text(obj_sub, MAP_STRINGID(pTopMenu->pItemsList[pTopMenu->iCurrentPos]->pSubMenu->pItemsList[CurValue]->iStringId));
                    */
                }
                //Update Language
                if(language_update_flag == true){
                    printf("Update Language!!\r\n");
                    /*
                    lv_obj_set_style_text_font(ui_GeneralSettingsButtonTxt, font_montserrat_24, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_SystemSettingsButtonTxt, font_montserrat_24, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_obj_set_style_text_font(ui_MovieSettingsButtonTxt, font_montserrat_24, LV_PART_MAIN | LV_STATE_DEFAULT);
                    lv_label_set_text(ui_GeneralSettingsButtonTxt, MAP_STRINGID(IDS_DS_GENERAL_SETTINGS));
                    lv_label_set_text(ui_MovieSettingsButtonTxt, MAP_STRINGID(IDS_DS_VIDEO_SETTINGS));
                    lv_label_set_text(ui_SystemSettingsButtonTxt, MAP_STRINGID(IDS_DS_SYSTEM_SETTINGS));

                    if(pTopMenu->iMenuId == MENUID_MAIN_MENU_GENERAL){
                        onButtonClick_GeneralSettingsButton();
                    } else if (pTopMenu->iMenuId == MENUID_MAIN_MENU_VIDEO) {
                        onButtonClick_MovieSettingsButton();
                    } else if (pTopMenu->iMenuId == MENUID_MAIN_MENU_SYSTEM) {
                        onButtonClick_SystemSettingsButton();
                    }
                    */
                    language_update_flag = false;
                }
            }
        }
    }
}

static void CusSubMenuListRefresh()
{
    uint32_t i;
    uint32_t btn_position_y = 0;

    uint32_t list_num;
    PSMENUITEM pItem = NULL;

    //Scrollable
    lv_obj_set_scroll_dir(ui_SubListPanel, LV_DIR_VER);
    lv_obj_set_scrollbar_mode(ui_SubListPanel, LV_SCROLLBAR_MODE_OFF);
    //Clean List
    printf(" Clean Sub List !!!\n");
    lv_obj_t * child = lv_obj_get_child(ui_SubListPanel, 0);
    while(child) {
        lv_obj_del(child);
        child = lv_obj_get_child(ui_SubListPanel, 0);
    }
    //Show List
    printf(" Show Sub List !!!\n");
    list_num = pSubMenu->iNumOfItems;
    for(i = 0; i < list_num; i++) {
        btn_position_y = ((i * 120) - 180);

        lv_obj_t * LeftSubMenuListItem = lv_btn_create(ui_SubListPanel);
        lv_obj_set_width(LeftSubMenuListItem, 470);
        lv_obj_set_height(LeftSubMenuListItem, 110);
        lv_obj_set_y(LeftSubMenuListItem, btn_position_y);
        lv_obj_set_x(LeftSubMenuListItem, lv_pct(0));
        lv_obj_set_align(LeftSubMenuListItem, LV_ALIGN_CENTER);
        lv_obj_add_flag(LeftSubMenuListItem, LV_OBJ_FLAG_SCROLL_ON_FOCUS);     /// Flags
        lv_obj_clear_flag(LeftSubMenuListItem, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
        lv_obj_set_style_bg_opa(LeftSubMenuListItem, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
        if (pSubMenu->iCurrentPos == i)
            lv_obj_set_style_bg_color(LeftSubMenuListItem, lv_color_hex(0x2d3495), LV_PART_MAIN | LV_STATE_DEFAULT);
        else
            lv_obj_set_style_bg_color(LeftSubMenuListItem, lv_color_hex(0x2f2f2f), LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_t * SubListButtonIcon = lv_label_create(LeftSubMenuListItem);
        lv_obj_set_width(SubListButtonIcon, 48);
        lv_obj_set_height(SubListButtonIcon, 48);
        lv_obj_set_y(SubListButtonIcon, 0);
        lv_obj_set_x(SubListButtonIcon, lv_pct(-45));
        lv_obj_set_align(SubListButtonIcon, LV_ALIGN_CENTER);
        lv_label_set_text(SubListButtonIcon, "");
        lv_obj_set_style_bg_img_src(SubListButtonIcon, &ui_img_icon_sub_video_resolution_png,
                                    LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_t * SubListButtonIconSub = lv_label_create(LeftSubMenuListItem);
        lv_obj_set_width(SubListButtonIconSub, 26);
        lv_obj_set_height(SubListButtonIconSub, 26);
        lv_obj_set_y(SubListButtonIconSub, 0);
        lv_obj_set_x(SubListButtonIconSub, lv_pct(45));
        lv_obj_set_align(SubListButtonIconSub, LV_ALIGN_CENTER);
        lv_label_set_text(SubListButtonIconSub, "");
        lv_obj_set_style_bg_img_src(SubListButtonIconSub, &ui_img_icon_sub_unlock_png, LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_t * SubListButtonLabel = lv_label_create(LeftSubMenuListItem);
        lv_obj_set_width(SubListButtonLabel, 240);
        lv_obj_set_height(SubListButtonLabel, 32);
        lv_obj_set_x(SubListButtonLabel, lv_pct(-5));
        lv_obj_set_y(SubListButtonLabel, lv_pct(0));
        lv_obj_set_align(SubListButtonLabel, LV_ALIGN_CENTER);
        lv_label_set_text(SubListButtonLabel, "Video");
        lv_obj_set_style_text_align(SubListButtonLabel, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_font(SubListButtonLabel, font_montserrat_26, LV_PART_MAIN | LV_STATE_DEFAULT);

        lv_obj_add_event_cb(LeftSubMenuListItem, CustSubMenuListBtnEventHandler, LV_EVENT_CLICKED, NULL);

        pItem = pSubMenu->pItemsList[i];
        lv_label_set_text(SubListButtonLabel, MAP_STRINGID(pItem->iStringId));
    }
}

/*
* Clock Settings
*/
#if (CLOCK_SETTING)
static void data_getings(void)
{
    time_t time0 = time(NULL);
    struct tm* t = localtime(&time0);

    Datetime[IDX_YEAR] = t->tm_year + 1900;
    Datetime[IDX_MONTH] = t->tm_mon + 1;
    Datetime[IDX_DAY] = t->tm_mday;
    Datetime[IDX_HOUR] = t->tm_hour;
    Datetime[IDX_MIN] = t->tm_min;
    Datetime[IDX_SEC] = t->tm_sec;
}

int Check_Validate_ClockSetting(int* pDatetime, int ubCheckType)
{
    int Year,Month,Day,Hour,Min,Sec;

    Year  = *(pDatetime+IDX_YEAR);
    Month = *(pDatetime+IDX_MONTH);
    Day   = *(pDatetime+IDX_DAY);
    Hour  = *(pDatetime+IDX_HOUR);
    Min   = *(pDatetime+IDX_MIN);
    Sec   = *(pDatetime+IDX_SEC);

    //Check Year
    if (ubCheckType & CHECK_YEAR) {
        if (Year > RTC_MAX_YEAR) {
            *(pDatetime+IDX_YEAR) = RTC_MIN_YEAR;
        } else if (Year < RTC_MIN_YEAR) {
            *(pDatetime+IDX_YEAR) = RTC_MAX_YEAR;
        }
        if (((Year %4 == 0) && (Year %100 != 0)) || (Year %400 == 0)) {//Leap Year
            if (Month == 2 && (Day == 30 || Day == 31)) {
                *(pDatetime+IDX_DAY) = RTC_MAX_DAY_FEB_LEAP_YEAR;
            }
        } else {
            if (Month == 2 && (Day == 29 || Day == 30 || Day == 31)) {
                *(pDatetime+IDX_DAY) = RTC_MAX_DAY_FEB_NONLEAP_YEAR;
            }
        }
    }
    //Check Month
    if (ubCheckType & CHECK_MONTH) {
        if (Month > RTC_MAX_MONTH) {
            *(pDatetime+IDX_MONTH) = RTC_MIN_MONTH;
        } else if (Month < RTC_MIN_MONTH) {
            *(pDatetime+IDX_MONTH) = RTC_MAX_MONTH;
        }
        if (Month == 2) {
            if (((Year %4 == 0) && (Year %100 != 0)) || (Year %400 == 0)) {//Leap Year
                if (Day == 30 || Day == 31) {
                    *(pDatetime+IDX_DAY) = RTC_MAX_DAY_FEB_LEAP_YEAR;
                }
            } else {
                if (Day == 29 || Day == 30 || Day == 31) {
                    *(pDatetime+IDX_DAY) = RTC_MAX_DAY_FEB_NONLEAP_YEAR;
                }
            }
        } else if (Month == 4 || Month == 6 || Month == 9 || Month == 11) {
            if (Day == 31) {
                *(pDatetime+IDX_DAY) = RTC_MAX_DAY_30;
            }
        }
    }
    //Check Day
    if (ubCheckType & CHECK_DAY) {
        if (Month==1  || Month==3 || Month==5 || Month==7 || Month==8 || Month==10 || Month==12) {
            if (Day >RTC_MAX_DAY_31) {
                *(pDatetime+IDX_DAY) = RTC_MIN_DAY;
            } else if (Day < RTC_MIN_DAY) {
                *(pDatetime+IDX_DAY) = RTC_MAX_DAY_31;
            }
        }

        if (Month==4 || Month==6 || Month==9 || Month==11) {
            if (Day >RTC_MAX_DAY_30) {
                *(pDatetime+IDX_DAY) = RTC_MIN_DAY;
            } else if (Day < RTC_MIN_DAY) {
                    *(pDatetime+IDX_DAY) = RTC_MAX_DAY_30;
            }
        }

        if (Month==2) {
            if (((Year %4 == 0) && (Year %100 != 0)) || (Year %400 == 0)) {//Leap Year
                if (Day >RTC_MAX_DAY_FEB_LEAP_YEAR) {
                    *(pDatetime+IDX_DAY) = RTC_MIN_DAY;
                } else if (Day < RTC_MIN_DAY) {
                    *(pDatetime+IDX_DAY) = RTC_MAX_DAY_FEB_LEAP_YEAR;
                }
            } else {
                if (Day >RTC_MAX_DAY_FEB_NONLEAP_YEAR) {
                    *(pDatetime+IDX_DAY) = RTC_MIN_DAY;
                } else if (Day < RTC_MIN_DAY) {
                    *(pDatetime+IDX_DAY) = RTC_MAX_DAY_FEB_NONLEAP_YEAR;
                }
            }
        }
    }
    //Check Hour
    if (ubCheckType & CHECK_HOUR) {
        if (Hour > RTC_MAX_HOUR) {
            *(pDatetime+IDX_HOUR) = RTC_MIN_HOUR;
        } else if (Hour < RTC_MIN_HOUR) {
            *(pDatetime+IDX_HOUR) = RTC_MAX_HOUR;
        }
    }
    //Check Minute
    if (ubCheckType & CHECK_MIN) {
        if (Min > RTC_MAX_MIN) {
            *(pDatetime+IDX_MIN) = RTC_MIN_MIN;
        } else if (Min < RTC_MIN_MIN) {
            *(pDatetime+IDX_MIN) = RTC_MAX_MIN;
        }
    }
    //Check Second
    if (ubCheckType & CHECK_SEC) {
        if (Sec > RTC_MAX_SEC) {
            *(pDatetime+IDX_SEC) = RTC_MIN_SEC;
        } else if (Sec < RTC_MIN_SEC) {
            *(pDatetime+IDX_SEC) = RTC_MAX_SEC;
        }
    }
    return CHECK_PASS;
}

int SetRtcTime(int* pDatetime)
{
    struct tm RtcTm;
    struct timeval tv;
    char rtctime[128] = {0};

    /* setting system clock */
    RtcTm.tm_sec = *(pDatetime+IDX_SEC);
    RtcTm.tm_min = *(pDatetime+IDX_MIN);
    RtcTm.tm_hour = *(pDatetime+IDX_HOUR);
    RtcTm.tm_mday = *(pDatetime+IDX_DAY);
    RtcTm.tm_mon = *(pDatetime+IDX_MONTH) - 1;    // month of year [0,11]
    RtcTm.tm_year = *(pDatetime+IDX_YEAR) - 1900;
    RtcTm.tm_isdst = 0;

    unsigned long timep = mktime(&RtcTm);
    tv.tv_sec = timep;
    tv.tv_usec = 0;
    if (settimeofday(&tv, (struct timezone *) 0) < 0) {
        printf("Set rtc datatime error!\n");
        return -1;
    }

    /* update hwclock */
    memset(rtctime, 0 , 128);
    sprintf(rtctime, "date -s \"%4d-%2d-%2d %2d:%2d:%2d\" &&hwclock --systohc",
                    *(pDatetime+IDX_YEAR), *(pDatetime+IDX_MONTH), *(pDatetime+IDX_DAY),
                    *(pDatetime+IDX_HOUR), *(pDatetime+IDX_MIN),  *(pDatetime+IDX_SEC));
    system(rtctime);

    return 0;
}

static void obtainListItemData_ClockSetYear(void) {
    uint32_t i;
    uint32_t list_num;
    char opts[256] = {0,};
    char opts_tmp[8] = {0,};

    list_num = RTC_MAX_YEAR - RTC_MIN_YEAR + 1;
    for(i = 0; i < list_num; i++) {
        if(i == (list_num - 1))
            sprintf(opts_tmp, "%d", RTC_MIN_YEAR + i);
        else
            sprintf(opts_tmp, "%d\n", RTC_MIN_YEAR + i);
        strcat(opts, opts_tmp);
    }
    /*
    lv_roller_set_options(ui_ClockSetYear, opts, LV_ROLLER_MODE_NORMAL);
    */

    if(list_refresh_first[IDX_YEAR] == 1){
        list_refresh_first[IDX_YEAR] = 0;
        /*
        lv_roller_set_selected(ui_ClockSetYear, Datetime[IDX_YEAR] - RTC_MIN_YEAR, LV_ANIM_OFF);
        */
    }
}

static void obtainListItemData_ClockSetMon(void) {
    uint32_t i;
    uint32_t list_num;
    char opts[256] = {0,};
    char opts_tmp[8] = {0,};

    list_num = RTC_MAX_MONTH;
    for(i = 0; i < list_num; i++) {
        if(i == (list_num - 1))
            sprintf(opts_tmp, "%d", RTC_MIN_MONTH + i);
        else
            sprintf(opts_tmp, "%d\n", RTC_MIN_MONTH + i);
        strcat(opts, opts_tmp);
    }
    /*
    lv_roller_set_options(ui_ClockSetMon, opts, LV_ROLLER_MODE_NORMAL);
    */

    if(list_refresh_first[IDX_MONTH] == 1){
        list_refresh_first[IDX_MONTH] = 0;
        /*
        lv_roller_set_selected(ui_ClockSetMon, Datetime[IDX_MONTH] - RTC_MIN_MONTH, LV_ANIM_OFF);
        */
    }
}

static void obtainListItemData_ClockSetDay(void) {
    uint32_t i;
    uint32_t list_num;
    char opts[256] = {0,};
    char opts_tmp[8] = {0,};

    if (Datetime[IDX_MONTH] == 2) {
        if (((Datetime[IDX_YEAR] %4 == 0) && (Datetime[IDX_YEAR] %100 != 0)) || (Datetime[IDX_YEAR] %400 == 0)) {//Leap Year
            cur_rtc_max_day = RTC_MAX_DAY_FEB_LEAP_YEAR;
            list_num = RTC_MAX_DAY_FEB_LEAP_YEAR;
        } else {
            cur_rtc_max_day = RTC_MAX_DAY_FEB_NONLEAP_YEAR;
            list_num = RTC_MAX_DAY_FEB_NONLEAP_YEAR;
        }
    } else if (Datetime[IDX_MONTH] == 4 || Datetime[IDX_MONTH] == 6 || Datetime[IDX_MONTH] == 9 || Datetime[IDX_MONTH] == 11) {
        cur_rtc_max_day = RTC_MAX_DAY_30;
        list_num = RTC_MAX_DAY_30;
    } else {
        cur_rtc_max_day = RTC_MAX_DAY_31;
        list_num = RTC_MAX_DAY_31;
    }
    for(i = 0; i < list_num; i++) {
        if(i == (list_num - 1))
            sprintf(opts_tmp, "%d", RTC_MIN_DAY + i);
        else
            sprintf(opts_tmp, "%d\n", RTC_MIN_DAY + i);
        strcat(opts, opts_tmp);
    }
    /*
    lv_roller_set_options(ui_ClockSetDay, opts, LV_ROLLER_MODE_NORMAL);
    */

    if(list_refresh_first[IDX_DAY] == 1){
        list_refresh_first[IDX_DAY] = 0;
        /*
        lv_roller_set_selected(ui_ClockSetDay, Datetime[IDX_DAY] - RTC_MIN_DAY, LV_ANIM_OFF);
        */
    }
}

static void obtainListItemData_ClockSetHour(void) {
    uint32_t i;
    uint32_t list_num;
    char opts[256] = {0,};
    char opts_tmp[8] = {0,};

    list_num = RTC_MAX_HOUR + 1;
    for(i = 0; i < list_num; i++) {
        if(i == (list_num - 1))
            sprintf(opts_tmp, "%d", RTC_MIN_HOUR + i);
        else
            sprintf(opts_tmp, "%d\n", RTC_MIN_HOUR + i);
        strcat(opts, opts_tmp);
    }
    /*
    lv_roller_set_options(ui_ClockSetHour, opts, LV_ROLLER_MODE_NORMAL);
    */

    if(list_refresh_first[IDX_HOUR] == 1){
        list_refresh_first[IDX_HOUR] = 0;
        /*
        lv_roller_set_selected(ui_ClockSetHour, Datetime[IDX_HOUR], LV_ANIM_OFF);
        */
    }
}

static void obtainListItemData_ClockSetMin(void) {
    uint32_t i;
    uint32_t list_num;
    char opts[256] = {0,};
    char opts_tmp[8] = {0,};

    list_num = RTC_MAX_MIN + 1;
    for(i = 0; i < list_num; i++) {
        if(i == (list_num - 1))
            sprintf(opts_tmp, "%d", RTC_MIN_MIN + i);
        else
            sprintf(opts_tmp, "%d\n", RTC_MIN_MIN + i);
        strcat(opts, opts_tmp);
    }
    /*
    lv_roller_set_options(ui_ClockSetMin, opts, LV_ROLLER_MODE_NORMAL);
    */

    if(list_refresh_first[IDX_MIN] == 1){
        list_refresh_first[IDX_MIN] = 0;
        /*
        lv_roller_set_selected(ui_ClockSetMin, Datetime[IDX_MIN], LV_ANIM_OFF);
        */
    }
}

static void obtainListItemData_ClockSetSec(void) {
    uint32_t i;
    uint32_t list_num;
    char opts[256] = {0,};
    char opts_tmp[8] = {0,};

    list_num = RTC_MAX_SEC + 1;
    for(i = 0; i < list_num; i++) {
        if(i == (list_num - 1))
            sprintf(opts_tmp, "%d", RTC_MIN_SEC + i);
        else
            sprintf(opts_tmp, "%d\n", RTC_MIN_SEC + i);
        strcat(opts, opts_tmp);
    }
    /*
    lv_roller_set_options(ui_ClockSetSec, opts, LV_ROLLER_MODE_NORMAL);
    */

    if(list_refresh_first[IDX_SEC] == 1){
        list_refresh_first[IDX_SEC] = 0;
        /*
        lv_roller_set_selected(ui_ClockSetSec, Datetime[IDX_SEC], LV_ANIM_OFF);
        */
    }
}

static void CusClockSetListRefresh(void)
{
    obtainListItemData_ClockSetYear();
    obtainListItemData_ClockSetMon();
    obtainListItemData_ClockSetDay();
    obtainListItemData_ClockSetHour();
    obtainListItemData_ClockSetMin();
    obtainListItemData_ClockSetSec();
}

bool onButtonClick_CancelButton(void) {
    printf(" ButtonClick CancelButton !!!\n");
    /*
    _ui_flag_modify(ui_ClockSettingPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_MenuPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
    */
    gcurrent_page_index = MENUID_MAIN_MENULIST;

    return false;
}

bool onButtonClick_OKButton(void) {
    printf(" ButtonClick OKButton !!!\n");

    int cur = 0;
    /*
    cur = lv_roller_get_selected(ui_ClockSetYear);
    cur = RTC_MIN_YEAR + cur;
    Datetime[IDX_YEAR] = cur;
    Check_Validate_ClockSetting(Datetime, CHECK_YEAR);

    cur = lv_roller_get_selected(ui_ClockSetMon);
    cur = RTC_MIN_MONTH + cur;
    Datetime[IDX_MONTH] = cur;
    Check_Validate_ClockSetting(Datetime, CHECK_MONTH);

    cur = lv_roller_get_selected(ui_ClockSetDay);
    cur = RTC_MIN_DAY + cur;
    Datetime[IDX_DAY] = cur;
    Check_Validate_ClockSetting(Datetime, CHECK_DAY);

    cur = lv_roller_get_selected(ui_ClockSetHour);
    cur = RTC_MIN_HOUR + cur;
    Datetime[IDX_HOUR] = cur;
    Check_Validate_ClockSetting(Datetime, CHECK_HOUR);

    cur = lv_roller_get_selected(ui_ClockSetMin);
    cur = RTC_MIN_MIN + cur;
    Datetime[IDX_MIN] = cur;
    Check_Validate_ClockSetting(Datetime, CHECK_MIN);

    cur = lv_roller_get_selected(ui_ClockSetSec);
    cur = RTC_MIN_SEC + cur;
    Datetime[IDX_SEC] = cur;
    Check_Validate_ClockSetting(Datetime, CHECK_SEC);

    SetRtcTime(Datetime);
    _ui_flag_modify(ui_ClockSettingPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_ADD);
    _ui_flag_modify(ui_MenuPanel, LV_OBJ_FLAG_HIDDEN, _UI_MODIFY_FLAG_REMOVE);
    gcurrent_page_index = MENUID_MAIN_MENULIST;
    */
    return false;
}
#endif
