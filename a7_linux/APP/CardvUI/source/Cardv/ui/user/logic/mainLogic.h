/*
* mainLogic.h
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#ifndef _MAIN_LOGIC_H_
#define _MAIN_LOGIC_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "TouchEventContext.h"

void onUI_init_main(void);
void onUI_show_main(void);
void onUI_hide_main(void);

bool onButtonClick_CameraButton(void);
bool onButtonClick_VideoButton(void);
bool onButtonClick_AudioButton(void);
bool onButtonClick_PlaybackButton(void);
bool onButtonClick_SettingsButton(void);
void onProgressChanged_BrightnessSeekBar(int progress);
void onProgressChanged_VolumeSeekBar(int progress);

bool onButtonClick_FormatConfirmNO(void);
bool onButtonClick_FormatConfirmYes(void);

#endif /* _MAIN_LOGIC_H_ */
