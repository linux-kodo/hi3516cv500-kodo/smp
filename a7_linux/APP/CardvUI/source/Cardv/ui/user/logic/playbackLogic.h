/*
* playbackLogic.h
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#ifndef _PLAYBACK_LOGIC_H_
#define _PLAYBACK_LOGIC_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void onUI_init_playback(void);
void onUI_show_playback(void);
void onUI_hide_playback(void);

bool onButtonClick_PlaybackQuitButton(void);
bool onButtonClick_PlaybackSubDelectButton(void);

#endif /* _PLAYBACK_LOGIC_H_ */
