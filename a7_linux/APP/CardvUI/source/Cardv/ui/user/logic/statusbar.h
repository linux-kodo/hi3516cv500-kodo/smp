/*
* statusbar.h
*
*  Created on: Mar 1, 2024
*      Author: XiongYingDan
*/
#ifndef _STATUS_BAR_H_
#define _STATUS_BAR_H_

void onUI_Timer_Statusbar(void);

#endif /* _STATUS_BAR_H_ */
