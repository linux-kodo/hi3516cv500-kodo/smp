/*
 * ui_commom.h
 *
 *  Created on: Mar 1, 2024
 *      Author: XiongYingDan
 */
#ifndef _UI_COMMON_H_
#define _UI_COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <time.h>
#include <signal.h>
#include <pthread.h>

#include "ui.h"
#include "lvgl/lvgl.h"
#include "ui_helpers.h"
#include "ui_events.h"

#include "module_common.h"

extern bool CusGetWidgetFlags(lv_obj_t * p);
extern void CusSetWidgetFlags(lv_obj_t * target, int32_t flag, int value);
extern void CusSetWidgetBgImgSrc(struct _lv_obj_t * obj, const void * value, lv_style_selector_t selector);

extern void CusStatusbarInit(void);
extern bool CusGetStatusbarFlags(void);
extern bool CusSetStatusbarFlags(char state);
extern void CusStatusbarWarnText(const char * text);

extern bool CusIsMainScreen(void);
extern bool CusIsVideoMenuScreen(void);
extern bool CusIsPlayBackScreen(void);
extern bool CusIsPowerOffScreen(void);

extern void CusChangeMainScreen(void);

#endif /* _UI_COMMON_H_ */