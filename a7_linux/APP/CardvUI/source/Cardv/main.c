
/**
 * @file main
 *
 */

/*********************
 *      INCLUDES
 *********************/
#define _DEFAULT_SOURCE /* needed for usleep() */
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#define SDL_MAIN_HANDLED /*To fix SDL's "undefined reference to WinMain" issue*/
#include "lvgl/lvgl.h"
#include "lv_drivers/display/fbdev.h"
#include "lv_drivers/indev/evdev.h"
#include "ui/ui.h"

#include "IPC_msg.h"
#include "IPC_cardvInfo.h"

#include "carimpl.h"
#include "MenuCommon.h"
#include "KeyEventContext.h"
#include "TimerEventContext.h"

#define APP_MAIN_ENABLE (1)

#if (APP_MAIN_ENABLE)
#include "module_main.h"
#endif

#define DISP_BUF_SIZE   (854 * 480)

#if (LV_TICK_CUSTOM)
#define TICK_CUSTOM     (0)
#else
#define TICK_CUSTOM     (1)
#endif

pthread_mutex_t mutex;

#if (TICK_CUSTOM)
static void *tick_thread(void * data)
{
    (void)data;

    while(1) {
        usleep(1000);
        lv_tick_inc(1); /*Tell LittelvGL that 5 milliseconds were elapsed*/
    }
    return 0;
}
#endif

int main(void)
{
    pthread_t pt;

    //app main init
    #if (APP_MAIN_ENABLE)
    app_main_init();
    #endif

    /*LittlevGL init*/
    lv_init();

    /*Linux frame buffer device init*/
    fbdev_init();

    /*A small buffer for LittlevGL to draw the screen's content*/
    static lv_color_t buf[DISP_BUF_SIZE];
    static lv_color_t buf1[DISP_BUF_SIZE];

    /*Initialize a descriptor for the buffer*/
    static lv_disp_draw_buf_t disp_buf;
    lv_disp_draw_buf_init(&disp_buf, buf, buf1, DISP_BUF_SIZE);

    /*Initialize and register a display driver*/
    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.draw_buf   = &disp_buf;
    disp_drv.flush_cb   = fbdev_flush;
    disp_drv.hor_res    = 480;
    disp_drv.ver_res    = 854;
    disp_drv.sw_rotate  = 1;
    disp_drv.rotated    = LV_DISP_ROT_90;
    lv_disp_drv_register(&disp_drv);

    evdev_init();
    static lv_indev_drv_t indev_drv_1;
    lv_indev_drv_init(&indev_drv_1); /*Basic initialization*/
    indev_drv_1.type = LV_INDEV_TYPE_POINTER;

    /*This function will be called periodically (by the library) to get the mouse position and state*/
    indev_drv_1.read_cb = evdev_read;
    lv_indev_t *mouse_indev = lv_indev_drv_register(&indev_drv_1);

    /*Set a cursor for the mouse*/
    //LV_IMG_DECLARE(mouse_cursor_icon);
    lv_obj_t * cursor_obj = lv_img_create(lv_scr_act());    /*Create an image object for the cursor */
    //lv_img_set_src(cursor_obj, &mouse_cursor_icon);         /*Set the image source*/
    lv_img_set_src(cursor_obj, NULL);                       /*Set the image source*/
    lv_indev_set_cursor(mouse_indev, cursor_obj);           /*Connect the image  object to the driver*/

    /*User Init*/
    pthread_mutex_init(&mutex, NULL); /* 初始化互斥锁 */

    /*User Init*/
    IPC_CarInfo_Open();
    IPC_CarInfo_Read(&carimpl);
    IPC_MsgToUI_Init();
    IPC_MsgToUI_RegisterMsgHandler(carimpl_msg_handler);
    IPC_MsgToUI_CreateThread();

    MenuSettingInit();
    Carimpl_SyncAllSetting();
    carimpl_MenuSetting_Init();
    start_key_event_ctx();

    CusStatusbarInit();
    cardv_timer_init();

    /*Create a Demo*/
    ui_init();
    printf(" lvgl run!\n");

    #if (TICK_CUSTOM)
    pthread_create(&pt, NULL, tick_thread, NULL);
    #endif

    /*Handle LitlevGL tasks (tickless mode)*/
    while(1) {
        pthread_mutex_lock(&mutex); //互斥锁上锁
        lv_timer_handler();
        pthread_mutex_unlock(&mutex);//互斥锁解锁
        usleep(5000);
    }

    return 0;
}

#if (LV_TICK_CUSTOM)
/*Set in lv_conf.h as `LV_TICK_CUSTOM_SYS_TIME_EXPR`*/
uint32_t custom_tick_get(void)
{
    static uint64_t start_ms = 0;
    if(start_ms == 0) {
        struct timeval tv_start = {0};
        gettimeofday(&tv_start, NULL);
        start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
    }

    struct timeval tv_now = {0};
    gettimeofday(&tv_now, NULL);
    uint64_t now_ms;
    now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;

    uint32_t time_ms = now_ms - start_ms;
    return time_ms;
}
#endif
