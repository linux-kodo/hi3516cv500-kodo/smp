#!/bin/sh
#IPADDR=`ifconfig wlan0 | head -n 2 |tail -n 1 |cut -d':' -f 2 | cut -d ' ' -f 1`
APSTA="AP"
if [ $APSTA = "STA" ]; then
	IPADDR="`nvconf get 1 wireless.sta.ipaddr`"
else
	IPADDR="192.72.1.1"
fi

sync
echo 1 > /proc/sys/vm/drop_caches


if [ "`pidof goahead`" = "" ]; then
echo "Start Goahead ..."
#goahead -v  --home /var/webserver/conf/ /var/webserver/www $IPADDR:80 &
#DIR PATH sync to route.txt
mkdir var/run/
mkdir var/run/wifi/
mkdir var/run/wifi/conf
mkdir var/run/wifi/www
cp -r /customer/wifi/webserver/conf/* /var/run/wifi/conf
cp -r /customer/wifi/webserver/www/* /var/run/wifi/www
sleep 1
goahead -v  --home /var/run/wifi/conf/ /var/run/wifi/www $IPADDR:80&
fi

#sync
#echo 1 > /proc/sys/vm/drop_caches

sleep 1

