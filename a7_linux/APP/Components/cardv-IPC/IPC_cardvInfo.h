/*
 * IPC_cardvInfo.h - Sigmastar
 *
 * Copyright (C) 2019 Sigmastar Technology Corp.
 *
 * Author: jeff.li <jeff.li@sigmastar.com.cn>
 * Desc: for Inter-Process Communication
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef _IPC_CARDV_INFO_H_
#define _IPC_CARDV_INFO_H_

#include "DCF.h"
#include <stddef.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

//==============================================================================
//
//                              MACRO DEFINES
//
//==============================================================================

//--------------------
//  System Data Type
//--------------------
/// data type unsigned char, data length 1 byte
typedef unsigned char MI_U8; // 1 byte
/// data type unsigned short, data length 2 byte
typedef unsigned short MI_U16; // 2 bytes
/// data type unsigned int, data length 4 byte
typedef unsigned int MI_U32; // 4 bytes
/// data type unsigned int, data length 8 byte
typedef unsigned long long MI_U64; // 8 bytes
/// data type signed char, data length 1 byte
typedef signed char MI_S8; // 1 byte
/// data type signed short, data length 2 byte
typedef signed short MI_S16; // 2 bytes
/// data type signed int, data length 4 byte
typedef signed int MI_S32; // 4 bytes
/// data type signed int, data length 8 byte
typedef signed long long MI_S64; // 8 bytes
/// data type float, data length 4 byte
typedef float MI_FLOAT; // 4 bytes

typedef unsigned char MI_BOOL;

#ifndef __KERNEL__
/// data type null pointer
#ifdef NULL
#undef NULL
#endif
#define NULL 0

#if !defined(__cplusplus)
#ifndef true
/// definition for true
#define true 1
/// definition for false
#define false 0
#endif
#endif

#if !defined(TRUE) && !defined(FALSE)
/// definition for TRUE
#define TRUE 1
/// definition for FALSE
#define FALSE 0
#endif
#endif

/// ASCII color code
#define ASCII_COLOR_RED "\033[1;31m"
#define ASCII_COLOR_WHITE "\033[1;37m"
#define ASCII_COLOR_YELLOW "\033[1;33m"
#define ASCII_COLOR_BLUE "\033[1;36m"
#define ASCII_COLOR_GREEN "\033[1;32m"
#define ASCII_COLOR_END "\033[0m"

#define IPC_CarInfo_Write_RecInfo(P)                                           \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_RecordInfo),               \
                             offsetof(struct CarDV_Info, stRecInfo))

#define IPC_CarInfo_Write_CapInfo(P)                                           \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_CaptureInfo),              \
                             offsetof(struct CarDV_Info, stCapInfo))

#define IPC_CarInfo_Write_DispInfo(P)                                          \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_DisplayInfo),              \
                             offsetof(struct CarDV_Info, stDispInfo))

#define IPC_CarInfo_Write_SdInfo(P)                                            \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_SDInfo),                   \
                             offsetof(struct CarDV_Info, stSdInfo))

#define IPC_CarInfo_Write_BrowserInfo(P)                                       \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_BrowserInfo),              \
                             offsetof(struct CarDV_Info, stBrowserInfo))

#define IPC_CarInfo_Write_UIModeInfo(P)                                        \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_UIModeInfo),               \
                             offsetof(struct CarDV_Info, stUIModeInfo))

#define IPC_CarInfo_Write_AdasInfo(P)                                          \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_AdasInfo),                 \
                             offsetof(struct CarDV_Info, stAdasInfo))

#define IPC_CarInfo_Write_DmsInfo(P)                                           \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_DmsInfo),                  \
                             offsetof(struct CarDV_Info, stDmsInfo))

#define IPC_CarInfo_Write_MenuInfo(P)                                          \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct CarDV_MenuInfo),                 \
                             offsetof(struct CarDV_Info, stMenuInfo))

#define IPC_CarInfo_Read_RecInfo(P)                                            \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_RecordInfo),                \
                            offsetof(struct CarDV_Info, stRecInfo))

#define IPC_CarInfo_Read_CapInfo(P)                                            \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_CaptureInfo),               \
                            offsetof(struct CarDV_Info, stCapInfo))

#define IPC_CarInfo_Read_DispInfo(P)                                           \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_DisplayInfo),               \
                            offsetof(struct CarDV_Info, stDispInfo))

#define IPC_CarInfo_Read_SdInfo(P)                                             \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_SDInfo),                    \
                            offsetof(struct CarDV_Info, stSdInfo))

#define IPC_CarInfo_Read_BrowserInfo(P)                                        \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_BrowserInfo),               \
                            offsetof(struct CarDV_Info, stBrowserInfo))

#define IPC_CarInfo_Read_UIModeInfo(P)                                         \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_UIModeInfo),                \
                            offsetof(struct CarDV_Info, stUIModeInfo))

#define IPC_CarInfo_Read_AdasInfo(P)                                           \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_AdasInfo),                  \
                            offsetof(struct CarDV_Info, stAdasInfo))

#define IPC_CarInfo_Read_DmsInfo(P)                                            \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_DmsInfo),                   \
                            offsetof(struct CarDV_Info, stDmsInfo))

#define IPC_CarInfo_Read_MenuInfo(P)                                           \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct CarDV_MenuInfo),                  \
                            offsetof(struct CarDV_Info, stMenuInfo))

#define ipc_cardvinfo_write_dvr_info(P)                                        \
  IPC_CarInfo_Write_PartInfo(P, sizeof(struct cardv_dvr_info),                 \
                             offsetof(struct CarDV_Info, st_dvr_info))

#define ipc_cardvinfo_read_dvr_info(P)                                         \
  IPC_CarInfo_Read_PartInfo(P, sizeof(struct cardv_dvr_info),                  \
                            offsetof(struct CarDV_Info, st_dvr_info))

//==============================================================================
//
//                              ENUM & STRUCT DEFINES
//
//==============================================================================

struct CarDV_RecordInfo {
  // TODO : add what you want.
  MI_U32 u32CurDuration[3];     // Sync MUXER_TYPE_NUM
  MI_U32 u32PreRecdDuration[3]; // Sync MUXER_TYPE_NUM
  MI_BOOL bAudioTrack;
  MI_BOOL bMuxing;      // writing Normal file to storage
  MI_BOOL bMuxingEmerg; // writing Emergency file to storage
  MI_BOOL bMuxingShare; // writing Share file to storage
  MI_BOOL bMuxingMD;    // writing Motion-detect file to storage
  // mark add
  MI_BOOL b_lcd_state; // 0:backlight close  1:backlight open
};

struct CarDV_CaptureInfo {
  // TODO : add what you want.
  MI_U32 u32FileCnt;
};

struct CarDV_SDInfo {
  // TODO : add what you want.
  MI_S32 s32ErrCode;
  MI_U64 u64FreeSize;
  MI_U64 u64TotalSize;
  MI_U64 u64OtherSpace;
  MI_U64 u64ReservedSpace;
  MI_U32 u32TotalLifeTime;
  MI_BOOL bStorageMount;
  MI_BOOL bStorageInsert;
  MI_BOOL bDCFMount;
};

struct CarDV_BrowserInfo {
  // TODO : add what you want.
  MI_U8 u8CamId;
  MI_U8 u8DBId;
  MI_U32 u32CurFileIdx[DB_NUM][CAM_NUM];
};

struct CarDV_UIModeInfo {
#define UI_VIDEO_MODE (0)
#define UI_CAPTURE_MODE (1)
#define UI_BROWSER_MODE (2)
#define UI_PLAYBACK_MODE (3)
#define UI_MENU_MODE (4)
  // TODO : add what you want.
  MI_U8 u8Mode;
};

struct CarDV_MenuInfo {
  // Still
  MI_U32 uiIMGSize; // 0
  MI_U32 uiIMGQuality;
  MI_U32 uiBurstShot;
  // Movie
  MI_U32 uiMOVSize;
  MI_U32 uiMOVQuality;
  MI_U32 uiMicSensitivity;
  MI_U32 uiMOVPreRecord;
  MI_U32 uiMOVClipTime;
  MI_U32 uiMOVPowerOffTime; // 8
  MI_U32 uiMOVSoundRecord;
  MI_U32 uiVMDRecTime;
  MI_U32 uiAutoRec;
  MI_U32 uiTimeLapseTime; // 12
  MI_U32 uiSlowMotion;
  MI_U32 uiHDR;
  MI_U32 uiWNR;
  MI_U32 uiNightMode;
  MI_U32 uiParkingMode;
  MI_U32 uiLDWS;
  MI_U32 uiFCWS;
  MI_U32 uiSAG;
  MI_U32 uiContrast;
  MI_U32 uiSaturation;
  MI_U32 uiSharpness;
  MI_U32 uiGamma;
  // Manual
  MI_U32 uiScene;
  MI_U32 uiEV;
  MI_U32 uiISO;
  MI_U32 uiWB;
  MI_U32 uiColor;
  MI_U32 uiEffect; // 30
                   // Playback
  MI_U32 uiPlaybackVideoType;
  MI_U32 uiSlideShowStart;
  MI_U32 uiSlideShowFile; // 33
  MI_U32 uiSlideShowEffect;
  MI_U32 uiSlideShowMusic;
  MI_U32 uiVolume;
  // Edit Tool
  MI_U32 uiFileEdit;
  // Media Tool
  MI_U32 uiMediaSelect; // 38
                        // General
  MI_U32 uiBeep;
  MI_U32 uiAutoPowerOff;
  MI_U32 uiDateTimeFormat;
  MI_U32 uiDateLogoStamp;
  MI_U32 uiGPSStamp;
  MI_U32 uiSpeedStamp;
  MI_U32 uiLanguage;
  MI_U32 uiResetSetting;
  MI_U32 uiFlickerHz;
  MI_U32 uiUSBFunction;
  MI_U32 uiLCDPowerSave;
  MI_U32 uiGsensorSensitivity;
  MI_U32 uiPowerOnGsensorSensitivity;
  MI_U32 uiTimeZone;
  MI_U32 uiMotionDtcSensitivity;
  // Battery
  MI_U32 uiBatteryVoltage;
  // Wifi
  MI_U32 uiWifi;
  // version
  MI_U32 uiVersion; // 56
};

struct cardv_satellite_info {
  MI_U32 id;     /**< Satellite PRN number */
  MI_U32 in_use; /**< Used in position fix */
  MI_U32 sig;    /**< Signal, 00-99 dB */
};

struct cardv_nmea_info {
  MI_U32 inuse;  /**< Number of satellites in use (not those in view) */
  MI_U32 inview; /**< Total number of satellites in view */
  struct cardv_satellite_info sat[12]; /**< Satellites information */
  MI_U32 state; /**< GPS state : 0 module pluged out  1 avild ;2 inavild*/
  MI_U32 speed;
  double dwLat;
  double dwLon;
};

struct cardv_dvr_info {
  struct cardv_nmea_info st_nmea_info;
};

struct CarDV_Info {
  // TODO : add what you want.
  struct CarDV_RecordInfo stRecInfo;
  struct CarDV_CaptureInfo stCapInfo;
  struct CarDV_SDInfo stSdInfo;
  struct CarDV_BrowserInfo stBrowserInfo;
  struct CarDV_UIModeInfo stUIModeInfo;
  struct CarDV_MenuInfo stMenuInfo;
  struct cardv_dvr_info st_dvr_info;
};

//==============================================================================
//
//                              FUNCTIONS
//
//==============================================================================

int IPC_CarInfo_Open(void);
int IPC_CarInfo_Close(void);
int IPC_CarInfo_Write(struct CarDV_Info *pCarInfo);
int IPC_CarInfo_Read(struct CarDV_Info *pCarInfo);
int IPC_CarInfo_Write_PartInfo(void *pCarPartInfo, ssize_t size,
                               ssize_t offset);
int IPC_CarInfo_Read_PartInfo(void *pCarPartInfo, ssize_t size, ssize_t offset);

#ifdef __cplusplus
}
#endif

#endif // _IPC_CARDV_INFO_H_
